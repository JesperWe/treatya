# Treatya #

This repository contains both back- and frontend (client) for treatya.com.

The App Backend is in the `tyb` folder, and the client in the `tyc` folder.

## Booking lifecycle ##

![Booking lifecycle](./doc/treatya-bookings.png)

## Getting Started / Setup ##

### Ubuntu 14.04 minimal ###

    apt-get update
    apt-get upgrade
    apt-get install -y bash-completion build-essential git curl sudo ufw language-pack-en
    vi /etc/bash.bashrc # enable completion 
    reboot
    #
    apt-get -y purge nodejs
    apt-get -y autoremove
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    apt-get install -y nodejs
    npm install -g nodemon
    #
    apt-get install -y nginx # Copy default config.
    #
    echo 'deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main' >/etc/apt/sources.list.d/pgdg.list
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    apt-get update
    apt-get install -y postgresql-9.6 postgresql-server-dev-9.6 postgresql-9.6-postgis-2.3 
    #
    wget https://dl.eff.org/certbot-auto
    chmod a+x certbot-auto
    ./certbot-auto certonly
    ....
    #
    # Monitoring: https://github.com/firehol/netdata
    #
    usermod -s /bin/bash www-data
    mkdir /var/www
    chown www-data:www-data /var/www
    #
    git config --global credential.helper cache
    git config --global credential.helper "cache --timeout=3600000"
    su - www-data
    git clone https://gitlab.com/JesperWe/treatya.git treatya
    #
    # Upload config files.
    # npm install in tyb and tyc.
    cd treatya/tyc
    npm run build
    #
    ufw default deny incoming
    ufw default allow outgoing
    ufw allow ssh
    ufw allow http
    ufw allow https
    # ufw allow from devhost
    ufw enable
    
### /etc/init/treatya-backend.conf ###
    
    start on filesystem and started networking
    setuid www-data
    respawn
    console log
    chdir /var/www/treatya/tyb
    env NODE_ENV=production
    env PORT=4000
    exec /usr/bin/node ./bin/www

### Postgres setup ###

```
CREATE EXTENSION postgis;
CREATE EXTENSION pgcrypto;
ALTER USER postgres PASSWORD '.........';
...
cd treatya/tyb
(cd schema; psql postgres://postgres:........@localhost:5432/postgres < treatya.sql)
```

#### Remote admin:

postgresql.conf

    listen_address = '*'
    
pg_hba.conf

    host    all             all             0.0.0.0/0               md5

Use `ufw` to allow only your dev computer.

### Backup ###

    postgres@www-prod-1:~$ crontab -l
    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    10 05 * * * ~/backup-script.sh >>/tmp/backup.log 2>&1

    postgres@www-prod-1:~$ more ~/backup-script.sh

    #!/bin/bash

    _now=$(date +"%Y-%m-%dT%H%M")
    _file="$_now"'_treatya.dump.sql'
    _glob="$_now"'_treatya.glob.sql'
    _arch="$_now"'_treatya-postgres-bck.7z'
    
    echo '\n\n'
    echo $_file
    
    pg_dumpall --globals-only --clean --if-exists >$_glob
    pg_dump postgres://postgres:_____@localhost:5432/postgres -Fc -Z9 >$_file
    
    7z a -pdaisi $_arch $_glob $_file
    
    aws s3 cp $_arch s3://treatya-backup/    
    rm $_file $_glob $_arch
