## Treatya Backend ##

Postgres/ExpressJS based server that does GraphQL using PostgraphQL and has a few REST endpoints besides that.

### Database backups ###

    $ apt-get install python3 python3-pip p7zip-full
    $ pip3 install --upgrade awscli
    $ aws configure
    ....
    $ more backup-script.sh
    #!/bin/bash
    
    _now=$(date +"%Y-%m-%dT%H%M")
    _file="$_now"'_treatya.dump.sql'
    _glob="$_now"'_treatya.glob.sql'
    _arch="$_now"'_treatya-postgres-bck.7z'
    
    date
    echo $_file
    
    pg_dumpall --globals-only --clean --if-exists >$_glob
    pg_dump postgres://postgres:____@localhost:5432/postgres -Fc -Z9 >$_file
    
    7z a -p____ $_arch $_glob $_file
    
    aws s3 cp $_arch s3://treatya-backup/    
    rm $_file $_glob $_arch
    $ crontab -l
    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    10 05 * * * ~/backup-script.sh >/tmp/backup.log 2>&1

### Restore ###

    $ aws s3 cp s3://treatya-backup/2017-02-22T1200_treatya-postgres-bck.7z ./
    $ 7z e -p____ 2017-02-22T1200_treatya-postgres-bck.7z
    $ dropdb postgres
    $ createdb postgres
    $ psql -f 2017-02-22T1200_treatya.glob.sql postgres
    # On Windows bash: export PGCLIENTENCODING=utf-8
    $ pg_restore -d postgres 2017-02-22T1200_treatya.dump.sql

### Manual migrations ###

- `$ pg_dump --data-only -n treatya* >dataonly.sql`
- In `schema/treatya.sql`: disable `bootstrap.sql`.
- `psql -f treatya.sql` now installs data-model only.
- To disable triggers when restoring data, add `SET session_replication_role = replica;` at start of `dataonly.sql`.
- Make modifications to `dataonly.sql` as needed.
- `psql -f dataonly.sql` (On Windows bash: `export PGCLIENTENCODING=utf-8`)
