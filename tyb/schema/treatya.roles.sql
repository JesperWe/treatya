-- Breakout roles since they are common for all databases.

drop role if exists treatya_postgraphql;
drop role if exists treatya_anonymous;
drop role if exists account;
drop role if exists supplier;
drop role if exists admin;

create role treatya_postgraphql login password 'daisi';

create role treatya_anonymous login password 'xyzzy';
grant treatya_anonymous to treatya_postgraphql;

create role admin;
grant admin to treatya_postgraphql;

create role supplier;
grant supplier to admin;

create role account;
grant account to supplier;
