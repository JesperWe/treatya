set search_path = treatya, public;

-- ---------------------------------------------------------------
-- MESSAGE
-- ---------------------------------------------------------------

create table message (
    id                                  serial primary key,
    account_id               uuid not null references account(id) on delete cascade,
    booking_id               integer not null references booking(id) on delete cascade,
    message                    text,
    seen                            boolean not null default false,
    created_at                timestamptz default now()
);
create index on message ( account_id );
create index on message ( booking_id );


-- ---------------------------------------------------------------
-- REVIEW
-- ---------------------------------------------------------------

create type review_target as enum (
  'CLIENT',
  'SUPPLIER'
);

create table review (
    id                                  serial primary key,
    target                         review_target,
    account_id               uuid not null references account(id) on delete cascade,
    reviewer_id              uuid not null references account(id) on delete cascade,
    supplier_id               integer not null references supplier(id) on delete cascade,
    booking_id               integer not null references booking(id) on delete cascade,
    comment                  text,
    rating                          real,
    created_at                timestamptz not null default now()
);
create index on review ( account_id );
create index on review ( supplier_id );

create or replace function treatya.new_review() returns trigger as
$$
    begin
        update treatya.account
            set rating = (
                select avg(rating) from treatya.review
                where account_id = NEW.account_id and target = 'CLIENT'
            )
            where id = NEW.account_id;
        update treatya.supplier
            set rating = (
                select avg(rating) from treatya.review
                where supplier_id = NEW.supplier_id and target = 'SUPPLIER'
            )
            where id = NEW.supplier_id;
        return NEW;
    end;
$$ language plpgsql strict security definer;

create trigger new_review
    after insert or update on treatya.review
    for each row execute procedure new_review();


-- ---------------------------------------------------------------
-- Message Of The Day
-- ---------------------------------------------------------------

create table motd (
    published_at   timestamptz not null default now(),
    message            jsonb
);
create index on motd ( published_at );


-- ---------------------------------------------------------------
-- Hook message archive
-- ---------------------------------------------------------------

create table treatya_private.hook (
    received_at   timestamptz not null default now(),
    type                  text,
    message         jsonb,
    handled          boolean default false
);


-- ---------------------------------------------------------------
-- NOTIFICATION
-- ---------------------------------------------------------------

set search_path = treatya_private, public;

create type notification_status as enum (
  'QUEUED',
  'STARTED',
  'DONE',
  'FAILED'
);

create table notification (
    id                                  serial primary key,
    account_id                uuid not null references treatya.account(id) on delete cascade,
    booking_id                integer,
    send_at                      timestamptz not null,
    post_processors    text[],
    status                          notification_status,
    email                           text,
    phone                         text,
    email_result             text,
    phone_result           text,
    sms                              text,
    subject                       text,
    body                           text
);
create index on notification ( send_at, status );

create function notification_cu() returns trigger as
$$
    begin
        if (TG_OP='INSERT') then
            notify notification_channel,'update';
        end if;
        if (TG_OP='UPDATE') then
            if OLD.send_at <> NEW.send_at then
                notify notification_channel,'update';
            end if;
        end if;
        return NEW;
    end;
$$ language plpgsql strict security definer;

create trigger notification_create_update
    after insert or update on notification
    for each row execute procedure notification_cu();

create function notification_d() returns trigger as
$$
    begin
        notify notification_channel,'delete';
        return OLD;
    end;
$$ language plpgsql strict security definer;

create trigger notification_delete
    after delete on notification
    for each row execute procedure notification_d();
