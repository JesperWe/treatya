
-- -----------------------------------------------------  Grants -----------------------------------------------------

set search_path = treatya, public;

grant usage on schema treatya to treatya_anonymous, account;
grant usage on schema treatya_private to admin, account;

grant select on table treatya.account to treatya_anonymous, account;
grant select on table treatya_private.account to account;
grant update, delete on table treatya.account to account;

grant select on table treatya.supplier to treatya_anonymous, account;
grant insert, update, delete on table treatya.supplier to account;
grant usage on sequence treatya.supplier_id_seq to account;

grant select on table treatya.account_favorite_supplier to account;
grant insert, update, delete on table treatya.account_favorite_supplier to account;

grant select on table treatya.product to treatya_anonymous, account;
grant insert, update, delete on table treatya.product to account;
grant usage on sequence treatya.product_id_seq to account;

grant select on table treatya.booking to account;
grant insert, update, delete on table treatya.booking to account;
grant usage on sequence treatya.booking_id_seq to account;

grant select on table treatya.account_bookings to treatya_postgraphql; -- a view actually

grant select on table treatya.timeslot to treatya_anonymous, account;
grant insert, update, delete on table treatya.timeslot to account;
grant usage on sequence treatya.timeslot_id_seq to account;

grant select on table treatya.motd to account;

grant execute on function treatya.get_settings(text) to admin;
grant execute on function treatya.update_settings(text, jsonb) to admin;
grant select on treatya.booking_info to admin;
grant execute on function treatya.all_bookings_by_date(timestamptz,timestamptz) to admin;

grant execute on function treatya.tstzrange_minus(tstzrange, tstzrange) to treatya_anonymous, account;
grant execute on function treatya.freeslots(treatya.service_type, timestamptz) to treatya_anonymous, account;
grant execute on function treatya.supplier_freeslots(integer) to treatya_anonymous, account;
grant execute on function treatya.nearby_unavailable_suppliers(treatya.service_type, text, text) to treatya_anonymous, account;

grant execute on function treatya.find_suppliers(treatya.service_type, text, text, timestamptz) to treatya_anonymous, account;
grant execute on function treatya.find_bookings(uuid) to account;

grant select on table treatya.supplier_private to admin; -- a view to the treatya_private.supplier_private table.

grant execute on function treatya.account_full_name(treatya.account) to treatya_anonymous, account;
grant execute on function treatya.account_email(treatya.account) to account;
grant execute on function treatya.account_role(treatya.account) to account;
grant execute on function treatya.account_payment(treatya.account) to account;
grant execute on function treatya.account_counters(uuid) to account;

grant execute on function treatya.update_account_email(uuid, text) to account;
grant execute on function treatya.update_account_phone(uuid, text) to account;
grant execute on function treatya.update_account_role(uuid, treatya.role) to account;
grant execute on function treatya.search_suppliers(text) to treatya_anonymous, account;
grant execute on function treatya.authenticate(text, text) to treatya_anonymous, account;
grant execute on function treatya.current_account() to treatya_anonymous, account;

grant execute on function treatya.register_account(text, text, text, text, text) to treatya_anonymous;

grant execute on function treatya.update_supplier_status(integer, treatya.supplier_statuses) to admin, treatya_postgraphql;
grant select on treatya.supplier_info to account;
grant execute on function treatya.get_supplier_info(integer) to account;
REVOKE ALL ON FUNCTION treatya.get_supplier_info(integer) FROM PUBLIC;
grant all on table treatya_private.supplier_private to  admin;

-- ------------------------------ Row Level Policies -------------------------------------------

alter table treatya.account enable row level security;
alter table treatya.supplier enable row level security;
alter table treatya.product enable row level security;
alter table treatya.booking enable row level security;
alter table treatya.timeslot enable row level security;
alter table treatya.account_favorite_supplier enable row level security;
alter table treatya_private.account enable row level security;

-- Account

create policy select_account on treatya.account
    for select to account
    using (
        (account.id = logged_in_account_id()) or
        (exists (select * from treatya.booking b where b.client_id = account.id and b.account_id = logged_in_account_id()))
    );

create policy update_account on treatya.account
    for update to account
    using (treatya.account.id = logged_in_account_id())
    with check (treatya.account.id  = logged_in_account_id());

create policy delete_account on treatya.account for delete using (false);

create policy admin_account on treatya.account
    for all to admin
    using (true);

create policy crud_account_account_favorite_supplier on treatya.account_favorite_supplier
    for all
    using (account_id  = logged_in_account_id())
    with check (account_id  = logged_in_account_id());

create policy select_account on treatya_private.account
    for select to account
    using ( ( account.account_id = logged_in_account_id() )
          or    ( 'admin' = current_setting('jwt.claims.role',true) ) );


--Supplier

create policy select_supplier on supplier
    for select using (true);

create policy insert_supplier on supplier
    for insert to account
    with check (supplier.account_id = logged_in_account_id() );

create policy update_supplier on supplier
    for update to account
    using (supplier.account_id = logged_in_account_id());

create policy delete_supplier on supplier for delete to admin
  using (true);

-- Product

create policy select_product on product for
    select using (true);

create policy c_product on product for insert
  with check (
        ((select s.account_id from supplier as s where s.id=supplier_id) = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)));

create policy u_product on product for update
  using (
        ((select s.account_id from supplier as s where s.id=supplier_id) = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)))
  with check (
        ((select s.account_id from supplier as s where s.id=supplier_id) = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)));

create policy d_product on product for delete
    using (
        ((select s.account_id from supplier as s where s.id=supplier_id) = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)));

-- Timeslot

create policy select_timeslot on timeslot for select using (true);
create policy cud_timeslot on timeslot for all
  using ((select s.account_id from supplier as s where s.id=supplier_id) = logged_in_account_id())
  with check ((select s.account_id from supplier as s where s.id=supplier_id) = logged_in_account_id());

-- Booking

create policy select_booking on booking for
    select using (
        (account_id  = logged_in_account_id()) or
        (client_id  = logged_in_account_id()) or
        ('admin' = current_setting('jwt.claims.role', true))
    );

create policy c_booking on booking for insert
  with check (
        (account_id = logged_in_account_id()) or
        (client_id = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)));

create policy u_booking on booking for update
  using (
        (account_id = logged_in_account_id()) or
        (client_id = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)))
  with check (
        (account_id = logged_in_account_id()) or
        (client_id = logged_in_account_id()) or
         ('admin' = current_setting('jwt.claims.role',true)));

create policy d_booking on booking for delete using ( false );

-- Message

grant select on table message to account;
grant insert, update, delete on table message to account;
grant usage on sequence message_id_seq to account;

alter table message enable row level security;

create policy crud_message on message for all
  using (
  (account_id  = logged_in_account_id()) or
   ((select  s.account_id from booking b, supplier s where b.id=booking_id and b.supplier_id = s.id) = logged_in_account_id()) or
   ((select  b.client_id from booking b where b.id=booking_id) = logged_in_account_id()));


-- Review

grant select on table review to treatya_anonymous, account;
grant insert, update, delete on table review to account;
grant usage on sequence review_id_seq to account;

alter table review enable row level security;

create policy r_review on review for select to public using (true);

create policy c_review on review for insert to account
  with check (reviewer_id = logged_in_account_id());

create policy u_review on review for update to account
    using (reviewer_id = logged_in_account_id())
    with check (reviewer_id = logged_in_account_id());

create policy d_review on review for delete to admin
    using ('admin'  = current_setting('role'));
