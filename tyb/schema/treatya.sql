begin;

set search_path = treatya, public;

drop schema if exists treatya cascade;
drop schema if exists treatya_private cascade;

create schema treatya;
create schema treatya_private;

create or replace function treatya.logged_in_account_id() returns uuid as $$
begin
        if current_setting('jwt.claims.id', true) = '' then
            return null::uuid;
        else
            return  current_setting('jwt.claims.id', true)::uuid;
        end if;
end;
$$ language plpgsql stable strict security definer;


\ir treatya.accounts.sql
\ir treatya.suppliers.sql
\ir treatya.find_suppliers.sql
\ir treatya.go.sql
\ir treatya.messaging.sql
-- \ir treatya.roles.sql

-- ---------------------------------------------------------------
-- SETTING
-- ---------------------------------------------------------------

set search_path = treatya, public;

create table treatya_private.setting (
    id                                  serial primary key,
    name                          text not null,
    value                          jsonb not null
);
create index on treatya_private.setting ( name );

create or replace function get_settings(
    setting_name text
) returns jsonb as $$
declare
    result text;
begin
    assert current_setting('role') = 'admin', 'Permission denied.';
    select value from treatya_private.setting where name = setting_name into result;
    return result;
end;
$$ language plpgsql stable strict security definer;

create or replace function update_settings(
    setting_name text,
    setting_value jsonb
) returns integer as $$
declare
    nrows integer;
begin
    assert current_setting('role') = 'admin', 'Permission denied.';

    update treatya_private.setting set value = setting_value where name = setting_name;
    get diagnostics nrows := ROW_COUNT;
    return nrows;
end;
$$ language plpgsql strict security definer;

create or replace function treatya.type_images( get_name text ) returns jsonb as $$
    select value->'typeImages' from treatya_private.setting where name=get_name;
$$ language sql strict stable security definer;


-- ---------------------------------------------------------------
-- Authorization
-- ---------------------------------------------------------------

drop policy  if exists c_review on review;
drop policy  if exists u_review on review;
drop policy  if exists d_review on review;

create type treatya.jwt_token as (
  role  text,
  exp  integer,
  id      uuid
);

create or replace function treatya.authenticate(
  email text,
  password text
) returns treatya.jwt_token as $$
declare
  accnt           treatya_private.account;
begin
    select a.* into accnt
        from treatya_private.account as a
        where a.email = lower($1);

    if accnt.password_hash = crypt(password, accnt.password_hash) then
        update treatya_private.account set last_login=now() where account_id=accnt.account_id;

        return (
            accnt.role,
            extract(epoch from (current_date + '7 days 5 hours'::interval)),
            accnt.account_id
        )::treatya.jwt_token;

    else
        return null;
    end if;
end;
$$ language plpgsql strict security definer;

comment on function treatya.authenticate(text, text) is 'Creates a JWT token that will securely identify a account and give them certain permissions.';

create function treatya.current_account() returns treatya.account as $$
  select *
  from treatya.account
  where id = treatya.logged_in_account_id();
$$ language sql stable;

comment on function treatya.current_account() is 'Gets the account who was identified by our JWT.';

\ir treatya.grants.sql
\ir bootstrap.sql

commit;
