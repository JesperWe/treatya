-- ---------------------------------------------------------------
-- SUPPLIERS
-- ---------------------------------------------------------------

set search_path = treatya, public;

create type service_type as enum (
  'HAIR',
  'NAILS',
  'EYES',
  'FEET',
  'MAKEUP',
  'INJECTION',
  'SKIN',
  'TAN',
  'TEETH',
  'HAIRREMOVAL',
  'MASSAGE'
);

create type currency as enum (
  'SEK',
  'EUR',
  'NOK',
  'DKK',
  'GBP'
);

create type booking_status as enum (
  'PRELIMINARY',
  'UNCONFIRMED',
  'CONFIRMED',
  'DENIED',
  'NOSHOW',
  'CANCELLED',
  'DELIVERED',
  'NOTDELIVERED'
);

create type supplier_statuses as enum (
  'APPLICATION',
  'CONFIRMED',
  'VERIFIED',
  'DENIED'
);

create type legal_type as enum (
    'INDIVIDUAL',
    'COMPANY'
);

create table supplier (
    id                           serial primary key,
    account_id        uuid not null unique references account(id) on delete cascade,
    title                       text not null check (char_length(title) < 100),
    intro                     text check (char_length(intro) < 300),
    description        text,
    image_cat          text,
    image_bg           text[],
    image_profile   text,
    image_profile_rot int not null default 0,
    type                      service_type,
    address1             text,
    address2             text,
    zip                         text,
    city                        text,
    country                text,
    geo_position    geography(POINT,4326),
    geo_address     text,
    rating                   real,
    created_at         timestamptz not null default now(),
    updated_at       timestamptz not null default now()
);
create index supplier_gix on supplier using gist ( geo_position );
create index on supplier (account_id);

create table treatya_private.supplier_private (
    supplier_id         integer primary key references treatya.supplier(id) on delete cascade,
    status                  supplier_statuses default 'APPLICATION',
    entity_name     text,
    entity_type       legal_type,
    entity_id             text,
    created_ip         text,
    created_agent text,
    stripe_id             text,
    stripe_needed text[],
    stripe_sk            text,
    stripe_pk           text,
    fee                        jsonb
);
create index on treatya_private.supplier_private ( stripe_id );

create view treatya.supplier_private as
    select * from treatya_private.supplier_private;

create function supplier_insert_application() returns trigger as
$$
    begin
        insert into treatya_private.supplier_private (supplier_id) values (NEW.id);
        return NEW;
    end;
$$ language plpgsql strict security definer;

create trigger supplier_private_create
    after insert on supplier
    for each row execute procedure supplier_insert_application();

create function update_supplier_private(
    supplier_id integer,
    ip text,
    agent text,
    entity_id text,
    entity_name text,
    entity_type legal_type
) returns integer as $$
declare
    nrows integer;
    user_id uuid;
    target_id uuid;
begin
    user_id = treatya.logged_in_account_id();
    target_id := (select account_id from treatya.supplier s where s.id=update_supplier_private.supplier_id);
    assert user_id is not null, 'Not Authenticated.';
    if target_id <> user_id then
        assert current_setting('role') = 'admin', 'Permission denied.';
    end if;

    update treatya_private.supplier_private
    set (created_ip,created_agent, entity_id,entity_name,entity_type) =
        (ip,agent, update_supplier_private.entity_id, update_supplier_private.entity_name, update_supplier_private.entity_type)
    where treatya_private.supplier_private.supplier_id = update_supplier_private.supplier_id;
    get diagnostics nrows := ROW_COUNT;
    return nrows;
end;
$$ language plpgsql strict security definer;


create view supplier_info as
    select supplier_id as id, entity_id, entity_name, entity_type, stripe_id, stripe_needed, stripe_pk from treatya_private.supplier_private;

create function get_supplier_info(
    supplier_id integer
) returns setof supplier_info as $$
declare
    nrows integer;
    user_id uuid;
    target_id uuid;
begin
    user_id := treatya.logged_in_account_id();
    target_id := (select account_id from treatya.supplier where id=supplier_id);
    if target_id <> user_id then
        assert current_setting('role') = 'admin', 'Permission denied.';
    end if;

    return query select * from treatya.supplier_info where id = supplier_id;
end;
$$ language plpgsql stable strict security definer;

create table treatya.account_favorite_supplier (
    account_id      uuid not  null references treatya.account(id) on delete cascade,
    supplier_id      integer not null references treatya.supplier(id) on delete cascade,
    constraint no_same unique (account_id, supplier_id)
);
create index on account_favorite_supplier (account_id);

create extension if not exists btree_gist;
create table timeslot (
    supplier_id        integer not null references supplier(id) on delete cascade,
    slot                       tstzrange not null,
    id                           serial primary key,

    constraint supplier_overlapping_timeslot_not_allowed
    exclude using gist (supplier_id with =, slot with &&)
);
create index on timeslot (supplier_id);

-- Timeslot: (from, to)
-- Wanted range: [from, to]

-- select supplier_id
-- from supplier as s, product as p, timeslot as t
-- where s.id = p.supplier_id and s.id = t.supplier_id and p.type = $TYPE
-- and $WANTEDRANGE @> t.time

create table product (
    id                            serial primary key,
    supplier_id          integer not null references supplier(id) on delete cascade,
    prod_title            text not null check (char_length(prod_title) < 280),
    prod_descr         text,
    image                    text,
    price                       integer,
    currency               currency,
    duration               interval,
    min_interval       interval,
    published            boolean not null default true,
    deleted                 boolean not null default false,
    type                       service_type,
    created_at          timestamptz not null default now(),
    updated_at        timestamptz not null default now()
);
create index on product (supplier_id);

create function product_duration_sec(product product) returns integer as $$
  select round(extract(epoch from product.duration))::integer;
$$ language sql stable;

create table booking (
    id                         serial primary key,
    account_id      uuid not null references account(id) on delete cascade, -- For the supplier
    supplier_id      integer not null references supplier(id) on delete cascade,
    client_id           uuid not null references account(id) on delete cascade,
    product_id      integer not null references product(id) on delete cascade,
    timeslot_id      integer references timeslot(id),
    address             text check (char_length(address) < 280),
    geo_position  geography(POINT,4326),
    price                   integer,
    currency           currency,
    type                    service_type,
    period                tstzrange not null,
    status                 booking_status,
    created_at       timestamptz default now(),
    updated_at     timestamptz default now(),

    constraint same_supplier_overlapping_booking_not_allowed
    exclude using gist (supplier_id with =, period with &&) where (status!='CANCELLED' and status!='NOTDELIVERED' and status!='DENIED')
);
create index on booking (account_id);
create index on booking (supplier_id);
create index on booking (client_id);
-- create index on booking (((notifications ->> 'propertyName')::int));

create table treatya_private.booking (
    booking_id      integer not null primary key references treatya.booking(id) on delete cascade,
    payment           jsonb default '{}',
    refund               jsonb default '{}',
    payout               jsonb default '{}',
    events                jsonb[]
);
create index on treatya_private.booking (booking_id);

create function booking_payment(booking booking) returns jsonb as $$
begin
    if current_setting('jwt.claims.account_id', true) = '' then
        return '{}'::jsonb;
    end if;

    if
        treatya.logged_in_account_id() = (select account_id from treatya.booking where id=booking_payment.booking.id) or
        treatya.logged_in_account_id() = (select client_id from treatya.booking where id=booking_payment.booking.id) or
        'admin' = current_setting('role')
    then
        return (select payment from treatya_private.booking where booking_id = booking.id);
    else
        return '{}'::jsonb;
    end if;
end;
$$ language plpgsql strict stable security definer;
comment on function booking_payment(booking) is 'Booking payment status.';

create function booking_payout(booking booking) returns jsonb as $$
begin
    if current_setting('jwt.claims.account_id', true) = '' then
        return '{}'::jsonb;
    end if;

    if
        treatya.logged_in_account_id() = (select account_id from treatya.booking where id=booking_payout.booking.id) or
        'admin' = current_setting('role')
    then
        return (select payout from treatya_private.booking where booking_id = booking.id);
    else
        return '{}'::jsonb;
    end if;
end;
$$ language plpgsql strict stable security definer;
comment on function booking_payout(booking) is 'Booking payout status.';

create function booking_refund(booking booking) returns jsonb as $$
begin
    if
        treatya.logged_in_account_id() = (select account_id from treatya.booking where id=booking_refund.booking.id) or
        treatya.logged_in_account_id() = (select client_id from treatya.booking where id=booking_refund.booking.id) or
        'admin' = current_setting('role')
    then
        return (select refund from treatya_private.booking where booking_id = booking.id);
    else
        return '{}'::jsonb;
    end if;
end;
$$ language plpgsql strict stable security definer;
comment on function booking_refund(booking) is 'Booking refund status.';

create function supplier_status(supplier supplier) returns supplier_statuses as $$
  select status from treatya_private.supplier_private where supplier_id = supplier.id
$$ language sql strict stable security definer;
comment on function supplier_status(supplier) is 'Suppliers application status.';

create function supplier_position(supplier supplier) returns text as $$
  select ST_AsGeoJSON(supplier.geo_position)
$$ language sql stable;
comment on function supplier_position(supplier) is 'Suppliers geographical position in GeoJSON format.';

create function supplier_product(supplier supplier) returns setof product as $$
  select * from product where supplier_id = supplier.id
$$ language sql stable;
comment on function supplier_product(supplier) is 'Suppliers products.';

create or replace function treatya.update_supplier_status(
    target_id integer,
    new_status treatya.supplier_statuses
) returns integer as $$
declare
    nrows integer;
    aid       uuid;
begin
    assert current_setting('role') = 'admin', 'Permission denied.';
    aid := (select account_id from treatya.supplier where id=target_id);
    if new_status = 'CONFIRMED' then
        if (select role from treatya_private.account where account_id=aid) <> 'admin' then
            update treatya_private.account set role='supplier' where account_id=aid;
         end if;
    end if;
    insert into treatya_private.supplier_private ( supplier_id, status ) values ( target_id, new_status )
        on conflict (supplier_id) do
        update set ( status ) = ( new_status );
    get diagnostics nrows := ROW_COUNT;
    return nrows;
end;
$$ language plpgsql strict security definer;

create function account_supplier(account account) returns supplier as $$
  select supplier.*
  from supplier as supplier
  where supplier.account_id = account.id
  order by created_at desc
  limit 1
$$ language sql stable;

create function search_suppliers(search text)
    returns setof supplier as $$
        select supplier.*
        from supplier as supplier
        where supplier.description ilike ('%' || search || '%') or supplier.title ilike ('%' || search || '%')
    $$ language sql stable;
comment on function search_suppliers(text) is 'Returns suppliers containing a given search term.';


create or replace view treatya.booking_info as
    select b.*, a.first_name, a.last_name, s.title, p.prod_title, pb.*
    from treatya.booking as b, treatya_private.booking as pb, treatya.account as a, treatya.supplier as s, treatya.product as p
    where b.id = pb.booking_id and s.id=b.supplier_id and a.id=b.client_id and p.id=b.product_id;

create or replace function treatya.all_bookings_by_date(created_from timestamptz, created_to timestamptz)
    returns setof treatya.booking_info as $$
        select * from treatya.booking_info
        where tstzrange(created_from,created_to) @> created_at
    $$ language sql stable;
comment on function treatya.all_bookings_by_date(timestamptz,timestamptz) is 'Returns all bookings created within period.';


create function upsert_product( p product )
    returns setof product as $$
begin
    return query
    insert into product select p.*
        on conflict (id) do
        update set
        ( prod_title, prod_descr, price, currency, type, published ) = ( p.prod_title, p.prod_descr, p.price, p.currency, p.type, p.published )
        returning *;
end;
$$ language plpgsql strict security definer;

create view account_bookings as
		select
            b.id, b.product_id, b.timeslot_id, b.client_id,
            b.address, b.geo_position, b.price, b.currency, b.period, b.status,
            p.supplier_id, p.type, p.prod_title, p.duration, p.min_interval,
            extract( epoch from p.duration ) as duration_sek,
            s.title as supplier_title, s.account_id,
            a.first_name, a.last_name
		from account as a, supplier as s, product as p, booking as b
		where s.account_id = a.id and b.product_id = p.id and s.id = p.supplier_id;

create function find_bookings(for_id uuid)
returns setof account_bookings as $$
begin
    assert (
        for_id = treatya.logged_in_account_id() or
        'admin' = current_setting('role')
    ) , 'Not permitted.';

    return query
    select *
    from treatya.account_bookings as ab
    where ab.account_id = for_id or ab.client_id = for_id
    order by lower(ab.period)
    limit 1000;
end;
$$ language plpgsql stable strict security definer;
comment on function find_bookings(uuid) is 'Returns all bookings involving my account as client or supplier.';

create type free_text_hit as (
    supplier_id      int,
    product_id      int,
    title                    text
);

create function free_text_search(term text)
returns setof free_text_hit as $$
    select id as supplier_id, null as product_id, title from treatya.supplier where title ~* term
    union
    select supplier_id, id as product_id, prod_title as title from treatya.product where prod_title ~* term;
$$ language sql stable;
comment on function find_bookings(uuid) is 'Free text search for products or suppliers.';
