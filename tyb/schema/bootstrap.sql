
-- ---------------------------------------  Bootstrap data  ------------------------------------------------

insert into treatya_private.setting values (default, 'live', '{"sms":"disable","charge":{"maximum":500,"minimum":50,"percent":10},"currency":{"cents":{"dkk":100,"eur":100,"gbp":100,"nok":100,"sek":100}},"typeImages":{"TAN":["category-images/spray_tan_1.jpg"],"EYES":["category-images/eyebrowsandlashes_1.jpg"],"FEET":["category-images/Feet_1.jpg","category-images/Feet_2.jpg"],"HAIR":["category-images/hair_3.jpg","category-images/hair_4.jpg"],"SKIN":["category-images/Skin_1.jpg","category-images/Skin_2.jpg","category-images/Skin_3.jpg"],"NAILS":["category-images/Nails_1.jpg","category-images/Nails_2.jpg","category-images/Nails_3.jpg"],"TEETH":["category-images/Teeth_1.jpg","category-images/Teeth_2.jpg"],"MAKEUP":["category-images/Hairmakeup_1.jpg","category-images/hairmakeup_2.jpg"],"INJECTION":["category-images/injections_1.jpg","category-images/injections_2.jpg"],"HAIRREMOVAL":["category-images/Hairremoval_1.jpg","category-images/Hairremoval_2.jpg","category-images/Hairremoval_3.jpg"]},"notification":{"bookingDenied":{"text":{"en":{"sms":"Your booking at ${startAt} of ${prod_title} has been denied by the supplier. Your payment has been refunded.","body":"Your booking at ${startAt} of ${prod_title} has been denied by the supplier.\nYour payment has been refunded.","subject":"Booking Denied"},"sv":{"sms":"Din bokning ${startAt} av ${prod_title} har nekats av behandlaren.","body":"Din bokning ${startAt} av ${prod_title} har nekats av behandlaren.","subject":"Din bokning blev nekad."}}},"startReminder":{"days":-1,"text":{"en":{"sms":"We want to remind you of your booking ${startAt}: ${prod_title}","body":"Booked Treatment: ${prod_title}\nStart-time: ${startAt}\nSupplier: ${supplier_title}\nClient: ${clientFullName}","subject":"Booking ${startAt}"},"sv":{"sms":"Påminner om din bokning ${startAt}: ${prod_title}","body":"Bokad behandling: ${prod_title}\nTid: ${startAt}\nBehandlare: ${supplier_title}\nKund: ${clientFullName}","subject":"Bokningspåminnelse ${startAt}"}},"hours":16},"bookingCreated":{"text":{"en":{"sms":"You have a new booking: ${startAt} ${prod_title}\nPlease log in and confirm!","body":"Booked Treatment: ${prod_title}\nStart-time: ${startAt}\nSupplier: ${supplier_title}\nClient: ${clientFullName}\n\nPlease log in to Treatya and confirm!","subject":"You have a new booking: ${startAt} ${prod_title}"},"sv":{"sms":"Du har en ny bokning: ${startAt} ${prod_title}\nLogga in och bekräfta den!","body":"Bokad behandling: ${prod_title}\nTid: ${startAt}\nBehandlare: ${supplier_title}\nKund: ${clientFullName}\n\nGlöm inte logga in på Treatya för att bekräfta den.","subject":"Du har en ny bokning: ${startAt} ${prod_title}"}}},"bookingExpired":{"text":{"en":{"sms":"Treatment ${prod_title} at ${startAt} was neither confirmed nor performed.","body":"The preliminary booking of this treatment was neither confirmed nor performed: ${prod_title}\nDate: ${startAt}\nClient: ${clientFullName}","subject":"Overdue booking has been cancelled"},"sv":{"sms":"Preliminärbokningen ${startAt} av ${prod_title} har anullerats.","body":"En preliminärbokad behandling har inte genomförts: ${prod_title}\nTid: ${startAt}\nKund: ${clientFullName}","subject":"En preliminärbokning har inte fullföljts"}},"hours":24,"postProcessors":["setBookingOverdue"]},"bookingCancelled":{"text":{"en":{"sms":"Booking at ${startAt} of ${prod_title} has been cancelled by the client.","body":"Booking at ${startAt} of ${prod_title} has been cancelled by the client.","subject":"Booking cancellation"},"sv":{"sms":"Bokningen ${startAt} av ${prod_title} har avbokats av kunden.","body":"Den avbokade behandlingen: ${prod_title}\nTid: ${startAt}\nKund: ${clientFullName}","subject":"Avbokning"}}},"bookingConfirmed":{"text":{"en":{"sms":"Booking at ${startAt} of ${prod_title} has been confirmed by the supplier.","body":"Booking at ${startAt} of ${prod_title} has been confirmed by the supplier.","subject":"Booking confirmation"},"sv":{"sms":"Bokningen ${startAt} av ${prod_title} har nu bekräftats av behandlaren.","body":"Den bekräftade behandlingen: ${prod_title}\nTid: ${startAt}\nKund: ${clientFullName}","subject":"Bekräftad bokning"}}},"emailVerification":{"text":{"en":{"html":"<h2>Greetings from Treatya!</h2>\nYou are one click away from verifying your email address.<br><br>\n<a href=\"${backend}/verify/link?l=${language}&p=${token}\">Click here!</a> ","text":"Greetings from Treatya!\n\nYou are one click away from verifying your email address.\n\nClick here: ${backend}/verify/link?l=${language}&p=${token}","subject":"treatya.com - Your e-mail verification information"},"sv":{"html":"<h2>Hej från Treatya!!</h2>\nDu är bara ett klick från en verifierad e-postaddress.<br><br>\n<a href=\"${backend}/verify/link?l=${language}&p=${token}\">Klicka här!</a> ","text":"Hej!\nEtt meddelande från Treatya\n\nDu är bara ett klick från en verifierad e-postaddress.\n\nKlicka här: ${backend}/verify/link?l=${language}&p=${token}","subject":"treatya.com - Verifiera din e-postaddress"}}},"afterBookingClient":{"text":{"en":{"sms":"We hope your treatment was wonderful and that you are happy with the results. Please give us your feedback at:\n${reviewLink}","body":"We hope your treatment was wonderful and that you are happy with the results. Please give us your feedback at:\n${reviewLink}\n\n(Booking at ${startAt} of ${prod_title})","subject":"Did your treatment work out well?"},"sv":{"sms":"Hoppas att din behandling gick bra och att du är nöjd. Tala gärna om hur det gick på:\n${reviewLink}","body":"Vi hoppas du är nöjd med din behandling. Tala gärna om hur det gick på: ${reviewLink}\n\nDin behandling: ${prod_title}\nTid: ${startAt}\nKund: ${clientFullName}","subject":"Hoppas att din behandling gick bra!"}},"hours":1},"afterBookingSupplier":{"text":{"en":{"sms":"We hope your treatment with ${clientFullName} was successful. Please give us your feedback at:\n${reviewLink}","body":"We hope your treatment with ${clientFullName} was successful. Please give us your feedback at:\n${reviewLink}\n\n(Booking at ${startAt} of ${prod_title})","subject":"Did your treatment work out well?"},"sv":{"sms":"Hoppas att behandlingen hos ${clientFullName} gick bra. Ge oss din feedback på:\n${reviewLink}","body":"Tala gärna om hur din behandling gick på: ${reviewLink}\n\nDin behandling: ${prod_title}\nTid: ${startAt}\nKund: ${clientFullName}","subject":"Hoppas att behandlingen gick bra!"}},"hours":1},"bookingNeverConfirmed":{"text":{"en":{"sms":"Treatment ${prod_title} at ${startAt} was never confirmed and has been automatically cancelled.","body":"The preliminary booking of this treatment was never confirmed by the supplier. It has been automatically cancelled.\n${prod_title}\nDate: ${startAt}\nClient: ${clientFullName}","subject":"Unconfirmed booking has been cancelled"},"sv":{"sms":"Preliminärbokningen ${startAt} av ${prod_title} har avbokats eftersom behandlaren inte bekräftat den.","body":"En preliminärbokad behandling har inte bekräftats av behandlaren. Den har därför avbokats.\n${prod_title}\nTid: ${startAt}\nKund: ${clientFullName}","subject":"En obekräftad preliminärbokning har avbokats"}}},"supplierApplicationApproved":{"text":{"en":{"sms":"Your supplier application has been approved. Start adding your products at https://treatya.com!","body":"Your supplier application has been approved.\nGo to https://treatya.com to add images and products to your supplier information.","subject":"Your supplier application has been approved"},"sv":{"sms":"Du är nu godkänd som behandlare. Gå till https://treatya.com och lägg upp bilder och dina produkter!","body":"Du är nu godkänd som behandlare.\nPå https://treatya.com lägger du upp bilder och skapar dina produkter.\nGlöm inte ställa in din behandlar-position.","subject":"Du är nu godkänd som behandlare"}},"hours":0}}}');


insert into treatya.account (id, first_name, last_name, about, geo_position, language, ui_settings, created_at, updated_at, motd)
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 'Adam', 'Treatya', '', '0101000020E6100000C656BAFA4BA74D40BD8D49751D353240', 'sv',
        '{"infoSplash":{"supplierWelcome":{"dismissed":true},"calendarIntro":{"dismissed":true}},"supplierRegistration": {"show": true, "steps": {"approved": {"done": true}, "application": {"done": true}}}}', '2016-11-19 10:59:42.400577+01', '2016-11-20 10:59:42.400577+01', now());
insert into treatya_private.account (account_id, role, email, email_verified, phone, phone_verified,  password_hash)
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 'admin', 'treatya@journeyman.se', true, '+46708761043', true, '$2a$08$ubRR3JFgBp9JrB7jh3qz0.BTPgpP..zBHXtYhfG/BMjymUkP3G8Z6');

insert into treatya.supplier (account_id, title, intro, description, type, geo_position, image_profile )
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 'Adam&Eva', 'Du bokar, vi fruffar allt vad vi orkar.', 'Alla våra skönhetsbehandlingar utförs med professionalitet och stort yrkeskunnande. Vår vackra och harmoniska miljö skänker lugn men ger också ny energi till kropp och själ. På alla våra skönhetsbehandlingar har vi 16-årsgräns förutom vid permanent make up och tandblekning där gränsen går vid 18 år.', 'HAIR',  ST_GeographyFromText('SRID=4326;POINT(59.3 17.9)'), 'https://s3.eu-central-1.amazonaws.com/treatya-promo/profile1.jpg');

insert into treatya.product (supplier_id,prod_title, prod_descr, duration, price, currency, type, published)
    values (1, 'Fruffning', 'We rockerst!','55 minutes', 550, 'SEK', 'HAIR',  true);



insert into treatya.account (id, first_name, last_name, about, language, geo_position, created_at, updated_at, motd)
    values ('daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 'Dominika', 'Peczynski', '', 'sv', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240', '2016-11-20 10:59:42.400577+01', '2016-11-20 10:59:42.400577+01', now());
insert into treatya_private.account (account_id, role, email, email_verified, phone, phone_verified, password_hash)
    values ('daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 'supplier', 'dominika@journeyman.se', true, '+46708761043', true, '$2a$08$ubRR3JFgBp9JrB7jh3qz0.BTPgpP..zBHXtYhfG/BMjymUkP3G8Z6');

insert into treatya.supplier (account_id, title, intro, description, type, geo_position, image_profile )
    values ('daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 'Mafioso', 'Kunden framför allt i centrum.', 'Permanent fransböjning öppnar upp din blick och gör den mer intensiv. Denna behandling kan också vara ett alternativ för dig som inte vill eller kan förlänga fransarna. Komplettera gärna med en fransfärgning för extra effekt. En snabb och skonsam behandling med utformade silikonpads som ger ögonfransarna en naturligt, snygg böj.', 'HAIR',  ST_GeographyFromText('SRID=4326;POINT(59.0 18.0)'), 'https://s3.eu-central-1.amazonaws.com/treatya-promo/profile2.jpg');

insert into treatya.product (supplier_id,prod_title, prod_descr, duration, price, currency, type, published)
    values (2, 'Balsamering', 'Hårda bandage.','35 minutes', 1550, 'SEK', 'HAIR',  true);
insert into treatya.product (supplier_id,prod_title, prod_descr, duration, price, currency, type, published)
    values (2, 'Lobotomering', 'Bli en lyckligare människa.','50 minutes', 950, 'SEK', 'HAIR',  true);
insert into treatya.product (supplier_id,prod_title, prod_descr, duration, price, currency, type, published)
    values (2, 'Avkontaminering', 'Som detox. Fast utan Botox.','50 minutes', 750, 'SEK', 'HAIR',  true);



insert into treatya.account (id, first_name, last_name, about, language, geo_position, created_at, updated_at, motd)
    values ('143f1e64-e984-49cf-a827-b738fa2ec363', 'Kunden', 'Användarsson', '', 'sv', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240', '2016-11-21 10:59:42.400577+01', '2016-11-21 10:59:42.400577+01', now());
insert into treatya_private.account (account_id, role, email, email_verified, phone, phone_verified, password_hash)
    values ('143f1e64-e984-49cf-a827-b738fa2ec363', 'account', 'lasse@journeyman.se', true, '+46708761043', true, '$2a$08$F16bgVDXDoKsmd3ajHk8KOY4jWJs/1If/AgXX5nfniSIdSowLDRVS');



insert into treatya.account (id, first_name, last_name, about, geo_position, created_at, updated_at)
    values ('05d83a6b-72e8-4917-a173-12b7a08e3af5', 'Nina', 'Olsson', null, null, '2017-01-30 11:28:13.300511-05', '2017-01-30 11:28:13.300511-05');
insert into treatya_private.account (account_id, role, email, email_verified, phone, phone_verified, password_hash, payment)
    values ('05d83a6b-72e8-4917-a173-12b7a08e3af5', 'account', 'ninamariaolsson@gmail.com', true, NULL, false, '$2a$08$hL3izVhFFlSalk3rKTuZkOEikoV7Ux2U.fiVX44mKS/LDQLcNBAlW', '{}');



insert into treatya.account (id, first_name, last_name, about, language, geo_position, created_at, updated_at, motd)
    values ('daf3d1e7-1234-4d64-8372-13e5f8fa4cca', 'Behandlare', 'Andersson', '', 'sv', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240', '2016-11-20 10:59:42.400577+01', '2016-11-20 10:59:42.400577+01', now());
insert into treatya_private.account (account_id, role, email, email_verified, phone, phone_verified, password_hash)
    values ('daf3d1e7-1234-4d64-8372-13e5f8fa4cca', 'supplier', 'behandlare1@journeyman.se', true, '+46708761043', true, '$2a$08$ubRR3JFgBp9JrB7jh3qz0.BTPgpP..zBHXtYhfG/BMjymUkP3G8Z6');

insert into treatya.supplier (account_id, title, intro, description, type, geo_position, image_profile )
    values ('daf3d1e7-1234-4d64-8372-13e5f8fa4cca', 'Behandlare1', 'Pellentesque porta sed purus id interdum. In tristique felis velit, non viverra lectus malesuada eu. Fusce ullamcorper libero id nibh ultricies, ac laoreet turpis consequat.', 'Morbi ultricies dignissim massa, eget viverra nisl iaculis id. Vestibulum id nisl bibendum, vulputate sapien et, mattis leo. Ut convallis odio nisi, vitae pharetra dolor accumsan ac.', 'HAIR',  ST_GeographyFromText('SRID=4326;POINT(59.0 18.0)'), 'https://s3.eu-central-1.amazonaws.com/treatya-promo/profile2.jpg');

insert into treatya.product (supplier_id,prod_title, prod_descr, duration, price, currency, type, published)
    values (3, 'Behandling-1', 'Phasellus faucibus posuere nibh, nec dictum sapien aliquam a. In tristique felis velit, non viverra lectus malesuada eu. Fusce ullamcorper libero id nibh ultricies, ac laoreet turpis consequat.','30 minutes', 550, 'SEK', 'HAIR',  true);



insert into treatya.account (id, first_name, last_name, about, language, geo_position, created_at, updated_at, motd)
    values ('daf3d1e7-5678-4d64-8372-13e5f8fa4cca', 'Behandlare', 'Andersson', '', 'sv', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240', '2016-11-20 10:59:42.400577+01', '2016-11-20 10:59:42.400577+01', now());
insert into treatya_private.account (account_id, role, email, email_verified, phone, phone_verified, password_hash)
    values ('daf3d1e7-5678-4d64-8372-13e5f8fa4cca', 'supplier', 'behandlare2@journeyman.se', true, '+46708761043', true, '$2a$08$ubRR3JFgBp9JrB7jh3qz0.BTPgpP..zBHXtYhfG/BMjymUkP3G8Z6');

insert into treatya.supplier (account_id, title, intro, description, type, geo_position, image_profile )
    values ('daf3d1e7-5678-4d64-8372-13e5f8fa4cca', 'Behandlare Två', 'Vestibulum ante ipsum primis in faucibus orci', 'Fusce eleifend diam a varius vestibulum. Morbi vulputate orci nec ligula tincidunt, vel semper magna sollicitudin.', 'HAIR',  ST_GeographyFromText('SRID=4326;POINT(59.0 18.0)'), 'https://s3.eu-central-1.amazonaws.com/treatya-promo/profile2.jpg');

insert into treatya.product (supplier_id,prod_title, prod_descr, duration, price, currency, type, published)
    values (4, 'Behandling-2', 'Etiam ex justo, semper a augue id, posuere interdum erat. Aliquam auctor maximus nisl, id ullamcorper lectus. Aliquam tincidunt felis at rhoncus mollis. Phasellus eget vehicula est. Pellentesque ornare ante et leo hendrerit scelerisque. Duis ac pharetra purus. Aliquam sit amet enim vel nunc posuere pharetra vitae in eros. Nulla hendrerit tempus faucibus. Pellentesque varius urna arcu.','30 minutes', 550, 'SEK', 'HAIR',  true);



update treatya_private.supplier_private set status = 'CONFIRMED';
update treatya_private.supplier_private set fee = '{ "minimum": 3, "maximum":10000, "percent": 5 }' where supplier_id=1;

insert into treatya.account_favorite_supplier values ('2bfd764c-bf72-44e6-b3b8-349947a06395',2);

insert into treatya.timeslot values (1, tstzrange(current_date+'9 hours'::interval,current_date+'12 hours'::interval));
insert into treatya.timeslot values (1, tstzrange(current_date+'37 hours'::interval,current_date+'41 hours'::interval));
insert into treatya.timeslot values (1, tstzrange(current_date+'33 hours'::interval,current_date+'36 hours'::interval));
insert into treatya.timeslot values (1, tstzrange(current_date+'59 hours'::interval,current_date+'60 hours'::interval));
insert into treatya.timeslot values (1, tstzrange(current_date+'61 hours'::interval,current_date+'66 hours'::interval));

insert into treatya.timeslot values (2, tstzrange(
        date_trunc('hour',current_timestamp)+'1 hours'::interval,
        date_trunc('hour',current_timestamp)+'5 hours'::interval
    ));
insert into treatya.timeslot values (2, tstzrange(current_date+'33 hours'::interval,current_date+'36 hours'::interval));
insert into treatya.timeslot values (2, tstzrange(current_date+'37 hours'::interval,current_date+'41 hours'::interval));
insert into treatya.timeslot values (2, tstzrange(current_date+'57 hours'::interval,current_date+'60 hours'::interval));
insert into treatya.timeslot values (2, tstzrange(current_date+'61 hours'::interval,current_date+'65 hours'::interval));
insert into treatya.timeslot values (2, tstzrange(current_date+'83 hours'::interval,current_date+'87 hours'::interval));
insert into treatya.timeslot values (2, tstzrange(current_date+'91 hours'::interval,current_date+'93 hours'::interval));

insert into treatya.timeslot values (3, tstzrange(current_date+'9 hours'::interval,current_date+'12 hours'::interval));
insert into treatya.timeslot values (3, tstzrange(current_date+'37 hours'::interval,current_date+'41 hours'::interval));
insert into treatya.timeslot values (3, tstzrange(current_date+'33 hours'::interval,current_date+'36 hours'::interval));
insert into treatya.timeslot values (3, tstzrange(current_date+'59 hours'::interval,current_date+'60 hours'::interval));
insert into treatya.timeslot values (3, tstzrange(current_date+'61 hours'::interval,current_date+'66 hours'::interval));

insert into treatya.timeslot values (4, tstzrange(current_date+'9 hours'::interval,current_date+'12 hours'::interval));
insert into treatya.timeslot values (4, tstzrange(current_date+'37 hours'::interval,current_date+'41 hours'::interval));
insert into treatya.timeslot values (4, tstzrange(current_date+'33 hours'::interval,current_date+'36 hours'::interval));
insert into treatya.timeslot values (4, tstzrange(current_date+'59 hours'::interval,current_date+'60 hours'::interval));
insert into treatya.timeslot values (4, tstzrange(current_date+'61 hours'::interval,current_date+'66 hours'::interval));

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 1,'daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 1, 3, 'Scheelegatan 11, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',550, 'SEK', 'HAIR',
    tstzrange(current_date+'9 hours'::interval, current_date+'9 hours 45 minutes'::interval),'CONFIRMED');
insert into treatya_private.booking values (1,
    '{"id": "ch_19uIGHDQtlvbBEWSLzL6B68z", "paid": true, "order": null, "amount": 1300, "object": "charge", "review": null, "source": {"id": "src_19uIFaDQtlvbBEWSbxW9NEUg", "flow": "redirect", "type": "three_d_secure", "owner": {"name": null, "email": null, "phone": null, "address": null, "verified_name": null, "verified_email": null, "verified_phone": null, "verified_address": null}, "usage": "single_use", "amount": 1300, "object": "source", "status": "consumed", "created": 1488792975, "currency": "sek", "livemode": true, "metadata": {}, "redirect": {"url": "https://hooks.stripe.com/redirect/authenticate/src_19uIFaDQtlvbBEWSbxW9NEUg?client_secret=src_client_secret_F7igC7hMNadPcr2uTtEHhRqg", "status": "succeeded", "return_url": "https://rest.treatya.com/stripe/connect/redirect/static"}, "client_secret": "src_client_secret_F7igC7hMNadPcr2uTtEHhRqg", "three_d_secure": {"card": "src_19uIFYDQtlvbBEWSjV3P9Uao", "customer": null, "authenticated": true}}, "status": "succeeded", "created": 1488792973, "dispute": null, "invoice": null, "outcome": {"type": "authorized", "reason": null, "risk_level": "normal", "network_status": "approved_by_network", "seller_message": "Payment complete."}, "refunds": {"url": "/v1/charges/ch_19uIGHDQtlvbBEWSLzL6B68z/refunds", "data": [], "object": "list", "has_more": false, "total_count": 0}, "captured": true, "currency": "sek", "customer": null, "livemode": true, "metadata": {"bookingId": "6"}, "refunded": false, "shipping": null, "application": null, "description": "Treatya Treatment - NAILS", "destination": null, "failure_code": null, "on_behalf_of": null, "fraud_details": {}, "receipt_email": null, "receipt_number": null, "transfer_group": "booking-6", "amount_refunded": 0, "application_fee": null, "failure_message": null, "source_transfer": null, "balance_transaction": "txn_19uIGKDQtlvbBEWSIKkm1vJY", "statement_descriptor": null}',
    '{}', '{}');

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 1,'daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 1, 3, 'Scheelegatan 11, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',550, 'SEK', 'HAIR',
    tstzrange(current_date+'34 hours'::interval, current_date+'34 hours 15 minutes'::interval),'CONFIRMED');

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 2, '2bfd764c-bf72-44e6-b3b8-349947a06395', 3, 9, 'Dalagatan 11, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',1850, 'SEK', 'HAIR',
    tstzrange(current_date+'64 hours 30 minutes'::interval, current_date+'65 hours 15 minutes'::interval),'CONFIRMED');

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 2, '2bfd764c-bf72-44e6-b3b8-349947a06395', 4, 9, 'Valhallavägen 123, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',150, 'SEK', 'HAIR',
    tstzrange(current_date+'61 hours 25 minutes'::interval, current_date+'62 hours 25 minutes'::interval),'UNCONFIRMED');

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('daf3d1e7-bd83-4d64-8372-13e5f8fa4cca', 2, '143f1e64-e984-49cf-a827-b738fa2ec363', 3, 9, 'Valhallavägen 123, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',150, 'SEK', 'HAIR',
    tstzrange(current_date+'57 hours 10 minutes'::interval, current_date+'57 hours 45 minutes'::interval),'CONFIRMED');

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 1, '143f1e64-e984-49cf-a827-b738fa2ec363', 1, 9, 'Valhallavägen 123, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',150, 'SEK', 'HAIR',
    tstzrange(current_date-'9 hours'::interval, current_date-'8 hours'::interval),'DELIVERED');

insert into treatya.booking ( account_id, supplier_id, client_id, product_id, timeslot_id, address, geo_position, price, currency, type, period, status )
    values ('2bfd764c-bf72-44e6-b3b8-349947a06395', 1, '143f1e64-e984-49cf-a827-b738fa2ec363', 1, 9, 'Valhallavägen 123, Stockholm', '0101000020E6100000757D5DF856A84D40C08D49B5F2103240',150, 'SEK', 'HAIR',
    tstzrange(current_date+'39 hours'::interval, current_date+'40 hours'::interval),'CANCELLED');

insert into treatya_private.booking values (6,
'{"id": "ch_19uIGHDQtlvbBEWSLzL6B68z", "paid": true, "order": null, "amount": 1300, "object": "charge", "review": null, "source": {"id": "src_19uIFaDQtlvbBEWSbxW9NEUg", "flow": "redirect", "type": "three_d_secure", "owner": {"name": null, "email": null, "phone": null, "address": null, "verified_name": null, "verified_email": null, "verified_phone": null, "verified_address": null}, "usage": "single_use", "amount": 1300, "object": "source", "status": "consumed", "created": 1488792975, "currency": "sek", "livemode": true, "metadata": {}, "redirect": {"url": "https://hooks.stripe.com/redirect/authenticate/src_19uIFaDQtlvbBEWSbxW9NEUg?client_secret=src_client_secret_F7igC7hMNadPcr2uTtEHhRqg", "status": "succeeded", "return_url": "https://rest.treatya.com/stripe/connect/redirect/static"}, "client_secret": "src_client_secret_F7igC7hMNadPcr2uTtEHhRqg", "three_d_secure": {"card": "src_19uIFYDQtlvbBEWSjV3P9Uao", "customer": null, "authenticated": true}}, "status": "succeeded", "created": 1488792973, "dispute": null, "invoice": null, "outcome": {"type": "authorized", "reason": null, "risk_level": "normal", "network_status": "approved_by_network", "seller_message": "Payment complete."}, "refunds": {"url": "/v1/charges/ch_19uIGHDQtlvbBEWSLzL6B68z/refunds", "data": [], "object": "list", "has_more": false, "total_count": 0}, "captured": true, "currency": "sek", "customer": null, "livemode": true, "metadata": {"bookingId": "6"}, "refunded": false, "shipping": null, "application": null, "description": "Treatya Treatment - NAILS", "destination": null, "failure_code": null, "on_behalf_of": null, "fraud_details": {}, "receipt_email": null, "receipt_number": null, "transfer_group": "booking-6", "amount_refunded": 0, "application_fee": null, "failure_message": null, "source_transfer": null, "balance_transaction": "txn_19uIGKDQtlvbBEWSIKkm1vJY", "statement_descriptor": null}',
 '{"id": "re_19v3FWDQtlvbBEWSQaL7EZR4", "amount": 155000, "charge": "ch_19v3EVDQtlvbBEWS6l0BJ96Q", "object": "refund", "reason": null, "status": "succeeded", "created": 1488973594, "currency": "sek", "metadata": {}, "receipt_number": null, "balance_transaction": "txn_19v3FWDQtlvbBEWS01joFJI6"}',
  '{"id": "tr_19uifKDQtlvbBEWSNjhmLxQp", "date": 1488894470, "type": "stripe_account", "amount": 300, "method": "standard", "object": "transfer", "status": "paid", "created": 1488894470, "currency": "sek", "livemode": true, "metadata": {"bookingId": "6"}, "reversed": false, "recipient": null, "reversals": {"url": "/v1/transfers/tr_19uifKDQtlvbBEWSNjhmLxQp/reversals", "data": [], "object": "list", "has_more": false, "total_count": 0}, "description": null, "destination": "acct_19ilqLFfZnn9XPIL", "source_type": "card", "failure_code": null, "transfer_group": "booking-6", "amount_reversed": 0, "application_fee": null, "failure_message": null, "source_transaction": "ch_19uIGHDQtlvbBEWSLzL6B68z", "balance_transaction": "txn_19uifKDQtlvbBEWSO46eG9ah", "destination_payment": "py_19uifKFfZnn9XPIL0qG24BO9", "statement_descriptor": null}');

insert into treatya.review (target, account_id, reviewer_id, supplier_id, booking_id, comment, rating)
    values ('SUPPLIER','daf3d1e7-bd83-4d64-8372-13e5f8fa4cca','143f1e64-e984-49cf-a827-b738fa2ec363',2,4,'Som balsam för själen.',4);

insert into motd values (default, '{"sv":"Välkommen som testpilot på Treatya!"}');
