-- ---------------------------------------------------------------
-- ACCOUNTS
-- ---------------------------------------------------------------

create extension if not exists "pgcrypto";

create type treatya.role as enum (
  'anonymous',
  'account',
  'supplier',
  'admin'
);


create table treatya.account (
  id                          uuid primary key,
  first_name        text not null check (char_length(first_name) < 80),
  last_name         text check (char_length(last_name) < 80),
  about                  text,
  event_seq         integer not null default 0,
  geo_position   geography(POINT,4326),
  geo_address    text,
  address_info    text,
  motd                   timestamptz,
  language           text,
  rating                  real,
  ui_settings        jsonb default '{}',
  created_at        timestamptz default now(),
  updated_at      timestamptz default now()
);

comment on table treatya.account is 'A user of the site.';
comment on column treatya.account.id is 'The primary unique identifier for the account.';
comment on column treatya.account.first_name is 'The user’s first name.';
comment on column treatya.account.last_name is 'The user’s last name.';
comment on column treatya.account.about is 'A short description about the user, written by the user.';
comment on column treatya.account.created_at is 'The time this user was created.';

create function treatya.account_full_name(account treatya.account) returns text as $$
  select account.first_name || ' ' || account.last_name
$$ language sql stable;
comment on function treatya.account_full_name(treatya.account) is 'A user’s full name which is a concatenation of their first and last name.';


create function treatya_private.set_updated_at() returns trigger as $$
begin
  new.updated_at := current_timestamp;
  return new;
end;
$$ language plpgsql;

create trigger account_updated_at before update
    on treatya.account
    for each row
    execute procedure treatya_private.set_updated_at();

create table treatya_private.account (
    account_id              uuid primary key references treatya.account(id) on delete cascade,
    role                            treatya.role,
    last_login                timestamptz default now(),
    email                         text not null unique check (email ~* '^.+@.+\..+$'),
    email_verified       boolean default false,
    phone                       text,
    phone_verified      boolean default false,
    password_hash    text not null,
    password_token  text,
    payment                  jsonb default '{}'
);

comment on table treatya_private.account is 'Private information about an account.';
comment on column treatya_private.account.account_id is 'The id of the account associated with this account.';
comment on column treatya_private.account.email is 'The email address of the account.';
comment on column treatya_private.account.password_hash is 'An opaque hash of the account’s password.';


create or replace function treatya.register_account(
    first_name text,
    last_name text,
    language text,
    email text,
    password text
) returns treatya.account as $$
declare
    account treatya.account;
begin
    insert into treatya.account (id, first_name, last_name, language, motd) values
        (gen_random_uuid(), first_name, last_name, substring(language,1,2), now())
        returning * into account;

    insert into treatya_private.account (account_id, role, email, password_hash) values
        (account.id, 'account', lower(email), crypt(password, gen_salt('bf',12)));
    return account;
end;
$$ language plpgsql strict security definer;
comment on function treatya.register_account(text, text, text, text, text) is 'Registers a single user and creates an account.';


create function treatya.account_pwd(
    password text,
    token text
) returns integer as $$
declare
    nrows integer;
begin
    update treatya_private.account set (password_hash, password_token) = (crypt(password, gen_salt('bf',12)), default) where password_token=token;
    get diagnostics nrows := ROW_COUNT;
    return nrows;
end;
$$ language plpgsql strict security definer;
comment on function treatya.account_pwd(text, text) is 'Sets the account password.';

create function treatya.account_email(account treatya.account) returns text as $$
  select email from treatya_private.account where treatya_private.account.account_id = account.id
$$ language sql strict stable;
comment on function treatya.account_email(treatya.account) is 'Account email.';

create function treatya.account_email_verified(account treatya.account) returns boolean as $$
  select email_verified from treatya_private.account where treatya_private.account.account_id = account.id
$$ language sql strict stable;
comment on function treatya.account_email(treatya.account) is 'Account email verification status.';

create function treatya.account_phone(account treatya.account) returns text as $$
  select phone from treatya_private.account where treatya_private.account.account_id = account.id
$$ language sql strict stable;
comment on function treatya.account_phone(treatya.account) is 'Account phone.';

create function treatya.account_phone_verified(account treatya.account) returns boolean as $$
  select phone_verified from treatya_private.account where treatya_private.account.account_id = account.id
$$ language sql strict stable;
comment on function treatya.account_phone(treatya.account) is 'Account phone verification status.';

create function treatya.account_role(account treatya.account) returns treatya.role as $$
  select role from treatya_private.account where treatya_private.account.account_id = account.id
$$ language sql strict stable;
comment on function treatya.account_role(treatya.account) is 'Account role.';

create function treatya.account_payment(account treatya.account) returns jsonb as $$
  select payment from treatya_private.account where treatya_private.account.account_id = account.id
$$ language sql strict stable;
comment on function treatya.account_payment(treatya.account) is 'Get payment object.';

create function treatya.update_account_email(
    target_id uuid,
    email text
) returns integer as $$
declare
    nrows integer;
    userId uuid;
begin
    userId = treatya.logged_in_account_id();
    assert userId is not null, 'Not Authenticated.';
    if target_id <> userId then
        assert current_setting('role') = 'admin', 'Permission denied.';
    end if;

    update treatya_private.account set (email, email_verified) = (lower(update_account_email.email), false) where account_id = target_id;
    get diagnostics nrows := ROW_COUNT;
    return nrows;
end;
$$ language plpgsql strict security definer;

create function treatya.update_account_phone(
    target_id uuid,
    phone text
) returns integer as $$
declare
    nrows integer;
    userId uuid;
begin
    userId = treatya.logged_in_account_id();
    assert userId is not null, 'Not Authenticated.';
    if target_id <> userId then
        assert current_setting('role') = 'admin', 'Permission denied.';
    end if;

    update treatya_private.account set (phone, phone_verified) = (update_account_phone.phone, false) where account_id = target_id;
    get diagnostics nrows := ROW_COUNT;
    return nrows;
end;
$$ language plpgsql strict security definer;

create function treatya.update_account_role(
    target_id uuid,
    role            treatya.role
) returns integer as $$
declare
    nrows integer;
    userId uuid;
begin
    userId = treatya.logged_in_account_id();
    assert userId is not null, 'Not Authenticated.';
    if target_id <> userId then
        assert current_setting('role') = 'admin', 'Permission denied.';
    end if;

    update treatya_private.account set (role) = (update_account_role.role) where account_id = target_id;
    get diagnostics nrows := ROW_COUNT;
    update treatya.account set event_seq = event_seq + 1 where id=target_id;
    return nrows;
end;
$$ language plpgsql strict security definer;


create table treatya_private.mailinglist (
    email                         text not null unique check (email ~* '^.+@.+\..+$'),
    first_name              text,
    last_name              text,
    created_at             timestamptz not null default now(),
    last_mail_at          timestamptz
);



create type treatya.counters as (
        bookings_confirmed      bigint,
        bookings_preliminary    bigint,
        bookings_for_account  bigint,
        unread_messages           bigint,
        event_seq                           integer
);

create or replace function treatya.account_counters( arg_id uuid )
returns setof  treatya.counters as $$
    declare
        for_id uuid;
    begin
        for_id := treatya.logged_in_account_id();
        if current_setting('role',true) = 'admin' then
            for_id := arg_id;
        end if;

        -- Do a weird little union to be able to count and return all counters in a single select.
        -- Source 1 is bookings, source 2 is messages.
        -- Join this with the account event sequence flag.

        return query
        with counters as (
            with merge_bookings_msgs as (
                select b.id, b.status, 1 as source, b.client_id as account_id
                    from treatya.booking b
                    where (b.account_id = for_id or b.client_id = for_id)
                         and upper(b.period) > now()

                union all
                select m.id, null as no_status, 2 as source, m.account_id
                    from treatya.booking b, treatya.message m
                    where m.booking_id = b.id
                        and upper(b.period) > now()
                        and m.seen is not true
            )
            select
                sum((status='CONFIRMED' and account_id != for_id and source=1)::int) as bookings_confirmed,
                sum((status='UNCONFIRMED' and account_id != for_id and source=1)::int) as bookings_preliminary,
                sum((account_id = for_id and source=1 and status<>'CANCELLED' and status<>'DENIED' and status<>'NOSHOW' and status<>'DELIVERED' and status<>'NOTDELIVERED')::int) as bookings_for_account,
                sum((source = 2 and account_id = for_id)::int) as unread_messages
            from merge_bookings_msgs
        )
        select counters.*, accnt.event_seq from counters join treatya.account as accnt on accnt.id=for_id;
    end;
$$ language plpgsql stable strict;
