-- ---------------------------------------------------------------
-- go.treatya.com links
-- ---------------------------------------------------------------

create table treatya_private.go (
    id                                  serial primary key,
    hash                            text not null,
    url                                text not null,
    once                           boolean default true,
    used                            boolean default false,
    created_at               timestamptz default now()
);
create index on treatya_private.go ( hash );
