-- ----------------------------------------------------------------------------------------------------------------------------------
-- Slightly humongous query that finds available treatments....

-- Step 1: Since Postgres '-' operator for ranges does not support a - b when a @> b, we
-- create our own, which returns two rows for this case.

create or replace function tstzrange_minus( a tstzrange, b tstzrange )
    returns setof tstzrange
    language 'sql'
as $$
    select * from unnest(case
    when b is null or a <@ b then '{}'
    when a @> b then array[
        tstzrange(lower(a), lower(b), '[)'),
        tstzrange(upper(b), upper(a), '[)')
    ]
    else array[a - b]
    end)
$$;

-- Step 2: freeslots() will find free slots for the desired product/time. Used in step 3.
-- "free slots" are defined as the supplier available timeslots minus his non-cancelled bookings.

-- We need a return type:
create type slot as (
        id                          int,
        supplier_id       int,
        slot                      tstzrange
);

-- Given timeslots and bookings, produce a table that looks like timeslots but has
-- all booking periods removed:

create or replace function treatya.freeslots( wanted_type treatya.service_type, at_time timestamptz )
    returns setof treatya.slot
    language 'sql' stable
as $$

    -- A Recursive CTE that results in all timeslots with overlapping bookings, subtracted by the booking periods.

    with recursive slot_slices as (
        select    a.id, a.supplier_id, a.slot
        from     candidate_slots a
        union
            select         slot_slices.id, slot_slices.supplier_id, d
            from           slot_slices
            left join      treatya.booking b
                on (b.supplier_id=slot_slices.supplier_id and b.status!='CANCELLED' and b.status!='DENIED')
            cross join  treatya.tstzrange_minus(slot_slices.slot, b.period) d -- this is in fact an implicit lateral join
            where        slot_slices.slot && b.period
    ),

    -- Get us a CTE of potential timeslots for the desired product/time.
    -- Limit the candidates to a certain time span around the desired time,
    -- and exclude slots from the passed.

    candidate_slots as (
        select timeslot.id, timeslot.supplier_id, timeslot.slot
        from treatya.supplier, treatya.product, treatya.timeslot, treatya_private.supplier_private
        where
            supplier_private.status = 'CONFIRMED' and
            supplier.id = supplier_private.supplier_id and
            product.supplier_id = supplier.id and
            timeslot.supplier_id = supplier.id and
            product.type = wanted_type and
            upper(timeslot.slot) > now() and
            upper(timeslot.slot) > ( at_time - interval '1 month' ) and
            lower(timeslot.slot) < ( at_time + interval '1 month' )
    )

    -- Now combine original (non-booked) slots with what's left of the slots that had bookings.

    select    slot_slices.*
    from      slot_slices
    left join treatya.booking b
        on
            b.status<>'CANCELLED' and b.status<>'DENIED' and
            slot_slices.supplier_id = b.supplier_id and slot_slices.slot && b.period -- Match up booking periods and slots. Will not hit split slots.
    where
        not isempty(slot_slices.slot) and b.period is null
$$;

-- Step 3: find_suppliers() is the main search/browse function, which combines freeslots() with geographic location and category
-- to  return available suppliers with bookable times.

-- We need a return type for Postgraphql:
create type product_slot as (
        supplier_id       int,
        account_id       uuid,
        title                      text,
        intro                    text,
        description       text,
        geo_position   geography(POINT,4326),
        image_profile  text,
        image_profile_rot  int,
        image_bg         text[],
        rating                  real,
        product_id       int,
        type                    service_type,
        prod_title         text,
        prod_descr      text,
        price                   integer,
        currency            currency,
        timeslot_id       int,
        duration             interval,
        duration_sek    double precision,
        min_interval     interval,
        slot                       tstzrange,
        distance             double precision,
        time_dist           double precision,
        time_dir             text
);

-- Now the function that does the actual query!
create or replace function treatya.find_suppliers(wanted_type treatya.service_type, near_latitude text, near_longitude text, at_time timestamptz)
returns setof treatya.product_slot  as
 $$
declare
	nearpoint geography;
begin
	nearpoint := ST_GeographyFromText('SRID=4326;POINT(' || near_latitude || ' ' || near_longitude || ')');

	return query
    with free_timeslots as (
         select * from treatya.freeslots( wanted_type, at_time )
     ),

    supplier_nearby as (

        -- Get us a join of suppliers and products.
        -- Can be filtered with geographics limit here if performance
        -- becomes an issue.

        with supplier_products as (
            SELECT
                s.id, s.account_id, s.title, s.intro, s.description, s.image_profile, s.image_profile_rot, s.image_bg, s.rating, s.geo_position,
                p.id as product_id,
                p.supplier_id, p.prod_title, p.prod_descr, p.type, p.duration, p.price, p.currency, p.min_interval
            from treatya.supplier s, treatya.product p
            where
                s.id = p.supplier_id and
                p.type = wanted_type
        )

        -- Now look for suppliers with slots that either fit, or are after
        -- the desired booking time.

        SELECT
            s.id as supplier_id,
            s.account_id,
            s.title, s.intro, s.description,
            s.geo_position,
            s.image_profile, s.image_profile_rot, s.image_bg,
            s.rating,
            s.product_id,
            s.type,
            s.prod_title, s.prod_descr,
            s.price, s.currency,
            slots.timeslot_id,
            s.duration,
            extract( epoch from s.duration ),
            s.min_interval,
            slots.slot,
            ST_Distance(s.geo_position, nearpoint) as distance,
            greatest(extract( epoch from (lower(slots.slot) - at_time)), 0.0) as time_dist,
            'LATER'::text as time_dir
        from
            supplier_products as s

        left join lateral (
            SELECT ts.id as timeslot_id, ts.supplier_id, ts.slot
            from
                free_timeslots as ts
            where
                ts.supplier_id=s.supplier_id and
                ((tstzrange( at_time, at_time + duration, '()' ) <@ slot) or
                (at_time <= lower(slot) and duration <= (upper(slot)-lower(slot))))
            order by upper(slot)
            limit 1
        ) slots on true
        order by distance
        limit 100
    )

    -- Combine with supplier slots that are before the desired booking time.

    SELECT * from (
            SELECT distinct on (supplier_id) *
            from (
                table supplier_nearby
                union all
                SELECT
                    sn.supplier_id,
                    sn.account_id,
                    sn.title, sn.intro, sn.description,
                    sn.geo_position,
                    sn.image_profile, sn.image_profile_rot, sn.image_bg,
                    sn.rating,
                    sn.product_id,
                    sn.type,
                    sn.prod_title, sn.prod_descr,
                    sn.price, sn.currency,
                    t.timeslot_id,
                    sn.duration,
                    extract( epoch from sn.duration ),
                    sn.min_interval,
                    t.slot,
                    sn.distance,
                    greatest( extract( epoch from (at_time + sn.duration - upper(t.slot))), 0) as time_dist,
                    'EARLIER'::text as time_dir
                from supplier_nearby sn
                cross join lateral (
                    SELECT free_timeslots.id as timeslot_id, free_timeslots.supplier_id, free_timeslots.slot from free_timeslots
                        where
                            free_timeslots.supplier_id = sn.supplier_id and
                            upper(free_timeslots.slot) <= (at_time + sn.duration) and
                            sn.duration <= (upper(free_timeslots.slot)-lower(free_timeslots.slot))
                        order by
                            upper(free_timeslots.slot) desc
                            limit 1
                    ) t
        ) sub
        where timeslot_id > 0 -- Exclude LATER rows where there was no later slot
        order by supplier_id
    ) sub
    order by time_dist, distance;  -- matches first, ordered by distance; then misses, ordered by time distance

end;
$$ language plpgsql stable security definer;
comment on function find_suppliers(service_type, text, text, timestamptz) is 'Returns available suppliers for a certain product type near a point location.';

-- ----------------------------------------------------------------------------------------------------------------------------------

-- Similar to the freeslots() function (which finds slots for all suppliers of products of a certain type)
-- the function supplier_freeslots() find all free slots for a certain supplier.

create or replace function treatya.supplier_freeslots( wanted_supplier_id int )
    returns setof slot
    language 'sql' stable security definer -- needed because we select from bookings.
as $$

    -- A Recursive CTE that results in ONLY timeslots with overlapping bookings, subtracted by booking periods.

    with recursive slot_slices as (
        select * from candidate
        union
            select         slot_slices.id, slot_slices.supplier_id, split_slot
            from           slot_slices
            left join      treatya.booking booking using (supplier_id)
            cross join  treatya.tstzrange_minus(slot_slices.slot, booking.period) split_slot -- this is in fact an implicit lateral join
            where
                slot_slices.slot && booking.period -- Only do minus() where there is any overlap
                and booking.status<>'CANCELLED'
                and booking.status<>'DENIED'
    ),

    -- Get us a CTE of potential timeslots for the desired supplier.
    -- Exclude slots from the passed and too far off in the future.

    candidate as (
        select timeslot.id, timeslot.supplier_id, timeslot.slot
        from treatya.timeslot
        where
            timeslot.supplier_id = wanted_supplier_id and
            upper(timeslot.slot) > now() and
            lower(timeslot.slot) < ( now() + interval '3 month' )
    )

    select    slot_slices.*
    from      slot_slices
    left join treatya.booking b
        on
            b.status<>'CANCELLED' and b.status<>'DENIED' and
            slot_slices.supplier_id = b.supplier_id and slot_slices.slot && b.period -- Match up booking periods and slots. Will not hit split slots.
    where
        not isempty(slot_slices.slot)     -- No slivers...
        and b.period is null                       -- Remove all slots with overlapping bookings, because they have split slots instead.
    order by lower(slot)
$$;

-- ----------------------------------------------------------------------------------------------------------------------------------

-- Find suppliers regardless of free slots, just close enough and in the right category.

create or replace function treatya.nearby_unavailable_suppliers( wanted_type  treatya.service_type, near_latitude text, near_longitude text )
    returns setof treatya.product_slot
    language sql stable
as $$
    select distinct on (supplier_id)
            s.id as supplier_id, s.account_id,
            s.title,  s.intro,  s.description,  s.geo_position,
            s.image_profile,  s.image_profile_rot,  s.image_bg, s.rating,
            sp.id as product_id, sp.type,
            sp.prod_title, sp.prod_descr,  sp.price, sp.currency,
            ts.id as timeslot_id,
            sp.duration,
            null::double precision as duration_sek,
            null::interval as min_interval,
            null::tstzrange as slot,
            null::double precision as distance,
            null::double precision as time_dist,
            null::text as time_dir
    from treatya.supplier as s
	inner join treatya.product as sp
	   on s.id=sp.supplier_id
	   and sp.published=true
	   and sp.type=wanted_type
	   and ST_Distance(s.geo_position,  ST_GeographyFromText('SRID=4326;POINT(' || near_latitude || ' ' || near_longitude || ')')) < 100000
	left join treatya.timeslot as ts
	   on ts.supplier_id = s.id and lower(ts.slot)>now()
    where ts.id is null;
$$;
