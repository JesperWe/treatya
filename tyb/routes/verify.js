const express = require( 'express' )
const JWT = require( 'jsonwebtoken' )
const moment = require( 'moment' )
const nodemailer = require( 'nodemailer' )
const http = require( 'axios' )
const querystring = require( 'querystring' )
const co = require( 'co' )
const _ = require( 'lodash' )
const crypto = require( 'crypto' )
const eventNotifier = require( '../eventNotifier' )
const authenticate = require( './authenticate' )
const config = require( '../config' )
const colors = require( 'colors/safe' )
const { getAccount, getAccountByEmail, errorHandler, getSettings, logWithTimestamp, lastCall } = require( './util-functions' )
let encryptor = require( 'simple-encryptor' )( { key: config.smtp.key, hmac: false } )

const router = express.Router()
const psql = require( './psql' )

function msgString( template, variables ) {
	let result = _.template( template )( variables )
	if( result && result.length ) return result
	logWithTimestamp( colors.red( 'Failed to process emailVerification %s' ) )
	logWithTimestamp( colors.red( error ) )
	return '(error while creating message)'
}


router.post( '/email', function( req, res ) {
	let params = authenticate( res, req.body, [ 'email', 'language' ] )
	if( !params ) return
	const { language, email } = params

	co( function* () {
		let settings = yield getSettings()
		let templates = _.get( settings, 'value.notification.emailVerification.text.' + language )
		if( !templates || !templates.subject ) {
			logWithTimestamp( colors.red( 'For Language: ' ) + language )
			logWithTimestamp( colors.red( 'No emailVerification in: \n ' ) + JSON.stringify( templates, null, 2 ) )
			throw new Error( 'Verification message templates not available.' )
		}

		const { username, password, host } = config.smtp
		const transporter = nodemailer.createTransport( `smtps://${username}:${password}@${host}` )
		const token = encodeURIComponent( encryptor.encrypt( email ) )
		const { backend } = config.web

		let message = _.mapValues( templates, template => msgString( template, { token, backend, language } ) )
		message.from = 'Treatya <mailer@treatya.com>'
		message.to = email

		let result = yield transporter.sendMail( message )
		logWithTimestamp( 'Verification Email ' + email + ' ' + JSON.stringify( result ) )
		res.send( { response: result.response } )
	} ).catch( error => {
		res.status( 500 )
		res.send( res.__( 'Could not send e-mail: ' ) + error.message )
	} )
} )

router.get( '/link', function( req, res ) {
	if( !req.query.p ) {
		res.status( 401 )
		res.send( 'Bad parameters' )
	}

	co( function* () {
				let email = encryptor.decrypt( req.query.p )
				if( !email ) {
					res.status( 401 ).end( 'Bad parameters' )
					return
				}
				const accounts = yield getAccountByEmail( email )

				if( accounts.length !== 1 ) {
					logWithTimestamp( 'Account verification failed: ' + email )
					logWithTimestamp( JSON.stringify( accounts ) )
					res.send( 'Account verification failed.' )
					res.end()
					return
				}
				const account = accounts[ 0 ]
				logWithTimestamp( 'Account id ' + account.id + ' verified email. phone_verified=%s', account.phone_verified )
				lastCall( 'update treatya_private.account set email_verified=true where account_id=' + account.id )
				yield psql.result( 'update treatya_private.account set email_verified=true where account_id=$1', account.id )
				eventNotifier.notify( account.id )

				if( account.phone_verified ) {
					let newUiSettings = Object.assign( {}, account.ui_settings )
					_.set( newUiSettings, 'supplierRegistration.steps.contacts', { done: true } )
					lastCall( 'update treatya.account set ui_settings' )
					yield psql.result(
							`update treatya.account set ui_settings=$2::jsonb where id=$1`,
							[ account.id, newUiSettings ] )
				}

				res.render( 'verified', {} )
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/phone/send', function( req, res ) {
	let params = authenticate( res, req.body, [ 'phone' ] )
	if( !params ) return

	co( function* () {
		let settings = yield getSettings()
		settings = settings.value

		let hmac = crypto.createHmac( 'sha256', config.postgraphql.secret )
				.update( params.phone )
				.digest( 'hex' ).substr( 0, 6 )

		let message = res.__( 'Treatya Verification code:' ) + '\n\n' + hmac
		logWithTimestamp( 'SMS to', params.phone, message )

		if( settings.sms !== 'disable' )
			yield http.request( {
						url: config.sms.url,
						method: 'post',
						data: querystring.stringify( {
							to: params.phone,
							message,
							from: 'Treatya'
						} ),
						auth: {
							username: config.sms.username,
							password: config.sms.password
						}
					} )
					.catch( error => {
						throw new Error( 'Could not send SMS: ' + error.message + ' (' + error.response.data + ')' )
					} )

		res.send( 'ok' )
	} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/phone/check', function( req, res ) {
	let params = authenticate( res, req.body, [ 'phone', 'hmac' ] )
	if( !params ) return

	let hmac = crypto.createHmac( 'sha256', config.postgraphql.secret )
			.update( params.phone )
			.digest( 'hex' ).substr( 0, 6 )
	logWithTimestamp( 'Phone HMAC', params.phone, hmac )

	if( hmac === params.hmac ) {
		psql.none( 'update treatya_private.account set phone_verified=true where account_id=$1', params.authInfo.id )
				.then( result => {
					res.send( 'ok' )
				} )
				.catch( error => {
					res.status( 500 )
					res.send( error.message )
				} )
	} else {
		res.status( 401 )
		res.send( res.__( 'Verification failed.' ) )
	}
} )

router.post( '/impersonate', function( req, res ) {
	let params = authenticate( res, req.body, [ 'accountId' ] )
	if( !params ) return

	if( params.authInfo.role !== 'admin' ) {
		res.status( 403 )
		res.send( 'No Permission' )
	}

	co( function* () {
		let settings = yield getSettings()
		settings = settings.value

		let account = yield getAccount( params.accountId )

		let token = {
			role: account.role,
			id: account.id,
			iat: moment().valueOf(),
			aud: "postgraphql",
			iss: "postgraphql"
		}

		res.status( 200 )
		let jwtSecret = Buffer.from( config.postgraphql.secret, 'hex' ).toString()
		res.send( { authToken: JWT.sign( token, jwtSecret, { expiresIn: '1h' } ) } )
	} )
			.catch( errorHandler.bind( { req, res } ) )
} )

module.exports = router
