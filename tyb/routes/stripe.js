const express = require( 'express' )
const JWT = require( 'jsonwebtoken' )
const authenticate = require( './authenticate' )
const router = express.Router()
const co = require( 'co' )
const _ = require( 'lodash' )
const http = require( 'axios' )
const i18n = require( 'i18n' )
const moment = require( 'moment' )
const colors = require( 'colors/safe' )
const eventNotifier = require( '../eventNotifier' )
const config = require( '../config' )
const stripe = require( 'stripe' )( config.stripe.isLive ? config.stripe.sk_live : config.stripe.sk_test )

const {
	psql, getSettings, errorHandler, getAccount, getAccountByStripeId, getSupplier, getBookingIdSeq,
	sleep, setBookingRefund, insertNotification, getProduct, saveStripeCustomerData, createBooking,
	createBookingPayment, setBookingPayment, setBookingStatus, getBooking, getSupplierWithAccountByBookingId,
	socketErrorHandler, lastCall, logWithTimestamp
} = require( './util-functions' )

const DEBUG = true

router.post( '/create', function( req, res ) {
	let params = authenticate( res, req.body, [ 'forUserId' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( 'Stripe Account Create' )

	co( function* () {
		let settings = yield getSettings()
		settings = settings.value

		let account = yield getAccount( params.forUserId )
		let supplier = yield getSupplier( params.forUserId )
		//DEBUG && logWithTimestamp( 'Supplier:\n', JSON.stringify( supplier, null, 2 ) )

		if( account.role !== 'supplier' ) throw new Error( 'connect/create(): User has incorrect role.' )
		if( supplier.status !== 'CONFIRMED' ) throw new Error( 'connect/create(): Supplier has incorrect status.' )

		let stripeAccnt = yield stripe.accounts.create( {
			type: 'custom',
			country: 'SE',
			email: account.email
		} )

		yield psql.result( 'update treatya_private.supplier_private set stripe_id=$2 where supplier_id=$1', [ supplier.id, stripeAccnt.id ] )

		let legal_entity = {
			business_name: supplier.entity_name,
			address: {
				line1: supplier.address1,
				line2: supplier.address2,
				postal_code: supplier.zip,
				city: supplier.city,
				country: supplier.country
			},
			first_name: account.first_name,
			last_name: account.last_name,
			type: supplier.entity_type.toLowerCase()
		}

		// Swedish SSN format simple parse.
		let dateOfBirth, personal_id
		if( supplier.entity_type === 'INDIVIDUAL' ) {
			personal_id = supplier.entity_id.trim()
			let n = personal_id.replace( /-/g, '' )
			if( /[0-9]*/.test( n ) ) {
				if( n.length === 10 ) {
					dateOfBirth = { year: parseInt( n.substring( 0, 2 ) ) }
					dateOfBirth.year += 1900
					if( dateOfBirth.year < 1910 ) dateOfBirth.year += 100
					dateOfBirth.month = parseInt( n.substring( 2, 4 ) )
					dateOfBirth.day = parseInt( n.substring( 4, 6 ) )
				}
				if( n.length === 12 ) {
					dateOfBirth = { year: parseInt( n.substring( 0, 4 ) ) }
					dateOfBirth.month = parseInt( n.substring( 4, 6 ) )
					dateOfBirth.day = parseInt( n.substring( 6, 8 ) )
				}
			}

			legal_entity.personal_id_number = personal_id
			legal_entity.dob = dateOfBirth
		} else {
			legal_entity.business_tax_id = supplier.entity_id.trim()
			legal_entity.additional_owners = ''
		}

		let tos_acceptance = {
			date: Math.floor( moment( supplier.created_at ).valueOf() / 1000 ),
			ip: supplier.created_ip,
			user_agent: supplier.created_agent
		}

		DEBUG && logWithTimestamp( 'SSN result:', dateOfBirth, personal_id, tos_acceptance )
		DEBUG && logWithTimestamp( JSON.stringify( legal_entity, null, 2 ) )
		DEBUG && logWithTimestamp( 'tos_acceptance:' )
		DEBUG && logWithTimestamp( JSON.stringify( tos_acceptance, null, 2 ) )

		let updateResult = yield stripe.accounts.update( stripeAccnt.id, {
			business_name: supplier.title,
			legal_entity,
			tos_acceptance
		} )

		//DEBUG && logWithTimestamp( 'Update Result:\n', JSON.stringify( updateResult, null, 2 ) )

		yield psql.result( 'update treatya.account set event_seq = event_seq + 1 where id=$1', [ params.forUserId ] )
		res.send( stripeAccnt.id )
	} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/delete', function( req, res ) {
	let params = authenticate( res, req.body, [ 'stripeId' ] )
	if( !params || !params.authInfo.role === 'admin' ) {
		res.status( 400 )
		res.end()
		return
	}
	const { stripeId, id } = params

	co( function* () {
				DEBUG && logWithTimestamp( 'Stripe Account Delete %s (by accountId %s)', stripeId, id )
				const stripeResult = yield stripe.accounts.del( stripeId )
				res.send( stripeResult )
				res.end()
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/detachSource', function( req, res ) {
	let params = authenticate( res, req.body, [ 'customerId', 'sourceId' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( 'Stripe Detach Source' )
	const { customerId, sourceId } = params

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value

				yield stripe.customers.deleteSource( customerId, sourceId )
				const customer = yield stripe.customers.retrieve( customerId )
				yield saveStripeCustomerData( params.id, customer )
				eventNotifier.notify( params.id )

				res.status( 204 )
				res.end()
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/patch', function( req, res ) {
	let params = authenticate( res, req.body, [ 'patch', 'stripeId' ] )
	if( !params ) return
	if( !params.authInfo.role === 'admin' ) return

	stripe.accounts.update(
			params.stripeId,
			params.patch
			)
			.then( stripe1 => {
				if( stripe1.object === 'account' ) {
					res.send( 'ok' )
					return
				}
				logWithTimestamp( '\nStripe Update Account Response:\n', stripe1 )
				throw new Error( 'Update Account failed.' )
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/bankaccount', function( req, res ) {
	let params = authenticate( res, req.body, [ 'iban' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( 'Stripe Add Bank Account' )

	psql.one(
			`select stripe_id, country from treatya.supplier s, treatya_private.supplier_private ps
			where s.id = ps.supplier_id and s.account_id=$1`, [ params.id ] )
			.then( accnt => {

				let bankAccount = {
					object: 'bank_account',
					account_number: params.iban,
					country: accnt.country,
					currency: { SE: 'SEK', FI: 'EUR' }[ accnt.country ]
				}

				if( !config.stripe.isLive ) {
					// https://stripe.com/docs/testing#account-numbers
					bankAccount.account_number = 'DE89370400440532013000'
					bankAccount.country = 'DE'
					bankAccount.currency = 'EUR'
				}

				DEBUG && logWithTimestamp( 'Stripe.createExternalAccount()\n', bankAccount )

				return stripe.accounts.createExternalAccount(
						accnt.stripe_id,
						{ external_account: bankAccount }
				)
			} )
			.then( stripe1 => {
				if( stripe1.object === 'bank_account' ) {
					res.send( 'ok' )
					return
				}
				logWithTimestamp( '\nStripe Create Bank Account Response:\n', stripe1 )
				throw new Error( 'Create Bank Account failed.' )
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/dob', function( req, res ) {
	let params = authenticate( res, req.body, [ 'dob' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( 'Stripe Add DOB' )

	psql.one(
			`select stripe_id from treatya.supplier s, treatya_private.supplier_private ps
			where s.id = ps.supplier_id and s.account_id=$1`, [ params.id ] )
			.then( accnt => {
				let d = moment( params.dob )
				let dob = {
					legal_entity: {
						dob: {
							year: d.year(),
							month: d.month() + 1,
							day: d.date()
						}
					}
				}

				DEBUG && logWithTimestamp( 'Stripe.accounts.update()\n', dob )

				return stripe.accounts.update( accnt.stripe_id, dob )
			} )
			.then( stripe1 => {
				if( stripe1.object === 'account' ) {
					res.send( 'ok' )
					return
				}
				logWithTimestamp( '\nStripe.accounts.update() Response:\n', stripe1 )
				throw new Error( 'Update DOB failed.' )
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/personal', function( req, res ) {
	let params = authenticate( res, req.body, [ 'personal' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( 'Stripe Add Legal Entity Personal' )

	psql.one(
			`select stripe_id from treatya.supplier s, treatya_private.supplier_private ps
			where s.id = ps.supplier_id and s.account_id=$1`, [ params.id ] )
			.then( accnt => {
				let personal = {
					legal_entity: params.personal
				}

				DEBUG && logWithTimestamp( 'Stripe.accounts.update()\n', personal )

				return stripe.accounts.update( accnt.stripe_id, personal )
			} )
			.then( stripe1 => {
				if( stripe1.object === 'account' ) {
					res.send( 'ok' )
					return
				}
				logWithTimestamp( '\nStripe.accounts.update() Response:\n', stripe1 )
				throw new Error( 'Update Legal Entity Personal failed.' )
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/document', function( req, res ) {
	let params = authenticate( res, req.body, [ 'doc' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( 'Stripe Connect ID Document' )

	psql.one(
			`select stripe_id, country from treatya.supplier s, treatya_private.supplier_private ps
			where s.id = ps.supplier_id and s.account_id=$1`, [ params.id ] )
			.then( accnt => {

				DEBUG && logWithTimestamp( 'Stripe.accounts.update() add doc:', params.doc )
				return stripe.accounts.update(
						accnt.stripe_id,
						{ legal_entity: { verification: { document: params.doc } } }
				)
			} )
			.then( stripe1 => {
				if( stripe1.object === 'account' ) {
					res.send( 'ok' )
					return
				}
				logWithTimestamp( '\nStripe Update Account Response:\n', stripe1 )
				throw new Error( 'Attach document to account failed.' )
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

function _setHandled( message ) {
	return psql.result( 'update treatya_private.hook set handled=true where message->>\'id\'=$1', [ message.id ] )
}

router.post( '/hook', function( req, res ) {
	DEBUG && logWithTimestamp( 'Stripe webhook!  (' + _.get( req, 'body.type' ) + ')' )
	let event = req.body
	let responseText = 'Sucessfully handled.'

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value

				yield psql.result( 'insert into treatya_private.hook (type,message) values ($1, $2)', [ event.type, event ] )
				switch( event.type ) {

					case 'account.updated': {
						// Happens when Stripe decides to ask for  more confirmation info, for example.
						let stripeAccount = event.data.object
						if( stripeAccount.type !== 'custom' ) {
							logWithTimestamp( colors.yellow( 'Warning: Ignored Stripe webhook for non-custom account id', stripeAccount.id ) )
							break
						}
						let needed = stripeAccount.verification.fields_needed

						let result = yield psql.result(
								'update treatya_private.supplier_private set stripe_needed=$1::text[] where stripe_id=$2',
								[ needed, stripeAccount.id ] )

						if( result.rowCount !== 1 ) {
							DEBUG && logWithTimestamp( 'connect/hook update result', result, event.id )
							throw new Error(
									'Hook account.updated handler update treatya_private supplier_private rowCount=' + result.rowCount )
						}

						yield psql.result( 'update treatya_private.hook set handled=true where message->>\'id\'=$1', [ event.id ] )
						result = yield getAccountByStripeId( stripeAccount.id )
						yield psql.result( 'update treatya.account set event_seq = event_seq + 1 where id=$1', [ result.account_id ] )
						eventNotifier.notify( result.account_id )

						break
					}

					case 'charge.refunded': {
						const bookingId = _.get( event, 'data.object.metadata.bookingId' )
						if( !bookingId ) throw new EvalError( 'Received webhook ' + event.type + ' with no data.object.metadata.bookingId' )
						const booking = yield getBooking( bookingId )
						DEBUG && logWithTimestamp( 'Booking status was ', booking.status )
						if( booking.status === 'UNCONFIRMED' ) {

							// If booking is still unconfirmed this is a Stripe 7 day timeout.
							yield addBookingEvent( bookingId, {
								timestamp: moment().format( config.date.default ),
								status: 'NOTDELIVERED',
								previousStatus: 'UNCONFIRMED',
								reason: 'Stripe Hook: Uncaptured -> charge.refunded'
							} )
							yield setBookingStatus( bookingId, 'NOTDELIVERED' )

							let client = yield getAccount( booking.client_id )
							let supplier = yield getSupplierWithAccountByBookingId( bookingId )
							logWithTimestamp( colors.yellow( 'Stripe hook:  ' + event.type + ' user_id ' + client.account_id + ' amount ' + _.get( event, 'data.object.amount' ) ) )
							logWithTimestamp( colors.yellow( '    Uncaptured charge expired!' ) )
							yield insertNotification( 'bookingNeverConfirmed', {
								target: 'supplier',
								client,
								supplier,
								booking,
								settings
							} )
							yield insertNotification( 'bookingNeverConfirmed', {
								target: 'client',
								client,
								supplier,
								booking,
								settings
							} )
						}
						yield setBookingRefund( bookingId, event )
						break
					}

					case 'payout.failed': {
						lastCall( config.treatya.telegram.api + 'sendMessage' )
						http.post( config.treatya.telegram.api + 'sendMessage', {
									chat_id: config.treatya.telegram.chat_id,
									text: 'A Stripe payout failed!\n' +
									_.get(event,'data.object.failure_message') +
									'\nAccount: https://dashboard.stripe.com/applications/users/' + event.account
								} )
								.catch( errorHandler.bind( { req, res } ) )
						break;
					}

						// Hooks we know we get but don't know what to do with yet.
					case 'source.chargeable':
					case 'source.consumed':
					case 'source.failed':
					case 'source.canceled':
					case 'customer.created':
					case 'customer.updated':
					case 'customer.source.created':
					case 'customer.source.deleted':
					case 'charge.succeeded':
					case 'charge.captured':
					case 'charge.failed':
					case 'balance.available':
					case 'transfer.created':
					case 'transfer.updated':
					case 'payout.created':
					case 'payout.paid':
					case 'account.external_account.created':
					case 'account.external_account.updated':
					case 'account.application.deauthorized':
					case 'payment.created': {
						logWithTimestamp( 'Stripe hook:', event.type, event.user_id, _.get( event, 'data.object.amount' ) )
						break
					}

					default:
						logWithTimestamp( colors.yellow( 'Warning: Unhandled Stripe webhook, message.type =') , event.type )
						throw new EvalError( 'Don\'t know how to handle ' + event.type )
				}
				res.status( 204 )
				res.end()
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.get( '/redirect/static', function( req, res ) {
	res.render( 'three_d_secure', {} )
} )

module.exports.connect = router

//------------------------------------------------------------------------------------------------
// Below are the socket.io handlers that process a charge.

stripeMessage = function( socket, params ) {
	DEBUG && logWithTimestamp( 'Stripe Message', params )
	let authInfo
	try {
		if( !params.jwt || !params.source || !params.productId || !params.moment || !params.address || !params.geoPosition ) {
			throw new Error( 'Bad parameters: ' + JSON.stringify( params ) )
		}
		let jwtSecret = Buffer.from( config.postgraphql.secret, 'hex' ).toString()
		authInfo = JWT.verify( params.jwt, jwtSecret )
	}
	catch( error ) {
		socket.emit( 'stripeError', error.message )
		return
	}

	co(
			function* () {
				DEBUG && logWithTimestamp( '\nStripe start.' )
				let settings = yield getSettings()
				settings = settings.value

				let account = yield getAccount( authInfo.id )
				socket.emit( 'stripe', { state: 'ACCOUNT', percentDone: 40 } )

				let product = yield getProduct( params.productId )
				if( !product ) throw new Error( 'Product not found.' )
				//DEBUG && logWithTimestamp( '\nProduct\n', JSON.stringify( product, null, 2 ) )
				socket.emit( 'stripe', { state: 'PRODUCT', percentDone: 40 } )

				let metadata = _.pick( account, [ 'first_name', 'last_name', 'geo_address', 'language', 'phone', 'email', 'role', 'account_id' ] )

				let customer = account.payment

				if( customer && customer.id ) {
					DEBUG && logWithTimestamp( 'OLD CUSTOMER' )
					socket.emit( 'stripe', { state: 'SAVINGSOURCE', percentDone: 50 } )
					DEBUG && logWithTimestamp( 'Saving source on customer' )
					if( !customer.sources.data.find( source => source.id === params.source ) ) {
						yield stripe.customers.createSource( customer.id, { source: params.source } )
						DEBUG && logWithTimestamp( 'Saved source %s on customer %s', params.source, customer.id )
						customer = yield stripe.customers.retrieve( customer.id )
					}
				} else {
					DEBUG && logWithTimestamp( 'NEW CUSTOMER' )
					customer = yield stripe.customers.create( {
						email: account.email,
						metadata,
						source: params.source,
					} )
				}
				socket.emit( 'stripe', { state: 'CUSTOMER', percentDone: 60 } )

				let saved = yield saveStripeCustomerData( account.account_id, customer )
				socket.emit( 'stripe', { state: 'ACCOUNT-SAVED', percentDone: 65 } )

				let bookingIdSeq = yield getBookingIdSeq()
				let booking

				//-------------------------------------------------------------------------------------------------------------------------------
				// Two different mechanisms here: 3DSecure source or standard charge.

				let source
				const config3DS = 'stripe.three_d_secure.' + params.three_d_secure
				const perform3DS = _.get( config, config3DS )
				if( perform3DS === undefined ) {
					logWithTimestamp( colors.red( 'Missing config: ' + config3DS ) )
				}

				if( perform3DS ) {
					source = yield stripe.sources.create( {
						type: 'three_d_secure',
						amount: product.price * (settings.currency.cents[ product.currency ] || 100),
						currency: product.currency,
						three_d_secure: { card: params.source },
						redirect: { return_url: config.web.backend + '/stripe/connect/redirect/static' }
					} )

					socket.emit( 'stripe', {
						state: '3DSOURCE-CREATED',
						percentDone: 65,
						redirect: source.redirect
					} )

					yield sleep( 2000 )

					do {
						yield sleep( 1000 )
						source = yield stripe.sources.retrieve( source.id )
						socket.emit( 'stripe', {
							state: '3DSOURCE-PENDING'
						} )
					}
					while( source.status === 'pending' )

					socket.emit( 'stripe', {
						percentDone: 70,
						state: '3DSOURCE-STATUS',
						status: source.status
					} )

					if( source.status !== 'chargeable' ) {
						throw new Error( 'Authorization of payment failed.' )
					}

				} else {
					source = { id: params.source }
				}

				//-------------------------------------------------------------------------------------------------------------------------------
				// Now the standard charge process.
				// First reserve a Charge on the user's card.

				let chargeData = {
					amount: product.price * (settings.currency.cents[ product.currency ] || 100),
					currency: product.currency,
					description: i18n.__( 'Treatya Treatment' ) + ' - ' + i18n.__( product.type ),
					customer: customer.id,
					source: source.id,
					metadata: { bookingId: bookingIdSeq.nextval },
					transfer_group: 'booking-' + bookingIdSeq.nextval,
					capture: false
				}

				try {
					booking = yield createBooking( authInfo, params, product, bookingIdSeq.nextval )
					socket.emit( 'stripe', { state: 'PRELIMINARY', percentDone: 70 } )
				} catch( error ) {
					if( error.message.indexOf( 'overlapping' ) >= 0 )
						throw new Error( 'Failed to create booking since it overlaps with other bookings.' )
					else
						throw new Error( 'Failed to create booking: ' + error.message )
				}

				//DEBUG && logWithTimestamp( 'chargeData', JSON.stringify( chargeData, null, 2 ) )
				let charge = yield stripe.charges.create( chargeData )
				socket.emit( 'stripe', { state: 'CHARGE', percentDone: 75 } )
				if( !booking || !booking.id ) throw new Error( 'Payment failed, booking info missing ...' )

				yield setBookingStatus( booking.id, 'UNCONFIRMED' )
				yield createBookingPayment( booking.id, charge )
				socket.emit( 'stripe', { state: 'BOOKING', bookingId: booking.id, percentDone: 90 } )

				if( params.capture !== false ) {
					let capturedCharge = yield stripe.charges.capture( charge.id )
					socket.emit( 'stripe', { state: 'CAPTURED', bookingId: booking.id, percentDone: 93 } )

					yield setBookingPayment( booking.id, capturedCharge )
					socket.emit( 'stripe', { state: 'SAVECAPTURED', percentDone: 95 } )
					booking.chargeId = capturedCharge.id
				}

				// Convert booking.period for use by the frontend calendar buttons.
				// Typical period string format from Postgres tstzrange:
				// ["2017-01-22 09:50:00+01","2017-01-22 10:40:00+01")
				let [ start, end ] = booking.period.replace( /["[\]()]/g, '' ).split( ',' )
				// Change to GraphQL datamodel.
				booking.period = { start: { value: start }, end: { value: end } }
				socket.emit( 'stripe', { state: 'DONE', percentDone: 100, booking } )
				DEBUG && logWithTimestamp( '\nStripe WebSocket done.' )
			}
	).catch( socketErrorHandler.bind( { socket, params } ) )
}

module.exports.stripeCharge = function( socket ) {
	socket.on( 'connect', msg => logWithTimestamp( 'Socket Connect:', msg ) )
	socket.on( 'stripe', ( data ) => stripeMessage( socket, data ) )
	socket.on( 'disconnect', msg => logWithTimestamp( 'Socket Disconnect:', msg ) )
}
