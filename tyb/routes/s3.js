const express = require( 'express' );
const router = express.Router();
const Crypto = require( 'crypto-js' );
const authenticate = require( './authenticate' );
const moment = require( 'moment' );
const AWS = require( 'aws-sdk' );
const { logWithTimestamp } = require( './util-functions' )
const config = require( '../config' );

const awsDate = moment().format( 'YYYYMMDD' ) + 'T000000Z';
const awsService = 's3';

function s3Params( filename ) {
	let credential = [ config.s3.id, awsDate, config.s3.region, awsService, 'aws4_request' ].join( '/' );

	let policy = s3UploadPolicy( filename, credential );
	let policyBase64 = new Buffer( JSON.stringify( policy ) ).toString( 'base64' );
	let signingKey = getSignatureKey( config.s3.secret, awsDate, config.s3.region, awsService );
	let signature = Crypto.HmacSHA256( policyBase64, signingKey ).toString( Crypto.enc.Hex );
	logWithTimestamp( 'signature', signature );

	return {
		key: filename,
		acl: 'public-read',
		success_action_status: '201',
		region: config.s3.region,
		policy: policyBase64,
		accessKeyId: config.s3.id,
		credentials: credential,
		date: awsDate,
		signature: signature
	}
}

function s3UploadPolicy( filename, credential ) {
	let p = {
		// 5 minutes into the future
		expiration: moment().add( 5, 'minutes	' ).format(),
		conditions: [
			{ bucket: config.s3.bucket },
			{ key: filename },
			{ acl: 'public-read' },
			{ success_action_status: "201" },
			// Optionally control content type and file size
			// {'Content-Type': 'application/pdf'},
			[ 'content-length-range', 0, 1000000 ],
			{ 'x-amz-algorithm': 'AWS4-HMAC-SHA256' },
			{ 'x-amz-credential': credential },
			{ 'x-amz-date': awsDate }
		],
	}
	logWithTimestamp( p );
	return p;
}

function getSignatureKey( key, dateStamp, regionName, serviceName ) {
	logWithTimestamp( 'getSignatureKey()', key, dateStamp, regionName, serviceName );
	let kDate = Crypto.HmacSHA256( dateStamp, 'AWS4' + key );
	logWithTimestamp( '        ', kDate.toString( Crypto.enc.Hex ) );

	let kRegion = Crypto.HmacSHA256( regionName, kDate );
	logWithTimestamp( '        ', kRegion.toString( Crypto.enc.Hex ) );

	let kService = Crypto.HmacSHA256( serviceName, kRegion );
	logWithTimestamp( '        ', kService.toString( Crypto.enc.Hex ) );

	let kSigning = Crypto.HmacSHA256( 'aws4_request', kService );
	logWithTimestamp( '        ', kSigning.toString( Crypto.enc.Hex ) );
	return kSigning;
}

router.get( '/policy', function( req, res, next ) {
	let params = authenticate( res, req.query, ['filename'] );
	if( !params ) return;

	let result = {
		endpoint: "https://" + config.s3.bucket + '.s3-' + config.s3.region + '.amazonaws.com',
		params: s3Params( params.id + '/' + params.filename )
	}
	res.send( result );
} );

router.get( '/url', function( req, res, next ) {
	let params = authenticate( res, req.query, ['filename'] );
	if( !params ) return;

	let uri = params.id + '/' + params.filename;
	let s3 = new AWS.S3( config.s3 );
	res.send( {
		url: s3.getSignedUrl( 'putObject', { Key: uri, Expires: 60 } ),
		src: "https://" + config.s3.bucket + '.s3-' + config.s3.region + '.amazonaws.com/' + uri
	} );
} );

module.exports = router;
