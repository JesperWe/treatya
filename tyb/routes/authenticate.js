const JWT = require( 'jsonwebtoken' )
const config = require( '../config' )
const colors = require( 'colors/safe' )
const { logWithTimestamp } = require( './util-functions' )

module.exports = function authenticate( res, params, wanted ) {
	let wantedParams = [ ...wanted ]
	wantedParams.push( 'id' )
	wantedParams.push( 'jwt' )
	let gotWanted = true
	wantedParams.forEach( param => {
		if( params[ param ] === undefined ) gotWanted = false
	} )

	if( !gotWanted ) {
		res.status( 401 )
		res.send( 'Bad parameters' )
		return false
	}

	let authInfo
	try {
		let jwtSecret = Buffer.from( config.postgraphql.secret, 'hex' ).toString()
		authInfo = JWT.verify( params.jwt, jwtSecret )
	}
	catch( error ) {
		let msg = 'Authenticate: Token Verification - ' + error.message
		res.status( 401 )
		res.send( msg )
		logWithTimestamp( colors.red( msg ) )
		logWithTimestamp( colors.grey( error.stack ) )
		return false
	}

	let result = Object.assign( {}, params )
	result.authInfo = authInfo
	return result
}
