const _ = require( 'lodash' )
const moment = require( 'moment' )
const colors = require( 'colors/safe' )
const config = require( '../config' )
const eventNotifier = require( '../eventNotifier' )
const stripe = require( 'stripe' )( config.stripe.isLive ? config.stripe.sk_live : config.stripe.sk_test )

const DEBUG = false

const psql = require( './psql' )
module.exports.psql = psql

let lastCall = '(unknown)'
let lastLog = moment().subtract( 1, 'minute' )

function logWithTimestamp( ...args ) {
	if( moment().diff( lastLog, 'seconds' ) > 3 ) {
		console.log( colors.grey( '-------------------' + moment().format( 'YYYY MM DD H:mm:ss' ) + '-------------------' ) )
		lastLog = moment()
	}
	console.log( ...args )
}

module.exports.logWithTimestamp = logWithTimestamp

module.exports.getAccount = function( id ) {
	lastCall = 'getAccount(' + id + ')'
	return psql.one(
			`SELECT * FROM treatya.account a, treatya_private.account pa WHERE a.id=pa.account_id and a.id=$1`, [ id ] )
}

module.exports.getAccountByBookingId = function( id ) {
	lastCall = 'getAccountByBookingId(' + id + ')'
	return psql.one(
			`select a.*, pa.* 
			from treatya.booking b, treatya.account a, treatya_private.account pa
			where pa.account_id = a.id and b.client_id=a.id and b.id=$1`, [ id ] )
}

module.exports.getAccountByEmail = function( email ) {
	lastCall = 'getAccountByEmail(' + email + ')'
	return psql.any( 'SELECT * FROM treatya.account a, treatya_private.account pa WHERE a.id=pa.account_id and pa.email=$1', [ email.toLowerCase() ] )
}

module.exports.getAccountByStripeId = function( id ) {
	lastCall = 'getAccountByStripeId(' + id + ')'
	return psql.one(
			`select s.account_id from treatya.supplier s, treatya_private.supplier_private ps 
								where s.id=ps.supplier_id and ps.stripe_id=$1`, [ id ] )
}

module.exports.getBooking = function( id ) {
	lastCall = 'getBooking(' + id + ')'
	return psql.one(
			`select b.*, pb.* from
				(select b.*, p.prod_title, p.duration 
					from treatya.booking b, treatya.product p 
					where b.product_id=p.id and b.id=$1
				) as b
			left outer join 
				treatya_private.booking as pb on(pb.booking_id = b.id)`, [ id ] )
}

module.exports.getOtherBookings = function( accountId, type ) {
	lastCall = 'getOtherBookings(' + accountId + ', ' + type + ')'
	return psql.any(
			`select b.*, pb.* from
				(select b.*, p.prod_title, p.duration 
					from treatya.booking b, treatya.product p 
					where 
						b.product_id=p.id and 
						b.status='UNCONFIRMED' and 
						b.client_id=$1 and 
						p.type=$2
				) as b
			left outer join 
				treatya_private.booking as pb on(pb.booking_id = b.id)`, [ accountId, type ] )
}

module.exports.getBookingIdSeq = function() {
	lastCall = 'getBookingIdSeq()'
	return psql.one( "select nextval(pg_get_serial_sequence('treatya.booking', 'id'))" )
}

module.exports.getProduct = function( productId ) {
	lastCall = 'getProduct(' + productId + ')'
	return psql.one( 'SELECT p.*, s.account_id FROM treatya.product p, treatya.supplier s WHERE p.id=$1 and p.supplier_id=s.id', [ productId ] )
}

module.exports.getSettings = function() {
	lastCall = 'getSettings()'
	return psql.one( 'select value from treatya_private.setting where name=$1', [ config.db.settings ] )
}

module.exports.getSupplier = function( id ) {
	lastCall = 'getSupplier(' + id + ')'
	return psql.one( 'SELECT * FROM treatya.supplier s, treatya_private.supplier_private ps WHERE s.id=ps.supplier_id and s.account_id=$1', [ id ] )
}

module.exports.getSupplierAccount = function( accountId ) {
	lastCall = 'getSupplierAccount(' + accountId + ')'
	return psql.one(
			`SELECT * FROM treatya.supplier s, treatya_private.supplier_private ps, treatya.account a, treatya_private.account pa
			WHERE s.id=ps.supplier_id and s.account_id=a.id and pa.account_id=a.id and a.id=$1`,
			[ accountId ] )
}

module.exports.getSupplierWithAccountByBookingId = function( id ) {
	lastCall = 'getSupplierWithAccountByBookingId(' + id + ')'
	return psql.one(
			`select s.*, ps.*, a.language, a.first_name, a.last_name, pa.email, pa.phone 
			from treatya.supplier s, treatya_private.supplier_private ps, treatya.booking b, treatya.account a, treatya_private.account pa
			where pa.account_id=a.id and a.id=s.account_id and s.id=ps.supplier_id and s.id=b.supplier_id and b.id=$1`, [ id ] )
}

module.exports.getAllNotifications = function() {
	lastCall = 'getAllNotifications()'
	return psql.any( `select * from treatya_private.notification where status = 'QUEUED' and send_at < now()+'2 minutes'::interval order by send_at` )
}

module.exports.createBooking = function( authInfo, params, product, id ) {
	lastCall = 'createBooking(..., ' + id + ')'
	return psql.one(
			`insert into treatya.booking values (
						$(id), $(account), $(supplier), $(client), $(prod), $(tslot), $(addr), 
						ST_GeographyFromText('SRID=4326;POINT(' || $(lat) || ' ' || $(lng) || ')'),
						$(price), $(cur), $(type), tstzrange($(from), $(to)), $(status), default, default
					) returning *`,
			{
				id,
				account: product.account_id,
				supplier: product.supplier_id,
				client: authInfo.id,
				prod: params.productId,
				tslot: null, /* timeslot_id ? */
				addr: params.address,
				lat: params.geoPosition.lat.toString(),
				lng: params.geoPosition.lng.toString(),
				price: product.price,
				cur: product.currency,
				type: product.type,
				from: params.moment,
				to: moment( params.moment )
						.add( product.duration.hours, 'h' )
						.add( product.duration.minutes, 'm' ).format(),
				status: 'PRELIMINARY'
			}
	)
}

module.exports.createBookingPayment = function( id, charge ) {
	lastCall = 'createBookingPayment(' + id + ', ...)'
	return psql.result( `insert into treatya_private.booking (booking_id, payment) values ($1, $2)`, [ id, charge ] )
}

module.exports.createSupplierPayout = function( supplier, booking, settings ) {
	lastCall = 'createSupplierPayout(...)'
	let feeToCharge = settings.charge

	if( supplier.fee ) feeToCharge = supplier.fee
	DEBUG && logWithTimestamp( 'Payout fee =', JSON.stringify( feeToCharge ) )

	let min, max, fee
	try {
		min = feeToCharge.minimum * settings.currency.cents[ booking.payment.currency ]
		max = feeToCharge.maximum * settings.currency.cents[ booking.payment.currency ]
		fee = Math.floor( booking.payment.amount * (feeToCharge.percent / 100) )
	}
	catch( e ) {
		logWithTimestamp( 'createSupplierPayout() Problem with fee =', JSON.stringify( feeToCharge ) )
		logWithTimestamp( e.message )
		if( !max ) max = 1e12
		if( !min ) min = 0
		if( !fee || fee < 0 ) fee = 0
	}

	if( fee > max ) fee = max
	if( fee < min ) fee = min
	let amount = booking.payment.amount - fee
	if( amount <= 0 ) return Promise.resolve( false )

	return stripe.transfers.create( {
		amount,
		currency: booking.payment.currency,
		destination: supplier.stripe_id,
		metadata: booking.payment.metadata,
		source_transaction: booking.payment.id
	} )
}

module.exports.insertMailinglist = function( email, firstName, lastName ) {
	lastCall = 'insertMailinglist(' + email + ', ...)'
	return psql.result( 'insert into treatya_private.mailinglist (email,first_name,last_name) values ($1,$2,$3)', [ email, firstName, lastName ] )
}

function _msgString( type, notification, setting, msg ) {
	let language = notification.language || 'en'
	language = language.substr(0,2)
	try {
		let template = _.get( setting, 'text[' + language + '].' + msg )
		let result = _.template( template )( notification )
		if( result && result.length ) return result
		throw new Error( 'Empty result from template \'' + template + '\'' )
	}
	catch( error ) {
		logWithTimestamp( colors.red( 'Failed to process %s/%s in language=%s:' ), type, msg, language )
		logWithTimestamp( colors.red( error ) )
		return '<error creating message text (language=' + language + ')>'
	}
	return _.get( setting, 'text[' + language + '].' + msg )
}

module.exports.insertNotification = function( type, options ) {
	lastCall = 'insertNotification(' + type + ', ...)'

	const { target, client, supplier, booking, settings } = options
	let receiver = client
	if( target === 'supplier' ) receiver = supplier

	let notification = {}

	if( receiver ) {
		notification = receiver
		notification.receiverEmail = receiver.email
		notification.receiverPhone = receiver.phone
		notification.receiverFirstName = receiver.first_name
		notification.receiverLastName = receiver.last_name
		notification.receiverFullName = receiver.first_name + ' ' + receiver.last_name
		notification.receiverLanguage = receiver.language
	} else {
		throw new Error( 'No receiver for notification.' )
	}

	let clientInfo = ''

	if( client ) {
		notification.clientEmail = client.email
		notification.clientPhone = client.phone
		notification.clientFirstName = client.first_name
		notification.clientLastName = client.last_name
		notification.clientFullName = client.first_name + ' ' + client.last_name
		notification.clientLanguage = client.language
		if( client.addressInfo && client.addressInfo.length ) clientInfo = ' (' + client.addressInfo + ')'
		notification.clientAddress = client.geo_address + clientInfo
	}
	if( supplier ) {
		notification.supplierEmail = supplier.email
		notification.supplierPhone = supplier.phone
		notification.supplierFirstName = supplier.first_name
		notification.supplierLastName = supplier.last_name
		notification.supplierFullName = supplier.first_name + ' ' + supplier.last_name
		notification.supplierLanguage = supplier.language
		notification.supplier_title = supplier.title
	}

	if( booking ) {
		if( !booking.start ) booking.start = moment( booking.period.split( '"' )[ 1 ] )
		notification.booking_id = booking.id
		notification.prod_title = booking.prod_title
		notification.supplier_title = supplier.title
		notification.startAt = booking.start && booking.start.format( config.date.default )
		notification.reviewLink = config.web.frontend + '/review/b/' + booking.booking_id
		notification.bookingLink = config.web.frontend + '/booking/' + booking.booking_id
		if( booking.address ) notification.clientAddress = booking.address + clientInfo
	} else {
		notification.booking_id = 0 // Has to be defined or the SQL insert fails.
	}

	let setting = _.get( settings, `notification.${type}` )
	if( !setting ) throw new Error( 'There are no notification settings for: ' + type +
			' Available: ' + JSON.stringify( Object.keys( settings.notification ) ) )

	notification.sms = _msgString( type, notification, setting, 'sms' )
	notification.subject = _msgString( type, notification, setting, 'subject' )
	notification.body = _msgString( type, notification, setting, 'body' )

	const { hours, days } = setting

	if( _.isNumber( days ) ) {
		// When .days is specified, .hours is the time of day on that day, not relative to booking period start.
		notification.send_at = moment( booking.start ).startOf( 'day' )
				.add( days, 'days' )
				.add( hours, 'hours' )
	} else {
		if( !hours ) { // Any falsey value --> Send immediately.
			notification.send_at = moment()
		} else {
			// No .days, so hours is relative to booking start.
			notification.send_at = moment( booking.start ).add( hours, 'hours' )
		}
	}

	if( notification.send_at.clone().add( 2, 'minutes' ).isBefore() ) { // Add a bit so we don't skip NOW notifications.
		logWithTimestamp( colors.blue( 'Skipping outdated notification: %s at %s to: %s/%s' ),
				type, notification.send_at.format( config.date.default ), notification.email, notification.phone )
		return Promise.resolve( true )
	}

	notification.post_processors = setting.postProcessors || []
	notification.status = 'QUEUED'

	DEBUG && logWithTimestamp( colors.yellow( '    + notification: %s at %s to: %s/%s' ),
			type, notification.send_at.format( config.date.default ), notification.email, notification.phone )
	DEBUG && logWithTimestamp( colors.yellow( '      %s' ), notification.sms )

	eventNotifier.notify( notification.account_id )

	return psql.result(
			"insert into treatya_private.notification " +
			"( account_id, booking_id, send_at, post_processors, status, email, phone, sms, subject, body ) values " +
			"( ${account_id}, ${booking_id}, ${send_at}, ${post_processors}::text[], ${status}, ${email}, ${phone}, ${sms}, ${subject}, ${body}  )", receiver )
}

module.exports.setAccountUISettings = function( id, uiSettings ) {
	lastCall = 'setAccountUISettings(' + id + ', ...)'
	return psql.result( 'update treatya.account set ui_settings=$2 where id=$1', [ id, uiSettings ] )
}

module.exports.pushAccountEvent = function( id ) {
	lastCall = 'pushAccountEvent(' + id + ')'
	if( !id ) logWithTimestamp( colors.red( 'Bad id for pushAccountEvent()' ) )
	eventNotifier.notify( id )
	return psql.result( 'update treatya.account set event_seq = event_seq + 1 where id=$1', [ id ], r => r.rowCount )
}

module.exports.setBookingPayment = function( bookingId, payment ) {
	lastCall = 'setBookingPayment(' + bookingId + ')'
	return psql.one(
			`update treatya_private.booking set payment = $1 where booking_id=$2 returning *`, [ payment, bookingId ] )
}

module.exports.setBookingRefund = function( bookingId, refund ) {
	lastCall = 'setBookingRefund(' + bookingId + ')'
	return psql.one(
			`insert into treatya_private.booking (booking_id, refund) values ($1, $2) on conflict(booking_id) do update set (refund)=($2) returning *`, [ bookingId, refund ] )
}

module.exports.setBookingStatus = function( id, status ) {
	lastCall = 'setBookingStatus(' + id + ', ' + status + ')'
	return psql.result( `update treatya.booking set status=$2 where id = $1`, [ id, status ] )
}

module.exports.addBookingEvent = function( id, event ) {
	lastCall = 'addBookingEvent(' + id + ') ' + JSON.stringify( event )

	return psql.result(
			`update treatya_private.booking
				set events = array_append(events, $2::jsonb)
				where booking_id=$1`,
			[ id, event ]
	)
}

module.exports.setNotificationStatus = function( id, status ) {
	lastCall = 'setNotificationStatus(' + id + ', ' + status + ')'
	return psql.result( `update treatya_private.notification set status=$2 where id=$1`, [ id, status ] )
}

module.exports.setNotificationResult = function( id, email_result, phone_result ) {
	lastCall = 'setNotificationResult(' + id + ', ...)'
	return psql.result( `update treatya_private.notification set (status,email_result,phone_result)=('DONE', $2, $3) where id=$1`,
			[ id, email_result, phone_result ] )
}

module.exports.removeBookingReminders = function( bookingId ) {
	lastCall = 'removeBookingReminders(' + bookingId + ')'
	return psql.result( `delete from treatya_private.notification where booking_id=$1 and status='QUEUED'`, bookingId )
}

module.exports.saveStripeCustomerData = function( accountId, customer ) {
	lastCall = 'saveStripeCustomerData(' + accountId + ')'
	return psql.result(
			`update treatya_private.account set payment = $1 where account_id=$2`,
			[ customer, accountId ]
	)
}

module.exports.refundBooking = function( booking ) {
	lastCall = 'refundBooking(...)'
	let charge = _.get( booking, 'payment.id' )
	if( !charge ) return Promise.resolve( {} )
	DEBUG && logWithTimestamp( colors.cyan( 'Refund charge id: %s' ), charge )
	return stripe.refunds.create( { charge } )
}

module.exports.savePayout = function( id, payout ) {
	lastCall = 'savePayout(' + id + ')'
	return psql.result( `update treatya_private.booking set payout = $2 where booking_id = $1`, [ id, payout ] )
}

module.exports.sleep = function( ms ) {
	return function( cb ) {
		setTimeout( cb, ms )
	}
}

module.exports.lastCall = function( call ) {
	lastCall = call
}

module.exports.errorHandler = function( error ) {
	DEBUG && logWithTimestamp( 'errorHandler', this.res.headersSent, error.message )
	if( this.res.headersSent ) return
	console.error( colors.red( '\n\n' + moment().format() ) )
	console.error( colors.red( 'Error during %s' ), this.req.originalUrl )
	console.error( colors.red( 'Last Call: %s' ), lastCall )
	console.error( 'Params:', this.req.body )
	console.error( colors.red( error.param + ' ' + error.message ) )
	console.error( colors.red( error.stack ) )
	error instanceof EvalError ? this.res.status( 501 ) : this.res.status( 400 )
	this.res.send( error.param + ' ' + error.message )
}

module.exports.socketErrorHandler = function( error ) {
	console.error( colors.red( '\n\n' + moment().format() ) )
	console.error( colors.red( 'Error during Stripe Charge socket io:' ) )
	console.error( colors.red( 'Last Call: %s' ), lastCall )
	console.error( 'Params:', this.params )
	console.error( colors.red( error.message ) )
	console.error( colors.red( error.stack ) )
	this.socket.emit( 'stripeError', { message: error.message, stack: error.stack } )
}

