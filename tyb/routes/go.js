const express = require( 'express' );
const moment = require( 'moment' );
const _ = require( 'lodash' );
const querystring = require( 'querystring' );
const authenticate = require( './authenticate' );
const config = require( '../config' );
const router = express.Router();
const co = require( 'co' );
const psql = require( './psql' );
const colors = require( 'colors/safe' );

let DEBUG = true;

function errorHandler( error ) {
	console.error( colors.red( '\nError during %s' ), this.req.originalUrl );
	console.error( 'Params:', this.req.body );
	console.error( colors.red( error.message ) );
	console.error( colors.red( error.stack ) );
	this.res.status( 401 );
	this.res.send( error.message );
}

function getLink( hash ) {
	return psql.any( 'select * from treatya_private.go where hash=$1', [ hash ] );
}

function delay() {
	return new Promise( function( resolve, reject ) {
		setTimeout( function() {
			resolve();
		}, 1000 )
	} );
}

router.get( '/:hash', function( req, res ) {
	let hash = req.params.hash;

	co( function *() {
		let link = yield getLink( hash );
		yield delay(); // Make life harder for hackers.

		if( !link || !link.url ) {
			res.redirect( 'https://treatya.com/' );
		} else {
			res.redirect( link.url )
		}

	} )
			.catch( errorHandler.bind( { req, res } ) );
} );

module.exports = router;
