const express = require( 'express' )
const http = require( 'axios' )
const psql = require( './psql' )
const crypto = require( 'crypto' )
const moment = require( 'moment' )
const colors = require( 'colors/safe' )
const co = require( 'co' );
const nodemailer = require( 'nodemailer' )
const JWT = require( 'jsonwebtoken' )
const config = require( '../config' )
const authenticate = require( './authenticate' )
const router = express.Router()

const {
	getSettings, getAccountByEmail, errorHandler, logWithTimestamp
} = require( './util-functions' )

const DEBUG = true

let transporter = nodemailer.createTransport( `smtps://${config.smtp.username}:${config.smtp.password}@${config.smtp.host}` )

// This endpoint receives an object originating from a Facebook login.
// The accessToken in the object is used to fetch accout profile data from Facebook, and
// if this is successful the user is registered in the Postgres database and the client
// is sent an authorization token looking like it was coming from Postgraphql.

function fetchProfile( fbResponse ) {
	return http.get( 'https://graph.facebook.com/me', {
		params: {
			access_token: fbResponse.accessToken,
			fields: 'email,first_name,last_name'
		}
	} )
}

function registerAccount( profile ) {
	let args = [ profile.data.first_name, profile.data.last_name, profile.data.email, profile.data.id ]
	return psql.func( 'treatya.register_account', args )
}

function setEmailVerified( id ) {
	return psql.any( 'update treatya_private.account set email_verified=true where account_id=$1', id )
}

function authenticateWithBackend( profile ) {
	let args = [ profile.data.email, profile.data.id ]
	return psql.func( 'treatya.authenticate', args )
}

router.post( '/facebook', function( req, res ) {
	let fbResponse = req.body

	if( fbResponse.accessToken ) {
		co( function* () {
					let profile = yield fetchProfile( fbResponse )
					let account
					try {
						account = yield getAccountByEmail( profile.data.email )
					}
					catch( e ) {
						// We use the Facebook user id as password.
						DEBUG && logWithTimestamp( '    ...Registering new account.' )
						account = yield registerAccount( profile )
						account = account[ 0 ]
						yield setEmailVerified( account.id )
					}
					DEBUG && logWithTimestamp( 'account:', JSON.stringify( account, null, 2 ) )

					let authData = yield authenticateWithBackend( profile )
					if( authData.length ) authData = authData[ 0 ]
					DEBUG && logWithTimestamp( 'authData:', JSON.stringify( authData, null, 2 ) )

					if( !authData ) throw new Error( 'Authenticate produced no results.' )
					if( !authData.id ) throw new Error( 'SAME-EMAIL' ) // Special case in frontend for better UX.
					if( !authData.role ) throw new Error( 'Authenticate produced no Role.' )

					let token = authData
					token.aud = 'postgraphql'
					token.iss = 'postgraphql'
					DEBUG && logWithTimestamp( 'token:', JSON.stringify( token, null, 2 ) )

					const jwtSecret = Buffer.from( config.postgraphql.secret, 'hex' ).toString()
					const authToken = JWT.sign( token, jwtSecret )

					res.status( 200 )
					res.send( { authToken } )
				}
		).catch( error => {
			DEBUG && logWithTimestamp( '    Error:', error.message )
			if( error.message === 'SAME-EMAIL' ) {
				res.status( 401 )
				res.send( {
					error: 'You already have a Treatya account with the same email as your Facebook account.',
					sameEmail: true
				} )
			} else {
				errorHandler.bind( { req, res } )( error )
			}
		} )
	}
} )

router.post( '/send-reset-pwd', function( req, res ) {
	let params = req.body
	if( !params.email ) {
		res.status( 401 )
		res.send( 'Bad parameters' )
		return false
	}

	co( function* () {
				let account = yield getAccountByEmail( params.email )
				if( !account.length ) {
					logWithTimestamp( colors.grey( 'Passwd reset request failed for: ' + params.email ) )
					res.send( 'not found' )
					res.end()
					return
				}
				account = account[ 0 ]

				token = crypto.createHmac( 'sha256', config.postgraphql.secret )
						.update( params.email + moment().format( 'YYYYMMDDHH' ) )
						.digest( 'hex' ).slice( -32 )

				const update = yield psql.result( 'update treatya_private.account set password_token=$1 where account_id=$2', [ token, account.account_id ] )
				if( update.rowCount !== 1 ) {
					throw new Error( 'Failed to update account ' + account.account_id + ' with reset token.' )
				}

				let sendVerifyMail = yield transporter.sendMail( {
					subject: res.__( 'treatya.com - Your password reset information' ),

					text: res.__( 'Greetings from Treatya!' ) + '\n\n' +
					res.__( 'You have requested a code to reset your password.' ) + '\n' +
					res.__( 'Click here to open a new app window:' ) + ' https://treatya.com/reset-passwd/' + token + '\n' +
					res.__( 'Copy and paste this code:' ) + '\n\n' + token + '\n',

					from: 'Treatya <mailer@treatya.com>',
					to: params.email
				} )
				res.send( sendVerifyMail )
				res.end()
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/refresh', function( req, res ) {
	let params = authenticate( res, req.body, [ 'accountId' ] )
	if( !params ) return

	co( function* () {
		let settings = yield getSettings()
		settings = settings.value

		let client = yield getAccount( params.accountId )
		DEBUG && logWithTimestamp( colors.yellow( '      client (%s)' ), client.language )

		res.send( 'ok' )
	} )
			.catch( errorHandler.bind( { req, res } ) )
} )

module.exports = router
