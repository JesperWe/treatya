const express = require( 'express' )
const nodemailer = require( 'nodemailer' )
const moment = require( 'moment' )
const _ = require( 'lodash' )
const http = require( 'axios' )
const authenticate = require( './authenticate' )
const config = require( '../config' )
const stripe = require( 'stripe' )( config.stripe.isLive ? config.stripe.sk_live : config.stripe.sk_test )
const router = express.Router()
const eventNotifier = require( '../eventNotifier' )
const co = require( 'co' )
const colors = require( 'colors/safe' )

const {
	getSettings, getAccountByBookingId, getBooking, getSupplierWithAccountByBookingId, insertNotification, errorHandler, lastCall,
	setBookingStatus, setBookingPayment, pushAccountEvent, removeBookingReminders, createSupplierPayout, addBookingEvent,
	savePayout, setBookingRefund, getAccount, setAccountUISettings, refundBooking, getOtherBookings, getSupplier, insertMailinglist,
	logWithTimestamp, getSupplierAccount
} = require( './util-functions' )

let DEBUG = true

let transporter = nodemailer.createTransport( `smtps://${config.smtp.username}:${config.smtp.password}@${config.smtp.host}` )

router.post( '/admin/supplier', function( req, res ) {
	let params = authenticate( res, req.body, [] )
	if( !params ) return

	lastCall( config.treatya.telegram.api + 'sendMessage' )
	http.post( config.treatya.telegram.api + 'sendMessage', {
		chat_id: config.treatya.telegram.chat_id,
		text: 'A new Supplier Application has arrived!'
	} ).catch( errorHandler )

	const sendVerifyMail = transporter.sendMail( {
				subject: res.__( 'New supplier application arrived.' ),
				text: res.__( 'User ID: ' ) + params.id,
				from: config.smtp.sender,
				to: config.admin.email
			} )
			.then( result => {
				res.send( result.data )
				res.end()
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/admin/register', function( req, res ) {
	let params = authenticate( res, req.body, [ 'email' ] )
	if( !params ) return

	lastCall( config.treatya.telegram.api + 'sendMessage' )
	http.post( config.treatya.telegram.api + 'sendMessage', {
				chat_id: config.treatya.telegram.chat_id,
				text: 'A new account was registered!\n' + params.email
			} )
			.then( () => {
				res.status( 204 )
				res.end()
			} )
			.catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/booking/create', function( req, res ) {
	let params = authenticate( res, req.body, [ 'bookingId' ] )
	if( !params ) return

	DEBUG && logWithTimestamp( colors.yellow( '/notify/booking/create:' ) )

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value

				let client = yield getAccountByBookingId( params.bookingId )
				DEBUG && logWithTimestamp( colors.yellow( '      Client: %s %s (%s)' ), client.first_name, client.last_name, client.language )

				let booking = yield getBooking( params.bookingId )
				DEBUG && logWithTimestamp( colors.yellow( '      booking period' ), booking.period )

				booking.clientFullName = client.first_name + ' ' + client.last_name

				let supplier = yield getSupplierWithAccountByBookingId( params.bookingId )
				DEBUG && logWithTimestamp( colors.yellow( '      supplier %s %s (%s)' ), supplier.id, supplier.title, supplier.language )
				yield pushAccountEvent( supplier.account_id )

				yield insertNotification( 'bookingCreated', { target: 'supplier', client, supplier, booking, settings } )
				yield insertNotification( 'startReminder', { target: 'supplier', client, supplier, booking, settings } )
				yield insertNotification( 'startReminder', { target: 'client', client, supplier, booking, settings } )
				yield insertNotification( 'afterBookingSupplier', { target: 'supplier', client, supplier, booking, settings } )
				yield insertNotification( 'afterBookingClient', { target: 'client', client, supplier, booking, settings } )
				yield insertNotification( 'bookingExpired', { target: 'client', client, supplier, booking, settings } )

				res.send( 'ok' )
				res.end()
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/mailinglist', function( req, res ) {
	const { email, firstName, lastName } = req.body
	DEBUG && logWithTimestamp( colors.yellow( '/mailinglist ' ) + email )

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value
				yield insertMailinglist( email, firstName, lastName )
				res.send( 'ok' )
				res.end()
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/booking/status-change', function( req, res ) {
	let params = authenticate( res, req.body, [ 'bookingId', 'status' ] )
	if( !params ) return
	const { bookingId, status } = params

	if( [ 'CANCELLED', 'DENIED', 'CONFIRMED', 'NOSHOW', 'DELIVERED', 'NOTDELIVERED' ].indexOf( status ) === -1 ) {
		console.error( colors.red( 'There is no handler for status ' + status ) )
		res.status( 401 )
		res.send( 'There is no handler for status ' + status )
		return
	}

	DEBUG && logWithTimestamp( colors.yellow( '/notify/booking/status-change:' ) )

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value

				const client = yield getAccountByBookingId( bookingId )
				DEBUG && logWithTimestamp( colors.yellow( '      Client: %s %s (%s)' ), client.first_name, client.last_name, client.language )

				const user = yield getAccount( params.id )

				yield setBookingStatus( bookingId, status )

				let booking = yield getBooking( bookingId )
				DEBUG && logWithTimestamp( colors.yellow( '      booking (%s)' ), booking.id )

				let supplier = yield getSupplierWithAccountByBookingId( bookingId )
				DEBUG && logWithTimestamp( colors.yellow( '      supplier %s/%s (%s)' ), supplier.id, supplier.account_id, supplier.language )

				booking.start = moment( booking.period.split( '"' )[ 1 ] )

				let paymentToRefund = false

				yield addBookingEvent( booking.id, {
					timestamp: moment().format( config.date.default ),
					byAccountId: params.id,
					byAccountEmail: user.email,
					status
				} )

				switch( status ) {
					case 'CONFIRMED': {
						if( _.get( booking, 'payment.captured' ) === false ) {
							let captured = yield stripe.charges.capture( booking.payment.id )
							yield setBookingPayment( booking.id, captured )
							DEBUG && logWithTimestamp( colors.green( '      Payment Captured Ok.' ) )
							booking.chargeId = captured.id
						}
						yield insertNotification( 'bookingConfirmed', {
							target: 'client',
							client,
							supplier,
							booking,
							settings
						} )
						eventNotifier.notify( supplier.account_id )

						if( config.treatya.cancelOthersOnBookingConfirmation ) {
							const otherBookings = yield getOtherBookings( booking.client_id, booking.type )
							logWithTimestamp( colors.green( '      Will cancel %s other bookings:' ), otherBookings.length )
							for( let i = 0; i < otherBookings.length; i++ ) {
								const otherBooking = otherBookings[ i ]

								const otherSupplier = yield getSupplierWithAccountByBookingId( otherBooking.id )
								logWithTimestamp( colors.green( '          %s: %s with %s (%s)' ), otherBooking.id, otherBooking.prod_title, otherSupplier.title, otherSupplier.email )

								yield addBookingEvent( otherBooking.id, {
									timestamp: moment().format( config.date.default ),
									byAccountId: params.id,
									byAccountEmail: user.email,
									status: 'CANCELLED',
									previousStatus: otherBooking.status,
									reason: 'Linked booking was confirmed'
								} )
								yield setBookingStatus( otherBooking.id, 'CANCELLED' )

								const refunded = yield refundBooking( otherBooking )

								yield setBookingRefund( otherBooking.id, refunded )

								yield insertNotification( 'bookingCancelled', {
									target: 'supplier',
									client,
									supplier: otherSupplier,
									booking: otherBooking,
									settings
								} )
							}
						}

						yield pushAccountEvent( client.id )
						break
					}

					case 'CANCELLED': {
						paymentToRefund = booking.payment || {}
						yield removeBookingReminders( bookingId )
						yield insertNotification( 'bookingCancelled', {
							target: 'supplier',
							client,
							supplier,
							booking,
							settings
						} )
						const updated = yield pushAccountEvent( supplier.account_id )
						eventNotifier.notify( client.id )
						break
					}

					case 'DENIED': {
						paymentToRefund = booking.payment || {}
						yield removeBookingReminders( bookingId )
						let res = yield insertNotification( 'bookingDenied', {
							target: 'client',
							client,
							supplier,
							booking,
							settings
						} )
						const updated = yield pushAccountEvent( client.id )
						eventNotifier.notify( supplier.account_id )
						break
					}

					case 'NOTDELIVERED': {
						yield removeBookingReminders( bookingId )
						paymentToRefund = booking.payment || {}
						yield pushAccountEvent( client.id )
						yield pushAccountEvent( supplier.account_id )
						break
					}

					case 'NOSHOW':
					case 'DELIVERED': {
						yield removeBookingReminders( booking.id )
						if( _.get( booking, 'payout.amount', 0 ) > 0 ) break // Already paid out.
						if( _.get( booking, 'payment.captured' ) === false ) {
							let captured = yield stripe.charges.capture( booking.payment.id )
							yield setBookingPayment( booking.id, captured )
							DEBUG && logWithTimestamp( colors.green( '      Payment Captured Ok.' ) )
						}

						if( _.get( supplier, 'stripe_needed.length' ) > 0 ) { //Only payout if Stripe Account is OK.

							logWithTimestamp( colors.yellow( ' ! Payment not done due to missing Stripe info: ' ) + _.get( supplier, 'stripe_needed.length' ) )
							yield addBookingEvent( booking.id, {
								timestamp: moment().format( config.date.default ),
								event: 'Payout blocked due to incomplete payment info',
								reason: supplier.stripe_needed
							} )

						} else {

							let payout = yield createSupplierPayout( supplier, booking, settings )
							if( payout === false ) {
								logWithTimestamp( colors.blue( 'Booking id %s, no supplier payout possible.' ), booking.id )
								break
							}
							if( !payout || !payout.id ) {
								yield addBookingEvent( booking.id, {
									timestamp: moment().format( config.date.default ),
									event: 'Failed to create payout in /notify/create:createSupplierPayout()',
									reason: payout
								} )
								throw new Error( 'Failed to create payout in /notify/create:createSupplierPayout(). ' )
							}

							DEBUG && logWithTimestamp( colors.cyan( 'Supplier Payout id %s amount %s' ), payout.id, payout.amount )
							yield addBookingEvent( booking.id, {
								timestamp: moment().format( config.date.default ),
								event: 'Payout done',
								id: payout.id
							} )
							yield savePayout( booking.id, payout )
						}

						yield pushAccountEvent( client.id )
						yield pushAccountEvent( supplier.account_id )
						break
					}
				}

				if( paymentToRefund && paymentToRefund.paid ) {
					let refunded = yield refundBooking( booking )
					yield setBookingRefund( bookingId, refunded )
					DEBUG && logWithTimestamp( colors.cyan( 'Refunded: %s' ), refunded.id )
					res.send( refunded )
					return
				}

				res.send( 'ok	' )
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/inquiry', function( req, res ) {
	const { inquiry, accountId, language } = req.body
	if( !accountId || !accountId.length ) return

	DEBUG && logWithTimestamp( colors.yellow( '/notify/inquiry:' ) )

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value

				const supplier = yield getSupplierAccount( accountId )
				const texts = _.get( settings, 'notification.supplierInquiry.text[' + language + ']' )
				if( !texts || !texts.subject ) throw new Error( "Can't fin settings.notification.supplierInquiry.text" )

				const message = {
					subject: texts.subject,
					text: texts.body + (inquiry || ''),
					from: config.smtp.sender,
					to: supplier.email
				}

				const result = yield transporter.sendMail( message )

				res.send( 'ok	' )
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/supplier/confirmed', function( req, res ) {
	let params = authenticate( res, req.body, [ 'accountId' ] )
	if( !params ) return

	co( function* () {
				let settings = yield getSettings()
				settings = settings.value

				let client = yield getAccount( params.accountId )
				DEBUG && logWithTimestamp( colors.yellow( '      client (language %s)' ), client.language )

				let supplier = yield getSupplier( params.accountId )

				yield  insertNotification( 'supplierApplicationApproved', { target: 'client', client, supplier, settings } )

				_.set( client, 'ui_settings.supplierRegistration.show', true )
				// Not sure if this should be displayed, maybe unnecessary...?
				_.set( client, 'ui_settings.supplierRegistration.steps.approved', { done: true } )
				yield setAccountUISettings( client.id, client.ui_settings )

				res.send( 'ok' )
			}
	).catch( errorHandler.bind( { req, res } ) )
} )

router.post( '/error', function( req, res ) {

	let params
	if( _.get( req, 'body.jwt' ) )
		params = authenticate( res, req.body, [ 'error' ] )
	else
		params = req.body

	co( function* () {
		const id = _.get( params, 'authInfo.id' )
		const now = moment().format( 'YYYY-MM-DD HH:mm' )

		let account
		if( id ) account = yield getAccount( id )

		if( account )
			logWithTimestamp( colors.red( '\nClient Error: %s (%s) at %s' ), account.email, id, now )
		else
			logWithTimestamp( colors.red( '\nAnonymous Client at', now ) )

		logWithTimestamp( colors.red( 'Agent: %s' ), req.body.agent )

		let message = _.get( req.body, 'error.message' )
		if( !message ) message = JSON.stringify( _.get( req.body, 'error.response.data' ) )
		if( message ) logWithTimestamp( colors.red( 'Error.message: %s' ), message )

		let url = _.get( req.body, 'error.config.url' )
		if( url ) logWithTimestamp( colors.grey( 'URL: %s' ), url )

		let stack = _.get( req.body, 'error.stack' )
		if( stack ) logWithTimestamp( colors.grey( 'Error.stack: %s' ), stack )

		res.status( 204 )
		res.end()
	} )
			.catch( errorHandler.bind( { req, res } ) )
} )

module.exports = router
