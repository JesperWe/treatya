const JWT = require( 'jsonwebtoken' )
const co = require( 'co' )
const _ = require( 'lodash' )
const i18n = require( 'i18n' )
const moment = require( 'moment' )
const colors = require( 'colors/safe' )
const config = require( './config' )

const { getSettings, getAccount } = require( './routes/util-functions' )

const DEBUG = false
let namespace

function socketErrorHandler( error ) {
	console.error( colors.red( '\nError in event notifier:' ) )
	console.error( 'Params:', this.params )
	console.error( colors.red( error.message ) )
	console.error( colors.red( error.stack ) )
	this.socket.emit( 'stripeError', { message: error.message, stack: error.stack } )
}

function getTokenId( params ) {
	let authInfo
	if( !params.jwt ) {
		throw new Error( 'Bad parameters: ' + JSON.stringify( params ) )
	}
	let jwtSecret = Buffer.from( config.postgraphql.secret, 'hex' ).toString()
	try {
		authInfo = JWT.verify( params.jwt, jwtSecret )
	}
	catch( error ) {
		console.log( 'Socket getTokenId(): ' + error.message )
		return 0
	}
	return authInfo.id
}

function authorize( socket, params ) {
	DEBUG && console.log( 'Connect attempt' )
	try {
		const id = getTokenId( params )
		socket.accountId = id
		DEBUG && console.log( '... connect successful for id', id )
	}
	catch( error ) {
		socket.emit( 'error', error.message )
	}
}

function event( socket, params ) {
	DEBUG && console.log( 'Send event attempt' )
	if( !socket.accountId ) {
		DEBUG && console.log( ' ! Not authorized' )
		return
	}
	const receiver = _.find( namespace.connected, socket => socket.accountId === params.receiver )
	if( !receiver ) {
		DEBUG && console.log( ' ! Receiver not found' )
		return
	}
	receiver.emit( 'event' )
	DEBUG && console.log( '... emit successful for id', params.receiver )
}

function disconnect( socket, params ) {
	DEBUG && console.log( 'Disconnect attempt', params )
	try {
		DEBUG && console.log( '...success: Now %s connected ids.', Object.keys( _.get( namespace, 'connected', [] ) ).length )
	}
	catch( error ) {
		socket.emit( 'error', error.message )
	}
}

module.exports.notify = ( id ) => {
	const clients = Object.keys( _.get( namespace, 'connected', [] ) )
	DEBUG && console.log( 'eventNotifier.notify(%s) (%s connected)', id, clients.length )
	const receiver = _.find( _.get( namespace, 'connected', {} ), socket => socket.accountId === id )
	if( !receiver ) {
		DEBUG && console.log( colors.red( ' ! Receiver socket not found.' ) )
		return
	}
	receiver.emit( 'event' )
}

module.exports.initialize = ( ns ) => {
	namespace = ns
	DEBUG && console.log( 'eventNotifier.initialize()', namespace.name )
	namespace.on( 'connect', ( socket ) => {
		DEBUG && console.log( 'Socket Connect. %s connections.', Object.keys( _.get( namespace, 'connected', [] ) ).length )
				socket.on( 'authorize', data => authorize( socket, data ) )
				socket.on( 'event', data => event( socket, data ) )
				socket.on( 'disconnect', data => {
					DEBUG && console.log( 'Socket Disconnect:', data )
					disconnect( socket, data )
				} )
			}
	)
}
