var config = {}

config.postgraphql = {
	database: 'dev',
	schema: 'treatya',
	secret: '___',
	account: 'treatya_postgraphql',
	passwd: '___'
}

config.facebook = {
	client_id: '258339574583090',
	client_secret: '___'
}

config.smtp = {
	sender: 'Treatya <mailer@treatya.com>',
	host: 'smtp.zoho.com',
	port: 587,
	protocol: 'TLS',
	username: 'mailer%40treatya.com',
	password: '___',
	key: '___'
}

config.date = {
	default: 'YYYY-MM-DD H:mm'
}

config.s3 = {
	params: {
		Bucket: 'treatya-user-public'
	},
	region: 'eu-central-1',
	accessKeyId: '___',
	secretAccessKey: '___'
}

config.google = {
	firebase: {
		apiKey: '___',
		authDomain: 'treatya-1479892927715.firebaseapp.com',
		databaseURL: 'https://treatya-1479892927715.firebaseio.com',
		storageBucket: 'treatya-1479892927715.appspot.com',
		messagingSenderId: '1053321335137'
	}
}

config.stripe = {
	isLive: false,
	three_d_secure: { required: true, optional: false },
	pk_test: '___',
	pk_live: '___',
	sk_test: '___',
	sk_live: '___'
}

config.sms = {
	url: "https://api.46elks.com/a1/SMS",
	username: '___',
	password: '___'
}

config.db = {
	connection: {
		admin: 'postgres://postgres:___@localhost:5432/postgres'
	},
	settings: 'live'
}

config.admin = {
	email: 'treatya@journeyman.se'
}

config.web = {
	backend: 'https://query.treatya.com',
	frontend: 'https://dev.treatya.com',
	graphql: '/q',
	graphiql: true,
	port: process.env.WEB_PORT || 9980
}

config.treatya = {
	cancelOthersOnBookingConfirmation: true,
	telegram: {
		api: 'https://api.telegram.org/bot487861407:___/',
		chat_id: ___
	}
}

module.exports = config;
