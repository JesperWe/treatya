#!/usr/bin/env node
const app = require( './app' )
const debug = require( 'debug' )( 'tyb:server' )
const moment = require( 'moment' )
const _ = require( 'lodash' )
const colors = require( 'colors/safe' )
const { stripeCharge } = require( './routes/stripe' )
const eventNotifier = require( './eventNotifier' )
const { logWithTimestamp } = require( './routes/util-functions' )

const port = +process.env.PORT || 4000

process.on( 'unhandledRejection', error => logWithTimestamp( error.stack || error ) )

const server = app.listen( port )
const io = require( 'socket.io' ).listen( server )

const charge = io
		.of( '/charge' )
		.on( 'connection', stripeCharge )

eventNotifier.initialize( io.of( '/event' ) )

const DEBUG = true;

// run notification queue with async wakeup.
const notificationJob = require( './notificationJob' )
let notificationWakeup

const _schedule = function() {
	DEBUG && logWithTimestamp( colors.grey( ' !  notification wakeup.' ) )
	notificationJob.getNextWakeupTime()
			.then( nextTime => {
				if( nextTime === false ) {
					DEBUG && logWithTimestamp( '... nothing scheduled.' )
				} else {
					let ms = nextTime.diff( moment() )
					if( ms < 0 ) ms = 0

					if( ms > 10000 ) {
						DEBUG && logWithTimestamp( colors.grey( '... next wakeup' ), nextTime.format( 'H:mm:ss' ), ms )
						if( notificationWakeup ) {
							clearTimeout( notificationWakeup )
						}
						notificationWakeup = setTimeout( processNotifications, ms )
					} else {
						//DEBUG && logWithTimestamp( colors.grey( '... ! Process immedeately' ) )
						processNotifications()
					}
				}
			} )
			.catch( error => {
				logWithTimestamp( 'Notification schedue() failed:', error )
			} )
}

const schedule = _.throttle( _schedule, 30000, { leading: true } )

const processNotifications = function() {
	logWithTimestamp( colors.grey( '... notificationJob processing start.' ) )
	try {
		notificationJob.processNotification()
				.then( result => {
					schedule()
				} )
				.catch( error => {
					logWithTimestamp( colors.red( 'Notification process() failed: %s' ), error )
					schedule()
				} )
	}
	catch( e ) {
		logWithTimestamp( colors.red( 'processNotification() failed: %s' ), error.stack || error )
	}
}

notificationJob.initialize( ( data ) => {
	schedule()
} )
