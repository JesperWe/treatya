const nodemailer = require( 'nodemailer' )
const http = require( 'axios' )
const querystring = require( 'querystring' )
const _ = require( 'lodash' )
const moment = require( 'moment' )
const co = require( 'co' )
const config = require( './config' )
const psql = require( './routes/psql' )
const colors = require( 'colors/safe' )
const {
	getSettings, getAllNotifications, setNotificationStatus, setNotificationResult, addBookingEvent,
	getBooking, setBookingStatus, setBookingRefund, refundBooking, removeBookingReminders,
	logWithTimestamp
} = require( './routes/util-functions' )

const DEBUG = true;

let transporter = nodemailer.createTransport( `smtps://${config.smtp.username}:${config.smtp.password}@${config.smtp.host}` )

module.exports = {
	initialize: ( callback ) => {
		psql.connect( { direct: true } )
				.then( connection => {
					connection.client.on( 'notification', data => {
						callback( data )
					} )
					callback( {} ) // invoke callback once when initializing.
					logWithTimestamp( colors.grey( 'notificationJob.initialize(): Listening on notification_channel' ) )
					return connection.none( 'LISTEN notification_channel' )
				} )
				.catch( error => {
					logWithTimestamp( 'PG Listen initialize error:', error )
				} )
	},

	processNotification: () => {
		return co(
				function* () {
					let settings = yield getSettings()
					settings = settings.value

					let notifications = yield getAllNotifications()

					if( !notifications.length ) {
						logWithTimestamp( colors.grey( '...process(): Nothing to do.' ) )
					} else {
						logWithTimestamp( colors.grey( '... ' + notifications.length + ' notifications to process.' ) )
						yield* notifications.map(
								co.wrap(
										function* ( notification ) {
											let emailResult = {}, phoneResult = {}, success = false
											try {
												let start = yield setNotificationStatus( notification.id, 'STARTED' )

												if( notification.email ) {
													logWithTimestamp( colors.grey( 'process(): Send %s to %s: %s' ), notification.id, notification.email, notification.sms )
													emailResult = yield transporter.sendMail( {
														from: config.smtp.sender,
														to: notification.email,
														subject: notification.subject,
														text: notification.body
													} )
												}

												if( notification.sms && notification.phone && notification.phone.length ) {
													phoneResult = 'disabled'
													if( settings.sms !== 'enable' ) {
														logWithTimestamp( colors.blue( 'Not sending SMS: %s' ), settings.sms )
													} else {
														const smsReq = {
															url: config.sms.url,
															method: 'post',
															data: querystring.stringify( {
																to: notification.phone,
																message: notification.sms,
																from: 'Treatya'
															} ),
															auth: {
																username: config.sms.username,
																password: config.sms.password
															}
														}
														phoneResult = yield http.request( smsReq )
													}
												}

												if( notification.post_processors.length ) {
													for( i = 0; i < notification.post_processors.length; i++ ) { // No forEach in generator...
														processor = notification.post_processors[ i ]

														switch( processor ) {
															case 'setBookingOverdue': {
																let booking = yield getBooking( notification.booking_id )
																yield addBookingEvent( booking.id, {
																	timestamp: moment().format( config.date.default ),
																	status: 'NOTDELIVERED',
																	previousStatus: booking.status,
																	reason: 'Overdue'
																} )
																yield setBookingStatus( booking.id, 'NOTDELIVERED' )
																yield removeBookingReminders( booking.id )
																if( !_.get( booking, 'refund.data.object.refunds' ) ) {
																	let refunded = yield refundBooking( booking )
																	yield setBookingRefund( booking.id, refunded )
																	DEBUG && logWithTimestamp( colors.cyan( 'Refunded: %s' ), refunded.id )
																}
															}
														}
													}
												}

												yield setNotificationStatus( notification.id, 'DONE' )
												yield setNotificationResult( notification.id, emailResult.response, JSON.stringify( phoneResult.data ) )
												success = true
											}
											catch( error ) {
												logWithTimestamp( colors.red( 'In processNotification(): %s' ), error.stack || error )
											}
											if( !success ) {
												yield setNotificationStatus( notification.id, 'ERROR' )
												yield setNotificationResult( notification.id, _.get( emailResult, 'response', 'ERROR' ),
														JSON.stringify( _.get( phoneResult, 'data', 'ERROR' ) ) )
											}
										}
								)
						)
						logWithTimestamp( colors.green( 'processNotification(): DONE' ) )
					}
				}
		).catch( error => {
			logWithTimestamp( colors.red( 'From processNotification(): %s' ), error.stack || error )
		} )
	},

	getNextWakeupTime() {
		return psql.any(
				`select * from treatya_private.notification 
					where status = 'QUEUED' 
					order by send_at limit 10`
				)
				.then( result => {
					if( !result.length ) return Promise.resolve( false )
					let sendAt = moment( result[ 0 ].send_at )
					if( sendAt.isBefore() ) sendAt = moment()
					return Promise.resolve( sendAt )
				} )
				.catch( error => {
					logWithTimestamp( colors.grey( 'From getNextWakeupTime():' ), error )
				} )
	}
}
