const express = require( 'express' )
const app = express()
const path = require( 'path' )
const cors = require( 'cors' )
const logger = require( 'morgan' )
const bodyParser = require( 'body-parser' )
const i18n = require( 'i18n' )
const postgraphql = require( 'postgraphql' ).postgraphql;
const { logWithTimestamp } = require( './routes/util-functions' )
const config = require( './config' )
const index = require( './routes/index' )
const auth = require( './routes/auth' )
const verify = require( './routes/verify' )
const notify = require( './routes/notify' )
const go = require( './routes/go' )
const s3 = require( './routes/s3' )
const stripe = require( './routes/stripe' )

// view engine setup
app.set( 'views', path.join( __dirname, 'views' ) )
app.set( 'view engine', 'hbs' )

i18n.configure( {
	locales: [ 'en', 'sv' ],
	directory: __dirname + '/locales',
	queryParameter: 'l'
} )
app.use( i18n.init )

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

app.use( logger( 'dev', {
	skip: function( req, res ) {
		return res.statusCode < 400
	}
} ) )

app.use( bodyParser.json() )
app.use( bodyParser.urlencoded( { extended: false } ) )
app.use( express.static( path.join( __dirname, 'public' ) ) )

app.use( cors( {
	origin: [ 'http://192.168.79.130:3000', 'https://treatya.com', 'https://dev.treatya.com', 'https://client.treatya.com', 'https://prod.treatya.com', 'http://localhost:3000', 'http://localhost:4000' ],
	credentials: true
} ) )

// GraphQL Endpoint
const { database, schema, secret, account, passwd } = config.postgraphql

if( !secret || !secret.length ) {
	throw new Error( 'Incomplete config file has no postgraphql secret.' )
}

app.use( postgraphql( `postgres://${account}:${passwd}@localhost:5432/${database}`, schema, {
	pgDefaultRole: 'treatya_anonymous',
	dynamicJson: true,
	jwtSecret: Buffer.from( secret, 'hex' ).toString(),
	jwtPgTypeIdentifier: 'treatya.jwt_token',
	enableCors: true,
	disableQueryLog: true,
	graphiql: config.web.graphiql,
	graphqlRoute: config.web.graphql,
	graphiqlRoute: '/qtest'
} ) )

app.use( '/', index )
app.use( '/auth', auth )
app.use( '/verify', verify )
app.use( '/notify', notify )
app.use( '/go', go )
app.use( '/s3', s3 )
app.use( '/stripe/connect', stripe.connect )

// catch 404 and forward to error handler
app.use( function( req, res, next ) {
	let err = new Error( 'Not Found' )
	err.status = 404;
	next( err )
} )

// error handler
app.use( function( err, req, res, next ) {
	logWithTimestamp( err.message )

	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get( 'env' ) === 'development' ? err : {};

	// render the error page
	res.status( err.status || 500 )
	res.render( 'error' )
} )

module.exports = app;
