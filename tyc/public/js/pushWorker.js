self.addEventListener( 'push', function( event ) {
	console.log( '[Service Worker] Push Received.' );
	console.log( `[Service Worker] Push had this data: "${event.data.text()}"` );

	const title = 'Testing Push Notifications';
	const options = {
		body: event.data.text(),
		// icon: 'images/icon.png',
		//badge: 'images/badge.png'
	};

	event.waitUntil( self.registration.showNotification( title, options ) );
} );

self.addEventListener( 'notificationclick', function( event ) {
	console.log( '[Service Worker] Notification click Received.' );
	event.notification.close();
	event.waitUntil(
			clients.postMessage(event)
	);
} );

// See https://github.com/web-push-libs/web-push for server side!
