const webpack = require( 'webpack' )
const { injectBabelPlugin } = require( 'react-app-rewired' )
const rewireLess = require( 'react-app-rewire-less' )
const GitRevisionPlugin = require( 'git-revision-webpack-plugin' )
const InterpolateHtmlPlugin = require( 'react-dev-utils/InterpolateHtmlPlugin' )
const Visualizer = require('webpack-visualizer-plugin');

module.exports = function override( config, env ) {

	config = injectBabelPlugin( [ 'import', { libraryName: 'antd', libraryDirectory: 'es', style: true } ], config )

	config = rewireLess.withLoaderOptions( {
		modifyVars: {
			"@icon-url": process.env.ICONS || "'https://treatya.com/treatyaicon'",
			"@primary-color": "#b00843",
			"@input-bg": "#fff",
			"@btn-default-bg": "#fff",
			"@font-size-base": "16px",
			"@text-color-secondary": "#45a098",
			"@border-radius-base": "3px",
			"@font-family": "Asap, Helvetica, Arial, sans-serif;",
			"@btn-height-lg": "44px",
			"@input-height-lg": "44px",
			"@btn-secondary-bg": "#56c8be",
			"@badge-height": "16px",
			"@rate-star-color": "#b00843",
			"@checkbox-size": "18px",
			"@radio-size": "18px",
			"@tooltip-bg": "#45a098",
			"@tooltip-arrow-width": "10px",
			"@table-header-bg": "#e9e9e9",
			"@table-header-sort-bg": "#d9d9d9",
			"@table-padding-vertical": "6px",
			"@table-padding-horizontal": "4px"
		},
	} )( config, env )

	const gitRevisionPlugin = new GitRevisionPlugin( { lightweightTags: true } )
	const version = JSON.stringify( gitRevisionPlugin.version() )

	config.plugins = (config.plugins || [])
			.concat( [
				new InterpolateHtmlPlugin( { VERSION: version } ),
				new webpack.DefinePlugin( { 'process.env.VERSION': version } ),
				new Visualizer()
			] )
	return config
}

