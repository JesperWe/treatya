## Scaffolding ##

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Due to the requirements from Ant.Design the project uses `react-app-rewired` to make some modifications: 

- Include only the used Ant.Design components in the build.
- Use the LESS preprocessor for CSS.
- Customize the Ant.Design theme using LESS modifyVars.
- Inject git version number in the html.

See https://ant.design/docs/react/use-with-create-react-app#Advanced-Guides

## config.js ##
 
This is not checked in to the repo since it is different for each vhost.

## .env ## 

One more file is needed in the root folder, named `.env`:
 ```bash
ICONS="'https://treatya.com/icon'"
```
This is an input file to the webpack build process, to override the icon
 location for the Ant Design icon file. It needs the correct canonical URL, 
 since it is fetched using XMLHttpRequest.
