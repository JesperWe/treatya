
suite( 'Accounts', function() {

	let page = Nightmare( { show: true, x: 5, y: 5, typeInterval: 10 } );
	this.timeout( 10000 );

	page
			.cookies.set( { url: 'http://localhost:3000', name: 'language', value: 'sv' } )
			.cookies.set( { url: 'http://localhost:3000', name: 'euCookieConsent', value: 'yes' } )
			.cookies.set( {
				url: 'http://localhost:3000',
				name: 'location',
				value: '{%22position%22:{%22lat%22:59.31682249044156%2C%22lng%22:18.17418814282223}%2C%22place%22:%22Nacka%2C%20Nacka%20Municipality%22%2C%22address%22:%22Trillans%20v%C3%A4g%2045%2C%20131%2049%20Nacka%2C%20Sweden%22}'
			} )
			.goto( 'http://localhost:3000' )
			.inject( 'css', 'test/no-animations.css' )

	test( 'We can log in.', function() {
		return page
				.click( '.login-button' )
				.wait( '#email' )
				.type( '#email', 'lasse@journeyman.se' )
				.type( '#password', 'daisi2daisi' )
				.click( '.ant-btn-primary' )
				.wait( '.landing-text' )
				.evaluate( () => document.querySelector( '.landing-text' ).textContent )
				.should.eventually.have.string( 'Skönhet' )
	} );


	test( 'No admin features for ordinary user', function() {
		return page
				.click( '.hamburger img' )
				.wait( 'i.anticon-message' )
				.visible( 'i.anticon-setting' )
				.should.eventually.equal(false)
	} );

	test( 'We can log out.', function() {
		return page
				.click( '.sidebar i.anticon-close' )
				.click( 'i.anticon-user' )
				.click( 'i.anticon-logout' )
				.wait( '.landing-text' )
				.evaluate( () => document.querySelector( '.login-button' ).textContent )
				.should.eventually.have.string( 'Login' )
	} );

	test( 'Admin login menu items', function() {
		return page
				.click( '.login-button' )
				.wait( '#email' )
				.type( '#email', 'treatya@journeyman.se' )
				.type( '#password', 'daisi' )
				.click( '.ant-btn-primary' )
				.wait( '.landing-text' )
				.click( '.hamburger img' )
				.wait( 'i.anticon-message' )
				.visible( 'i.anticon-setting' )
				.should.eventually.equal(true)
	} );

	test( 'Saving admin settings', function() {
		// Add a text setting, reload and check that it was saved.
		return page
				.click( 'i.anticon-setting' )
				.wait( '.jsoneditor-outer' )
				.click( '.jsoneditor-outer > div > table > tbody > tr:nth-last-child(2) > td:nth-child(2) > button' )
				.wait( 'button.jsoneditor-append.jsoneditor-default' )
				.click( 'button.jsoneditor-append.jsoneditor-default' )
				.type( '.jsoneditor-outer > div > table > tbody > tr:nth-last-child(2) div[contenteditable="true"].jsoneditor-field', 'test' )
				.type( '.jsoneditor-outer > div > table > tbody > tr:nth-last-child(2) div[contenteditable="true"].jsoneditor-value', 'testvalue' )
				.click( 'button.ant-btn-primary' )
				.refresh()
				.evaluate( () => document.querySelector( 'table.jsoneditor-tree' ).textContent )
				.should.eventually.have.string( 'testvalue' )
	} );
} );
