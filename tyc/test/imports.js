global.Nightmare = require( 'nightmare' );
global.chai = require( 'chai' );
global.chaiAsPromised = require( 'chai-as-promised' );
chai.use(chaiAsPromised);
global.expect = chai.expect;
chai.should();
