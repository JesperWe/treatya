import React from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import http from 'axios'
import errorHandler from '../errorHandler'
import Error404 from '../Error404'
import config from '../config'
import './Page.less'

const DEBUG = false

class Page extends React.Component {
	state = {
		loading: false,
		staticPagesLoaded: false
	}

	componentWillMount() {
		if( this.props.staticPages ) this.setState( { staticPagesLoaded: true } )
		else {
			DEBUG && console.log('Page.componentWillMount: Loading pages.')
			this.loadStaticPages()
		}
	}

	loadStaticPages = () => {
		if( this.state.loading ) return
		this.setState( { loading: true } )
		http.post( config.cms.getCollection + 'pages?token=' + config.cms.token )
				.then( result => {
					const staticPages = result.data
					if( staticPages.error ) throw Error( 'Static Pages load failed: ' + staticPages.error )
					this.props.onStaticPages( staticPages.entries )
					this.setState( { staticPagesLoaded: true, loading: false } )
				} )
				.catch( errorHandler )
	}

	render() {
		const { staticPages } = this.props
		if(!this.state.staticPagesLoaded) return null

		const route = this.props.pageRoute || this.props.params.route
		let page = staticPages.find( page => page.route === route )
		if( !page ) return <Error404/>

		const lang = this.props.language === 'en' ? '' : '_' + this.props.language
		const bodyKey = 'body' + lang

		return (
				<div className={'page'} dangerouslySetInnerHTML={{ __html: page[ bodyKey ] }}/>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				staticPages: state.staticPages,
				language: state.language
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onStaticPages: ( staticPages ) => dispatch( { type: 'STATICPAGES', staticPages } )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )( Page )
)

