import React from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import http from 'axios'
import _map from 'lodash/map'
import { Collapse } from 'antd'
import moment from 'moment'
import errorHandler from '../errorHandler'
import config from '../config'
import './FAQ.less'

class FAQ extends React.Component {
	state = { loaded: false }

	componentWillMount() {
		if( !this.props.faq ) {
			const filter = {}, limit = 0, sort = { category: 1 }

			http.post( config.cms.getCollection + 'faq?token=' + config.cms.token, { filter, limit, sort } )
					.then( result => {
						const allFaqs = result.data
						if( allFaqs.error ) throw Error( 'FAQ load failed: ' + allFaqs.error )

						// Put each category in a separate array to allo iteration over categories.
						let faqs = {}
						allFaqs.entries.forEach( faq => {
							if( !faqs[ faq.category ] ) faqs[ faq.category ] = []
							faqs[ faq.category ].push( faq )
						} )

						this.props.onFAQ( faqs )
						this.setState( { loaded: true } )
					} )
					.catch( errorHandler )
		} else {
			this.setState( { loaded: true } )
		}
	}

	render() {
		const { t } = this.props

		//const lang = this.props.language === 'en' ? '' : '_' + this.props.language
		const lang = '_sv'
		const questionKey = 'question' + lang
		const answerKey = 'answer' + lang

		return (
				<div className="faq">

					<h3>{t( 'Frequently Asked Questions' )}</h3>
					{this.state.loaded &&
					_map( this.props.faq, ( category, catKey ) => (
							<div className="category">
								<h2>{t('For')} {t( catKey )}</h2>
								<Collapse key={catKey}>
									{
										category.map( ( faq, index ) => {
													if( !faq[ questionKey ] || !faq[ questionKey ].length ) return null

													return (
															<Collapse.Panel header={faq[ questionKey ]}
																			className="question" key={index}>
																<div className="answer"
																	 dangerouslySetInnerHTML={{ __html: faq[ answerKey ] }}/>
																<h6>{t( 'Last updated' )}: {moment.unix( faq._modified ).format( config.date.friendly )}</h6>
															</Collapse.Panel>)
												}
										)
									}
								</Collapse>
							</div>
					) )
					}

				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				language: state.language,
				faq: state.faq
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onFAQ: ( faq ) => dispatch( { type: 'FAQ', faq } )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )( FAQ )
)
