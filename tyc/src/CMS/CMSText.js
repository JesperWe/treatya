import React from 'react'
import { connect } from 'react-redux'
import http from 'axios'
import PropTypes from 'prop-types'
import errorHandler from '../errorHandler'
import config from '../config'

class CMSText extends React.Component {
	state = { loaded: false }

	componentWillMount() {
		if( !this.props.cmsTexts ) {
			this.props.onCmsTexts( [] )
			http.post( config.cms.getCollection + 'texts?token=' + config.cms.token )
					.then( result => {
						const cmsTexts = result.data
						if( cmsTexts.error ) throw Error( 'CMSTexts load failed: ' + cmsTexts.error )
						this.props.onCmsTexts( cmsTexts.entries )
						this.setState( { loaded: true } )
					} )
					.catch( errorHandler )
		} else {
			this.setState( { loaded: true } )
		}
	}

	render() {
		const { identifier, className, cmsTexts, language } = this.props
		if( !this.state.loaded || !cmsTexts || !cmsTexts.length ) return <span>loading...</span>

		if( !identifier ) return <span>[ ! missing text identifier ]</span>

		const text = cmsTexts.find( t => t.identifier === identifier )
		if( !text ) return <span>[ ! text identifier: <b>{identifier}</b> not found ]</span>


		const lang = language === 'en' ? '' : '_' + language
		const value = text[ 'text' + lang ]

		if( !value || !value.length ) return <span>[ ! empty text identifier: <b>{identifier}</b> ]</span>

		return (<span className={className} title={identifier}
					 dangerouslySetInnerHTML={{ __html: value }}/>)
	}
}

CMSText.propTypes = {
	identifier: PropTypes.string.isRequired,
	className: PropTypes.string
}


export default connect(
		function mapStateToProps( state ) {
			return {
				language: state.language,
				cmsTexts: state.cmsTexts
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onCmsTexts: ( cmsTexts ) => dispatch( { type: 'CMSTEXTS', cmsTexts } )
			}
		}
)(
		CMSText
)
