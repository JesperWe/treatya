import gql from 'graphql-tag'
import _get from 'lodash/get'
import moment from 'moment'

const getSupplier = {
	gql: gql`
		query accountSupplier($accountId: Uuid!) {
		  allSuppliers(condition: {accountId: $accountId}) {
			nodes {
			  id
			  title intro description
			  imageBg imageProfile imageProfileRot
			  address1 address2 zip city country
			  rating status
		}}}`,
	options: {
		skip: ( ownProps ) => {
			let run = ownProps.authInfo && ownProps.authInfo.id
			return !run
		},
		options: ( { authInfo } ) => ( {
			variables: { accountId: authInfo && authInfo.id },
			fetchPolicy: 'network-only'
		} ),
		props: ( { ownProps, data: { loading, allSuppliers, refetch } } ) => {
			return ({
				supplierLoading: loading,
				supplier: allSuppliers && allSuppliers.nodes[ 0 ],
				refetch: refetch
			})
		}
	}

}

const updateSupplier = {
	gql: gql`
		mutation ( $id: Int!, $supplier: SupplierPatch! ) {
		  updateSupplierById( input: { id: $id, supplierPatch: $supplier } ) {
			supplier {
				id title intro description type
				imageProfile imageProfileRot
				address1 address2 zip city country
			}
		}}`,
	options: {
		props: ( { mutate, ownProps } ) => ({
			updateSupplier: ( supplierPatch ) => {
				return mutate( {
					variables: { id: ownProps.supplier.id, supplier: supplierPatch },
					refetchQueries: [ 'supplier' ]
				} )
			}
		})
	}
}

const setSupplierStatus = {
	gql: gql`
		mutation ($id: Int!, $status: SupplierStatuses!) {
			updateSupplierStatus(input: {targetId: $id, newStatus: $status}) {
				clientMutationId
			}
		}`,
	options: {
		props: ( { mutate, ownProps } ) => ({
			setSupplierStatus: ( status ) => {
				return mutate( { variables: { id: ownProps.supplier.id, status } } )
			}
		})
	}
}

const addFavourite = {
	gql: gql`
		mutation ($supplierId: Int!, $accountId:Uuid!) {
			createAccountFavoriteSupplier(input: {accountFavoriteSupplier: {supplierId: $supplierId accountId: $accountId}}) 
			{clientMutationId}}`
}

const deleteFavourite = {
	gql: gql`
		mutation ($supplierId: Int!, $accountId: Uuid!) {
			deleteAccountFavoriteSupplierByAccountIdAndSupplierId(input: {accountId: $accountId, supplierId: $supplierId}) 
			{clientMutationId}
		}`
}

const findSuppliers = {
	gql: gql`
		query findSuppliers( $wantedType: ServiceType!, $nearLatitude: String! , $nearLongitude: String!, $atTime: Datetime! ) {
			findSuppliers(
				wantedType:$wantedType, 
				nearLatitude: $nearLatitude, nearLongitude: $nearLongitude, 
				atTime: $atTime, first:12) { 
				nodes { 
				supplierId accountId
				timeslotId productId
				title intro description geoPosition
				imageProfile imageProfileRot imageBg rating
				type prodTitle prodDescr
				price currency durationSek
				slot { start { value } end { value } }
				timeDist timeDir
		}}}`,
	options: {
		skip: ( { desiredServiceType, desiredBookingMoment, location } ) => {
			if( !desiredServiceType ) return true
			if( !desiredBookingMoment ) return true
			if( !_get( location, 'position.lat' ) ) return true
		},
		options: ( { desiredServiceType, desiredBookingMoment, location } ) => ( {
			variables: {
				wantedType: desiredServiceType,
				nearLatitude: (_get( location, 'position.lat' ) || '').toString(),
				nearLongitude: (_get( location, 'position.lng' ) || '').toString(),
				atTime: desiredBookingMoment.format()
			}
		} ),
		props: ( { ownprops, data: { loading, findSuppliers, refetch, error } } ) => {
			return ({
				loading: loading,
				suppliersError: error,
				suppliers: findSuppliers && findSuppliers.nodes,
				refetch: refetch
			})
		}
	}
}

const getSupplierAndProducts = {
	gql: gql`
		query supplier($id: Int!) {
			supplierById(id: $id) {
			id accountId
			title intro description
			imageBg imageProfile imageProfileRot
			rating status
			productsBySupplierId(condition: {published: true, deleted: false})
			{ nodes { 
				id type prodTitle prodDescr 
				price currency duration { hours minutes }
			} }
			reviewsBySupplierId(condition: {target: SUPPLIER}) {
			nodes {comment rating createdAt}
		}}}`,
	options: {
		options: ( { params } ) => ( {
			variables: { id: params.supplierId },
			fetchPolicy: 'network-only'
		} ),
		props: ( { ownProps, data: { loading, supplierById, error } } ) => {
			return ({
				supplierLoading: loading,
				supplier: supplierById,
				products: supplierById && supplierById.productsBySupplierId.nodes,
				reviews: supplierById && supplierById.reviewsBySupplierId.nodes,
				supplierError: error
			})
		}
	}
}

const createSupplier = {
	gql: gql`
		mutation (
			$accountId: Uuid!, $title: String!,
			$address1: String!, $address2: String, $zip: String!, $city: String!, $country: String!
		) {
			createSupplier( input: {
			supplier: {accountId: $accountId, title: $title,
			address1:$address1, address2:$address2, zip:$zip, city:$city, country:$country}}) {
				supplier { id }
		}}`,
	options: {
		props: ( { mutate } ) => ({
			createSupplier: ( supplier ) => mutate( {
				variables: { ...supplier },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
}

const updateSupplierPrivate = {
	gql: gql`
		mutation ($id: Int!, $ip: String!, $agent: String!, $entityName:String!,
			$entityType:LegalType!, $entityId: String!) {
			updateSupplierPrivate( input: {supplierId: $id, ip: $ip, agent: $agent, entityName: $entityName, entityId:$entityId, entityType:$entityType}) {
			clientMutationId
		}}`,
	options: {
		props: ( { mutate } ) => ({
			updateSupplierPrivate: ( { id, agent, ip, entityName, entityType, entityId } ) => mutate( {
				variables: { id, agent, ip, entityName, entityType, entityId }
			} )
		})
	}
}

const supplierFreeslots = {
	gql: gql`
	query supplierFreeslots($id: Int!) { supplierFreeslots(wantedSupplierId: $id) {
		nodes {
			slot { start {value} end {value} }
		}
	}}`,
	options: {
		skip: ( { supplier } ) => {
			if( !supplier || !supplier.id ) return true
		},
		options: ( { supplier } ) => ( {
			variables: { id: supplier && supplier.id },
			fetchPolicy: 'network-only'
		} ),
		props: ( { ownprops, data: { loading, supplierFreeslots, error } } ) => {
			return ({
				supplierFreeslots: {
					loading, error,
					data: supplierFreeslots && supplierFreeslots.nodes && supplierFreeslots.nodes.map( free => {
						// Parse string data into Epocs for easy >< comparisions.
						return {
							date: moment( free.slot.start.value ).startOf( 'day' ).valueOf(),
							start: moment( free.slot.start.value ).valueOf(),
							end: moment( free.slot.end.value ).valueOf()
						}
					} ),
				}
			})
		}
	}
}

export {
	getSupplier,
	updateSupplier,
	setSupplierStatus,
	addFavourite,
	deleteFavourite,
	findSuppliers,
	getSupplierAndProducts,
	createSupplier,
	updateSupplierPrivate,
	supplierFreeslots
}
