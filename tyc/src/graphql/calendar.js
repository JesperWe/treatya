import gql from 'graphql-tag'

const DEBUG = false;

function isLoggedOut( ownProps ) {
	let run = ownProps.authInfo && ownProps.authInfo.id;
	DEBUG && console.log( 'Skipping:', !run, run );
	return !run;
}

const findBookings = {
	gql: gql`
		query findBookings($accountId: Uuid!) {
			findBookings( forId:$accountId) {
				nodes {
					id, accountId, clientId, productId, supplierId,
					supplierTitle, firstName, lastName, status,
					prodTitle, price, currency, address, geoPosition,
					period { start { value } end { value } }, 
					minInterval { minutes }
				}
		}}`,
	options: {
		skip: isLoggedOut,
		options: ( { authInfo } ) => ( {
			variables: { accountId: authInfo && authInfo.id },
		} ),
		props: ( { ownProps, data: { loading, findBookings, refetch, error } } ) => {
			return ({
				bookingsLoading: loading,
				bookingsError: error,
				bookings: findBookings && findBookings.nodes,
				refetch: refetch
			});
		}
	}
};

const myTimeslots = {
	gql: gql`
		query myTimeSlots($id:Int!) { supplierById(id: $id) {
			timeslotsBySupplierId {
				nodes {
				id supplierId
				slot { start { value } end { value } }
			}
		}}}`,
	options: {
		skip: ( { account } ) => !account.supplierId,
		options: ( { account } ) => {
			return {
				variables: { id: account.supplierId },
			};
		},
		props: ( { ownProps, data: { loading, supplierById, refetch } } ) => {
			return ({
				slotsLoading: loading,
				slots: supplierById && supplierById.timeslotsBySupplierId && supplierById.timeslotsBySupplierId.nodes,
				refetch
			});
		}
	}
};

const createTimeslot = {
	gql: gql`
		mutation ($supplierId:Int!, $start:Datetime!, $end:Datetime!) {createTimeslot( input: {
			timeslot: {
				supplierId: $supplierId
				slot: {
					start: { value: $start, inclusive: true }
					end:{ value: $end, inclusive: true }
				}
			}
		}){timeslot {
			id supplierId
			slot{start{value} end{value}}
		}}}`,
	options: {
		props: ( { mutate } ) => ({
			createTimeslot: ( slot ) => mutate( {
				variables: { ...slot },
				refetchQueries: [ 'myTimeSlots' ]
			} )
		})
	}
};


const updateTimeslot = {
	gql: gql`
		mutation updateTimeslot($id: Int!, $patch: TimeslotPatch!) {
			updateTimeslotById(input: {id: $id, timeslotPatch: $patch}) {
				timeslot{id}
		}}`,
	options: {
		props: ( { mutate } ) => ({
			updateTimeslot: ( id, patch ) => mutate( {
				variables: { id, patch },
				refetchQueries: [ 'myTimeSlots' ]
			} )
		})
	}
};


const deleteTimeslot = {
	gql: gql`
		mutation ($timeslotId: Int!) {deleteTimeslotById( input: { id: $timeslotId } )  
			{timeslot {id}}
		}`,
	options: {
		props: ( { mutate } ) => ({
			deleteTimeslot: ( timeslotId ) => mutate( {
				variables: { timeslotId },
				refetchQueries: [ 'myTimeSlots' ]
			} )
		})
	}
};

export { createTimeslot, updateTimeslot, deleteTimeslot, findBookings, myTimeslots };
