import gql from 'graphql-tag'
import _get from 'lodash/get'

const typeImages = {
	gql: gql`query typeImages ($name:String!) {typeImages(getName:$name)}`,
	options: {
		props: ( { ownProps, data: { loading, error, refetch, typeImages } } ) => {
			return ({
				typeImages: {
					loading, error, refetch,
					data: typeImages
				}
			});
		}
	}
}

const serviceTypes = {
	gql: gql`query serviceTypes {__type(name:"ServiceType"){enumValues{name}}}`,
	options: {
		props: ( { ownProps, data: { loading, __type } } ) => {
			return ({
				serviceTypes: {
					loading,
					types: __type && __type.enumValues.map( type => type.name )
				}
			});
		}
	}
}

const freeTextSearch = {
	gql: gql`query ($term: String!) {
		freeTextSearch(term: $term) {
			nodes { supplierId productId title }
		}}`,
	options: {
		props: ( { ownProps, data: { loading, refetch, error, freeTextSearch } } ) => {
			return ({
				freeText: {
					loading, refetch, error,
					results: freeTextSearch && freeTextSearch.nodes
				}
			});
		}
	}
}

const nearbyUnavailableSuppliers = {
	gql: gql`
		query nearbyUnavailableSuppliers( $wantedType: ServiceType!, $nearLatitude: String! , $nearLongitude: String! ) {
			nearbyUnavailableSuppliers(
				wantedType:$wantedType, 
				nearLatitude: $nearLatitude, nearLongitude: $nearLongitude, 
				) { 
				nodes { 
				supplierId accountId productId
				title intro description geoPosition
				imageProfile imageProfileRot imageBg rating
				type prodTitle prodDescr
				price currency
		}}}`,
	options: {
		skip: ( { desiredServiceType, desiredBookingMoment, location } ) => {
			if( !desiredServiceType ) return true
			if( !desiredBookingMoment ) return true
			if( !_get( location, 'position.lat' ) ) return true
		},
		options: ( { desiredServiceType, location } ) => ({
			variables: {
				wantedType: desiredServiceType,
				nearLatitude: (_get( location, 'position.lat' ) || '').toString(),
				nearLongitude: (_get( location, 'position.lng' ) || '').toString()
			}
		}),
		props: ( { ownprops, data: { loading, nearbyUnavailableSuppliers, error } } ) => {
			return ({
				nearbyUnavailableSuppliers: {
					loading, error,
					suppliers: nearbyUnavailableSuppliers && nearbyUnavailableSuppliers.nodes
				}
			})
		}
	}
}

export { typeImages, serviceTypes, freeTextSearch, nearbyUnavailableSuppliers }
