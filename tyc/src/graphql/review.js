import gql from 'graphql-tag'
import _get from 'lodash/get'

const getReview = {
	gql: gql`
		query ($reviewerId: Uuid!, $bookingId: Int!) {
			allReviews(condition: {reviewerId: $reviewerId, bookingId: $bookingId}) {
				nodes {
					id comment rating accountId reviewerId supplierId bookingId
				}
			}
		}`,
	options: {
		skip: ( ownProps ) => {
			return !ownProps.bookingId || !_get( ownProps, 'authInfo.id' )
		},
		options: ( { bookingId, authInfo } ) => ({
			variables: {
				bookingId,
				reviewerId: _get( authInfo, 'id' )
			}
		}),
		props: ( { ownProps, data: { loading, allReviews, refetch } } ) => {
			return ({
				reviewLoading: loading,
				review: _get( allReviews, 'nodes[0]' ),
			});
		}
	}
}

const createReview = {
	gql: gql`
		mutation ( $accountId: Uuid!, $reviewerId: Uuid!, $supplierId: Int!, $bookingId: Int!, $target: ReviewTarget!, $rating: Float!, $comment: String ) {
		 	createReview( input: { review: {
		  		accountId: $accountId, 
		  		reviewerId: $reviewerId, 
		  		supplierId: $supplierId, 
		  		bookingId: $bookingId,
		  		target: $target, 
		  		rating: $rating,
		  		comment: $comment
		  	} } ) {
		 		review { id, accountId, reviewerId, supplierId, bookingId, rating, comment }
			}
		}`,
	options: {
		props: ( { mutate } ) => ({
			createReview: ( review ) => mutate( {
				variables: { ...review }
			} )
		})
	}
}

const updateReview = {
	gql: gql`
		mutation ( $id: Int!, $rating: Float!, $comment: String ) {
		 	updateReviewById( input: { id: $id, reviewPatch: {
		  		rating: $rating,
		  		comment: $comment
		  	} } ) {
		 		review { id, accountId, reviewerId, supplierId, bookingId, rating, comment }
			}
		}`,
	options: {
		props: ( { mutate } ) => ({
			updateReview: ( id, rating, comment ) => mutate( {
				variables: { id, rating, comment }
			} )
		})
	}
}


export {
	getReview,
	createReview,
	updateReview
}
