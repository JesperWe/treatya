import gql from 'graphql-tag'
import _get from 'lodash/get'
import config from '../config'

const doLogin = {
	gql: gql`
		mutation ($email: String!, $password: String!) {authenticate(input: {email: $email, password:$password}) {
			jwtToken
		}}`,
	options: {
		props: ( { mutate } ) => ({
			login: ( { email, password } ) => mutate( {
				variables: { email, password },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
};

const registerAccount = {
	gql: gql`
		mutation ($email: String!, $password: String!, $firstName: String!, $lastName: String!, $language: String!) {
			registerAccount(input: {
				email: $email, password:$password, firstName:$firstName, lastName:$lastName, language:$language
			}) { clientMutationId }
		}`,
	options: {
		props: ( { mutate } ) => ({
			registerAccount: ( form ) => mutate( { variables: { ...form } } )
		})
	}
};

const getSupplierInfo = {
	gql: gql`
		query getSupplierInfo( $id:Int! ) {
		  getSupplierInfo(supplierId: $id) {
			nodes {
			  entityId
			  entityName
			  entityType
			  stripeId
			  stripePk
			  stripeNeeded
		}}}`
};

const allMotds = {
	gql: gql`
		{ allMotds(orderBy:PUBLISHED_AT_DESC) {
			nodes {
			  publishedAt
			  message
			}}
		}`
};

const accountMotd = {
	gql: gql`
		mutation ($id: Uuid!, $motd: Datetime!) {
		  updateAccountById(input: {id: $id, accountPatch: {motd: $motd}}) {
			account {
			  id
			  motd
			}}}`
};

const accountInfo = {
	gql: gql`
		query accountInfo($accountId: Uuid!) {
			accountById(id: $accountId) {
				id
				firstName lastName fullName
				motd addressInfo
				geoPosition geoAddress
				email emailVerified
				phone phoneVerified
				role language uiSettings payment
				suppliersByAccountId {
					nodes { 
						id status geoPosition geoAddress
						productsBySupplierId { 
							nodes{
								id
								prodTitle
								prodDescr
								price
								currency
								type
								duration {
									hours minutes
								}
								published
								deleted
							}
						}
					}
				}
				bookingsByClientId {
					nodes { status }
				}
				accountFavoriteSuppliersByAccountId {
					nodes { supplierId }
			}}}`,
	options: {
		skip: ( { accountId } ) => !accountId,
		options: ( { accountId } ) => {
			return {
				variables: { accountId },
				fetchPolicy: 'network-only'
			};
		},
		props: ( { ownProps, data: { loading, accountById, error } } ) => {
			return ({
				accountById: {
					loading, error,
					data: accountById
				}
			});
		}
	}
};

const accountCounters = {
	gql: gql`
		query accountCounters($accountId: Uuid!) {
		  accountCounters(argId: $accountId) {
			nodes {
			  bookingsPreliminary
			  bookingsConfirmed
			  bookingsForAccount
			  unreadMessages
			  eventSeq
		}}}`,
	options: { // Currently unused since the query is done manually.
		skip: ( { authInfo } ) => !authInfo || !authInfo.id,
		options: ( { authInfo } ) => {
			return { variables: { accountId: authInfo.id }, pollInterval: config.stripe.isLive ? 5000 : undefined }
		},
		props: ( { ownProps, data: { loading, accountCounters, refetch, error } } ) => {
			let bookings = _get( accountCounters, 'nodes[0]' );
			return ({
				counters: {
					loading,
					error,
					refetch,
					bookings
				}
			});
		}
	}
};

const updateAccount = {
	gql: gql`
		mutation ($id: Uuid!, $accountPatch: AccountPatch! ) {
			updateAccountById( input: { id: $id, accountPatch: $accountPatch } )
			{ clientMutationId }
		}`,
	options: {
		props: ( { mutate } ) => ({
			updateAccount: ( id, accountPatch ) => mutate( {
				variables: { id, accountPatch },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
};

const updatePhone = {
	gql: gql`
		mutation ($id: Uuid!, $phone: String!) {
			updateAccountPhone(input: {targetId: $id, phone: $phone}) {
				query {
					accountById(id: $id) {
						id phone phoneVerified
		}}}}`,
	options: {
		props: ( { mutate } ) => ({
			updateAccountPhone: ( id, phone ) => mutate( {
				variables: { id, phone },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
};

const updateEmail = {
	gql: gql`
		mutation ($id:Uuid!, $email: String!) {
			updateAccountEmail(input: {targetId: $id, email: $email}) 
				{ integer }
		}`,
	options: {
		props: ( { mutate } ) => ({
			updateAccountEmail: ( id, email ) => mutate( {
				variables: { id, email },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
};

const updateAddressInfo = {
	gql: gql`
		mutation ($id:Uuid!, $email: String!) {
			updateAccountEmail(input: {targetId: $id, email: $email}) 
				{ integer }
		}`,
	options: {
		props: ( { mutate } ) => ({
			updateAccountEmail: ( id, email ) => mutate( {
				variables: { id, email },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
};

const deleteAccount = {
	gql: gql`mutation ($id: Uuid!) {
		deleteAccountById(input: {id: $id}) {
			deletedAccountId
		}}`,
	options: {
		props: ( { mutate } ) => ({
			deleteAccount: ( id ) => mutate( {
				variables: { id },
				refetchQueries: [ 'getAccounts' ]
			} )
		})
	}
}

export {
	registerAccount, doLogin,
	getSupplierInfo, allMotds,
	accountMotd, accountInfo,
	accountCounters, deleteAccount,
	updateAddressInfo,
	updateAccount, updatePhone, updateEmail
}
