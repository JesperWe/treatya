import gql from 'graphql-tag'

const updateAccountPosition = {
	gql: gql`
		mutation ($id: Uuid!, $pos: String!, $addr: String!) {
		  updateAccountById(input: {id: $id, accountPatch: {geoPosition: $pos, geoAddress: $addr}}) {
			account {
			  id
			  geoPosition
			  geoAddress
		}}}`,
	options: {
		props: ( { mutate } ) => ({
			updateAccountPosition: ( id, pos, addr ) => mutate( {
				variables: { id, pos, addr },
				refetchQueries: [ 'accountInfo' ]
			} )
		})
	}
};


const updateSupplierPosition = {
	gql: gql`
		mutation ($id: Int!, $pos: String!, $addr: String!) {
		  updateSupplierById(input: {id: $id, supplierPatch: {geoPosition: $pos, geoAddress: $addr}}) {
			supplier {
			  id
			  geoPosition
			  geoAddress
		}}}`,
	options: {
		props: ( { mutate } ) => ({
			updateSupplierPosition: ( id, pos, addr ) => mutate( { variables: { id, pos, addr } } )
		})
	}
};


export {
	updateAccountPosition,
	updateSupplierPosition
}
