import gql from 'graphql-tag'

const getMessages = {
	gql: gql`
		query getMessages($id: Int!) {
		  allMessages(condition: {bookingId: $id}, orderBy: CREATED_AT_ASC) {
			nodes {
			  id createdAt
			  accountId
			  message
			  seen
		}}}`,
	options: {
		options: ( { bookingId } ) => ( {
			variables: { id: bookingId },
			fetchPolicy: 'network-only'
		} ),
		props: ( { ownProps, data: { loading, error, allMessages, refetch } } ) => {
			return ({
				messages: {
					loading, refetch, error,
					data: allMessages && allMessages.nodes
				}
			});
		}
	}
}

const myNewMessages = {
	gql: gql`
		query myNewMessages($id: Uuid!) {
		  allMessages(condition: {accountId: $id, seen: false}) {
			nodes {
			  id createdAt
			  bookingId
			  message
			  seen
		}}}`,
	options: {
		skip: ( { authInfo } ) => (!authInfo),
		options: ( { authInfo } ) => ( {
			variables: { id: authInfo && authInfo.id }
		} ),
		props: ( { ownProps, data: { loading, error, allMessages, refetch } } ) => {
			return ({
				newMessages: {
					loading, refetch, error,
					data: allMessages && allMessages.nodes
				}
			});
		}
	}
}

const createMessage = {
	gql: gql`
		mutation createMessage($accountId: Uuid!, $bookingId: Int!, $message: String!, $createdAt: Datetime!) {
		  createMessage(input: {message: {
			accountId: $accountId, 
			bookingId: $bookingId,
			 createdAt: $createdAt,
			message: $message}}) {
			 message{id, accountId, bookingId, message, seen, createdAt}
		  }
		}`,
	options: {
		props: ( { mutate } ) => ({
			createMessage: ( msg ) => mutate( {
				variables: { ...msg },
				refetchQueries: [ 'getMessages' ]
			} )
		})
	}
}

const seenMessage = {
	gql: gql`
		mutation ($id: Int!) {
		  updateMessageById( input: {id: $id, messagePatch: {seen: true}} ) {
			message { id seen }}}`,
	options: {
		props: ( { mutate } ) => ({
			seenMessage: ( id ) => mutate( {
				variables: { id },
				refetchQueries: [ 'getMessages', 'myNewMessages' ]
			} )
		})
	}
}

export { getMessages, createMessage, myNewMessages, seenMessage }
