import gql from 'graphql-tag'
import _get from 'lodash/get'

/* let fragments = {
	product: gql`
    fragment SupplierProducts on Supplier {
		productsBySupplierId {
			nodes {
				id
				prodTitle
				prodDescr
				price
				currency
				type
				duration {
					hours minutes
				}
				published
				deleted
			}}
    }`
}; */

const accountSupplierProducts = {
	gql: gql`
		query AccountSupplierProducts($id: Uuid!) {
		  accountById(id: $id) {
			id
			suppliersByAccountId {
				nodes {
					id
					productsBySupplierId { 
						nodes{
							id
							prodTitle
							prodDescr
							price
							currency
							type
							duration {
								hours minutes
							}
							published
							deleted
						}
					}
				}
		}}}`,
	options: {
		skip: ( ownProps ) => {
			let run = ownProps.authInfo && ownProps.authInfo.id;
			return !run;
		},
		options: ( { authInfo } ) => ({
			variables: { id: authInfo && authInfo.id },
		}),
		props: ( { ownProps, data: { loading, error, accountById, refetch } } ) => {
			let result = _get( accountById, 'suppliersByAccountId.nodes[0].productsBySupplierId.nodes' );
			return ({
				products: {
					loading, refetch, error,
					data: result
				}
			});
		}
	}
}

const productCreate = {
	gql: gql`
		mutation ( $supplierId: Int!, $prodTitle: String!, $prodDescr: String!, $price: Int!, 
				$currency: Currency!, $duration: IntervalInput!, 
				$type: ServiceType!, $published: Boolean!, $accountId: Uuid! ) {
		  createProduct( input: { product: {
			supplierId: $supplierId
			prodTitle: $prodTitle
			prodDescr: $prodDescr
			price: $price
			currency: $currency
			duration: $duration
			type: $type
			published: $published
		  }}){
			query{
				accountById(id:$accountId){
					id,suppliersByAccountId{
						nodes {
							id
							productsBySupplierId { 
								nodes{
									id
									prodTitle
									prodDescr
									price
									currency
									type
									duration {
										hours minutes
									}
									published
									deleted
								}
							}
						}
					}
				}
			}
		}}`,
	options: {
		props: ( { mutate } ) => ({
			productCreate: ( product, accountId ) => mutate( {
				variables: { ...product, accountId }
			} )
		})
	}
}

const productUpdate = {
	gql: gql`
		mutation ( $productId: Int!, $product: ProductPatch!, $accountId: Uuid! ) {
		updateProductById( input: { id: $productId, productPatch: $product } ) {
			query{
				accountById(id:$accountId){
					id,suppliersByAccountId{
						nodes {
							id
							productsBySupplierId { 
								nodes{
									id
									prodTitle
									prodDescr
									price
									currency
									type
									duration {
										hours minutes
									}
									published
									deleted
								}
							}
						}
					}
				}
			}
		}}`,
	options: {
		props: ( { mutate } ) => ({
			productUpdate: ( productId, product, accountId ) => mutate( {
				variables: { productId, product, accountId }
			} )
		})
	}
}

const productDelete = {
	gql: gql`
		mutation ( $productId: Int!, $accountId: Uuid! ) {
		  deleteProductById ( input: { id: $productId } ) {
			query{
				accountById(id:$accountId){
					id,suppliersByAccountId{
						nodes {
							id
							productsBySupplierId { 
								nodes{
									id
									prodTitle
									prodDescr
									price
									currency
									type
									duration {
										hours minutes
									}
									published
									deleted
								}
							}
						}
					}
				}
			}
		}}`,
	options: {
		props: ( { ownProps, mutate } ) => {
			return {
				productDelete: ( productId, accountId ) => mutate( {
					variables: { productId, accountId }
				} )
			}
		}
	}
}

export {
	accountSupplierProducts,
	productCreate,
	productDelete,
	productUpdate
};
