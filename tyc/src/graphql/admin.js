import gql from 'graphql-tag'

const getAccounts = {
	gql: gql`query getAccounts {
		allAccounts {
			nodes {
				id firstName lastName role
				createdAt email phone
				suppliersByAccountId {
					nodes { id title description status address1 address2 zip city country }
		}}}}`,
	options: {
		props: ( { ownProps, data: { loading, allAccounts } } ) => {
			return ({
				accountsLoading: loading,
				accounts: allAccounts && allAccounts.nodes
			});
		}
	}
};

const roles = {
	gql: gql`query {__type(name:"Role"){enumValues{name}}}`,
	options: {
		props: ( { ownProps, data: { loading, __type } } ) => {
			return ({
				rolesLoading: loading,
				roles: __type && __type.enumValues
			});
		}
	}
};

const supplierStatuses = {
	gql: gql`query {__type(name:"SupplierStatuses"){enumValues{name}}}`,
	options: {
		props: ( { ownProps, data: { loading, __type } } ) => {
			return ({
				statusesLoading: loading,
				supplierStatuses: __type && __type.enumValues
			});
		}
	}
};

const updateAccountRole = {
	gql: gql`
		mutation ($accountId: Uuid!, $role: Role!) {
		  updateAccountRole(input: { targetId: $accountId, role: $role }){clientMutationId}
		}`,
	options: {
		props: ( { mutate } ) => ({
			updateAccountRole: ( accountId, role ) => mutate( { variables: { accountId, role } } )
		})
	}
};

const updateSupplierStatus = {
	gql: gql`
		mutation ($id: Int!, $status: SupplierStatuses!) {
			updateSupplierStatus(input: { targetId: $id, newStatus: $status }){clientMutationId}
		}`,
	options: {
		props: ( { mutate } ) => ({
			updateSupplierStatus: ( id, status ) => mutate( {
				variables: { id, status },
				refetchQueries: [ 'getAccounts' ]
			} )
		})
	}
};

const supplierPrivate = {
	gql: gql`query { allSupplierPrivates { nodes { supplierId stripeId stripeNeeded } } }`,
	options: {
		props: ( { ownProps, data: { loading, allSupplierPrivates } } ) => {
			let data = [];
			if( allSupplierPrivates && allSupplierPrivates.nodes )
				allSupplierPrivates.nodes.forEach( s => data[ s.supplierId ] = s );

			return ({ supplierPrivate: { loading, data } });
		}
	}
};

const getBookings = {
	gql: gql`query allBookingsByDate($createdFrom: Datetime!, $createdTo: Datetime!) {
	  allBookingsByDate(createdFrom: $createdFrom, createdTo: $createdTo) {
		nodes {
		  id
		  accountId
		  supplierId
		  clientId
		  firstName lastName
		  prodTitle
		  title
		  createdAt
		  status
		  period { start { value } end { value } }
		  payment  refund payout events
		}
	  }
	}`,
	options: {
		options: ( { createdFrom, createdTo } ) => ({
			variables: {
				createdFrom: createdFrom.toDate(),
				createdTo: createdTo.toDate()
			}
		}),
		props: ( { ownProps, data: { loading, allBookingsByDate } } ) => {
			return ({
				bookingsLoading: loading,
				bookings: allBookingsByDate && allBookingsByDate.nodes
			});
		}
	}
};

export { getAccounts, roles, supplierStatuses, updateAccountRole, updateSupplierStatus, supplierPrivate, getBookings }
