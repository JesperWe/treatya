import gql from 'graphql-tag'

const myBookings = {
	gql: gql`
		query myBookings($id: Uuid!) {
		allBookings(condition: {clientId: $id}, orderBy:PERIOD_DESC) {
			nodes {
				id accountId clientId supplierId
				address geoPosition
				period { start { value } end {value} }
				price currency status
				productByProductId { id prodTitle type minInterval { minutes } }
				accountByClientId{ id fullName }
				accountByAccountId{ id fullName }
				supplierBySupplierId{ id title }
			}}}`,
	options: {
		skip: ( ownprops ) => !ownprops.authInfo || !ownprops.authInfo.id,
		options: ( { authInfo } ) => ( {
			variables: { id: authInfo && authInfo.id }
		} ),
		props: ( { ownprops, data: { loading, allBookings, error } } ) => {
			return ({
				myBookings: {
					loading, error,
					data: allBookings && allBookings.nodes
				}
			})
		}
	}
}

const myClientBookings = {
	gql: gql`
		query myClientBookings($id: Uuid!) {
		allBookings(condition: {accountId: $id}, orderBy:PERIOD_ASC) {
			nodes {
				id accountId clientId supplierId
				address geoPosition
				period { start { value } end {value} }
				price currency status
				productByProductId { id prodTitle, minInterval { minutes } }
				accountByClientId{ id fullName }
				accountByAccountId{ id fullName }
				supplierBySupplierId{ id title }
			}}}`,
	options: {
		skip: ( ownprops ) => !ownprops.authInfo || !ownprops.authInfo.id || !ownprops.account.supplierId,
		options: ( { authInfo } ) => ( {
			variables: { id: authInfo && authInfo.id },
			fetchPolicy: 'network-only'
		} ),
		props: ( { ownprops, data: { loading, allBookings, error } } ) => {
			return ({
				myClientBookings: {
					loading, error,
					data: allBookings && allBookings.nodes
				}
			})
		}
	}
}

const booking = {
	gql: gql`
		query Booking($id: Int!) { bookingById(id:$id) {
		id supplierId accountId clientId
		supplierBySupplierId {
			id
			title rating
			imageBg imageProfile}
		accountByClientId { id fullName rating addressInfo }
		productId
		productByProductId {
			id
			prodTitle prodDescr type
			duration { hours minutes }	}
		address geoPosition
		price currency payment payout refund
		status type 
		period { start {value} end {value} }
		createdAt updatedAt
	  }}`,
	options: {
		skip: ( ownProps ) => {
			return !(ownProps.bookingId && ownProps.bookingId > 0)
		},
		options: ( { bookingId } ) => ( {
			variables: { id: bookingId },
			fetchPolicy: 'network-only'
		} ),
		props: ( { ownProps, data: { loading, bookingById, refetch } } ) => {
			return ({
				bookingLoading: loading,
				booking: bookingById,
				bookingRefetch: refetch
			})
		}
	}
}

const bookingCreate = {
	gql: gql`
		mutation ( $supplierId: Int!, $accountId: Uuid!, $productId: Int!, $clientId: Int!, $address: String, $geoPosition: String,
			$startTime: Datetime!, $endTime: Datetime!, $status: BookingStatus!, $type: ServiceType, $price: Int, $currency: Currency ) {
		  createBooking( input: { booking: {
			supplierId: $supplierId
			accountId: $accountId
			productId: $productId
			clientId: $clientId
			address: $address
			geoPosition: $geoPosition
			type: $type,
			price: $price,
			currency: $currency,
			period:{start:{value:$startTime, inclusive: false}, end:{value:$endTime, inclusive: false}}
			status: $status
		  }}){
			clientMutationId
		}}`,
	options: {
		props: ( { mutate } ) => ({
			bookingCreate: ( booking ) => mutate( {
				variables: { ...booking },
				refetchQueries: [ 'findBookings', 'myBookings', 'accountCounters', 'findSuppliers' ]
			} )
		})
	}
}

const bookingDelete = {
	gql: gql`
		mutation ($id:Int!) {
		  deleteBookingById(input: {id: $id}) {
			clientMutationId
		} }`,
	options: {
		props: ( { mutate } ) => ({
			bookingDelete: ( id ) => mutate( {
				variables: { id },
				refetchQueries: [ 'findBookings', 'myBookings', 'accountCounters', 'findSuppliers' ]
			} )
		} )
	}
}

const bookingUpdate = {
	gql: gql`
		mutation ($id: Int!, $patch:BookingPatch!) {
		  updateBookingById(input: {id: $id, bookingPatch: $patch}) {
			clientMutationId
			}}`,
	options: {
		props: ( { mutate } ) => ({
			bookingUpdate: ( id, patch ) => mutate( {
				variables: { id, patch },
				refetchQueries: [ 'findBookings', 'myBookings', 'Booking', 'accountCounters', 'findSuppliers' ]
			} )
		} )
	}
}

export { myBookings, myClientBookings, booking, bookingCreate, bookingDelete, bookingUpdate }
