import React from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import { Input, Icon } from 'antd'
import moment from 'moment'
import errorHandler from '../errorHandler'
import './Messages.less'
import { graphql, compose } from 'react-apollo'
import { getMessages, createMessage, seenMessage } from '../graphql/messages'

//const DEBUG = true

class Messages extends React.Component {

	state = {
		message: ''
	}

	componentWillReceiveProps( next ) {
		let { messages } = next
		if( messages.loading ) return
		if( !messages.data || !messages.data.length ) return
		let otherAccountId = this.props.booking.clientId
		if( otherAccountId === this.props.authInfo.id ) otherAccountId = this.props.booking.accountId

		messages.data.forEach( msg => {
			if( msg.seen || msg.accountId !== this.props.authInfo.id ) return
			this.props.seenMessage( msg.id )
					.then( () => {
						this.props.onSendNotificationToAccountId( otherAccountId )
					} )
					.catch( errorHandler )
		} )
	}

	componentDidUpdate() {
		this.messages.scrollTop = this.messages.scrollHeight
	}

	send = () => {
		// Message.accountId should be the id of the other guy.
		let accountId = this.props.booking.clientId
		if( accountId === this.props.authInfo.id ) accountId = this.props.booking.accountId

		this.props.createMessage( {
					accountId,
					bookingId: this.props.booking.id,
					message: this.state.message,
					createdAt: moment().toISOString()
				} )
				.then( () => {
					this.setState( { message: null } )
					this.props.onSendNotificationToAccountId( accountId )
				} )
				.catch( errorHandler )
	}

	messageDater = ( msg, index ) => {
		let createdAt = moment( msg.createdAt )
		let result = createdAt.format( 'LLL' )
		if( this.lastMesssageDate && index !== 0 ) {
			if( createdAt.isSame( this.lastMessageDate, 'day' ) ) result = createdAt.format( 'H:mm' )
		}
		this.lastMesssageDate = createdAt
		return result
	}

	render() {
		const { t } = this.props
		let { messages } = this.props
		let buttonClass = this.state.message ? 'button-shown' : 'button-hidden'

		return (
				<div className="messages">
					<div className="messages-wrap" ref={element => this.messages = element}>
						{!(messages.data && messages.data.length) && t( 'No messages yet.' )}
						{(messages.data && (messages.data.length > 0)) &&
						messages.data.map( ( msg, index ) => {
							let fromTo = 'from'
							if( msg.accountId === this.props.authInfo.id ) fromTo = 'to'
							return (
									<div className={'message ' + fromTo} key={index}>
										<div className="message-text">{msg.message}</div>
										<div className="message-date">
											{this.messageDater( msg, index )}
											{msg.seen && (fromTo === 'from') && <Icon type="check"/>}
										</div>
									</div>
							)
						} )
						}
					</div>
					<div className="relative">
						<Input.TextArea
								value={this.state.message}
								onChange={( e ) => this.setState( { message: e.target.value } )}
								placeholder={t( 'Discuss this booking.' )}
								autosize={{ minRows: 1, maxRows: 5 }}/>
						<div className={'send-button ' + buttonClass} onClick={this.send}>
							<Icon type="plane" style={{ fontSize: '24px' }}/>
						</div>
					</div>
				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onSendNotificationToAccountId: ( id ) => dispatch( { type: 'SENDNOTIFICATION', id } )
			}
		}
)(
		compose(
				graphql( getMessages.gql, getMessages.options ),
				graphql( seenMessage.gql, seenMessage.options ),
				graphql( createMessage.gql, createMessage.options )
		)(
				translate( [ 'common' ], { wait: true } )( Messages )
		)
)
