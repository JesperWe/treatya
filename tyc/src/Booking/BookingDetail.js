import React from 'react'
import { browserHistory } from 'react-router'
import { translate } from 'react-i18next'
import i18n from '../i18n'
import http from 'axios'
import { Row, Col, Button, Icon, Alert, Popconfirm, Spin, Tag, Rate, notification } from 'antd'
import moment from 'moment'
import Amount from '../Amount'
import Messages from './Messages'
import errorHandler from '../errorHandler'
import { graphql, compose } from 'react-apollo'
import { booking } from '../graphql/booking'
import config from '../config'
import './BookingDetail.less'

class BookingDetail extends React.Component {

	bookingState = ( newState ) => {
		http.post( config.backend.url + '/notify/booking/status-change', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					bookingId: this.props.booking.id,
					status: newState
				}, {
					headers: { 'Accept-Language': i18n.language }
				} )
				.then( result => {
					this.props.bookingRefetch()
					if( result.data && result.data.object ) {
						if( result.data.object === 'refund' && result.data.status === 'succeeded' )
							notification.info( { message: this.props.t( 'A refund of the payment has been requested.' ) } )
					}
				} )
				.catch( errorHandler )
	}

	goToRating = () => {
		browserHistory.push( '/review/b/' + this.props.booking.id )
	}

	render() {
		const { t } = this.props

		if( !this.props.booking ) return (
				<div className="content-width content-pad centered">
					{this.props.bookingLoading && <Spin size="large"/>}
					{!this.props.bookingLoading &&
					<Alert
							message={t( 'Oops!' )}
							description={t( 'You do not have a booking with this id.' )}
							type="warning"
							showIcon/>
					}
				</div>
		)

		let booking = Object.assign( {}, this.props.booking )
		booking.from = moment( booking.period.start.value )
		booking.to = moment( booking.period.end.value )

		let account = booking.accountByClientId
		let supplier = booking.supplierBySupplierId
		let product = booking.productByProductId

		let isSupplier = this.props.authInfo.id === booking.accountId
		let isClient = this.props.authInfo.id === booking.clientId

		let address = booking.address.split( ',' ).map( ( line, index ) => (
				<span key={index}>{line} <br/></span>) )

		let showClientActionButtons =
				([ 'PRELIMINARY', 'UNCONFIRMED', 'CONFIRMED' ].indexOf( booking.status ) >= 0) &&
				(this.props.booking.clientId === this.props.authInfo.id) &&
				(booking.from.isAfter())

		// TODO Handle #98
		if( moment( booking.from ).subtract( 24, 'hours' ).isBefore() && booking.status === 'CONFIRMED' )
			showClientActionButtons = false

		let showSupplierActionButtons = ([ 'PRELIMINARY', 'UNCONFIRMED' ].indexOf( booking.status ) >= 0) &&
				(this.props.booking.accountId === this.props.authInfo.id)
		// TODO ... not sure what the business logic should be here?
		//if( moment( booking.period.start.value ).subtract( 1, 'hours' ).isBefore() ) showSupplierActionButtons = false

		let canReview = booking.from.isBefore() && (
				booking.status === 'DELIVERED' ||
				booking.status === 'CONFIRMED' ||
				booking.status === 'UNCONFIRMED'
		)

		let color = {
			PRELIMINARY: undefined,
			UNCONFIRMED: 'orange',
			CONFIRMED: 'green',
			DENIED: 'red',
			NOSHOW: 'red',
			CANCELLED: undefined,
			DELIVERED: 'blue'
		}[ booking.status ]

		let payment, payout, refund

		if( booking.payment && booking.payment.id ) {
			let adminLink = null
			if( this.props.authInfo.role === 'admin' ) {
				adminLink = (
						<a target="stripe" href={'https://dashboard.stripe.com/payments/' + booking.payment.id}><Icon
								type="login"/></a>)
			}

			let paymentStatus = t( 'succeeded' )
			if( booking.payment.captured === false ) paymentStatus = t( 'not-captured' )

			payment = (
					<div>
						{adminLink}
						{' ' + t( 'Payment' ) + ' ' + paymentStatus + ' '}
						<span className="capitalize">{moment( booking.payment.created * 1000 ).format( config.date.friendly )}</span>
					</div>)
		}

		if( booking.payout && booking.payout.id ) {
			let adminLink = null
			if( this.props.authInfo.role === 'admin' ) {
				adminLink = (<a target="stripe"
								href={'https://dashboard.stripe.com/applications/transfers/' + booking.payout.id}><Icon
						type="share-alt"/></a>)
			}
			payout = (
					<div>
						{adminLink}
						{' ' + t( 'Payout' ) + ' ' + t( booking.payout.status ) + ' '}
						<span className="capitalize">{moment( booking.payout.created * 1000 ).format( config.date.friendly )}</span>
						<br/>
						{t( 'Amount' )} <Amount value={booking.payout}/> {booking.payout.currency.toUpperCase()}
					</div>)
		}

		if( booking.refund && booking.refund.id ) {
			refund = (
					<div>
						{' ' + t( 'Refund' ) + ' ' + t( booking.refund.status ) + ' '}
						<span className="capitalize">{moment( booking.refund.created * 1000 ).format( config.date.friendly )}</span>
					</div>)
		}

		return (
				<div className="booking-page content-main content-width">
					<h3>{t( 'Booking Details' )}</h3>

					<Row gutter={16} type="flex">

						<Col xs={24} sm={12} md={8}>
							<div className={'info-box'}>
								<h3 className="prod-title">{product.prodTitle}</h3>
								<div className="prod-supplier">
									{t( 'by-supplier' )}<br/>
									<b>{supplier.title}</b>
								</div>

								{canReview &&
								<span onClick={this.goToRating} style={{cursor:'pointer'}}>
									<Button>{t('Leave a review!')}</Button>
									<br/>
									<Rate count={5} value={supplier.rating} disabled/>
								</span>}

								{!canReview &&
								<Rate count={5} value={supplier.rating} disabled/> }

								<div className="prod-descr">{product.prodDescr}</div>
							</div>
						</Col>

						<Col xs={24} sm={12} md={8}>
							<div className={'info-box'}>
								<div className="client-wrap" onClick={this.goToRating}>
									<div className="small-label">{t( 'Booking for' )}:</div>
									<h3 className="client-name">{account && account.fullName}</h3>
									{isSupplier &&
									<Rate count={5} value={account.rating} disabled/>
									}
									<div className="client-address">{address}</div>
									<div className="address-info">{account.addressInfo}</div>
								</div>

								{showClientActionButtons &&
								<div className="buttons">
									<Popconfirm title={t( 'Are you sure you want to cancel this booking?' )}
												onConfirm={() => this.bookingState( 'CANCELLED' )} okText={t( 'Yes' )}
												cancelText={t( 'No' )}>
										<Button className="responsive">
											<Icon type="close-circle"/> {t( 'Cancel Booking' )}
										</Button>
									</Popconfirm>
								</div>
								}

								{showSupplierActionButtons &&
								<div className="buttons">
									<Popconfirm title={t( 'Are you sure you want to confirm this booking?' )}
												onConfirm={() => this.bookingState( 'CONFIRMED' )} okText={t( 'Yes' )}
												cancelText={t( 'No' )}>
										<Button className="responsive" type="success" size="large">
											<Icon type="check"/> {t( 'Confirm Booking' )}
										</Button>
									</Popconfirm>
									<Popconfirm title={t( 'Are you sure you want to deny this booking?' )}
												onConfirm={() => this.bookingState( 'DENIED' )} okText={t( 'Yes' )}
												cancelText={t( 'No' )}>
										<Button className="responsive" type="danger" size="large">
											<Icon type="close"/> {t( 'Deny Booking' )}
										</Button>
									</Popconfirm>
								</div>
								}

							</div>
						</Col>

						<Col xs={24} sm={12} md={8}>
							<div className={'info-box status'}>
								<span className="capitalize horizon-spacing">{booking.from.format( config.date.friendly )}</span>
								<Tag className="upper-case" color={color}>{t( booking.status )}</Tag>
								<br/>
								<Icon type="clock-circle-o"/> {booking.from.format( 'H:mm' )} &ndash; {booking.to.format( 'H:mm' )}

								<div>
									<span className="price-currency no-wrap">
										<div className="small-label">{t( 'Price' )}</div>
									</span>
									<span className="amount">
										<Amount value={booking.price}/>
									</span>
									<span className="price-currency">
										<div className="small-label">{booking.currency}</div>
									</span>
								</div>

								{payment && <div className="payment">{payment}</div>}
								{payout && <div className="payment payout">{payout}</div>}
								{refund && <div className="payment refund">{refund}</div>}
							</div>
						</Col>

						<Col xs={24} sm={12} md={8}>
							<div className={'info-box'}>
								<h3>
									{t( 'Chat with the ' )}
									{isSupplier && t( 'client' )}
									{isClient && t( 'supplier' )}
								</h3>
								<Messages booking={booking} {...this.props}/>
							</div>
						</Col>

					</Row>
				</div>
		)
	}
}

export default compose(
		graphql( booking.gql, booking.options )
)(
		translate( [ 'common' ], { wait: true } )(
				BookingDetail
		)
)
