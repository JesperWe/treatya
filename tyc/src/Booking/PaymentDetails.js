import React from 'react'
import { translate } from 'react-i18next'
import http from 'axios'
import i18n from '../i18n'
import io from 'socket.io-client'
import { Alert, notification } from 'antd'
import PaymentStripe from './PaymentStripe'
import CalendarExport from '../Calendar/CalendarExport'
import CMSText from '../CMS/CMSText'
import errorHandler from '../errorHandler'
import config from '../config'
import './PaymentDetails.less'

const DEBUG = false

class PaymentDetails extends React.Component {

	state = {
		wsConnected: undefined,
		valid: {},
		redirect: {},
		show3DSecureFrame: false,
		paymentInProgress: false,
		paymentError: false,
		paymentAttemptComplete: false,
		paymentInfoIncomplete: true
	}

	completeWithError = ( error ) => {
		this.setState( { paymentInProgress: false } )
		errorHandler( error )
		this.props.onComplete( { error } )
	}

	// During the payment cycle we keep a websocket connection to the backend.
	// This way the payment process status can be communicated in real time to the client.
	// This also means that a successfully payed booking ends up in the websocket message handler.
	onWebSocketMessage = ( data ) => {

		const { error, percentDone, booking } = data
		if( error ) {
			console.log( error.message )
			this.setState( {
				paymentAttemptComplete: true,
				paymentError: error.message,
				paymentInProgress: false
			} )
			config.tracking && window._paq.push( [ 'trackEvent', 'Booking', 'Payment fail:' + JSON.stringify( error ) ] )
			notification.error( {
				message: this.props.t( 'Error from Card processing' ),
				description: error.message || JSON.stringify( error )
			} )
			this.props.onComplete( { error } )
			return
		}

		this.setState( { percentDone } )

		switch( data.state ) {
			case '3DSOURCE-CREATED': {
				this.setState( {
					show3DSecureFrame: true,
					redirect: data.redirect
				} )
				break
			}
			case '3DSOURCE-STATUS': {
				this.setState( {
					show3DSecureFrame: false
				} )
				break
			}
			case 'DONE': {
				// This state means that the payment is fully executed, and that the backend has successfully
				// updated the database with all info. All that remains is to queue all relevant notifications.
				http.post( config.backend.url + '/notify/booking/create', {
							id: this.props.authInfo.id,
							jwt: this.props.authToken,
							bookingId: booking.id
						}, {
							headers: { 'Accept-Language': i18n.language }
						} )
						.catch( error => {
							if( error.response && error.response.data ) {
								config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.response.data, 'Notification Service' ] )
								console.log( '/notify/booking/create:', error.response.data )
								notification.error( { message: 'From Notification Service:' + error.response.data } )
							}
							else {
								config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Notification Service' ] )
								console.log( '/notify/booking/create:', error )
								notification.error( { message: 'From Notification Service:' + error.message + ' (' + error.status + ')' } )
							}
						} )

				if( config.tracking ) {
					window._paq.push( [ 'trackEvent', 'Booking', 'Payment success.', booking.currency, booking.price ] )
					window._paq.push( [ 'trackEcommerceOrder', booking.id, booking.price ] )
				}
				this.setState( {
					booking,
					paymentInProgress: false,
					paymentAttemptComplete: true
				} )
				DEBUG && console.log( 'PaymentDetails DONE. Closing WS.' )
				this.connection.disconnect()
				this.props.onComplete( { complete: true, booking } )
				break
			}
			default:
				break
		}
	}

	// NB!!!
	// The use of <Cards> combined with Ant Design <Input>s makes it hard to get refs to the form elements.
	// <Cards> also does not have a "reset form" function.
	// So to make sure the card form is reinitialized when it's reopened we use the "changing key prop" trick
	// for the entire PaymentDetails component. This causes it to mount/unmount each time the modal is shown..

	componentWillMount( next ) {
		DEBUG && console.log( 'PaymentDetails.componentWillMount()\n', this.props )
		// Open socket connection to backend:
		if( this.props.visible ) {
			if( !this.connection && !this.state.paymentAttemptComplete ) {
				DEBUG && console.log( 'Try Open WebSocket' )
				try {
					this.connection = io( config.backend.charge )

					this.connection.on( 'connect', () => {
						DEBUG && console.log( 'WebSocket connected.' )
						this.setState( { wsConnected: 'open' } )
					} )

					this.connection.on( 'stripe', this.onWebSocketMessage )

					this.connection.on( 'error', message => {
						this.setState( { wsConnected: false } )
						console.log( 'Could not create the payment server connection.\n', message )
						this.completeWithError( new Error( 'Could not create the payment server connection: ' + message ) )
					} )

					this.connection.on( 'stripeError', e => {
						this.setState( { wsConnected: false } )
						this.completeWithError( e )
					} )

					this.connection.on( 'disconnect', ( message ) => {
						if( message === 'io client disconnect' ) {
							this.setState( { wsConnected: false, paymentInProgress: false } )
							return
						}
						DEBUG && console.log( 'WebSocket Closed:', message )
						this.setState( { wsConnected: 'closed', paymentInProgress: false } )
					} )
				}
				catch( error ) {
					this.setState( { wsConnected: 'error' } )
					this.completeWithError( error )
				}
			}
		}
	}

	componentWillUnmount( next ) {
		if( this.connection ) {
			this.connection.disconnect()
			DEBUG && console.log( 'Disconnect WebSocket on UnMount.' )
		}
	}

	render() {
		const { t } = this.props

		return (
				<div className="payment vertical-spacing">

					{this.state.wsConnected === 'open' &&
					<div id="card-wrapper"
						 style={{ display: (this.state.paymentInProgress || this.state.paymentAttemptComplete) ? 'none' : 'block' }}>

						<PaymentStripe {...this.props} connection={this.connection}
									   show3DSecureFrame={this.state.show3DSecureFrame}
									   secureRedirect={this.state.redirect}
						/>

					</div>
					}

					{!this.state.paymentError && this.state.paymentAttemptComplete &&
					<div className="booked">
						<div className="message">
							{<CMSText identifier="prel-booking-done-1"/>}
						</div>

						<CalendarExport event={{
							start: this.state.booking.period.start.value,
							end: this.state.booking.period.end.value,
							title: this.props.product.prodTitle + ' (' + this.props.supplier.title + ')',
							address: this.state.booking.address
						}}/>

						<Alert type="info" showIcon message={<CMSText identifier="prel-booking-done-2"/>}/>

					</div>
					}

					{this.state.wsConnected === 'closed' &&
					<Alert type="warning" showIcon
						   message={t( 'Payment server connection has closed.' )}
						   description={t( 'Close and reopen this window to recreate the connection.' )}/>
					}

					{this.state.wsConnected === 'error' &&
					<Alert type="error" showIcon
						   message={t( 'Payment server connection is down.' )}
						   description={t( 'Try reloading the page in a few minutes.' )}/>
					}

				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( PaymentDetails )
