import React from 'react'
import { injectStripe, CardNumberElement, CardExpiryElement, CardCVCElement } from 'react-stripe-elements'
import { Button, Row, Col } from 'antd'
import config from '../config'

const inputStyle = {
	base: {
		fontFamily: 'Verdana, Helvetica, Arial, sans-serif',
		fontSize: '15px',
		textAlign: 'center'
	}
}

class PaymentStripeForm extends React.Component {
	state = { ready: false }

	onNumber = ( event ) => {
		if( !event.error ) this.setState( { number: event.complete } )
	}

	onExpiry = ( event ) => {
		if( !event.error ) this.setState( { expiry: event.complete } )
	}

	onCVC = ( event ) => {
		if( !event.error ) this.setState( { cvc: event.complete } )
	}

	handlePayment = ( event ) => {
		event.preventDefault()
		config.tracking && window._paq.push( [ 'trackEvent', 'Booking', 'Payment initiated' ] )

		this.props.parentState( { paymentInProgress: true, percentDone: 10 } )
		const { account, useSource, handlePayment } = this.props

		if( useSource === 'new' ) {
			this.props.stripe.createSource( {
						owner: {
							name: account.fullName,
							email: account.email
						}
					} )
					.then( handlePayment )
					.catch( this.props.completeWithError )
		} else {
			handlePayment( { source: useSource } )
		}
	}

	render() {
		const { useSource, t } = this.props
		const { number, expiry, cvc } = this.state

		let disabled = !number || !expiry || !cvc
		if( useSource && useSource !== 'new' ) disabled = false

		return (
				<form className="stripe-form">
					{useSource === 'new' &&
					<Row gutter={12}>

						<Col xs={24} sm={12}>
							<CardNumberElement className="ant-input ant-input-lg stripe-input-wrap"
											   style={inputStyle}
											   placeholder={t( 'Card number' )}
											   onChange={this.onNumber}/>
						</Col>
						<Col xs={12} sm={6}>
							<CardExpiryElement className="ant-input ant-input-lg stripe-input-wrap"
											   style={inputStyle}
											   onChange={this.onExpiry}/>
						</Col>
						<Col xs={12} sm={6}>
							<CardCVCElement className="ant-input ant-input-lg stripe-input-wrap"
											style={inputStyle}
											onChange={this.onCVC}/>
						</Col>
					</Row>
					}

					<Button size="large" className="responsive"
							disabled={disabled}
							type="primary" onClick={this.handlePayment}>
						{t( 'Book & Pay immediately' )}
					</Button>
				</form>
		)
	}
}

export default injectStripe( PaymentStripeForm )
