import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { Modal, Row, Col, Icon, Alert, notification } from 'antd';
import moment from 'moment';
import wkx from 'wkx';
import bufferImpl from 'buffer';

const Buffer = bufferImpl.Buffer;
import Login from '../Account/Login'
import Amount from '../Amount';

import './BookingDetail.less'

class ChangeBookingModal extends Component {

	state = {
		visible: false
	}

	componentWillReceiveProps( next ) {
		if( next.product && next.product.supplierId ) this.setState( { visible: true } );
	}

	createBooking = () => {
		const { t } = this.props;
		let { lat, lng } = this.props.location.position;
		let geoPoint = new wkx.Point( lat, lng );
		geoPoint.srid = 4326;
		let geoPosition = (new Buffer( geoPoint.toEwkb() )).toString( 'hex' );

		this.setState( { visible: false } );
		this.props.selectProduct( {} );

		this.props.bookingCreate( {
					clientId: this.props.authInfo.id,
					supplierId: this.props.product.supplierId,
					accountId: this.props.product.supplierId,
					productId: this.props.product.productId,
					type: this.props.product.type,
					price: this.props.product.price,
					currency: this.props.product.currency,
					address: this.props.location.address,
					geoPosition,
					status: 'PRELIMINARY',
					startTime: this.props.desiredBookingMoment,
					endTime: moment( this.props.desiredBookingMoment ).add( this.props.product.durationSek, 's' )
				} )
				.then( result => {
					notification.success( { message: t( 'A preliminary booking was created.' ) } )
				} )
				.catch( error => {
					console.log( error.message );
					console.log( error.stack );
					notification.error( { message: error.message } )
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Booking Create' ] );
				} );
	}

	render() {
		const { t } = this.props;

		if( !this.props.product ) return (null);
		let booking = this.props.product;

		let clientOrLogin;
		let { account, authInfo } = this.props;
		if( authInfo && authInfo.id ) {
			let address = this.props.location.address.split( ',' ).map( ( line, index ) => (
					<span key={index}>{line} <br/></span>) );

			booking.from = this.props.desiredBookingMoment;
			booking.to = moment( this.props.desiredBookingMoment ).add( booking.durationSek, 's' )

			clientOrLogin = (
					<client>
						<div className={'info-box'}>
							<div className="client-wrap">
								<div className="small-label">{t( 'Booking for' )}:</div>
								<div className="client-name">{account.fullName}</div>
								<div className="client-address">{address}</div>
							</div>
						</div>

						<div className={'info-box'}>
							{booking.from.format( 'dddd YYYY-MM-DD' )}<br/>
							<Icon type="clock-circle-o"/> {booking.from.format( 'H:mm' )} &ndash; {booking.to.format( 'H:mm' )}
						</div>

						<div className={'info-box'}>
								<span className="price-currency no-wrap">
									<div className="small-label">{t( 'Price' )}</div>
								</span>
							<span className="amount">
									<Amount value={booking.price}/>
								</span>
							<span className="price-currency">
									<div className="small-label">{booking.currency}</div>
								</span>
						</div>
					</client>);
		}
		else {
			clientOrLogin =
					<Login showIcon
						   header={<Alert message={t( 'Please login to book this treatment!' )} type="warning"
						   />}/>
		}

		return (
				<Modal visible={this.state.visible} width="90%"
					   okText={t( 'Add to bookings' )}
					   cancelText={t( 'Cancel' )}
					   onOk={this.createBooking}
					   onCancel={() => {
						   this.setState( { visible: false } );
						   this.props.selectProduct( {} );
					   }}
					   closable={false} className="booking-modal">
					<Row gutter={24}>
						<Col xs={24} sm={24} md={12} className="col">
							<img className="responsive" role="presentation" src={booking.imageProfile}/>
							<div className="prod-title">{booking.prodTitle}</div>
							<div className="prod-supplier">
								<b>{booking.title}</b>
							</div>
							<div className="prod-descr">{booking.prodDescr}</div>
						</Col>
						<Col xs={24} sm={24} md={12} className="col">
							{clientOrLogin}
						</Col>
					</Row>
				</Modal>
		)
	}
}

ChangeBookingModal.defaultProps = { productId: undefined };

import { booking, bookingCreate } from '../graphql/booking';

export default compose(
		graphql( bookingCreate.gql, bookingCreate.options ),
)(
		translate( [ 'common' ], { wait: true } )(
				ChangeBookingModal
		)
);
