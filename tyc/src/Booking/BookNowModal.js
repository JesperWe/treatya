import React from 'react'
import { translate } from 'react-i18next'
import moment from 'moment'
import { browserHistory } from 'react-router'
import diff from 'deep-diff'
import _get from 'lodash/get'
import { Row, Col, Modal, Button, Alert, Icon, Input } from 'antd'
import Collapse from 'rc-collapse'
import { graphql, compose } from 'react-apollo'
import Amount from '../Amount'
import CMSText from '../CMS/CMSText'
import PaymentDetails from './PaymentDetails'
import config from '../config'
import './BookNowModal.less'
import { myBookings } from '../graphql/booking'
import { updateAccount } from '../graphql/account'

const DEBUG = false

class BookNowModal extends React.Component {
	state = {
		openCounter: 0,
		addressInfo: ''
	}

	componentWillMount() {
		DEBUG && console.log( '\n(BookNowModal) componentWillMount\n' )
		this.setState( { addressInfo: _get( this.props, 'account.addressInfo' ) } )
	}

	componentWillUnmount() {
		DEBUG && console.log( '\n(BookNowModal) componentWillUnmount\n' )
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( '\n\n(BookNowModal) componentWillReceiveProps', JSON.stringify( diff( next, this.props ) ) )
		let openCounter = this.state.openCounter
		if( next.visible && !this.props.visible ) {
			openCounter++
			this.setState( { openCounter } )
			DEBUG && console.log( '   openCounter', openCounter )
		}

		if( _get( next, 'account.addressInfo' ) !== _get( this.props, 'account.addressInfo' ) )
			this.setState( { addressInfo: _get( next, 'account.addressInfo' ) } )

	}

	onAddressInfo = ( event ) => {
		this.setState( { addressInfo: event.target.value } )
	}

	saveAddressInfo = () => {
		this.props.updateAccount( this.props.account.id, { addressInfo: this.state.addressInfo } )
	}

	render() {
		const { t, account, supplier, location, product, visible, onCancel, desiredBookingMoment, myBookings } = this.props

		if( !product ) return null

		let previousBookings
		if( myBookings && myBookings.data && !myBookings.loading ) {
			previousBookings = myBookings.data.filter( booking => (
					moment( booking.period.start.value ).isAfter() &&
					booking.productByProductId.type === product.type &&
					booking.status === 'UNCONFIRMED'
			) )
		}

		return (
				<Modal visible={visible}
					   className="book-now-modal supplier"
					   closable={false} width={752}
					   onCancel={onCancel}
					   footer={null}
					   key={this.state.openCounter}>

					<Row gutter={20}>
						<Col xs={24} sm={12}>
							<div className="info-box">
								<div className="center-wrap">
									<span className="small-label">{t( 'Treatment' )}:</span><br/>
									<div className="title">{product.prodTitle}</div>
									<div className="moment capitalize">{desiredBookingMoment.format( config.date.friendly )}</div>
									<div>{t( 'by-supplier' )} {supplier.title}</div>
								</div>
							</div>
						</Col>
						<Col xs={24} sm={12}>
							<div className="info-box">
								<div className="center-wrap">
									<span className="small-label">{t( 'Booking for' )}:</span><br/>
									<span className="client-name">{account.fullName}</span>
									<span className="client-address">
									{location.address && location.address.split( ',' ).map( ( line, index ) => (
											<span key={index}><br/>{line}</span>) )}
								</span>
								</div>
							</div>
						</Col>
					</Row>

					{previousBookings && previousBookings.length > 0 &&
					<Row gutter={20} className="previous-bookings">
						<Col xs={24} sm={12}>
							<Alert type="info" showIcon
								   message={<CMSText identifier="booking-explain-connected-preliminary"/>}/>
						</Col>
						<Col xs={24} sm={12}>
							<div className="info-box">
								<div className="center-wrap">
									<h4>{t( 'Outstanding preliminary bookings' )}</h4>
									{previousBookings.map( booking => (
											<div className="previous-booking" key={booking.id}
												 onClick={() => browserHistory.push( '/booking/' + booking.id )}>
												<h6><Icon
														type="clock-circle-o"/> {moment( booking.period.start.value ).format( config.date.friendly )}
												</h6>
												{booking.supplierBySupplierId.title} / {booking.productByProductId.prodTitle}
											</div>
									) )}
								</div>
							</div>
						</Col>
					</Row>
					}

					<div className="payment-form comment">
						<div className="label">{t('Booking info (how to get there, door code etc)')}</div>
						<Input.TextArea size="large" autosize={{ minRows: 2, maxRows: 6 }}
										value={this.state.addressInfo} onChange={this.onAddressInfo}
										onBlur={this.saveAddressInfo}
										placeholder={t( 'Door code or other instructions and comments for the supplier.' )}
						/>
					</div>

					<Collapse activeKey={this.state.explanation ? 'explanation' : null} destroyInactivePanel={true}>
						<Collapse.Panel showArrow={false} header={null} key="explanation">
							<Alert type="info" showIcon message={<CMSText identifier="prel-booking-explanation"/>}/>
						</Collapse.Panel>
					</Collapse>

					<div className="modal-pay-header">
						{t( 'Pay the total of' )}
						<Amount value={product.price}/>{' '}
						<span className="currency">{product.currency}</span>
						<Icon type="info-circle" className="explanation-icon"
							  onClick={() => this.setState( { explanation: !this.state.explanation } )}/>
					</div>

					<PaymentDetails {...this.props}
									onComplete={status => {
										DEBUG && console.log( 'PaymentDetails.onComplete', status )
										this.setState( { paymentVisible: false } )
										if( status.complete && !status.error ) {
											this.props.onBookingId( _get( status, 'booking.id' ) )
											return
										}
										if( status.error ) {
											DEBUG && console.log( 'PaymentDetails.onComplete ERROR', status.error.message )
											this.props.onCancel()
										}
									}}
					/>

					<div className="footer">
						<Button size="large" onClick={this.props.onCancel}>{t( 'Close' )}</Button>
					</div>

				</Modal>
		)
	}
}

export default compose(
		graphql( myBookings.gql, myBookings.options ),
		graphql( updateAccount.gql, updateAccount.options )
)(
		translate( [ 'common' ], { wait: true } )( BookNowModal )
)
