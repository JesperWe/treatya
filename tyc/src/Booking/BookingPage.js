import React from 'react'
import { connect } from 'react-redux'
import BookingDetail from './BookingDetail'
import { Alert } from 'antd'


class BookingPage extends React.Component {

	render() {
		if( !this.props.authInfo || !this.props.authInfo.id )
			return ( <div className="browser">
				<Alert showIcon type="warning" message="Not authenticated!"/>
			</div>)

		return (
				<BookingDetail bookingId={this.props.params.bookingId} {...this.props}/>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				location: state.location,
				place: state.place,
				account: state.account,
				authToken: state.authToken,
				authInfo: state.authInfo,
				desiredServiceType: state.desiredServiceType,
				desiredBookingMoment: state.desiredBookingMoment
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onServiceType: ( serviceType ) => dispatch( { type: 'SERVICETYPE', serviceType } ),
				onBookingMoment: ( bookingMoment ) => dispatch( { type: 'BOOKINGMOMENT', bookingMoment } ),
			}
		}
)( BookingPage )
