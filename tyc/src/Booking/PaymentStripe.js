import React from 'react'
import scriptLoader from 'react-async-script-loader'
import { Modal, Progress, Select, notification } from 'antd'
import _get from 'lodash/get'
import http from 'axios'
import diff from 'deep-diff'
import { StripeProvider, Elements } from 'react-stripe-elements'
import Loading from '../Loading'
import PaymentStripeForm from './PaymentStripeForm'
import config from '../config'
import './PaymentDetails.less'
import i18n from '../i18n'
import errorHandler from '../errorHandler'

const DEBUG = false

class PaymentStripe extends React.Component {

	state = {
		apiLoaded: false,
		oldSources: null,
		useSource: null,
		paymentInProgress: false,
		paymentError: false,
		paymentAttemptComplete: false,
		paymentInfoIncomplete: true
	}

	componentWillMount() {
		const oldSources = _get( this.props, 'account.payment.sources.data', [] )
		this.setState( {
			useSource: oldSources.length ? oldSources[ 0 ] : 'new',
			oldSources
		} )
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( '\n(PaymentStripe) componentWillReceiveProps\n', JSON.stringify( diff( next, this.props ) ) )

		// Check for errors loading payment library.
		const { t, visible, isScriptLoaded, isScriptLoadSucceed } = next
		if( visible === false ) return
		if( isScriptLoaded !== this.props.isScriptLoaded || isScriptLoadSucceed !== this.props.isScriptLoadSucceed ) {
			if( isScriptLoaded ) {
				if( isScriptLoadSucceed ) {
					this.setState( { apiLoaded: true } )
					DEBUG && console.log( '... Stripe configured.' )
				} else {
					notification.warning( { message: t( 'Failed to load payment processing module.' ) } )
				}
			}
		}
	}

	closeModal = () => {
		this.props.onComplete( {
			error: this.state.paymentError,
			complete: this.state.paymentAttemptComplete
		} )
	}

	handlePayment = ( response ) => {
		const { source, error } = response

		if( error ) {
			config.tracking && window._paq.push( [ 'trackEvent', 'Booking', 'Payment: ' + error.message ] )
			this.setState( { paymentError: error.message, paymentInProgress: false } )
			console.log( 'Stripe Source Error:', error )
			notification.error( {
				message: this.props.t( 'Error from Card processing' ),
				description: JSON.stringify( error )
			} )
			return
		}

		if( !source ) return

		this.setState( { percentDone: 40 } )
		DEBUG && console.log( 'Stripe source:', JSON.stringify( source ) )

		this.props.connection.emit( 'stripe', {
					id: this.props.id,
					jwt: this.props.authToken,
					source: source.id,
					capture: false,
					three_d_secure: source.card.three_d_secure,
					productId: this.props.product.id,
					moment: this.props.desiredBookingMoment,
					address: this.props.location.address,
					geoPosition: this.props.location.position
				}
		)
	}

	changeCard = ( value ) => {
		DEBUG && console.log( 'changeCard:', value )
		DEBUG && console.log( 'oldSources:', JSON.stringify( this.state.oldSources ) )
		const { t } = this.props

		let source
		if( value === 'new' ) {
			source = 'new'
			this.setState( { useSource: source } )
			return
		}

		if( value && value.length ) {
			source = this.state.oldSources.find( source => source.id === value )
			this.setState( { useSource: source } )
			return
		}

		const { useSource } = this.state
		if( value === undefined && useSource && useSource.id ) {
			Modal.confirm( {
				title: t( 'Remove card' ),
				content: t( 'Do you want to remove this card from your list of saved cards?' ),
				okText: t( 'Yes' ),
				cancelText: t( 'No' ),
				onOk: () => {
					http.post( config.backend.url + '/stripe/connect/detachSource', {
								id: this.props.authInfo.id,
								jwt: this.props.authToken,
								customerId: _get( this.props, 'account.payment.id' ),
								sourceId: useSource.id
							}, {
								headers: { 'Accept-Language': i18n.language }
							} )
							.then( result => {
								notification.success( { message: t( 'The card has been removed.' ) } )
							} )
							.catch( errorHandler )
				}
			} )
		}
	}

	render() {
		const { t } = this.props
		const { apiLoaded, useSource } = this.state

		const oldSources = _get( this.props, 'account.payment.sources.data', [] )

		if( !apiLoaded ) return <Loading/>

		return (
				<div>

					<StripeProvider apiKey={config.stripe.isLive ? config.stripe.pk_live : config.stripe.pk_test}>
						<div className="payment-form">

							{oldSources && oldSources.length > 0 &&
							< Select size="large" onChange={this.changeCard} className="credit-card-menu"
									 dropdownClassName="payment-form"
									 allowClear={true} style={{ width: '100%' }}
									 defaultValue={oldSources.length ? oldSources[ 0 ].id : 'new'}
									 placeholder={t( 'Select card to pay with' )}>
								{oldSources.map( source =>
										<Select.Option key={source.id} value={source.id}>
								<span>
								{source.card.brand + ' '}
									<span className="last4"><span>**** **** ****</span> {source.card.last4}</span>
									{' (' + source.card.exp_month}/{source.card.exp_year + ') '}
								</span>
										</Select.Option> )}
								<Select.Option value="new">{t( 'Use another card...' )}</Select.Option>
							</Select>
							}

							<Elements locale={this.props.language}>
								<PaymentStripeForm {...this.props} handlePayment={this.handlePayment}
												   useSource={useSource}
												   parentState={s => this.setState( s )}/>
							</Elements>

							<div className="payment-progress">
								<Progress percent={this.state.percentDone} showInfo={false}
										  status={this.state.paymentError ? 'exception' : (this.state.paymentAttemptComplete ? 'success' : 'active')}
										  style={{ opacity: this.state.paymentInProgress ? 1 : 0 }}/>
							</div>

							<div id="cards">
								<img src="/assets/card-payment.png" className="form-payment" alt=""/>
							</div>

						</div>
					</StripeProvider>

					<Modal visible={this.props.show3DSecureFrame} maskClosable={false}
						   closable={false} className="payment-iframe"
						   onCancel={this.closeModal} width={750}
						   footer={null}>

						<div>
							{this.props.secureRedirect.url &&
							<iframe src={this.props.secureRedirect.url} frameBorder={0} title="3DSecure"/>
							}
						</div>

					</Modal>
				</div>
		)
	}
}

export default scriptLoader( 'https://js.stripe.com/v3/' )( PaymentStripe )
