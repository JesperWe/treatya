import React from 'react'
import { connect } from 'react-redux';
import moment from 'moment'
import { translate } from 'react-i18next'
import _get from 'lodash/get'
import { browserHistory } from 'react-router'
import { Row, Col, Alert, Tag, Badge, Radio } from 'antd'
import Loading from '../Loading'
import CalendarExportButtons from '../Calendar/CalendarExport'
import config from '../config'
import { graphql, compose } from 'react-apollo'
import { myBookings, myClientBookings } from '../graphql/booking'
import { myNewMessages } from '../graphql/messages'
import './BookingOverview.less'

// const DEBUG = false

class BookingOverview extends React.Component {

	state = {
		displayType: 'current'
	}

	isBookingCurrent = ( b ) => {
		return moment( b.period.end.value ).add( 1, 'h' ).isAfter() &&
				{
					PRELIMINARY: true,
					UNCONFIRMED: true,
					CONFIRMED: true,
					DENIED: false,
					NOSHOW: false,
					CANCELLED: false,
					DELIVERED: false
				}[ b.status ]
	}

	newMessages = ( booking ) => {
		let msgCount = 0
		if( this.props.newMessages.data ) {
			this.props.newMessages.data.forEach( msg => {
				if( msg.bookingId === booking.id ) msgCount++
			} )
		}
		return msgCount
	}

	periodCell = ( booking ) => {
		let start = moment( booking.period.start.value )
		// let end = moment(period.end.value)
		return (
				<span>{start.format( config.date.friendly )}</span>
		)
	}

	rowClick = ( booking, index ) => {
		browserHistory.push( '/booking/' + booking.id )
	}

	rowClassName = ( booking ) => {
		let cn = booking.status.toLowerCase()
		if( booking.accountId === this.props.authInfo.id ) cn += ' supplier'
		return 'booking ' + cn
	}

	statusTag = ( booking ) => {
		let color = {
			PRELIMINARY: undefined,
			UNCONFIRMED: 'orange',
			CONFIRMED: 'green',
			DENIED: 'red',
			NOSHOW: 'red',
			CANCELLED: undefined,
			DELIVERED: 'blue'
		}[ booking.status ]

		return <Tag color={color}>{this.props.t( booking.status )}</Tag>
	}

	otherPartyName = ( booking ) => {
		if( booking.accountId === this.props.authInfo.id )
			return booking.accountByClientId.fullName
		else
			return booking.supplierBySupplierId.title
	}

	bookingList = ( bookings ) => {
		return bookings
				.map( booking => (
						<Row type="flex" key={booking.id}
							 className={this.rowClassName( booking )}>

							<Col xs={{ span: 24, order: 1 }} sm={{ span: 16, order: 1 }} md={{ span: 10, order: 1 }}
								 className="messages" onClick={() => this.rowClick( booking )}>
								<h4 className="title">{booking.productByProductId.prodTitle}</h4>
								<Badge count={this.newMessages( booking )}/>
							</Col>

							<Col onClick={() => this.rowClick( booking )}
										xs={{ span: 24, order: 2 }} sm={{ span: 16, order: 3 }} md={{ span: 6, order: 2 }}>
								({this.otherPartyName( booking )})
							</Col>

							<Col className="status" onClick={() => this.rowClick( booking )}
								 xs={{ span: 10, order: 4 }} sm={{ span: 8, order: 4 }} md={{ span: 3, order: 3 }}>
								{this.statusTag( booking )}
							</Col>

							<Col onClick={() => this.rowClick( booking )}
									xs={{ span: 14, order: 3 }} sm={{ span: 8, order: 2 }} md={{ span: 5, order: 4 }}>
								<span className="date-friendly">{this.periodCell( booking )}</span>
								<CalendarExportButtons size="small" event={{
									start: booking.period.start.value,
									end: booking.period.end.value,
									title: booking.productByProductId.prodTitle + ' (' + booking.supplierBySupplierId.title + ')',
									address: booking.address
								}}/>
							</Col>

						</Row>
				) )
	}

	onDisplayChange = ( event ) => {
		this.setState( { displayType: event.target.value } )
	}

	render() {
		const { t, authInfo, myBookings } = this.props
		const { displayType } = this.state
		let result = null

		if( !authInfo || !authInfo.id )
			result = <Alert showIcon type="warning" message={t( 'Not authenticated!' )}/>

		if( !myBookings || !myBookings.data || myBookings.loading ) {
			result = <Loading/>
		} else {
			let bookings = myBookings.data.filter( booking => this.isBookingCurrent( booking ) !== (displayType !== 'current') )
			let clientBookings = _get(this.props, 'myClientBookings.data', [] ).filter( booking => this.isBookingCurrent( booking ) !== (displayType !== 'current') )

			if( bookings.length + clientBookings.length === 0 )
				result = <Alert showIcon type="info" message={t( 'You do not have any bookings.' )}/>
			else
				result = (
						<div className="bookings-table">
							{bookings.length > 0 &&
							<div>
								<h2 className="centered">{t( 'My Bookings' )}</h2>
								{this.bookingList( bookings )}
							</div>
							}
							{clientBookings.length > 0 &&
							<div>
								<h2 className="centered">{t( 'My Clients Bookings' )}</h2>
								{this.bookingList( clientBookings )}
							</div>
							}
						</div>)
		}

		return (
				<div className="booking-overview content-width">

					<Radio.Group onChange={this.onDisplayChange} defaultValue="current">
						<Radio.Button value="current">{t( 'Current' )}</Radio.Button>
						<Radio.Button value="archived">{t( 'Archived' )}</Radio.Button>
					</Radio.Group>

					{result}

				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				location: state.location,
				language: state.language,
				place: state.place,
				account: state.account,
				authInfo: state.authInfo,
				desiredServiceType: state.desiredServiceType,
				desiredBookingMoment: state.desiredBookingMoment
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {}
		}
)( compose(
		graphql( myBookings.gql, myBookings.options ),
		graphql( myClientBookings.gql, myClientBookings.options ),
		graphql( myNewMessages.gql, myNewMessages.options )
)( translate( [ 'common' ], { wait: true } )( BookingOverview ) ) )

