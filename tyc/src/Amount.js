import React from 'react';

const Amount = ( { value } ) => {

	function format( val ) {
		let result = (val % 1000).toString();
		if( val >= 1000 ) result = '000'.substr( result.length ) + result;
		let k = Math.floor( val / 1000 );
		if( k > 0 ) result = k.toString() + ' ' + result;
		return result;
	}

	if( !value ) return null;

	if( 'number' === typeof value ) {
		return (<span className="amount">{format( value )}</span>);
	}

	if( 'object' === typeof value ) {

		if( value.amount ) { // Stripe object
			return (<span className="stripe-amount">{format( Math.floor(value.amount/100) )}</span>);
		}

		if( value.price ) { // Booking or Product
			return (<span className="amount">{format( value.price )}</span>);
		}
	}

	return (<span className="amount">{'<' + typeof value + '>?'}</span>);
};

export default Amount;
