import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { Modal, Alert, Button } from 'antd'
import MapWidget from '../Location/MapWidget'
import config from '../config'
import './LocationRequestWidget.less'

class LocationRequestWidget extends Component {
	state = { visible: false, showUseButton: false }

	componentWillReceiveProps( next ) {
		if( next.visible !== this.state.visible ) {
			this.setState( { visible: next.visible } )
			if( next.visible ) {
				let searchbox = document.querySelector( '.gm-style input' )
				if( searchbox ) {
					searchbox.value = ''
					searchbox.setAttribute( 'placeholder', this.props.t( 'Search...' ) )
					this.setState( { showUseButton: false } )
				}
			}
		}
	}

	onCancel = () => {
		this.setState( { visible: false } )
		this.props.requestManualLocation && this.props.requestManualLocation( false )
		if( this.props.onCancel ) this.props.onCancel()
	}

	onPlacesChanged = ( places ) => {
		this.setState( { showUseButton: true } )
		this.props.onPlacesChanged( places )
	}

	onMarkerDragEnd = ( markerEvent ) => {
		this.setState( { showUseButton: true } )
		this.props.onMarkerDragEnd( markerEvent )
	}

	render() {
		const { t } = this.props
		let location = this.props.location || config.location
		let zoom = 13
		let position = location.position
		if( !position ) {
			position = config.location.position
			zoom = config.location.zoom
		}

		return (
				<Modal visible={this.state.visible} className="no-footer" wrapClassName="location-request"
					   onCancel={this.onCancel} closable={false} footer={<div></div>}>

					{this.props.alert &&
					<Alert {...this.props.alert}/>
					}

					{this.state.showUseButton &&
					<Button size="large" type="primary"
							className="responsive vertical-spacing"
							onClick={this.props.onUseLocation}>
						{t( 'Use this location' )}
					</Button>
					}

					<div className="map-widget" style={{ height: '400px' }}>
						<MapWidget
								markers={[ location ]}
								onPlacesChanged={this.onPlacesChanged}
								onMarkerDragEnd={this.onMarkerDragEnd}
								options={{ mapTypeControl: false, streetViewControl: false }}
								defaultZoom={zoom}
								defaultCenter={position}
						/>
					</div>

				</Modal>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( LocationRequestWidget )
