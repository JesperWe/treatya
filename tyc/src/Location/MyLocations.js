import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { graphql, compose } from 'react-apollo'
import _isEqual from 'lodash/isEqual'
import _get from 'lodash/get'
import _set from 'lodash/set'
import _cloneDeep from 'lodash/cloneDeep'
import wkx from 'wkx'
import bufferImpl from 'buffer'
import LocationRequestWidget from './LocationRequestWidget'
import { Button, Row, Col, notification } from 'antd'
import config from '../config'
import './MyLocations.less'
import { updateAccountPosition, updateSupplierPosition } from '../graphql/location'
import { updateAccount } from '../graphql/account'

const Buffer = bufferImpl.Buffer

class MyLocations extends Component {

	state = {
		location: {
			address: '',
			position: undefined
		},
		newLocation: {},
		visible: false,
		currentLocationType: undefined
	}

	componentWillMount() {
		if( this.props.location && this.props.location.position ) {
			this.setState( { location: this.props.location } )
		}
	}

	componentWillReceiveProps( next ) {
		if( !_isEqual( next.location, this.props.location ) ) {
			this.setState( { location: next.location } )
		}
	}

	onMarkerDragEnd = ( mapEvent ) => {
		let location = {
			position: {
				lat: mapEvent.latLng.lat(),
				lng: mapEvent.latLng.lng()
			}
		}

		if( window.geocoder ) {
			window.geocoder.geocode( { location: location.position },
					( results, status ) => {
						if( status === 'OK' ) {
							if( results.length ) {

								// Prefer the finest sublocality
								let resultIndex = results.findIndex( result => {
									let sublocalityIndex = result.types.indexOf( 'sublocality' )
									return ( sublocalityIndex >= 0 )
								} )

								// If no sublocality, do we have a locality?
								if( resultIndex < 0 ) {
									resultIndex = results.findIndex( result => {
										let sublocalityIndex = result.types.indexOf( 'locality' )
										return ( sublocalityIndex >= 0 )
									} )
								}

								// Neither. Just pick the first result.
								if( resultIndex < 0 ) resultIndex = 0

								location.address = results[ 0 ].formatted_address
								document.querySelector( '.gm-style input' ).setAttribute( 'placeholder', location.address )
							} else {
								console.log( 'LocationService Geocoder: No results found' )
							}
							this.setState( { location } )
						} else {
							notification.error( { message: 'Geocoder failed due to: ' + status } )
						}
					}
			)
		}
	}

	changeLocation = ( currentLocationType ) => {
		this.setState( { currentLocationType, visible: true } )
		switch( currentLocationType ) {
			case 'home':
				this.setState( { location: this.props.account.location } )
				break
			case 'supplier':
				this.setState( { location: { position: undefined } } )
				break
			default:
				this.setState( { location: { position: undefined } } )
				break
		}
	}

	placesChanged = ( places ) => {
		this.setState( {
			location: {
				address: places[ 0 ].address,
				position: {
					lat: places[ 0 ].position.lat(),
					lng: places[ 0 ].position.lng()
				}
			}
		} )
	}

	onUseLocation = () => {
		this.saveLocation( this.state.location )
		this.setState( { visible: false } )
	}

	saveLocation = ( location ) => {
		const { t } = this.props
		this.props.onLocate( location )

		let { lat, lng } = location.position
		let geoPoint = new wkx.Point( lat, lng )
		geoPoint.srid = 4326
		let geoPosition = (new Buffer( geoPoint.toEwkb() )).toString( 'hex' )

		switch( this.state.currentLocationType ) {
			case 'home':
				this.props.updateAccountPosition( this.props.authInfo.id, geoPosition, location.address )
						.then( result => {
							notification.success( { message: t( 'Your Home location was updated.' ) } )
						} )
						.catch( error => {
							console.log( 'saveHomeLocation()', location, error )
							config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Save Home Location' ] )
							notification.error( { message: error.message } )
						} )
				break
			case 'supplier':
				this.props.updateSupplierPosition( this.props.account.supplierId, geoPosition, location.address )
						.then( result => {
							notification.success( { message: t( 'Your Supplier location was updated.' ) } )
							if( !_get( this.account, 'uiSettings.supplierRegistration.steps.location' ) ) {
								let uiSettings = _cloneDeep( this.props.account.uiSettings )
								_set( uiSettings, 'supplierRegistration.steps.location', { done: true } )
								_set( uiSettings, 'infoSplash.supplierWelcome', { dismissed: true } )
								this.props.updateAccount( this.props.authInfo.id, { uiSettings } )
							}
						} )
						.catch( error => {
							console.log( 'saveSupplierLocation()', this.state.location, error )
							config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Save Supplier Location' ] )
							notification.error( { message: error.message } )
						} )
				break
			default:
				break
		}
	}

	onPlacesChanged = ( places ) => {
		console.log( 'MyLocations.onPlacesChanged()', places )
		let newLocation = {
			...places[ 0 ],
			position: { lat: places[ 0 ].position.lat(), lng: places[ 0 ].position.lng() } // Function -> Value
		}
		this.setState( { location: newLocation } )
	}

	render() {
		const { t } = this.props

		let homeAddress = this.props.account.geoAddress
		if( homeAddress ) {
			homeAddress = homeAddress.split( ',' ).map( ( line, index ) => {
				return (<span key={index}>{index > 0 && <br/>}{line}</span>)
			} )
		}

		let supplierAddress = _get( this.props.account, 'suppliersByAccountId.nodes[0].geoAddress' )
		if( supplierAddress ) {
			supplierAddress = supplierAddress.split( ',' ).map( ( line, index ) => {
				return (<span key={index}>{index > 0 && <br/>}{line}</span>)
			} )
		}

		return (
				<div className="mylocations-component">
					<div className="addresses">
						<Row gutter={20}>
							<Col xs={24} sm={12}>
								<h3>{t( 'Current Home Location' )}</h3>
								{homeAddress || t( '...missing.' )}

								<Button type="primary" size="large"
										className="responsive vertical-spacing"
										onClick={() => this.changeLocation( 'home' )}>
									{t( 'Change your Home location' )}</Button>

							</Col>
							{this.props.account && this.props.account.supplierId > 0 &&
							<Col xs={24} sm={12}>
								<h3>{t( 'Current Supplier Location' )}</h3>
								{supplierAddress || t( '...missing.' )}
								<Button type="primary" size="large"
										className="responsive vertical-spacing"
										onClick={() => this.changeLocation( 'supplier' )}>
									{t( 'Change your Supplier location' )}</Button>
							</Col>
							}
						</Row>
					</div>

					<LocationRequestWidget
							visible={this.state.visible}
							location={this.state.location}
							onCancel={() => this.setState( { visible: false } )}
							onPlacesChanged={this.placesChanged}
							onMarkerDragEnd={this.onMarkerDragEnd}
							onUseLocation={this.onUseLocation}
					/>

				</div>
		)
	}
}

export default
compose(
		graphql( updateAccount.gql, updateAccount.options ),
		graphql( updateAccountPosition.gql, updateAccountPosition.options ),
		graphql( updateSupplierPosition.gql, updateSupplierPosition.options )
)(
		translate( [ 'common' ], { wait: true } )( MyLocations )
)
