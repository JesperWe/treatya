import React from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import SearchBox from 'react-google-maps/lib/components/places/SearchBox'
import _ from 'lodash'
import config from '../config'
import './MapWidget.less'

const DEBUG = false

const INPUT_STYLE = {
	boxSizing: `border-box`,
	MozBoxSizing: `border-box`,
	border: `1px solid transparent`,
	width: `240px`,
	height: `32px`,
	marginTop: `1rem`,
	padding: `0 12px`,
	borderRadius: `3px`,
	boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
	fontSize: `14px`,
	outline: `none`,
	textOverflow: `ellipses`,
}

const WidgetGoogleMap = withGoogleMap( props => {
	return (
			<div>
				<GoogleMap
						defaultZoom={props.defaultZoom}
						defaultCenter={props.defaultCenter}
						center={props.center}
						onBoundsChanged={props.onBoundsChanged || _.noop}
						ref={props.onMapMounted}
						onClick={props.onMapClick || _.noop}
						options={props.options}
				>
					<SearchBox
							ref={props.onSearchBoxMounted}
							controlPosition={2}
							onPlacesChanged={props.onPlacesChanged}>

						<input style={INPUT_STYLE} placeholder={props.t( 'Search...' )}/>

					</SearchBox>
					{props.markers && props.markers.map( ( marker, index ) => (
							<Marker position={marker.position} key={index} draggable={true}
									onDragEnd={props.onMarkerDragEnd}/>
					) )}


				</GoogleMap>
			</div>
	)
} )

class MapWidget extends React.Component {

	state = {
		center: undefined,
		bounds: undefined,
		markers: undefined
	}

	componentWillMount() {
		DEBUG && console.log( 'MapWidget.componentWillMount', this.props.markers )
		if( this.props.googleApiStatus !== 'ok' ) {
			DEBUG && console.log( '  Google API', this.props.googleApiStatus )
			let head = document.head || document.getElementsByTagName( 'head' )[ 0 ]
			let script = document.createElement( 'script' )

			script.type = 'text/javascript'
			script.charset = 'utf8'
			script.async = 'async'
			const { api, key, libraries } = config.google
			script.src = api + '?key=' + key + '&libraries=' + libraries
			script.onload = () => {
				DEBUG && console.log( '  Google API: Ok' )
				window.geocoder = new window.google.maps.Geocoder()
				this.props.onGoogleApiStatus( 'ok' )
			}
			head.appendChild( script )
			return
		}
	}

	componentWillReceiveProps( nextProps ) {
		DEBUG && console.log( 'MapWidget.componentWillReceiveProps', nextProps, this.state.markers )

		// For unknown reasons it does not work to set state in componentWillMount() ...
		if( !this.state.center )
			this.setState( {
				center: this.props.defaultCenter || config.location,
				markers: this.props.markers || []
			} )
	}

	handleMapMounted = ( map ) => {
		this._map = map
	}

	handleBoundsChanged = () => {
		this.setState( {
			bounds: this._map.getBounds(),
			center: this._map.getCenter()
		} )
	}

	handleSearchBoxMounted = ( searchBox ) => {
		this._searchBox = searchBox
	}

	handlePlacesChanged = () => {
		const places = this._searchBox.getPlaces()
		// Add a marker for each place returned from search bar
		const markers = places.map( place => ({
			position: place.geometry.location,
			address: place.formatted_address
		}) )
		// Set markers set map center to first search result
		const mapCenter = markers.length > 0 ? markers[ 0 ].position : this.state.center
		this.setState( {
			center: mapCenter,
			markers
		} )
		if( this.props.onPlacesChanged ) this.props.onPlacesChanged( markers )
	}

	render() {
		if( this.props.googleApiStatus !== 'ok' ) return null
		if( !this.state.center ) return null

		return (
				<WidgetGoogleMap
						{...this.props}
						containerElement={<div style={{ height: `100%` }}/>}
						mapElement={<div style={{ height: `100%` }}/>}
						center={this.state.center}
						onMapMounted={this.handleMapMounted}
						onBoundsChanged={this.handleBoundsChanged}
						onSearchBoxMounted={this.handleSearchBoxMounted}
						bounds={this.state.bounds}
						onPlacesChanged={this.handlePlacesChanged}
						markers={this.state.markers}
						onMarkerDragEnd={this.props.onMarkerDragEnd || _.noop}
						defaultZoom={this.props.defaultZoom}
						options={this.props.options}
				/>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				googleApiStatus: state.googleApiStatus
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onGoogleApiStatus: ( googleApiStatus ) => dispatch( { type: 'GOOGLE_API', googleApiStatus } )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )( MapWidget )
)
