import React from 'react'
import { connect } from 'react-redux'
import isEqual from 'lodash/isEqual'
import { translate } from 'react-i18next'
import cookie from 'browser-cookies'
import LocationRequestWidget from '../Location/LocationRequestWidget'
import config from '../config'

const DEBUG = false

class LocatorService extends React.Component {

	constructor( props ) {
		super( props )

		this.positionOptions = {
			enableHighAccuracy: true,
			maximumAge: 0,
			timeout: Infinity,
		}

		this.geolocationProvider = (typeof (navigator) !== 'undefined' && navigator.geolocation)

		this.state = {
			isGeolocationAvailable: Boolean( this.geolocationProvider ),
			isGeolocationEnabled: true, // be optimistic
			positionError: null,
			visible: false
		}

		if( this.props.userDecisionTimeout ) {
			this.userDecisionTimeoutId = setTimeout( () => {
				this.onPositionError()
			}, this.props.userDecisionTimeout )
		}
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( 'LocatorService.componentWillReceiveProps()', next )

		if( next.manualLocationRequested && !this.props.manualLocationRequested ) {
			cookie.erase( 'location' )
			this.setState( { visible: true } )
		}

		if( next.deviceLocationRequested && !this.props.deviceLocationRequested ) {
			cookie.erase( 'location' )
			this.doDeviceLocation()
			return
		}

		if( !isEqual( next.location, this.props.location ) && !next.location.error ) {
			if( next.location.address && next.location.address.length ) return  // Don't do reverse if we already have an address.
			if( next.location.position ) this.locateReverse( next.location.position )
			return
		}

		// When logging in, prefer the cookie location if we have one.
		let cookieLocation = cookie.get( 'location' )
		if( cookieLocation )
			try {
				cookieLocation = JSON.parse( cookieLocation )
				return // Yes we do so skip account info.
			}
			catch( e ) {
			}

		if( !isEqual( next.account, this.props.account ) && next.account && next.account.position ) {
			let location
			try {
				let coord = JSON.parse( next.account.position ).coordinates
				location = { lat: coord[ 0 ], lng: coord[ 1 ] }
			}
			catch( error ) {
			}

			this.locateReverse( location )
		}
	}

	cancelUserDecisionTimeout = () => {
		if( this.userDecisionTimeoutId ) {
			clearTimeout( this.userDecisionTimeoutId )
		}
	}

	onPositionError = ( positionError ) => {
		DEBUG && console.log( 'LocatorService.onPositionError()', positionError )
		this.cancelUserDecisionTimeout()
		this.setState( {
			position: null,
			isGeolocationEnabled: false,
			positionError
		} )

		// If we have a position on a cookie, use that instead.
		try {
			let geo = JSON.parse( cookie.get( 'location' ) )
			if( geo && geo.location && geo.location.lat ) this.props.onLocate( geo.location )
		} catch( error ) {
			console.log( 'Cookie Location:', error.message )
		}

		// Ask for manual positioning.
		this.setState( { visible: true } )
		this.props.onLocate( { error: positionError }, true )
	}

	onPositionSuccess = ( position ) => {
		this.cancelUserDecisionTimeout()
		let latLng = {
			lat: position.coords.latitude,
			lng: position.coords.longitude
		}
		this.setState( {
			position: latLng,
			isGeolocationEnabled: true,
			positionError: null
		} )

		DEBUG && console.log( 'onPositionSuccess()' )
		this.props.onLocate( { position: latLng } )
	}

	componentDidMount() {
		let cookieLocation = cookie.get( 'location' )

		if( cookieLocation )
			try {
				cookieLocation = JSON.parse( cookieLocation )
				DEBUG && console.log( 'cookieLocation', cookieLocation )
				this.setState( {
					position: cookieLocation.position,
				} )
				this.props.onLocate( cookieLocation )
				return
			}
			catch( e ) {
				console.log( 'LocatorService.componentDidMount() cookieLocation:', e )
			}

		this.doDeviceLocation()
	}

	doDeviceLocation() {
		if( !this.geolocationProvider ) return

		if( !this.geolocationProvider.getCurrentPosition ) {
			throw new Error( 'The provided geolocation provider is invalid' )
		}

		this.geolocationProvider.getCurrentPosition( this.onPositionSuccess, this.onPositionError, this.positionOptions )
	}

	locateReverse( position ) {
		DEBUG && console.log( 'LocationService.locateReverse( %s )', JSON.stringify( position ) )
		if( this.props.googleApiStatus !== 'ok' ) {
			DEBUG && console.log( '  Google API', this.props.googleApiStatus )
			let head = document.head || document.getElementsByTagName( 'head' )[ 0 ]
			let script = document.createElement( 'script' )

			script.type = 'text/javascript'
			script.charset = 'utf8'
			script.async = 'async'
			const { api, key, libraries } = config.google
			script.src = api + '?key=' + key + '&libraries=' + libraries
			script.onload = () => {
				DEBUG && console.log( '  Google API: Ok' )
				window.geocoder = new window.google.maps.Geocoder()
				this.props.onGoogleApiStatus( 'ok' )
				this.props.onComponentsLoading( false )
				setTimeout( () => this.locateReverse( position ), 500 )
			}

			// Load with delay since this will always happen at App launch and we
			// don't need location immediately.
			setTimeout( () => {
				if( this.props.googleApiStatus === 'ok' ) return
				head.appendChild( script )
			}, 4000 )
			return
		}
		if( !window.geocoder ) return
		if( !window.geocoder.geocode ) {
			throw new Error( 'The provided geocoder is invalid' )
		}
		let oldLocationStr = cookie.get( 'location' )
		if( oldLocationStr ) try {
			let oldLocation = JSON.parse( oldLocationStr )
			let distance = window.google.maps.geometry.spherical.computeDistanceBetween(
					new window.google.maps.LatLng( position.lat, position.lng ),
					new window.google.maps.LatLng( oldLocation.position.lat, oldLocation.position.lng )
			)
			if( distance < 200 ) {
				this.props.onLocateReverse( oldLocation )
				DEBUG && console.log( 'Reverse Geolocation from Cookie, distance =', distance, 'm.' )
				return
			}
		}
		catch( error ) {
			console.log( 'Geolocation Cookie fail:', error.message )
		}

		if( !position.lat || !position.lng ) return

		window.geocoder.geocode( { location: position },
				( results, status ) => {
					if( status === 'OK' ) {
						if( results.length ) {

							// Prefer the finest sublocality
							let resultIndex = results.findIndex( result => {
								let sublocalityIndex = result.types.indexOf( 'sublocality' )
								return ( sublocalityIndex >= 0 )
							} )

							// If no sublocality, do we have a locality?
							if( resultIndex < 0 ) {
								resultIndex = results.findIndex( result => {
									let sublocalityIndex = result.types.indexOf( 'locality' )
									return ( sublocalityIndex >= 0 )
								} )
							}

							// Neither. Just pick the first result.
							if( resultIndex < 0 ) resultIndex = 0

							let names = results[ resultIndex ].address_components
							let place = names[ 0 ].long_name
							if( names.length > 1 ) {
								if( names[ 0 ].long_name === names[ 1 ].long_name ) place += ', ' + names[ 2 ].long_name
								else place += ', ' + names[ 1 ].long_name
							}

							let address = results[ 0 ].formatted_address

							// Store the result in a cookie to avoid repeated API calls for the same place.
							let placeCookie = { position, place, address }
							DEBUG && console.log( 'cookie.set()', placeCookie )
							cookie.set( 'location', JSON.stringify( placeCookie ), { expires: 10 } )

							// Save to redux store.
							this.props.onLocateReverse( { position, place, address } )

						} else {
							console.log( 'LocationService Geocoder: No results found' )
						}
					} else {
						throw new Error( 'Geocoder failed due to: ' + status )
					}
				}
		)
	}

	onPlacesChanged = ( places ) => {
		let location = {
			position: {
				lat: places[ 0 ].position.lat(),
				lng: places[ 0 ].position.lng()
			},
			address: places[ 0 ].address
		}
		DEBUG && console.log( 'onPlacesChanged', location )
		this.setState( { location } )
	}

	onMarkerDragEnd = ( mapEvent ) => {
		DEBUG && console.log( 'onMarkerDragEnd' )
		let eventPosition = {
			lat: mapEvent.latLng.lat(),
			lng: mapEvent.latLng.lng()
		}
		DEBUG && console.log( eventPosition )
		this.setState( { location: { position: eventPosition } } )
	}

	onUseLocation = () => {
		this.props.onLocate( this.state.location )
		this.setState( { visible: false } )
	}

	componentWillUnmount() {
		this.cancelUserDecisionTimeout()
	}

	render() {
		const { t } = this.props
		let alert
		if( this.props.manualLocationRequested ) alert = null
		else alert = {
			type: 'info', showIcon: true,
			message: t( 'You have disabled location services in your web browser.' ),
			description: t( 'Select your location to enable Treatya to show search results in your area.' )
		}

		return (
				<LocationRequestWidget
						visible={this.state.visible}
						onCancel={() => this.setState( { visible: false } )}
						onPlacesChanged={this.onPlacesChanged}
						onMarkerDragEnd={this.onMarkerDragEnd}
						onUseLocation={this.onUseLocation}
						alert={alert}
						requestManualLocation={this.props.requestManualLocation}
				/>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				location: state.location,
				account: state.account,
				componentsLoading: state.componentsLoading,
				googleApiStatus: state.googleApiStatus,
				manualLocationRequested: state.manualLocationRequested,
				deviceLocationRequested: state.deviceLocationRequested
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onComponentsLoading: ( payload ) => dispatch( { type: 'COMPONENTS_LOADING', payload } ),
				onGoogleApiStatus: ( googleApiStatus ) => dispatch( { type: 'GOOGLE_API', googleApiStatus } ),
				requestManualLocation: ( status ) => dispatch( { type: 'REQUEST_MANUAL_LOCATION', status } ),
				onLocate: ( location, deviceLocatorError ) => dispatch( {
					type: 'GEOLOCATE',
					location,
					deviceLocatorError
				} ),
				onLocateReverse: ( location ) => dispatch( { type: 'GEOREVERSE', location } )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )( LocatorService )
)
