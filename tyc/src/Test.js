import React from 'react';
import { Slider } from 'antd';
//import moment from 'moment';

class Test extends React.Component {

	state = {}

	onChange = ( bookingMoment ) => {
	}

	marks = {
		0: '0°C',
		26: '26°C',
		37: '37°C',
		100: {
			style: {
				color: '#f50',
			},
			label: <strong>100°C</strong>,
		}
	}


	render() {
		return (
				<div className="test-component content-width content-pad centered">
					<div style={{ height: 300 }}>
						<Slider vertical range marks={this.marks} defaultValue={[ 26, 37 ]}/>
					</div>
				</div>
		);
	}
}

export default Test;
