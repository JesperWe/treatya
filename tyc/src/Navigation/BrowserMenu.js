import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { Row, Col, Button, DatePicker, TimePicker } from 'antd'
import { browserHistory } from 'react-router'
import LocationMenu from './LocationMenu'
import ServiceTypeMenu from './ServiceTypeMenu'
import moment from 'moment'
import { graphql } from 'react-apollo'
import { serviceTypes } from '../graphql/browse'
import 'csshake/dist/csshake-slow.css'
import './BrowserMenu.less'

const DEBUG = true

function disabledDate( current ) {
	// Allow today but not before today.
	return moment( current ).add( 1, 'day' ).isBefore()
}

class BrowserMenu extends Component {
	state = {
		bookingDate: undefined,
		bookingTime: moment().add( 80, 'm' ).startOf( 'hour' ),
		shake: null
	}

	componentWillMount() {
		if( this.props.desiredBookingMoment ) this.setState( { bookingDate: this.props.desiredBookingMoment } )
	}

	componentWillReceiveProps( next ) {
		// Sync redux desiredServiceType with URL "t" parameter.
		if( next.query.t ) {
			if( this.props.serviceTypes && !this.props.serviceTypes.loading
					&& this.props.serviceTypes.types.indexOf( next.query.t ) >= 0 ) {
				this.props.onServiceType( next.query.t )
			}
		}

		if( next.desiredBookingMoment !== this.props.desiredBookingMoment ) {
			this.setState( {
				bookingDate: next.desiredBookingMoment,
				bookingTime: next.desiredBookingMoment,
			} )
		}
	}

	onBookingDate = ( value ) => {
		let dateTime = moment( value )
		if( value.isBefore() ) dateTime = moment()

		dateTime.hour( this.state.bookingTime.hour() )
		dateTime.minute( this.state.bookingTime.minute() )
		dateTime.startOf( 'minute' )
		if( dateTime.isBefore() ) dateTime.add( 1, 'days' )
		this.setState( { bookingMoment: dateTime } )
		this.props.onBookingMoment( dateTime )
	}

	onBookingTime = ( value ) => {
		if( !value ) return
		let dateTime = moment( this.state.bookingDate )
		if( !dateTime ) dateTime = moment()
		dateTime.hour( value.hour() )
		dateTime.minute( value.minute() )
		dateTime.startOf( 'minute' )
		this.setState( { bookingMoment: dateTime } )
		this.props.onBookingMoment( dateTime )
	}

	onServiceType = ( value ) => {
		DEBUG && console.log( 'ServiceType: ', value )
		this.props.onServiceType( value )
		if( !this.props.landingPage ) this.doSearch( value )
	}

	doSearch = ( nextType ) => {
		if( !this.props.desiredServiceType ) {
			this.setState( { shake: 'ServiceType' } )
			setTimeout( () => {
				this.setState( { shake: null } )
			}, 900 )
			return
		}

		this.props.counter && this.props.counter()

		if( typeof nextType === 'string' )
			browserHistory.push( '/?t=' + nextType )
		else
			browserHistory.push( '/?t=' + this.props.desiredServiceType )
	}

	render() {
		const { t } = this.props

		let dropdownAlign
		if( this.props.landingPage ) {
			dropdownAlign = {
				points: [ 'bl', 'tl' ], offset: [ 0, -4 ],
				overflow: { adjustX: 0, adjustY: 0 /* means do not auto-flip */ }
			}
		} else {
			dropdownAlign = {
				points: [ 'tl', 'bl' ], offset: [ 0, 4 ],
				overflow: { adjustX: 0, adjustY: 0 /* means do not auto-flip */ }
			}
		}

		// Spans are dependent on landingpage since we do not show the search button on the result view.
		let colSpans = this.props.landingPage ? {
			sm: [ 12, 12, 7, 5, 12 ],
			md: [ 12, 12, 7, 5, 12 ],
			lg: [ 6, 5, 4, 3, 6 ]
		} : {
			sm: [ 12, 12, 14, 10, 0 ],
			md: [ 8, 6, 6, 4, 0 ],
			lg: [ 8, 6, 6, 4, 0 ]
		}

		return (
				<div className={'browser-menu ' + this.props.decoration}
					 style={{ backgroundImage: 'url(' + this.props.background + ')' }}>
					<div className="menu content-width">

						{this.props.intro &&
						<h2 className="landing-text" dangerouslySetInnerHTML={{ __html: this.props.intro }}/>}

						<Row gutter={8}>

							<Col xs={{ span: 24, order: 1 }} sm={{ span: colSpans.sm[ 0 ], order: 1 }}
								 md={{ span: colSpans.md[ 0 ], order: 1 }} lg={{ span: colSpans.lg[ 0 ], order: 1 }}>
								<LocationMenu {...this.props} size="large" dropdownAlign={dropdownAlign}/>
							</Col>

							<Col xs={{ span: 24, order: 2 }} sm={{ span: colSpans.sm[ 1 ], order: 2 }}
								 md={{ span: colSpans.md[ 1 ], order: 2 }} lg={{ span: colSpans.lg[ 1 ], order: 2 }}>
								<div className="shake-trigger">
									<div className={this.state.shake === 'ServiceType' ? 'shake-slow	shake-constant' : ''}>
										<ServiceTypeMenu onChange={this.onServiceType} size="large"
														 placeholder={t( 'Choose category or type search...' )}
														 value={this.props.desiredServiceType}
														 dropdownAlign={dropdownAlign}/>
									</div>
								</div>
							</Col>

							<Col xs={{ span: 14, order: 3 }} sm={{ span: colSpans.sm[ 2 ], order: 3 }}
								 md={{ span: colSpans.md[ 2 ], order: 3 }} lg={{ span: colSpans.lg[ 2 ], order: 3 }}>
								<DatePicker size="large" allowClear={false}
											value={this.state.bookingDate}
											className={'booking-date'}
											placeholder={t( 'Today' )}
											onChange={this.onBookingDate}
											disabledDate={disabledDate}
											format="YYYY-MM-DD"
											dropdownAlign={dropdownAlign}/>
							</Col>

							<Col xs={{ span: 10, order: 4 }} sm={{ span: colSpans.sm[ 3 ], order: 4 }}
								 md={{ span: colSpans.md[ 3 ], order: 4 }} lg={{ span: colSpans.lg[ 3 ], order: 4 }}>
								<TimePicker format="HH:mm" size="large"
											value={this.state.bookingTime}
											onChange={this.onBookingTime}
											minuteStep={15} inputReadOnly={true}
											hideDisabledOptions={true} focusOnOpen={false}
											dropdownAlign={dropdownAlign}/>
							</Col>

							{this.props.landingPage &&
							<Col xs={{ span: 24, order: 5 }} sm={{ span: colSpans.sm[ 4 ], order: 5 }}
								 md={{ span: colSpans.md[ 4 ], order: 5 }} lg={{ span: colSpans.lg[ 4 ], order: 5 }}>
								<Button type="primary responsive" size="large"
										onClick={this.doSearch}>{t( 'Find available treatments' )}</Button>
							</Col>
							}

						</Row>
					</div>
				</div>
		)
	}
}

export default graphql( serviceTypes.gql, serviceTypes.options )(
		translate( [ 'common' ], { wait: true } )( BrowserMenu )
)
