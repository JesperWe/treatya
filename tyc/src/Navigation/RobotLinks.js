import React from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import gql from 'graphql-tag'
//import _get from 'lodash/get'
import { withApollo } from 'react-apollo'
import http from 'axios'
import errorHandler from '../errorHandler'
import config from '../config'
import './SiteMenu.less'

const gqlAllSuppliers = gql`
query {
  allSuppliers {
    nodes {
      id
      title
      description
      productsBySupplierId {
        nodes {
          id
          prodTitle
          published
        }
      }}}}`

class RobotLinks extends React.Component {

	state = {}

	loadStaticPages = () => {
		this.setState( { loading: true } )
		http.post( config.cms.getCollection + 'pages?token=' + config.cms.token )
				.then( result => {
					const staticPages = result.data.entries
					this.setState( { staticPages } )
				} )
				.catch( errorHandler )
	}

	loadSuppliers = () => {
		this.props.client.query( { query: gqlAllSuppliers } )
				.then( result => {
					this.setState( { suppliers: result.data.allSuppliers.nodes } )
				} )
				.catch( errorHandler )
	}

	componentDidMount() {
		this.loadSuppliers()
		this.loadStaticPages()
	}

	render() {
		const { staticPages } = this.state
		const lang = this.props.language === 'en' ? '' : '_' + this.props.language

		const aboutPage = staticPages && staticPages.length && staticPages.find( p => p.route === 'about' )

		return (
				<div>

					{this.props.landingPage &&
					<div className="description">
						{aboutPage &&
						<div dangerouslySetInnerHTML={{ __html: aboutPage.body_sv }}/>
						}
					</div>
					}

					<div className="site-links">

						{staticPages && staticPages.length &&
						staticPages.map( page => (
								<div key={page.route}>
									<a href={'/page/' + page.route}>{page[ 'title' + lang ]}</a>
								</div>
						) )
						}

					</div>
				</div>
		)
	}
}

// {suppliers && suppliers.length &&
// suppliers
// 		.filter( supplier => {
// 			const prods = _get( supplier, 'productsBySupplierId.nodes' )
// 			if( !prods ) return false
// 			return prods.findIndex( p => p.published === true ) >= 0
// 		} )
// 		.map( supplier => (
// 				<div key={supplier.id}>
// 					<h3>{supplier.title}</h3>
// 					<div className="description">{supplier.description}</div>
// 				</div>
// 		) )
// }

export default withApollo(
		connect(
				function mapStateToProps( state ) {
					return {
						language: state.language,
						authToken: state.authToken,
						authInfo: state.authInfo,
						account: state.account,
						supplierInfo: state.supplierInfo,
						counters: state.counters,
						location: state.location,
						staticPages: state.staticPages,
						landingPage: state.onLandingPage,
						deviceLocatorError: state.deviceLocatorError,
						desiredBookingMoment: state.desiredBookingMoment
					}
				},
				function mapDispatchToProps( dispatch ) {
					return {
						onLanguage: ( language ) => dispatch( { type: 'LANGUAGE', language } ),
						onLogout: () => dispatch( { type: 'LOGOUT' } ),
						requestManualLocation: ( status ) => dispatch( {
							type: 'REQUEST_MANUAL_LOCATION',
							status
						} ),
						requestDeviceLocation: ( status ) => dispatch( {
							type: 'REQUEST_DEVICE_LOCATION',
							status
						} ),
						onBookingMoment: ( bookingMoment ) => dispatch( { type: 'BOOKINGMOMENT', bookingMoment } ),
						onServiceType: ( serviceType ) => dispatch( { type: 'SERVICETYPE', serviceType } ),
						onLocate: ( location ) => dispatch( { type: 'GEOLOCATE', location } ),
						onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } ),
						onStaticPages: ( staticPages ) => dispatch( { type: 'STATICPAGES', staticPages } )
					}
				}
		)(
				translate( [ 'common' ], { wait: true } )( RobotLinks )
		)
)
