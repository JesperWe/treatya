import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { translate } from 'react-i18next'
import screenfull from 'screenfull'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'
import _get from 'lodash/get'
import { Menu, Row, Col, Icon, Tooltip } from 'antd'
import AccountButton from './AccountButton'
import moment from 'moment'
import http from 'axios'
import errorHandler from '../errorHandler'
import i18n from '../i18n'
import config from '../config'
import './SiteMenu.less'

const SubMenu = Menu.SubMenu

class SiteMenu extends React.Component {
	state = {
		currentMenu: '1',
		openKeys: [],
		menuOpen: false,
		betaTag: false,
		isFullscreen: false,
		loading: false,
		supplierTipVisible: 0,
		staticPagesLoaded: false
	}

	showSupplierTooltip( props ) {
		if( props.landingPage && this.state.supplierTipVisible === 0 ) {
			setTimeout( () => this.setState( { supplierTipVisible: 1 } ), 2000 )
			setTimeout( () => this.setState( { supplierTipVisible: 2 } ), 12000 )
			return
		}
		if( !props.landingPage && this.state.supplierTipVisible === 1 )
			this.setState( { supplierTipVisible: 2 } )
	}

	componentWillMount() {
		this.showSupplierTooltip( this.props )
		const urlParams = new URLSearchParams( window.location.search )
		if( urlParams.has( 'robot' ) ) this.setState( { clientIsRobot: true } )
	}

	componentWillReceiveProps( next ) {
		this.showSupplierTooltip( next )
	}

	componentDidMount() {
		// On iOS screenfull is boolean === true...???
		if( typeof screenfull === 'object' ) {
			screenfull.onchange( () => {
				this.setState( { isFullscreen: screenfull.isFullscreen } )
			} )
		} else {
			this.setState( { isFullscreen: true } ) // So iOS will never show fullscreen.
		}
	}

	handleClick = ( event ) => {
		this.setState( { currentMenu: event.key } )
		const { staticPages } = this.props

		let selectedItem = event.key.split( '-' )
		switch( selectedItem[ 0 ] ) {
			case 'top':
				this.toggleMenu()
				break
			case 'language':
				let lang = selectedItem[ 1 ]
				i18n.changeLanguage( lang )
				this.props.onLanguage( lang )
				// We need to kick the Router to re-render, otherwise React Context does not reach the AntD components in
				// BrowserMenu. This will break on some pages, but hopefully people don't change language down there...
				browserHistory.push( window.location.pathname + '?language=' + lang )
				moment.locale( lang )
				this.toggleMenu()
				if( this.props.authInfo )
					this.props.accountLanguage( this.props.authInfo.id, lang )
				break
			default:
				let page = staticPages.find( page => page.route === event.key )

				let gotoRoute

				if( page ) {
					gotoRoute = '/page/' + page.route
					if( page.external ) gotoRoute = '/' + page.route
				} else {
					gotoRoute = '/' + event.key
				}

				browserHistory.push( gotoRoute )
				this.toggleMenu()
				break
		}
	}

	onOpenChange = ( openKeys ) => {
		const state = this.state
		const latestOpenKey = openKeys.find( key => !(state.openKeys.indexOf( key ) > -1) )
		const latestCloseKey = state.openKeys.find( key => !(openKeys.indexOf( key ) > -1) )

		let nextOpenKeys = []
		if( latestOpenKey ) {
			nextOpenKeys = this.getAncestorKeys( latestOpenKey ).concat( latestOpenKey )
		}
		if( latestCloseKey ) {
			nextOpenKeys = this.getAncestorKeys( latestCloseKey )
		}
		this.setState( { openKeys: nextOpenKeys } )
	}

	getAncestorKeys( key ) {
		const map = {
			sub3: [ 'sub2' ],
		}
		return map[ key ] || []
	}

	goHome = () => {
		browserHistory.push( '/' )
		this.props.onLandingPage( true )
		this.props.onServiceType( undefined )
	}

	goFullscreen = ( event ) => {
		if( screenfull.enabled ) {
			screenfull.request()
		}
	}

	loadStaticPages = () => {
		this.setState( { loading: true } )
		http.post( config.cms.getCollection + 'pages?token=' + config.cms.token )
				.then( result => {
					const staticPages = result.data
					if( staticPages.error ) throw Error( 'Static Pages load failed. Menu will not be complete. ' + staticPages.error )
					this.props.onStaticPages( staticPages.entries )
					this.setState( { staticPagesLoaded: true, loading: false } )
				} )
				.catch( errorHandler )
	}

	toggleMenu = () => {
		this.setState( { supplierTipVisible: false } )

		if( !this.state.staticPagesLoaded && !this.state.loading ) this.loadStaticPages()

		this.setState( {
			menuOpen: !this.state.menuOpen,
			currentMenu: undefined
		} )
	}

	disabledMinutes = ( hour ) => {
		let minutes = []
		for( let i = 0; i < 60; i++ ) if( i % 5 !== 0 ) minutes.push( i )
		return minutes
	}

	render() {
		const { t, staticPages } = this.props
		const { staticPagesLoaded, clientIsRobot } = this.state

		const isSupplier = _get( this.props, 'account.suppliersByAccountId.nodes[0].id' ) > 0
		const lang = this.props.language === 'en' ? '' : '_' + this.props.language
		const appleFullscreen = window.matchMedia( "(max-width: 500px)" ).matches && window.navigator.standalone

		return (
				<div className={'site-menu' +
				((this.props.landingPage) ? ' landing' : '') +
				(!!this.props.authInfo ? ' login' : ' logout')}>
					<div className={'sidebar' + (appleFullscreen ? ' apple-fullscreen' : '')}
						 style={{ left: this.state.menuOpen ? '0' : '-240px' }}>
						<Menu
								mode="inline" theme="dark"
								openKeys={this.state.openKeys}
								selectedKeys={[ this.state.currentMenu ]}
								style={{ width: 240 }}
								onOpenChange={this.onOpenChange}
								onClick={this.handleClick}
						>
							<Menu.Item key="top-close">Treatya <Icon type="close"
																	 onClick={this.toggleMenu}/></Menu.Item>

							<SubMenu key="sub1" title={<span><Icon type="flag"/> {t( 'Language' )}</span>}>
								<Menu.Item key="language-sv">{t( 'Swedish' )}</Menu.Item>
								<Menu.Item key="language-en">{t( 'English' )}</Menu.Item>
							</SubMenu>

							{(this.props.authInfo && this.props.authInfo.role === 'admin') &&
							<SubMenu key="sub-admin" title={<span><Icon type="lock"/> {t( 'Admin' )}</span>}>
								<Menu.Item key="admin/users">
									<span><Icon type="team"/> {t( 'Users' )}</span>
								</Menu.Item>
								<Menu.Item key="admin/stripe">
									<span><Icon type="credit-card"/> {t( 'Stripe' )}</span>
								</Menu.Item>
								<Menu.Item key="admin/bookings">
									<span><Icon type="calendar"/> {t( 'Bookings' )}</span>
								</Menu.Item>
								<Menu.Item key="admin/settings">
									<span><Icon type="setting"/> {t( 'Settings' )}</span>
								</Menu.Item>
							</SubMenu>
							}

							{!isSupplier &&
							<Menu.Item key="register/supplier">
								<span><Icon type="team"/> {t( 'Become Supplier' )}</span>
							</Menu.Item>
							}

							{staticPagesLoaded && staticPages.filter( page => page.menu ).map( page =>
									<Menu.Item key={page.route}>
										<span><Icon type={page.icon}/> {page[ 'title' + lang ]}</span>
									</Menu.Item>
							)}
						</Menu>
					</div>

					<div className={'topbar' + (appleFullscreen ? ' apple-fullscreen' : '')}>

						{appleFullscreen &&
						<div className="iPhone-status-bar" style={{ height: '18px' }}></div>
						}

						<Row type="flex" gutter={12} style={{ justifyContent: 'space-between' }}>

							<Col xs={{ span: 9, order: 1 }} sm={{ span: 5, order: 1 }}>
								<div className="hamburger">
									<Tooltip placement="right" overlayClassName="supplier-splash"
											 visible={!isSupplier && (this.state.supplierTipVisible === 1)}
											 title={
												 <span onClick={() => browserHistory.push( '/register/supplier' )}>{t( 'Click here to register as a treatment supplier!' )}</span>
											 }>
										<Icon type="hamburger" className="treatya-iconfont" style={{ fontSize: '22px' }}
											  onClick={this.toggleMenu} alt="Menu"/>
									</Tooltip>
								</div>
							</Col>

							<Col xs={{ span: 6, order: 2 }} sm={{ span: 7, order: 2 }}>
								<div className="site-branding">
									<div className="logo" onClick={this.goHome}>
										{clientIsRobot && <h1>Treatya</h1>}
										{!clientIsRobot &&
										<Icon type="treatya" className="treatya-iconfont" style={{ fontSize: '7rem' }}/>
										}
									</div>
								</div>
							</Col>

							<Col xs={{ span: 9, order: 3 }} sm={{ span: 5, order: 4 }}
								 className="push-right top-pad no-wrap">
								<AccountButton {...this.props}/>
								{window.matchMedia( "(max-width: 600px)" ).matches && !this.state.isFullscreen && !window.navigator.standalone &&
								<Icon type="arrows-alt" onClick={this.goFullscreen}
									  style={{ cursor: 'pointer', marginLeft: '10px', fontSize: '22px' }}/>
								}
							</Col>

						</Row>
					</div>

				</div>
		)
	}
}

const gqlAccountLanguage = gql`
mutation ($id: Uuid!, $lang: String!) {
  updateAccountById(input: {id: $id, accountPatch: {language: $lang}}) {
    account {
      id
      language
    }
  }
}`
let accountLanguageOptions = {
	props: ( { mutate } ) => ({
		accountLanguage: ( id, lang ) => mutate( {
			variables: { id, lang },
			refetchQueries: [ 'accountInfo' ]
		} )
	})
}

export default graphql( gqlAccountLanguage, accountLanguageOptions )(
		connect(
				function mapStateToProps( state ) {
					return {
						language: state.language,
						authToken: state.authToken,
						authInfo: state.authInfo,
						account: state.account,
						supplierInfo: state.supplierInfo,
						counters: state.counters,
						location: state.location,
						staticPages: state.staticPages,
						landingPage: state.onLandingPage,
						deviceLocatorError: state.deviceLocatorError,
						desiredBookingMoment: state.desiredBookingMoment
					}
				},
				function mapDispatchToProps( dispatch ) {
					return {
						onLanguage: ( language ) => dispatch( { type: 'LANGUAGE', language } ),
						onLogout: () => dispatch( { type: 'LOGOUT' } ),
						requestManualLocation: ( status ) => dispatch( { type: 'REQUEST_MANUAL_LOCATION', status } ),
						requestDeviceLocation: ( status ) => dispatch( { type: 'REQUEST_DEVICE_LOCATION', status } ),
						onBookingMoment: ( bookingMoment ) => dispatch( { type: 'BOOKINGMOMENT', bookingMoment } ),
						onServiceType: ( serviceType ) => dispatch( { type: 'SERVICETYPE', serviceType } ),
						onLocate: ( location ) => dispatch( { type: 'GEOLOCATE', location } ),
						onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } ),
						onStaticPages: ( staticPages ) => dispatch( { type: 'STATICPAGES', staticPages } )
					}
				}
		)(
				translate( [ 'common' ], { wait: true } )( SiteMenu )
		)
)
