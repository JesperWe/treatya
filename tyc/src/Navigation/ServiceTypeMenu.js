import React from 'react'
import { translate } from 'react-i18next'
import { browserHistory } from 'react-router'
import errorHandler from '../errorHandler'
import { withApollo, graphql } from 'react-apollo'
import { serviceTypes, freeTextSearch } from '../graphql/browse'
import './ServiceTypeMenu.less'
import config from '../config'
import { AutoComplete, Input, Icon, Select } from 'antd'
const { Option, OptGroup } = AutoComplete

const DEBUG = false

class ServiceTypeMenu extends React.Component {

	state = {
		value: undefined,
		showSearch: false,
		types: [],
		suppliers: [],
		products: [],
		options: []
	}

	createOptions = ( serviceTypes ) => {
		const { t } = this.props
		if( serviceTypes && serviceTypes.types && this.state.types.length === 0 ) {
			let types = serviceTypes.types.map( type => (
					<Option value={type} key={type}>{t( type )}</Option>
			) )

			types.unshift( <Option value="search" key="search"><Icon type="search"/> {t( 'Search...' )}</Option> )
			this.setState( { types } )
		}
	}

	componentWillMount() { // Needed to set state when navigating history, componentWillReceiveProps does not fire.
		DEBUG && console.log( 'ServiceTypeMenu.componentWillMount()' )
		let { serviceTypes } = this.props
		this.createOptions( serviceTypes )
	}

	componentWillReceiveProps( next ) {
		//DEBUG && console.log( 'ServiceTypeMenu.componentWillReceiveProps()', next )
		let { serviceTypes } = next
		this.createOptions( serviceTypes )
	}

	onSelect = ( value, option ) => {
		DEBUG && console.log( 'onSelect( value, label )', value, option.props.children )

		if( value === 'search' ) {
			this.setState( { value: undefined, showSearch: true } )
			setTimeout( () => document.getElementById( 'search-input' ).focus(), 100 )
		} else {
			this.setState( { value: undefined } )
			if( this.props.onChange ) this.props.onChange( value, option )
		}
	}

	onSearchSelect = ( value, option ) => {
		DEBUG && console.log( 'onSelect( value, label )', value, option.props.children )
		config.tracking && window._paq.push( [ 'trackSiteSearch', option.props.children ] )

		this.doNotRunOnChangeAfterOnSelectFlag = true
		let ids = value.split( '-' )
		if( ids.length === 2 && (+ids[ 0 ] > 0) ) {
			browserHistory.push( '/supplier/' + ids[ 0 ] )
			return
		}
		this.setState( { value: undefined } )
		if( this.props.onChange ) this.props.onChange( value, option )
		return false
	}

	onChange = ( searchValue, label ) => {
		DEBUG && console.log( 'onChange( value, label )', this.doNotRunOnChangeAfterOnSelectFlag, searchValue, label )

		if( this.doNotRunOnChangeAfterOnSelectFlag ) return

		if( searchValue.length < 2 ) {
			this.setState( {
				searchValue,
				options: [],
				suppliers: [],
				products: []
			} )
			return
		}

		// We need to keep track of the running query, since <AutoComplete> will not re-render the menu when
		// options change (as a result of the query returning), if the value has not changed too. So when value changes
		// we block component updates until query is complete.
		this.queryRunning = true

		DEBUG && console.log( '... Do query' )
		config.tracking && window._paq.push( [ 'trackSiteSearch', searchValue ] )

		this.props.client.query( {
					query: freeTextSearch.gql,
					variables: { term: searchValue },
					fetchPolicy: 'network-only'
				} )
				.then( result => {
					DEBUG && console.log( '... From query:', result.data.freeTextSearch.nodes.length )
					this.queryRunning = false
					let suppliers = []
					let products = []

					result.data.freeTextSearch.nodes.forEach( ( hit, index ) => {

						let opt = <Option value={ hit.supplierId + '-' + (hit.productId || '0')  } key={ 'F' + index }>
							{ hit.title }
						</Option>

						if( hit.productId )
							products.push( opt )
						else
							suppliers.push( opt )
					} )

					let options = []
					if( suppliers.length + products.length > 0 ) {
						if( suppliers.length ) options.push(
								<OptGroup key="S" label={this.props.t( 'Suppliers' )}>{suppliers}</OptGroup>
						)

						if( products.length ) options.push(
								<OptGroup key="P" label={this.props.t( 'Treatments' )}>{products}</OptGroup>
						)
					}

					this.setState( { options, searchValue, suppliers, products } )
				} )
				.catch( error => {
					this.queryRunning = false
					errorHandler( error )
				} )
	}

	fakeFilter = ( inputValue, option ) => {
		DEBUG && console.log( '.......fake', inputValue, option.props )
		return true // Needed to fool AutoComplete into showing the menu as we search.
	}

	realFilter = ( option ) => {
		DEBUG && console.log( '_____real', option.props )
		if( !option ) return false
		if( !this.state.value ) return true
		return option.props.children.toLowerCase().indexOf( this.state.value.toLowerCase() ) !== -1
	}

	render() {
		const { t } = this.props

		DEBUG && console.log( 'render() %s suppliers %s products, search %s', this.state.suppliers.length, this.state.products.length, this.state.searchValue )

		this.doNotRunOnChangeAfterOnSelectFlag = false

		let value // State has priority over props
		if(this.state.value) value = this.state.value
		else if( this.props.value ) value = this.props.value

		let searchClass = 'hide'
		let selectClass = 'show'
		if( this.state.showSearch ) {
			searchClass = 'show'
			selectClass = 'hide'
			DEBUG && console.log( '   ...options', JSON.stringify( this.state.options ) )
		}

		return (
				<div className="service-type-menu">

					<div className={searchClass}>
						<AutoComplete
								size={this.props.size}
								value={this.state.searchValue}
								dataSource={this.state.options}
								dropdownMatchSelectWidth={true}
								onChange={this.onChange}
								onSelect={this.onSearchSelect}
								placement="topCenter"
								placeholder={t( 'Search text...' )}
								filterOption={this.fakeFilter}
								dropdownAlign={this.props.dropdownAlign}>

							<Input size="large" id="search-input" suffix={
								<span><Icon type="search"/> <Icon type="close-circle"
																  onClick={() => this.setState( {
																	  showSearch: false,
																	  options: [],
																	  searchValue: undefined
																  } )}/>
							</span>
							}/>
						</AutoComplete>
					</div>

					<div className={selectClass}>
						<Select children={this.state.types}
								value={value} size={this.props.size}
								dropdownMatchSelectWidth={true}
								onSelect={this.onSelect}
								placement="topCenter"
								placeholder={this.props.placeholder}
								dropdownAlign={this.props.dropdownAlign}>
						</Select>
					</div>
				</div>
		)
	}
}

export default
withApollo(
		graphql( serviceTypes.gql, serviceTypes.options )(
				translate( [ 'common' ], { wait: true } )(
						ServiceTypeMenu
				)
		)
)
