import React, { Component } from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import { Select, Icon } from 'antd'

const { Option } = Select

class LocationMenu extends Component {

	state = {
		value: []
	}

	handleMenuClick = ( value ) => {
		switch( value ) {
			case 'select':
				this.props.requestManualLocation( true )
				this.setState( { value: [] } )
				break
			case 'device':
				this.props.requestDeviceLocation( true )
				this.props.onComponentsLoading( true )
				this.setState( { value: [] } )
				break
			case 'home':
				this.props.onLocate( { position: this.props.account.location.position } )
				this.setState( { value: [] } )
				break
			default:
		}
	}

	render() {
		const { t } = this.props

		let buttonText = this.props.location && this.props.location.address
		if( !buttonText ) buttonText = t( 'Select location...' )
		buttonText = (<span><Icon type="environment" className="treatya-icon"/> {buttonText}</span>)

		let options = [ {
			value: 'select',
			label: (<span><Icon type="environment" className="treatya-icon"/> {t( 'Select manual location...' )}</span>)
		} ]

		if( !this.props.deviceLocatorError )
			options.push( {
				value: 'device',
				label: (<span><Icon type="scan" className="treatya-icon"/> {t( 'Device auto-location' )}</span>)
			} )

		if( this.props.account && this.props.account.location && this.props.account.location.position )
			options.push( {
				value: 'home',
				label: (<span><Icon type="home" className="treatya-icon"/> {t( 'Home location' )}</span>)
			} )

		return (
				<div className={"location-menu " + this.props.className}>
					<Select value={this.state.value} placeholder={buttonText} size={this.props.size}
							onChange={this.handleMenuClick}
							dropdownAlign={this.props.dropdownAlign}>
						{options.map( option => (
								<Option value={option.value} key={option.value}>{option.label}</Option>
						) )}
					</Select>
				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onComponentsLoading: ( payload ) => dispatch( { type: 'COMPONENTS_LOADING', payload } )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )( LocationMenu )
)
