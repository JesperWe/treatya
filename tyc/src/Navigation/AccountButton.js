import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { Menu, Dropdown, Icon, Badge } from 'antd'
import cookie from 'browser-cookies'
import config from '../config'

import './AccountButton.less'

class AccountButton extends Component {

	state = {
		visible: false
	}

	handleVisibleChange = ( flag ) => {
		this.setState( { visible: flag } )
	}

	go = ( event, uri ) => {
		event.stopPropagation()
		event.preventDefault()
		browserHistory.push( uri )
		this.setState( { visible: false } )
	}

	logout = ( event ) => {
		cookie.erase( 'auth-token' )
		config.tracking && window._paq.push( [ 'trackEvent', 'Account', 'Logout' ] )
		this.props.onLogout()
		this.props.onLandingPage( true )
		this.setState( { visible: false } )
		browserHistory.push( '/' )

	}

	stopImpersonation = () => {
		cookie.set( 'auth-token', cookie.get( 'admin-token' ) )
		cookie.erase( 'admin-token' )
		window.location.reload()
	}

	render() {
		const { t } = this.props
		const stripeNeeded = this.props.supplierInfo && this.props.supplierInfo.stripeNeeded

		const accountDropdown =
				<Menu className="account-menu">

					<Menu.Item>
						<a onClick={event => this.go( event, '/profile' )}>
							<Icon type="user"/> {t( 'Account Settings' )}</a>
					</Menu.Item>

					{this.props.account && this.props.account.supplierId && this.props.authInfo &&
					(this.props.authInfo.role === 'supplier' || this.props.authInfo.role === 'admin' ) &&
					<Menu.Item>
						<a onClick={event => this.go( event, '/calendar' )}>
							<Icon type="calendar"/> {t( 'My Calendar' )}</a>
					</Menu.Item>
					}

					<Menu.Item>
						<a onClick={event => this.go( event, '/booking-overview' )}>
							<Icon type="schedule"/> {t( 'My Bookings' )}</a>
					</Menu.Item>

					{ stripeNeeded && stripeNeeded.length > 0 && this.props.authInfo &&
					<Menu.Item>
						<a onClick={event => this.go(event, '/stripe' )} className="warning">
							<Icon type="exclamation-circle" /> {t( 'Action needed' )}</a>
					</Menu.Item>
					}

					<Menu.Item>
						<a onClick={this.logout}><Icon type="logout"/> {t( 'Logout' )}</a>
					</Menu.Item>

					{ cookie.get( 'admin-token' ) !== null &&
					<Menu.Item>
						<a onClick={this.stopImpersonation}>
							<Icon type="logout"/> Stop Impersonation</a>
					</Menu.Item>
					}

					{ this.props.authInfo && this.props.authInfo.role === 'admin' &&
					<Menu.Item>
						<a onClick={event => this.go( event, '/test' )}>
							<Icon type="right"/> Test</a>
					</Menu.Item>
					}
				</Menu>

		let { counters } = this.props
		let clientCount = 0, supplierCount = 0, msgCount = 0
		if( counters ) {
			clientCount = +counters.bookingsForAccount || 0
			supplierCount = ((+counters.bookingsPreliminary) + (+counters.bookingsConfirmed)) || 0
			msgCount = +counters.unreadMessages || 0
		}

		let accountMenu = this.props.authToken ?
				<div className="account-button">

					<a onClick={() => browserHistory.push( '/booking-overview' )} className="account-button-badge messages">
						<Badge count={msgCount}/>
					</a>

					<a onClick={() => browserHistory.push( '/booking-overview' )} className="account-button-badge preliminary">
						<Badge count={clientCount}/>
					</a>

					<a onClick={() => browserHistory.push( '/calendar' )} className="account-button-badge confirmed">
						<Badge count={supplierCount}/>
					</a>

					<Dropdown overlay={accountDropdown} trigger={[ 'click' ]}  placement="bottomRight"
							  visible={this.state.visible} onVisibleChange={this.handleVisibleChange}>
						<a className="ant-dropdown-link" title={this.props.account.fullName}>
							<div className="first-name">{this.props.account.firstName}</div>
							<Icon type="user" style={{ fontSize: '22px' }}/>
						</a>
					</Dropdown>
				</div>
				:
				<a className="login" onClick={() => browserHistory.push( '/login' )}>{t( 'Login' )}</a>

		return (
				<div className="account-menu">{accountMenu}</div>
		)
	}
}

export default AccountButton
