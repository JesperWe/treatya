import React, { Component } from 'react';
import { translate } from 'react-i18next';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import 'moment/locale/sv';
import { message } from 'antd';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import './Calendar.css';

const DEBUG = true;

moment.locale( 'sv' );
BigCalendar.momentLocalizer( moment );

let formats = {
	agendaTimeRangeFormat: ( { start, end }, culture, local ) => {
		return local.format( start, 'HH:mm', culture ) + ' — ' +
				local.format( end, 'HH:mm', culture );
	},
	timeGutterFormat: 'HH:mm',
	eventTimeRangeFormat: ( { start, end }, culture, local ) => {
		return local.format( start, 'HH:mm', culture ) + ' - ' +
				local.format( end, 'HH:mm', culture );
	},
	selectRangeFormat: ( { start, end }, culture, local ) => {
		return local.format( start, 'HH:mm', culture ) + ' - ' +
				local.format( end, 'HH:mm', culture );
	}
}

function HMM( date ) {
	return moment( date ).format( 'H:mm' );
}

function Event( event ) {
	return (
			<span>
				<strong>{event.title}</strong>
				<br/>
				{ event.desc}
			</span>
	)
}

function eventPropGetter( event, start, end, isSelected ) {
	DEBUG && console.log( 'eventPropGetter', event.title, isSelected )
	let colors = ['#f7f7f7', '#ffffcc','#c7e9b4','#7fcdbb','#41b6c4','#2c7fb8','#253494'];
	return {
		className: '',
		style: { backgroundColor: colors[+event.className] }
	}
}

class Calendar extends Component {
	state = {
		modalVisible: false,
		selecting: true,
		event: null,
		eventPopup: null
	}

	deleteTimeslot = ( id ) => {
		this.props.deleteTimeslot( id )
				.catch( error => {
					console.log( error.message );
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Timeslot Delete' ] );
					notification.error( { message:  error.message } )
				} )
	}

	onSelectSlot = ( info ) => { // info: object { start: Date, end: Date, slots: array<Date> }
		console.log( 'onSelectSlot()', HMM( info.start ), HMM( info.end ), info.className );
		this.setState( { selecting: false } );
		if( !info.start || !info.end ) return;
		if( info.start.getTime() === info.end.getTime() ) return;

		let start = moment( info.start );
		let end = moment( info.end );

		if( start.isBefore() ) return;

		// Make whole day selection include the last day.
		if( start.hour() + start.minute() + end.hour() + end.minute() === 0 ) {
			end.add( 23, 'h' ).add( 59, 'm' );
		}

		this.props.createTimeslot( {
					supplierId: this.props.account.supplierId,
					start: start.format(),
					end: end.format()
				} )
				.then( result => {
					notification.success( { message:  'Time slot created.' } )
				} )
				.catch( error => {
					console.log( error.message );
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Timeslot Create' ] );
					notification.error( { message:  error.message } )
				} )
	}

	onSelectEvent = ( event, domEvent ) => {
		console.log( 'onSelectEvent()', HMM( event.start ), HMM( event.end ), event.className, event.id );
		this.setEventPopup( event );
	}

	render() {
		DEBUG && console.log( 'CalendarWidget.render()' );
		return (
				<div className="calendar" style={{ height: '100%' }}>

					<BigCalendar
							events={this.props.events}
							timeslots={2} toolbar={true}
							culture='en-gb'
							defaultView='week'
							min={moment('07:00', 'hh:mm').toDate()}
							formats={formats}
							components={{ event: Event }}
							eventPropGetter={eventPropGetter}
							selectable
							onSelectSlot={this.onSelectSlot}
							onSelectEvent={this.onSelectEvent}
							onSelecting={this.onSelecting}
					/>
				</div>
		);
	}
}

export default translate( [ 'common' ], { wait: true } )( Calendar );
