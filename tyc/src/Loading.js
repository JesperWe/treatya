import React from 'react'
import { Spin } from 'antd'
import './Loading.less'

export default function( props ) {
	return (
			<div className="loading-container">
				<Spin size="large"/>
			</div>
	)
}
