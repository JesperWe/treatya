var config = {};

config.date = {
	default: 'YYYY-MM-DD H:mm',
	friendly: 'ddd D MMM H:mm',
	friendlyNoTime: 'ddd D MMM',
	noTime: 'YYYY-MM-DD'
}

config.cdn = {
	site: 'https://s3.eu-central-1.amazonaws.com/treatya-site/',
	account: 'https://s3.eu-central-1.amazonaws.com/treatya-account/',
	promo: 'https://dfc9gsjqi2t6m.cloudfront.net/',
	promoImages: [
		'tada-girl.jpg'
	]
}

config.cms = {
	url: 'https://cockpit.treatya.com/',
	getCollection: 'https://cockpit.treatya.com/api/collections/get/',
	token: '___'
}

config.location = {
	position: { lat: 59.3256828, lng: 18.0715579 },
	zoom: 11
}

config.idle = {
	timeout: 20000,
	versionCheckUrl: 'https://dev.treatya.com/index.html'
}

config.backend = {
	settings: 'live',
	url: 'https://query.treatya.com',
	charge: 'https://query.treatya.com/charge',
	event: 'https://query.treatya.com/event',
	graphql: '/q',
	pushNotifications: {
		enabled: false,
		apiKey: '___'
	}
}

config.tracking = false

config.stripe = {
	isLive: false,
	pk_test: '___',
	pk_live: '___'
}

config.google = {
	api: 'https://maps.googleapis.com/maps/api/js',
	key: '___',
	libraries: 'geometry,places'
}

config.treatya = {
	davatar: 'https://dfc9gsjqi2t6m.cloudfront.net/dominika1.jpg'
}

export default config;
