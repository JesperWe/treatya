import React from 'react';
import { Row, Col, Alert } from 'antd';

function Error404( props ) {
	return (
			<div className="error-404">
				<Row style={{ height: '100%', width: '100%', justifyContent: 'center' }} type="flex">
					<Col sm={18}>
						<Alert
								message="Oops!"
								description="There's nothing here... Did you mistype the URL maybe?"
								type="warning"
								showIcon/>
					</Col>
				</Row>
			</div>
	);
}

export default Error404;
