import moment from 'moment'
import i18n from '../i18n'

// Swedish peprsonnummer handling

function parseSwedishSSN( input ) {
	if( !input ) return false

	if( input.indexOf( '-' ) < 0 ) {
		if( input.length === 10 ) {
			input = input.slice( 0, 6 ) + "-" + input.slice( 6 );
		} else {
			input = input.slice( 0, 8 ) + "-" + input.slice( 8 );
		}
	}

	let dateOfBirth;
	let parts = input.trim().split( '-' )
	if( parts.length !== 2 ) return false

	let dateStr = parts[ 0 ];
	if( /[0-9]*/.test( dateStr ) ) {
		if( dateStr.length === 6 ) {
			dateOfBirth = moment( dateStr, 'YYMMDD' )
		}
		if( dateStr.length === 8 ) {
			dateOfBirth = moment( dateStr, 'YYYYMMDD' )
		}
		if( !dateOfBirth || !dateOfBirth.isValid() ) return false

		if( dateStr.length === 6 && dateOfBirth.isAfter( moment().subtract( 12, 'years' ) ) )
			dateOfBirth = dateOfBirth.subtract( 100, 'years' )

		if( dateOfBirth.isAfter( moment() ) ) return false
		if( dateOfBirth.isBefore( moment().subtract( 120, 'years' ) ) ) return false

		// Now the luhn checksum

		let digits = dateOfBirth.format( 'YYMMDD' ) + parts[ 1 ]
		if( digits.length !== 10 ) return false
		let digit, sum = 0

		for( let i = 0; i < 10; i = i + 1 ) {
			digit = parseInt( digits.charAt( i ), 10 );
			if( i % 2 === 0 ) {
				digit *= 2;
			}
			if( digit > 9 ) {
				digit -= 9;
			}
			sum += digit;
		}
		if( sum % 10 === 0 ) return dateOfBirth
	}
	return false
}

function isValidSwedishSSN( rule, value, callback ) {
	let state = parseSwedishSSN( value )
	if( state === false ) callback( i18n.t( 'Invalid SSN' ) )
	callback()
}

export { parseSwedishSSN, isValidSwedishSSN }
