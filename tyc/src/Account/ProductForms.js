import React from 'react'
import { translate } from 'react-i18next'
import _get from 'lodash/get'
import _set from 'lodash/set'
import _cloneDeep from 'lodash/cloneDeep'
import errorHandler from '../errorHandler'
import { Input, Form, Select, Row, Col, Button, Switch, Popconfirm, Icon, notification } from 'antd'
import config from '../config'
import { graphql, compose, withApollo } from 'react-apollo'
import { accountSupplierProducts, productCreate, productDelete, productUpdate } from '../graphql/product'
import { serviceTypes } from '../graphql/browse'
import { updateAccount } from '../graphql/account'
import './ProductForms.less'

const Option = Select.Option
const DEBUG = false

const ProductForm = Form.create()( ( props ) => {
	const { getFieldDecorator } = props.form

	let closureProps = props

	let handleSubmit = ( event ) => {
		event.preventDefault()
		closureProps.form.validateFields( ( error, values ) => {
			if( !error ) {
				DEBUG && console.log( 'Received values of form: ', values )
				let product = {
					...values,
					published: !!values.published,
					duration: { minutes: +values.duration },
					supplierId: closureProps.account.supplierId,
					newProductIndex: closureProps.disableRemove ? closureProps.product.id : undefined
				}
				closureProps.onSave( product )
			}
		} )
	}

	let handleDelete = () => {
		closureProps.onDelete( closureProps.product.id )
	}

	let durationInMinutes = ( duration ) => {
		if( !duration ) return 0
		let d = 0
		if( duration.hours ) d += duration.hours * 60
		if( duration.minutes ) d += duration.minutes
		return d
	}

	const { t } = props

	return (
			<Form className="product" onSubmit={handleSubmit} hideRequiredMark={true}>
				<Row gutter={12}>
					<Col xs={24} sm={12}>
						<Form.Item label={t( 'Product Title' )} hasFeedback={false}>
							{getFieldDecorator( 'prodTitle', {
								initialValue: props.product.prodTitle,
								validateTrigger: 'onBlur',
								rules: [ { required: true, message: t( 'Enter a product title.' ) } ]
							} )( <Input size="large" placeholder={t( 'Product Title' )}/> )}
						</Form.Item>
					</Col>
					<Col xs={24} sm={12}>
						<Form.Item label={t( 'Treatment type' )} hasFeedback={false}>
							{getFieldDecorator( 'type', {
								initialValue: props.product.type,
								validateTrigger: 'onChange',
								rules: [ { required: true, message: t( 'Select product type.' ) } ]
							} )(
									<Select size="large" placeholder={t( 'Service Type...' )}>
										{props.serviceTypes && props.serviceTypes.map( ( type ) => {
											return <Option value={type} key={type}>{t( type )}</Option>
										} )}
									</Select>
							)}
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={12}>
					<Col xs={24}>
						<Form.Item label={t( 'Product description' )} hasFeedback={false}>
							{getFieldDecorator( 'prodDescr', {
								initialValue: props.product.prodDescr,
								rules: [ { required: true, message: t( 'Enter a product description.' ) } ]
							} )( <Input.TextArea size="large" autosize={{ minRows: 2, maxRows: 6 }}
												 placeholder={t( 'Product description' )}/> )}
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={12}>
					<Col xs={14} sm={7}>
						<Form.Item label={t( 'Price' )} hasFeedback={false}>
							{getFieldDecorator( 'price', {
								initialValue: props.product.price,
								getValueFromEvent: ( e ) => {
									return +e.target.value || e.target.value
								},
								rules: [ { required: true, type: 'integer', message: t( 'Enter price amount.' ) } ]
							} )( <Input size="large" type="number" placeholder={t( 'Amount' )}/> )}
						</Form.Item>
					</Col>
					<Col xs={10} sm={5}>
						<Form.Item label={t( 'Currency' )} hasFeedback={false}>
							{getFieldDecorator( 'currency', {
								initialValue: props.product.currency,
								validateTrigger: 'onChange',
								rules: [ { required: true, message: t( 'Select price currency.' ) } ]
							} )(
									<Select size="large" placeholder={t( 'Currency...' )}>
										<Option value="SEK">SEK</Option>
										<Option value="EUR">EUR</Option>
										<Option value="NOK">NOK</Option>
										<Option value="DKK">DKK</Option>
										<Option value="GBP">GBP</Option>
									</Select>
							)}
						</Form.Item>
					</Col>
					<Col xs={16} sm={8}>
						<Form.Item label={t( 'Duration' )} hasFeedback={false}>
							{getFieldDecorator( 'duration', {
								initialValue: (durationInMinutes( props.product.duration ) || 45).toString(),
								validateTrigger: 'onChange',
								rules: [ { required: true, message: t( 'Select treatment duration.' ) } ]
							} )(
									<Select size="large" placeholder={t( 'Treatment duration...' )}>
										<Option value="15">15 min</Option>
										<Option value="20">20 min</Option>
										<Option value="30">30 min</Option>
										<Option value="45">45 min</Option>
										<Option value="60">60 min</Option>
										<Option value="75">1 h 15 min</Option>
										<Option value="90">1 h 30 min</Option>
										<Option value="104">1 h 45 min</Option>
										<Option value="120">2 h</Option>
										<Option value="150">2.5 h</Option>
										<Option value="180">3 h</Option>
										<Option value="240">4 h</Option>
										<Option value="300">5 h</Option>
									</Select>
							)}
						</Form.Item>
					</Col>
					<Col xs={8} sm={4}>
						<Form.Item label={t( 'Public' )}>
							{getFieldDecorator( 'published', {
								initialValue: props.product.published,
								valuePropName: 'checked'
							} )( <Switch checkedChildren={t( 'Yes' )} unCheckedChildren={t( 'No' )}/> )}
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={12}>
					<Col xs={12}>
						<Form.Item>
							{getFieldDecorator( 'delete', {} )(
									<Popconfirm title={t( 'Are you sure?' )} okText={t( 'Yes' )} cancelText={t( 'No' )}
												onConfirm={handleDelete}>
										<Button type="danger" size="large"
												disabled={props.disableRemove}>
											<Icon type="delete"/> {t( 'Remove' )}
										</Button>
									</Popconfirm>
							)}
						</Form.Item>
					</Col>
					<Col xs={12}>
						<Form.Item>
							{getFieldDecorator( 'save', {} )(
									<Button size="large" type="default" htmlType="submit">
										<Icon type="save"/> {t( 'Save' )}
									</Button>
							)}
						</Form.Item>
					</Col>
				</Row>
			</Form>
	)
} )

class ProductForms extends React.Component {
	state = {
		newProducts: []
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( 'componentWillReceiveProps(next):', next )
	}

	addForm = () => {
		let { newProducts } = this.state
		newProducts.push( {
			id: newProducts.length,
			published: true,
			currency: 'SEK'
		} )
		this.setState( { newProducts } )
	}

	createProduct = ( productForm ) => {
		if( productForm.newProductIndex !== undefined ) {
			let newProducts = [].concat( this.state.newProducts )
			newProducts.splice( productForm.newProductIndex, 1 )
			this.setState( { newProducts } )
		}

		this.props.productCreate( productForm, this.props.authInfo.id )
				.then( result => {
					notification.success( { message: this.props.t( 'Product saved.' ) } )

					if( !_get( this.account, 'uiSettings.supplierRegistration.steps.products' ) ) {
						let uiSettings = _cloneDeep( this.props.account.uiSettings )
						_set( uiSettings, 'supplierRegistration.steps.products', { done: true } )
						_set( uiSettings, 'infoSplash.supplierWelcome', { dismissed: true } )
						this.props.updateAccount( this.props.authInfo.id, { uiSettings } )
					}
				} )
				.catch( errorHandler )
	}

	updateProduct = ( productId, productForm ) => {
		this.props.productUpdate( productId, productForm, this.props.authInfo.id )
				.then( result => {
					notification.success( { message: this.props.t( 'Product saved.' ) } )
				} )
				.catch( error => {
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Product Save' ] )
					notification.error( { message: error.message } )
				} )
	}

	deleteProduct = ( productId ) => {
		this.props.productDelete( productId, this.props.authInfo.id )
				.then( result => {
					notification.success( { message: this.props.t( 'Product deleted.' ) } )
				} )
				.catch( error => {
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Product Delete' ] )
					notification.error( { message: error.message } )
				} )
	}

	render() {
		const { t } = this.props

		return (
				<div className="product-forms">

					{this.props.products.data && this.props.products.data.map( product =>
							<ProductForm product={product} t={t}
										 account={this.props.account}
										 onSave={( productForm ) => this.updateProduct( product.id, productForm )}
										 onDelete={this.deleteProduct} serviceTypes={this.props.serviceTypes.types}
										 key={product.id}/>
					)}

					{this.state.newProducts.map( product =>
							<ProductForm product={product} t={t}
										 account={this.props.account}
										 onSave={this.createProduct}
										 key={'n' + product.id} serviceTypes={this.props.serviceTypes.types}
										 disableRemove={true}/>
					)}

					<Row>
						<Col xs={0} sm={12}>
						</Col>
						<Col xs={24} sm={12}>
							<Button type="default" size="large" className="responsive" onClick={this.addForm}>
								<Icon type="plus-circle-o"/> {t( 'Add Product' )}
							</Button>
						</Col>
					</Row>

				</div>
		)
	}
}

ProductForms.defaultProps = { products: [] }

export default withApollo( compose(
		graphql( serviceTypes.gql, serviceTypes.options ),
		graphql( accountSupplierProducts.gql, accountSupplierProducts.options ),
		graphql( productCreate.gql, productCreate.options ),
		graphql( productUpdate.gql, productUpdate.options ),
		graphql( productDelete.gql, productDelete.options ),
		graphql( updateAccount.gql, updateAccount.options )
)( translate( [ 'common' ], { wait: true } )( ProductForms ) ) )
