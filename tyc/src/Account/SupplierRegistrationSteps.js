import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _get from 'lodash/get'
import { translate } from 'react-i18next'
import { graphql } from 'react-apollo'
import { updateAccount } from '../graphql/account'
import { Steps, Progress, Icon } from 'antd'
import './SupplierRegistrationSteps.less'

const { Step } = Steps

class SupplierRegistrationSteps extends React.Component {

	uiSettings = {
		supplierRegistration: {
			show: true,
			steps: [
				{
					id: 'contacts',
					title: this.props.t( 'Verified contacts' ),
					description: this.props.t( 'Your e-mail and phone number needs verification.' ),
					link: '/profile'
				},
				{
					id: 'location',
					title: this.props.t( 'Location' ),
					description: this.props.t( 'Set your geographical location.' ),
					link: '/profile'
				},
				{
					id: 'products',
					title: this.props.t( 'Products' ),
					description: this.props.t( 'Create your product offerings.' ),
					link: '/profile'
				},
				{
					id: 'images',
					title: this.props.t( 'Images' ),
					description: this.props.t( 'Upload images and preview your presentation.' ),
					link: '/profile'
				},
				{
					id: 'slots',
					title: this.props.t( 'Time Slots' ),
					description: this.props.t( 'Set your bookable time slots.' ),
					link: '/calendar'
				}
			]
		}
	}

	state = {
		steps: {},
		showGuide: false,
	}

	componentWillMount() {
		let steps = _get( this.props.account, 'uiSettings.supplierRegistration.steps' )
		if( steps ) this.setState( { steps } )
	}

	componentWillReceiveProps( next ) {
		if( next.account ) {
			let steps = _get( next.account, 'uiSettings.supplierRegistration.steps' )
			if( steps ) this.setState( { steps } )
		}
	}

	render() {
		const { t } = this.props

		let showSteps = _get( this.props.account, 'uiSettings.supplierRegistration.show' )
		let percentDone = 0
		if( showSteps ) {
			let direction = 'horizontal', maxHeight = '100px'
			if( window.matchMedia( "(max-width: 640px)" ).matches ) {
				direction = 'vertical'
				maxHeight = '500px'
			}

			let lastStep, stepsDone = 0, noSteps = 0
			this.uiSettings.supplierRegistration.steps.forEach( ( step, index ) => {
				noSteps++
				step.done = _get( this.state, 'steps.' + step.id + '.done' )

				if( step.done ) {
					stepsDone++
					step.status = 'finish'
					if( lastStep && !lastStep.done ) lastStep.status = 'error'
				}
				else {
					step.status = 'wait'
					if( !lastStep || lastStep.done ) step.status = 'process'
				}
				lastStep = step
			} )

			percentDone = 100 * (stepsDone / noSteps)

			return (
					<div className="supplier-registration-steps vertical-spacing">
						<div onClick={() => this.setState( { showGuide: !this.state.showGuide } )}>
							<Icon type="caret-right"/>
							{' ' + t( 'Supplier registration progress' ) + ': '}
							<Icon type="question-circle"/>
						</div>
						<Progress percent={percentDone} status="active"
								  onClick={() => this.setState( { showGuide: !this.state.showGuide } )}/>

						<Steps direction={direction} className="supplier-guide"
							   style={{ maxHeight: (this.state.showGuide ? maxHeight : 0) }}>
							{this.uiSettings.supplierRegistration.steps.map( step => {
								return <Step title={<Link to={step.link}>{step.title}</Link>}
											 description={step.description}
											 status={step.status} key={step.id}/>
							} )}
						</Steps>
					</div>
			)
		}
		return (null)
	}
}

export default graphql( updateAccount.gql, updateAccount.options )(
		connect(
				function mapStateToProps( state ) {
					return {
						account: state.account,
					}
				},
				function mapDispatchToProps( dispatch ) {
					return {}
				}
		)(
				translate( [ 'common' ], { wait: true } )(
						SupplierRegistrationSteps
				)
		)
)
