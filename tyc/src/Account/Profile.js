import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { Alert, Icon, Button, Collapse } from 'antd'
import ProfilePane from './ProfilePane'
import { translate } from 'react-i18next'
import _get from 'lodash/get'
import MyLocations from '../Location/MyLocations'
import SupplierPane from './SupplierPane'
import ProductForms from './ProductForms'
import SupplierRegistrationSteps from '../Account/SupplierRegistrationSteps'
import './Profile.less'

const customPanelStyle = {}

class Profile extends React.Component {

	render() {
		const { t } = this.props

		if( !this.props.authInfo || !this.props.authInfo.id ) {
			return (<div className="browser content-pad content-width">
				<Alert showIcon type="warning" message={t( 'Not authenticated!' )} className="vertical-spacing"/>
			</div>)
		}

		let productPane = false
		let role = this.props.account && this.props.account.role
		if( role === 'ADMIN' || role === 'SUPPLIER' ) productPane = true

		return (
				<div className="profile content-width content-pad">
					<SupplierRegistrationSteps {...this.props}/>

					<Collapse bordered={false}>
						<Collapse.Panel header={t( 'Account' )} className="profile-collapse" key="contact"
										style={customPanelStyle}>
							<ProfilePane {...this.props} />
						</Collapse.Panel>

						<Collapse.Panel header={t( 'Locations' )} className="profile-collapse" key="location"
										style={customPanelStyle}>
							<MyLocations {...this.props}/>
						</Collapse.Panel>

						{this.props.account.supplierId &&
						<Collapse.Panel header={t( 'Supplier description' )} className="profile-collapse" key="supplier"
										style={customPanelStyle}>
							<SupplierPane {...this.props}/>
						</Collapse.Panel>
						}

						{productPane && (this.props.account.supplierId !== undefined) &&
						<Collapse.Panel header={t( 'Products' )} className="profile-collapse" key="product"
										style={customPanelStyle}>
							<ProductForms {...this.props}/>
						</Collapse.Panel>
						}
					</Collapse>

					{_get( this.props.account, 'suppliersByAccountId.nodes[0].status' ) === 'CONFIRMED' &&
					<div className="content-width-medium centered">
						<b className="vertical-spacing">{t( 'Calendar' )}</b>
						<div className="bottom-space">
							<div className="vertical-spacing">
								{t( 'When all the above info is complete, you need to proceed to your calendar page to add your available time slots.' )}
							</div>
							<Button type="primary" size="large" className="responsive vertical-spacing"
									onClick={() => browserHistory.push( '/calendar' )}>
								<Icon type="calendar"/> {t( 'Calendar' )}
							</Button>
						</div>
					</div>
					}

				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				authToken: state.authToken,
				authInfo: state.authInfo,
				account: state.account,
				location: state.location
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				updatePosition: ( position ) => dispatch( { type: 'GEOLOCATE', location: { position } } ),
				onLocate: ( location, deviceLocatorError ) => dispatch( {
					type: 'GEOLOCATE',
					location,
					deviceLocatorError
				} )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )( Profile )
)
