import React from 'react'
import { graphql, compose } from 'react-apollo'
import { translate } from 'react-i18next'
import { Input, Form, Select, Row, Col, Button, notification } from 'antd'
import VerifyPhoneButton from './VerifyPhoneButton'
import VerifyEmailButton from './VerifyEmailButton'
import config from '../config';
import { updateAccount, updateEmail, updatePhone } from '../graphql/account'

const Option = Select.Option
const DEBUG = false

const ProfileForm = Form.create( {
	onFieldsChange( props, changedFields ) {
		props.onChange( changedFields )
	}
} )( ( props ) => {
			const { getFieldDecorator } = props.form
			let role
			if( props.role === 'ADMIN' ) {
				role = <Row gutter={12}>
					<Col xs={24} sm={12}>
						<Form.Item label="Role">
							{getFieldDecorator( 'role', {
								initialValue: props.role,
								validateTrigger: 'onChange'
							} )(
									<Select size="large">
										<Option value="ADMIN">Administrator</Option>
										<Option value="SUPPLIER">Supplier</Option>
										<Option value="ACCOUNT">Client</Option>
									</Select>
							)}
						</Form.Item>
					</Col>
					<Col xs={24} sm={12}>
					</Col>
				</Row>
			}

			let onSave = ( props ) => {
				props.form.validateFields( ( error, values ) => {
					if( !error ) {
						DEBUG && console.log( 'Received values of form: ', values )
						props.onSave( values )
					}
				} )
			}

			const { t } = props
			return (
					<Form hideRequiredMark={true}>
						{role}
						<Row gutter={12}>
							<Col xs={24} sm={12}>
								<Form.Item label={t( 'First name' )} hasFeedback={true}>
									{getFieldDecorator( 'firstName', {
										initialValue: props.firstName,
										validateTrigger: 'onBlur',
										rules: [ { required: true, message: t( 'Please enter your first name.' ) } ]
									} )( <Input size="large"/> )}
								</Form.Item>
							</Col>
							<Col xs={24} sm={12}>
								<Form.Item label={t( 'Last name' )} hasFeedback={true}>
									{getFieldDecorator( 'lastName', {
										initialValue: props.lastName,
										validateTrigger: 'onBlur',
										rules: [ { required: true, message: t( 'Please enter your last name.' ) } ]
									} )( <Input size="large"/> )}
								</Form.Item>
							</Col>
						</Row>

						<Row gutter={12}>
							<Col xs={24} sm={12}>
								<Form.Item label={t( 'Mobile phone' )} hasFeedback={true}>
									{getFieldDecorator( 'phone', {
										initialValue: props.phone,
										rules: [ {
											required: true, pattern: /^\+[0-9,+,-]{9,}$/,
											message: t( 'Please enter your mobile phone number including country code.' )
										} ]
									} )( <Input size="large" type="tel" placeholder={t( 'Like this: +469999999' )}/> )}
								</Form.Item>
								<VerifyPhoneButton {...props} onSuccess={() => props.onPhoneSaved()}/>
							</Col>
							<Col xs={24} sm={12}>
								<Form.Item label={t( 'E-Mail' )} hasFeedback={true}>
									{getFieldDecorator( 'email', {
										initialValue: props.email,
										rules: [ {
											required: true,
											type: 'email',
											message: t( 'Please enter your e-mail address.' )
										} ]
									} )( <Input size="large"/> )}
								</Form.Item>
								<VerifyEmailButton {...props}/>
							</Col>
						</Row>

						<Row gutter={12} className="vertical-spacing">
							<Col xs={0} sm={12}>
							</Col>
							<Col xs={24} sm={12}>
								<Button type="primary" size="large"
										loading={props.inProgress}
										onClick={() => onSave( props )}>
									{t( 'Save contact details' )}
								</Button>
							</Col>
						</Row>
					</Form>
			)
		}
)

class ProfilePane extends React.Component {

	state = {
		phoneUnsaved: false,
		emailUnsaved: false,
		inProgress: false
	}

	changedForm = ( changedFields ) => {
		/*forEach( changedFields, ( props, fieldName ) => {
		 if( props.dirty ) return
		 //console.log( '(saveForm)', fieldName, props.value )
		 } ) */
		if( changedFields.phone ) {
			if( changedFields.phone.value !== this.props.account.phone )
				this.setState( {
					phoneUnsaved: changedFields.phone.errors ? undefined : true,
					phoneUnsavedValue: changedFields.phone.value
				} )
		}
		if( changedFields.email ) {
			if( changedFields.email.value !== this.props.account.email )
				this.setState( {
					emailUnsaved: true,
					emailUnsavedValue: changedFields.email.value
				} )
		}
	}

	saveProfile = ( profile ) => {
		const { t } = this.props
		this.neededMutationCounter = 0

		if( profile.phone !== this.props.account.phone ) {
			this.neededMutationCounter++
			this.props.updateAccountPhone( this.props.authInfo.id, profile.phone )
					.then( result => {
						this.setState( { phoneUnsaved: false } )
						if( --this.neededMutationCounter === 0 )
							notification.info( { message: t( 'Saved successfully.' ) } )
					} )
					.catch( error => {
						console.log( error.message )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Phone Update' ] )
						notification.error( { message: error.message } )
					} )
		}

		if( profile.email !== this.props.account.email ) {
			this.neededMutationCounter++
			this.props.updateAccountEmail( this.props.authInfo.id, profile.email )
					.then( result => {
						this.setState( { emailUnsaved: false } )
						if( --this.neededMutationCounter === 0 )
							notification.info( { message: t( 'Saved successfully.' ) } )
					} )
					.catch( error => {
						console.log( error.message )
						if( error.message.indexOf( 'account_email_key' ) < 0 ) {
							config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Email Update' ] )
							notification.error( { message: error.message } )
						} else {
							notification.warning( {
								message: t( 'E-mail was not saved.' ),
								description: t( 'This e-mail is already associated with an account.' )
							} )
						}
					} )
		}

		let patch = {}
		if( profile.firstName !== this.props.account.firstName ) patch.firstName = profile.firstName
		if( profile.lastName !== this.props.account.lastName ) patch.lastName = profile.lastName

		if( Object.keys( patch ).length ) {
			this.setState( { inProgress: true } )
			this.neededMutationCounter++
			this.props.updateAccount( this.props.authInfo.id, patch )
					.then( result => {
						if( --this.neededMutationCounter === 0 ) {
							this.setState( { inProgress: false } )
							notification.info( { message: t( 'Saved successfully.' ) } )
						}
					} )
					.catch( error => {
						console.log( error.message )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Account Update' ] )
						this.setState( { inProgress: false } )
						notification.error( { message: error.message } )
					} )
		}
	}

	render() {

		return (
				<div className="bottom-space">
					<ProfileForm {...this.props.account} t={this.props.t}
								 authToken={this.props.authToken}
								 onChange={this.changedForm}
								 onSave={this.saveProfile} inProgress={this.state.inProgress}
								 phoneUnsaved={this.state.phoneUnsaved}
								 phoneUnsavedValue={this.state.phoneUnsavedValue}
								 onPhoneSaved={() => this.setState( { phoneUnsaved: false } )}
								 emailUnsaved={this.state.emailUnsaved}
								 emailUnsavedValue={this.state.emailUnsavedValue}
					/>
				</div>
		)
	}
}

export default compose(
		graphql( updateAccount.gql, updateAccount.options ),
		graphql( updatePhone.gql, updatePhone.options ),
		graphql( updateEmail.gql, updateEmail.options )
)( translate( [ 'common' ], { wait: true } )( ProfilePane ) )
