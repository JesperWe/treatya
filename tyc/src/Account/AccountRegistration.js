import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import jwt from '../jwt'
import cookie from 'browser-cookies'
import moment from 'moment'
import http from 'axios'
import _get from 'lodash/get'
import { graphql, compose } from 'react-apollo'
import errorHandler from '../errorHandler'
import { translate } from 'react-i18next'
import { Input, Button, Form, Checkbox, Alert, notification } from 'antd'
import VerifyPhoneButton from '../Account/VerifyPhoneButton'
import VerifyEmailButton from '../Account/VerifyEmailButton'
import config from '../config'
import { doLogin, registerAccount } from '../graphql/account'
import CMSText from '../CMS/CMSText'
import './Registration.less'

const DEBUG = false

class Registration extends React.Component {

	state = {
		email: '',
		password: '',
		passwordDirty: false,
		inProgress: false,
		accountIncomplete: false,
		verification: false
	}

	componentWillMount() {
		DEBUG && console.log( 'Registration.componentWillMount', this.props, this.state )
		let { view } = this.props.params

		const { account } = this.props
		let verification = account && account.id
		let accountIncomplete
		if( verification ) accountIncomplete = !account.emailVerified || !account.phoneVerified

		this.setState( {
			url: _get( this.props, 'location.query.url' ),
			password: (_get( this.props, 'location.query.password' ) === 'true') ? window._registerPrefilledPassword : undefined,
			email: _get( this.props, 'location.query.email' ),
			supplierRequest: (view === 'supplier'),
			verification, accountIncomplete,
			noVerification: _get( this.props, 'location.query.noVerification' )
		} )
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( 'Registration.componentWillReceiveProps', next )
		let accountIncomplete = false
		let { account } = next
		if( account && account.id ) {
			accountIncomplete = !account.emailVerified || !account.phoneVerified

			accountIncomplete = accountIncomplete && this.state.noVerification !== 'true'

			DEBUG && console.log( 'Account', (accountIncomplete ? 'Incomplete' : 'Complete') )
			this.setState( { accountIncomplete, verification: true } )
			if( !accountIncomplete && this.state.url ) {
				browserHistory.push( this.state.url )
			}
		}
	}

	componentDidMount() {
		const { form } = this.props
		const { email, password } = this.state
		if( password && password.length ) form.validateFields( [ 'password' ], { force: true } )
		if( email && email.length ) form.validateFields( [ 'email' ], { force: true } )
	}

	handleSubmit = ( event ) => {
		const { t } = this.props
		event.preventDefault()
		this.props.form.validateFields( ( error, values ) => {
			if( !error ) {
				console.log( 'Received values of form: ', values )
				if( !values.agreement ) {
					notification.info( { message: t( 'Please read and agree to the terms and conditions before continuing.' ) } )
					return
				}

				this.setState( { inProgress: true } )
				values.language = this.props.language
				this.props.registerAccount( values )
						.then( result => {
							//notification.success( { message: t( 'Your account was successfully created.' ) } )
							this.setState( { inProgress: true } )

							this.props.login( values ).then(
									result => {
										let authToken = result.data.authenticate.jwtToken
										if( authToken ) {
											let authInfo = jwt.decode( authToken, '', true )
											console.log( authInfo )
											cookie.set( 'auth-token', authToken, { expires: moment( authInfo.exp * 1000 ).subtract( 2, 'hours' ).toDate() } )
											this.props.onLogin( authToken, authInfo ) // Redux

											if( config.tracking ) {
												window._paq.push( [ 'trackEvent', 'Account', 'Registration completed.' ] )
												http.post( config.backend.url + '/notify/admin/register', {
													id: authInfo.id,
													jwt: authToken,
													email: values.email
												} )
											}

											if( this.state.url )
												this.setState( { verification: true } )
											else if( !this.state.supplierRequest )
												browserHistory.push( '/' )

										} else {
											notification.warning( { message: 'Authetication failed.' } )
										}
									},
									error => {
										cookie.erase( 'auth-token' )
										errorHandler( error )
									}
							)
						} )
						.catch( error => {
							this.setState( { inProgress: false } )
							if( error.message.indexOf( 'account_email_key' ) >= 0 )
								notification.error( { message: t( 'This e-mail is already associated with an account.' ) } )
							else
								errorHandler( error )
						} )
			}
		} )
	}

	checkPassword = ( rule, value, callback ) => {
		const { t } = this.props
		const form = this.props.form
		if( value && value !== form.getFieldValue( 'password' ) ) {
			callback( t( 'The passwords do not match.' ) )
		} else {
			callback()
		}
	}

	checkConfirm = ( rule, value, callback ) => {
		const form = this.props.form
		if( value && this.state.passwordDirty ) {
			form.validateFields( [ 'confirm' ], { force: true } )
		}
		callback()
	}

	handlePasswordBlur = ( event ) => {
		const value = event.target.value
		this.setState( { passwordDirty: this.state.passwordDirty || !!value } )
	}

	render() {
		const { t, form, account, authToken, authInfo, requestEmailVerificationCheck } = this.props
		const { getFieldDecorator } = form
		const { verification, accountIncomplete } = this.state

		let introText = (_get( this.props, 'params.view' ) === 'supplier') ?
				<CMSText identifier="supplier-account-registration-intro"/> :
				<CMSText identifier="account-registration-intro"/>

		return (
				<div className="registration content-width-medium content-pad">

					{!verification &&
					<div>
						<div className="vertical-spacing">{introText}</div>
						<Form layout="vertical" onSubmit={this.handleSubmit} hideRequiredMark>
							<Form.Item label={t( 'First name' )} hasFeedback>
								{getFieldDecorator( 'firstName', {
									rules: [ { required: true, min: 2, message: t( 'First name is required.' ) } ],
								} )( <Input size="large"/> )}
							</Form.Item>
							<Form.Item label={t( 'Last name' )} hasFeedback>
								{getFieldDecorator( 'lastName', {
									rules: [ {
										required: true,
										min: 2,
										message: t( 'Last (family) name is required.' )
									} ],
								} )( <Input size="large"/> )}
							</Form.Item>
							<Form.Item label={t( 'E-mail' )} hasFeedback>
								{getFieldDecorator( 'email', {
									initialValue: this.state.email,
									rules: [ {
										type: 'email', message: t( 'The input is not valid E-mail.' ),
									}, {
										required: true, message: t( 'Please input your E-mail.' ),
									} ],
								} )( <Input size="large" type="email"/> )}
							</Form.Item>
							<Form.Item

									label={t( 'Password' )}
									hasFeedback
							>
								{getFieldDecorator( 'password', {
									initialValue: this.state.password,
									rules: [ {
										required: true,
										message: t( 'Minimum 8 characters of mixed types.' ),
										min: 8
									}, {
										validator: this.checkConfirm
									} ]
								} )(
										<Input type="password" size="large" onBlur={this.handlePasswordBlur}/>
								)}
							</Form.Item>
							<Form.Item

									label={t( 'Confirm Password' )}
									hasFeedback
							>
								{getFieldDecorator( 'confirm', {
									rules: [ {
										required: true, message: t( 'Please confirm your password.' ),
									}, {
										validator: this.checkPassword,
									} ],
								} )(
										<Input type="password" size="large"/>
								)}
							</Form.Item>
							<Form.Item style={{ marginBottom: 8 }}>
								{getFieldDecorator( 'agreement', {
									valuePropName: 'checked',
								} )(
										<div>
											<Checkbox>
												{t( 'I agree to the ' )}
												<a onClick={() => browserHistory.push( '/page/terms' )}>{t( 'Treatya Terms & Conditions' )}</a>
											</Checkbox>
										</div>
								)}
							</Form.Item>

							<Form.Item>
								<Button type="primary" size="large"
										htmlType="submit"
										loading={this.state.inProgress}>
									{t( 'Register' )}
								</Button>
							</Form.Item>
						</Form>
					</div>
					}

					{verification && accountIncomplete &&
					<div>
						<Alert type="warning" showIcon className="responsive vertical-spacing"
							   message={t( 'We need to have your contact details verified.' )}
						/>

						<CMSText identifier="verification-explain"/>

						<div className="buttons">
							<VerifyPhoneButton {...account} withInput={true}
											   authToken={authToken} language={this.props.language}
											   authInfo={authInfo}/>

							<div className="vertical-spacing"><CMSText identifier="verification-email"/></div>

							<VerifyEmailButton {...account}
											   authToken={authToken} language={this.props.language}
											   requestEmailVerificationCheck={requestEmailVerificationCheck}/>
						</div>

					</div>
					}

				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				authToken: state.authToken,
				authInfo: state.authInfo,
				account: state.account,
				language: state.language
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onRegistered: ( authToken, authInfo ) => dispatch( { type: 'LOGIN', authToken, authInfo } ),
				onLogin: ( authToken, authInfo ) => dispatch( { type: 'LOGIN', authToken, authInfo } )
			}
		}
)(
		compose(
				graphql( doLogin.gql, doLogin.options ),
				graphql( registerAccount.gql, registerAccount.options )
		)(
				Form.create()(
						translate( [ 'common' ], { wait: true } )( Registration )
				)
		)
)
