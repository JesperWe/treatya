import React from 'react'
import { connect } from 'react-redux'
import http from 'axios'
import scriptLoader from 'react-async-script-loader'
import cookie from 'browser-cookies'
import CMSText from '../CMS/CMSText'
import { translate } from 'react-i18next'
import IBAN from 'iban'
import { Input, Form, Button, Alert, DatePicker, Select, notification } from 'antd'
import config from '../config'
import StripeFileUpload from './StripeFileUpload'

const DEBUG = false

const AccountForm = Form.create()( class extends React.Component {
	handleSubmit = ( e ) => {
		e.preventDefault()
		this.props.form.validateFields( ( err, values ) => {
			let iban = (values.accountNo || '').replace( / /g, '' )
			if( !err ) {
				this.props.onSaveIBAN( iban )
			}
		} )
	}

	render() {
		const { t } = this.props
		const { getFieldDecorator } = this.props.form
		return (
				<Form onSubmit={this.handleSubmit}>
					<Form.Item label={t( 'Bank Account (IBAN)' )} hasFeedback={true}>
						{getFieldDecorator( 'accountNo', {
							rules: [ {
								required: true,
								validator( rule, value, callback ) {
									var errors = []
									if( !IBAN.isValid( (value || '').trim().replace( / /g, '' ) ) ) {
										errors.push( new Error( 'Invalid IBAN number' ) )
									}
									callback( errors )
								},
								message: t( 'Invalid IBAN number' )
							} ],
						} )(
								<Input size="large" placeholder="Bank Account (IBAN)"/>
						)}
					</Form.Item>
					<Form.Item>
						<Button type="primary" htmlType="submit">{t( 'Save' )}</Button>
					</Form.Item>
				</Form>
		)
	}
} )

const PersonalForm = Form.create()( class extends React.Component {
	handleSubmit = ( e ) => {
		e.preventDefault()
		this.props.form.validateFields( ( err, values ) => {
			if( !err ) {
				let personal = Object.assign( {}, values )
				personal.personal_address = {
					line1: values.line1,
					postal_code: values.postal_code,
					city: values.city,
					country: values.country
				}
				delete personal.line1
				delete personal.postal_code
				delete personal.city
				delete personal.country
				this.props.onSave( personal )
			}
		} )
	}

	render() {
		const { t } = this.props
		const { getFieldDecorator } = this.props.form
		return (
				<Form onSubmit={this.handleSubmit}>

					<Form.Item label={t( 'First name' )} hasFeedback={true}>
						{getFieldDecorator( 'first_name', {
							rules: [ { required: true, message: t( 'First name is required.' ) } ]
						} )( <Input placeholder={t( 'First name' )}/> )}
					</Form.Item>

					<Form.Item label={t( 'Last name' )} hasFeedback={true}>
						{getFieldDecorator( 'last_name', {
							rules: [ { required: true, message: t( 'Last (family) name is required.' ) } ]
						} )( <Input placeholder={t( 'Last name' )}/> )}
					</Form.Item>

					<Form.Item label={t( 'Social Security Number' )} hasFeedback={true}>
						{getFieldDecorator( 'personal_id_number', {
							rules: [ { required: true, message: t( 'Must be supplied.' ) } ]
						} )( <Input placeholder={t( 'Social Security Number' )}/> )}
					</Form.Item>

					<Form.Item label={t( 'Home address' )} hasFeedback={true}>
						{getFieldDecorator( 'line1', {
							rules: [ { required: true, message: t( 'Must be supplied.' ) } ]
						} )( <Input placeholder={t( 'Home address' )}/> )}
					</Form.Item>

					<Form.Item label={t( 'Postal code' )} hasFeedback={true}>
						{getFieldDecorator( 'postal_code', {
							rules: [ { required: true, message: t( 'Must be supplied.' ) } ]
						} )( <Input placeholder={t( 'Postal code' )}/> )}
					</Form.Item>

					<Form.Item label={t( 'City' )} hasFeedback={true}>
						{getFieldDecorator( 'city', {
							rules: [ { required: true, message: t( 'Must be supplied.' ) } ]
						} )( <Input placeholder={t( 'City' )}/> )}
					</Form.Item>

					<Form.Item label={t( 'Country' )} hasFeedback={true}>
						{getFieldDecorator( 'country', {
							initialValue: 'SE',
							rules: [ { required: true, message: t( 'Select country' ) } ]
						} )(
								<Select placeholder={t( 'Select country' )}>
									<Select.Option value="SE">{t( 'Sweden' )}</Select.Option>
								</Select> )}
					</Form.Item>

					<Form.Item>
						<Button type="primary" htmlType="submit">{t( 'Save' )}</Button>
					</Form.Item>

				</Form>
		)
	}
} )

class StripeAccount extends React.Component {
	state = {
		sent: {}
	}

	// We save what we have sent on a cookie in order to give better feedback / UX.
	// Stripe sometimes takes several minutes to respond with the webhook and then
	// there is the polling delay too.
	componentWillMount() {
		try {
			let c = cookie.get( 'stripe-sent' )
			if( c ) this.setState( { sent: JSON.parse( c ) } )
		}
		catch( e ) {
		}
	}

	componentWillUnmount() {
		cookie.set( 'stripe-sent', JSON.stringify( this.state.sent ) )
	}

	componentWillReceiveProps( next ) {
		// Check for errors loading payment library.
		const { t, isScriptLoaded, isScriptLoadSucceed } = next
		if( isScriptLoaded !== this.props.isScriptLoaded || isScriptLoadSucceed !== this.props.isScriptLoadSucceed ) {
			if( isScriptLoaded ) {
				if( isScriptLoadSucceed ) {
					window.Stripe.setPublishableKey( config.stripe.pk_test )
					DEBUG && console.log( 'Stripe configured Ok.' )
				} else {
					notification.warning( { message: t( 'Failed to load payment processing module.' ) } )
				}
			}
		}
	}

	sent = ( info ) => {
		let sent = this.state.sent
		sent[ info ] = 1
		this.setState( { sent } )
	}

	onSaveIBAN = ( iban ) => {
		const { t } = this.props
		http.post( config.backend.url + '/stripe/connect/bankaccount', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					iban
				} )
				.then( ( response ) => {
					notification.success( { message: t( 'Saved successfully.' ) } )
					this.sent( 'external_account' )
				} )
				.catch( error => {
					if( error.response.data ) {
						console.log( error.response.data )
						notification.error( { message: error.response.data } )
					} else {
						console.log( error.stack )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Save IBAN' ] )
						notification.error( { message: error.message } )
					}
				} )
	}

	onSaveDOB = ( dob ) => {
		if( !dob ) return
		http.post( config.backend.url + '/stripe/connect/dob', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					dob
				} )
				.then( ( response ) => {
					this.sent( 'dob' )
					notification.success( { message: this.props.t( 'Saved successfully.' ) } )
				} )
				.catch( error => {
					if( error.response.data ) {
						console.log( error.response.data )
						notification.error( { message: error.response.data } )
					} else {
						console.log( error.stack )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Save DOB' ] )
						notification.error( { message: error.message } )
					}
				} )
	}

	onSaveLegalPersonal = ( personal ) => {
		const { t } = this.props
		http.post( config.backend.url + '/stripe/connect/personal', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					personal
				} )
				.then( ( response ) => {
					this.sent( 'personal' )
					notification.success( { message: t( 'Saved successfully.' ) } )
				} )
				.catch( error => {
					if( error.response.data ) {
						console.log( error.response.data )
						notification.error( { message: error.response.data } )
					} else {
						console.log( error.stack )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Save Personal' ] )
						notification.error( { message: error.message } )
					}
				} )
	}

	onUpload = ( stripeDoc ) => {
		const { t } = this.props
		if( !stripeDoc.object === 'file_upload' ) {
			config.tracking && window._paq.push( [ 'trackEvent', 'Error', 'File upload failed', 'Stripe ID Document' ] )
			notification.error( { message: t( 'Document was not received properly by Stripe.' ) } )
			return
		}
		http.post( config.backend.url + '/stripe/connect/document', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					doc: stripeDoc.id
				} )
				.then( ( response ) => {
					this.sent( 'document' )
					notification.success( { message: t( 'Saved successfully.' ) } )
				} )
				.catch( error => {
					if( error.response.data ) {
						console.log( error.response.data )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.response.data, 'Stripe Doc Upload' ] )
						notification.error( { message: error.response.data } )
					} else {
						console.log( error.stack )
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Stripe Doc Upload' ] )
						notification.error( { message: error.message } )
					}
				} )
	}

	render() {
		const { t } = this.props
		const needed = this.props.supplierInfo.stripeNeeded

		return (
				<div className="content-width content-pad" style={{ maxWidth: '450px' }}>

					{needed && (needed.length === 0) &&
					<Alert message={t( 'Stripe page info success message' )}
						   description={t( 'Stripe page info success descr' )}
						   showIcon type="success"/>
					}

					{needed && (needed.length > 0) &&
					<Alert showIcon type="info"
						   message={<CMSText identifier="stripe-page-info-message"/>}
						   description={<CMSText identifier="stripe-page-info-descr"/>}
					/>
					}

					{needed && (needed.indexOf( 'legal_entity.dob.day' ) >= 0) &&
					<div>
						<h3 style={{ marginTop: '1rem' }}>{t( 'Your date of birth' )}</h3>

						{this.state.sent.dob &&
						<Alert message={t( 'Stripe is processing your information.' )} type="success" showIcon/>
						}

						{!this.state.sent.dob &&
						<div>
							<CMSText identifier="stripe-legal-rep-dob"/>
							< DatePicker size="large" onChange={this.onSaveDOB} showDateInput={false}/>
						</div>
						}

					</div>
					}

					{needed && (needed.indexOf( 'legal_entity.personal_address.city' ) >= 0) &&
					<div>
						<h3 style={{ marginTop: '1rem' }}>{t( 'Company Representative' )}</h3>

						{this.state.sent.personal &&
						<Alert message={t( 'Stripe is processing your information.' )} type="success" showIcon/>
						}

						{!this.state.sent.personal &&
						<div>
							<CMSText identifier="stripe-legal-rep"/>
							<PersonalForm {...this.props} onSave={this.onSaveLegalPersonal}/>
						</div>
						}

					</div>
					}

					{needed && (needed.indexOf( 'external_account' ) >= 0) &&
					<div>
						<h3 style={{ marginTop: '1rem' }}>{t( 'Bank Account Information' )}</h3>

						{this.state.sent.external_account &&
						<Alert message={t( 'Stripe is processing your information.' )} type="success" showIcon/>
						}

						{!this.state.sent.external_account &&
						<div>
							<CMSText identifier="stripe-bank-account-info"/>
							< AccountForm {...this.props} onSaveIBAN={this.onSaveIBAN}/>
						</div>
						}

					</div>
					}

					{needed && (needed.indexOf( 'legal_entity.verification.document' ) >= 0) &&
					<div>
						<h3 style={{ marginTop: '1rem' }}>{t( 'Identity verification document' )}</h3>

						{this.state.sent.document &&
						<Alert message={t( 'Stripe is processing your information.' )} type="success" showIcon/>
						}

						{!this.state.sent.document &&
						<div>
							<CMSText identifier='stripe-bank-id-verification-info'/>
							<br/>
							<StripeFileUpload text={t( 'Upload ID verification document' )}
											  onUploadComplete={this.onUpload}/>
						</div>
						}

					</div>
					}

				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				authToken: state.authToken,
				authInfo: state.authInfo,
				account: state.account,
				supplierInfo: state.supplierInfo,
				location: state.location
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onAccount: ( account ) => dispatch( { type: 'ACCOUNT', account } ),
				onSupplierInfo: ( supplierInfo ) => dispatch( { type: 'SUPPLIERINFO', supplierInfo } )
			}
		}
)(
		translate( [ 'common' ], { wait: true } )(
				scriptLoader( 'https://js.stripe.com/v2/' )(
						StripeAccount
				)
		)
)
