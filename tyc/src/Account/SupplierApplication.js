import React from 'react'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import { Link, browserHistory } from 'react-router'
import { graphql, compose } from 'react-apollo'
import http from 'axios'
import _get from 'lodash/get';
import errorHandler from '../errorHandler'
import { isValidSwedishSSN } from './personnummer'
import { Input, Form, Row, Col, Button, Select, Radio, Alert, notification } from 'antd'
import CMSText from '../CMS/CMSText'
import AccountRegistration from './AccountRegistration'
import config from '../config'
import { createSupplier, updateSupplierPrivate } from '../graphql/supplier'
import { updateAccount } from '../graphql/account'
import './Registration.less'

class ApplicationForm extends React.Component {
	state = {
		entityCompany: true
	}

	handleSubmit = ( event ) => {
		event.preventDefault()
		this.props.form.validateFields( ( error, values ) => {
			if( !error ) {
				console.log( 'Received values of form: ', values )
				let supplier = { ...values }
				this.props.onSave( supplier )
			}
		} )
	}

	render() {
		const { getFieldDecorator } = this.props.form
		const t = this.props.t
		return (
				<div className="supplier-application">
					<div className="instructions">
						<h3><CMSText identifier="supplier-registration-header"/></h3>
						<p><CMSText identifier="supplier-registration-intro"/></p>

						<h3><CMSText identifier="supplier-registration-agreements-header"/></h3>

						<p><CMSText identifier="supplier-registration-agreements"/> <Link to="/page/terms">{t( 'Treatya Terms & Conditions' )}</Link></p>

						<p>
							<CMSText identifier="supplier-registration-stripe"/>{' '}
							<a href="https://stripe.com/se/legal" target="_blank" rel="noopener noreferrer">
								Stripe Connected Account Agreement
							</a>
						</p>
					</div>

					<Form onSubmit={this.handleSubmit}  hideRequiredMark={true}>
						<Row gutter={12} className="vertical-spacing">
							<Col xs={24} sm={24}>
								<Form.Item label={t( 'Supplier name' )} hasFeedback={true}>
									{getFieldDecorator( 'title', {
										validateTrigger: 'onBlur',
										rules: [ { required: true, message: t( 'Enter your supplier title.' ) } ]
									} )( <Input name="title" size="large" placeholder={t( 'Supplier title' )}/> )}
								</Form.Item>
							</Col>

							<Col xs={24} className="vertical-spacing">
								<CMSText identifier="stripe-supplier-legal-info"/>
							</Col>

							<Col xs={24}>
								<Form.Item style={{ textAlign: 'center' }}>
									{getFieldDecorator( 'entityType', {
										initialValue: (this.props.supplier && this.props.supplier.entityType) || 'COMPANY',
										rules: [ { required: true, message: t( 'Select entity type.' ) } ]
									} )(
											<Radio.Group size="large"
														 onChange={( e ) => this.setState( { entityCompany: (e.target.value === 'COMPANY') } )}>
												<Radio.Button name="corporate"
															  value="COMPANY">{t( 'COMPANY' )}</Radio.Button>
												<Radio.Button name="person"
															  value="INDIVIDUAL">{t( 'INDIVIDUAL' )}</Radio.Button>
											</Radio.Group>
									)}
								</Form.Item>
							</Col>

							<Col xs={24} sm={12}>
								<Form.Item label={this.state.entityCompany ? t( 'Company Name' ) : t( 'Name' )}
										   hasFeedback={true}>
									{getFieldDecorator( 'entityName', {
										initialValue: this.props.supplier && this.props.supplier.entityName,
										rules: [ { required: true, message: t( 'Enter entity name.' ) } ]
									} )( <Input name="company" size="large"
												placeholder={this.state.entityCompany ? t( 'Company Name' ) : t( 'Name' )}/> )}
								</Form.Item>
							</Col>

							<Col xs={24} sm={12}>
								<Form.Item
										label={this.state.entityCompany ? t( 'Company Registration Number' ) : t( 'Social Security Number' )}
										hasFeedback={true}>
									{getFieldDecorator( 'entityId', {
										initialValue: this.props.supplier && this.props.supplier.entityId,
										rules: [
											{ required: true, message: t( 'Enter the number.' ) },
											{ validator: this.state.entityCompany ? undefined : isValidSwedishSSN }
										]
									} )( <Input name="regno" size="large"
												placeholder={this.state.entityCompany ? t( 'Company Registration Number' ) : t( 'Social Security Number' )}/> )}
								</Form.Item>
							</Col>

							<Col xs={24} sm={12}>
								<Form.Item label={t( 'Postal address' )} hasFeedback={true}>
									{getFieldDecorator( 'address1', {
										initialValue: this.props.supplier && this.props.supplier.address1,
										rules: [ { required: true, message: t( 'Enter postal address.' ) } ]
									} )( <Input name="address1" size="large"
												placeholder={t( 'Postal address line 1' )}/> )}
								</Form.Item>
								<Form.Item label={t( 'Line 2' )} hasFeedback={true}>
									{getFieldDecorator( 'address2', {
										initialValue: this.props.supplier && this.props.supplier.address2,
										rules: [ { required: false } ]
									} )( <Input name="address2" size="large"
												placeholder={t( 'Postal address line 2' )}/> )}
								</Form.Item>
							</Col>

							<Col xs={24} sm={12}>
								<Form.Item label={t( 'Postal code' )} hasFeedback={true}>
									{getFieldDecorator( 'zip', {
										initialValue: this.props.supplier && this.props.supplier.zip,
										rules: [ { required: true, message: t( 'Enter postal code.' ) } ]
									} )( <Input name="zip" size="large" placeholder={t( 'Postal code' )}/> )}
								</Form.Item>
								<Form.Item label={t( 'City' )} hasFeedback={true}>
									{getFieldDecorator( 'city', {
										initialValue: this.props.supplier && this.props.supplier.city,
										rules: [ { required: true, message: t( 'Enter postal city.' ) } ]
									} )( <Input name="city" size="large" placeholder={t( 'City' )}/> )}
								</Form.Item>
								<Form.Item label={t( 'Country' )} hasFeedback={true}>
									{getFieldDecorator( 'country', {
										initialValue: this.props.supplier ? this.props.supplier.country : "SE",
										rules: [ { required: true, message: t( 'Select country' ) } ]
									} )(
											<Select placeholder={t( 'Select country' )} size="large">
												<Select.Option value="SE">{t( 'Sweden' )}</Select.Option>
											</Select> )}
								</Form.Item>
							</Col>

							<Col xs={24}>
								<Form.Item>
									{getFieldDecorator( 'save', {} )(
											<Button type="primary" size="large" htmlType="submit"
													loading={this.props.inProgress}>
												{t( 'Apply Now' )}
											</Button>
									)}
								</Form.Item>
							</Col>
						</Row>
					</Form>
				</div>
		)
	}
}

class SupplierApplication extends React.Component {
	state = {
		inProgress: false
	}

	saveSupplierApplication = ( supplier ) => {
		const { t } = this.props
		supplier.accountId = this.props.authInfo.id
		this.setState( { inProgress: true } )
		this.props.createSupplier( supplier )
				.then( result => {
					supplier.id = result.data.createSupplier.supplier.id
					return http.get( 'https://icanhazip.com' )
				} )
				.then( result => {
					return this.props.updateSupplierPrivate( {
						id: supplier.id,
						ip: result.data.trim(),
						agent: navigator.userAgent,
						entityName: supplier.entityName,
						entityType: supplier.entityType,
						entityId: supplier.entityId
					} )
				} )
				.then( result => {
					this.setState( { inProgress: false } )
					notification.success( { message: t( 'Supplier created.' ) } )
					config.tracking && window._paq.push( [ 'trackEvent', 'Account', 'Supplier Application Completed.' ] )
					return http.post( config.backend.url + '/notify/admin/supplier', {
						id: this.props.authInfo.id,
						jwt: this.props.authToken,
					} )
				} )
				.then( result => {
					let { uiSettings } = this.props.account
					uiSettings = Object.assign( {}, uiSettings, { supplierRegistration: { steps: { application: { done: true } } } } )
					return this.props.updateAccount( this.props.authInfo.id, { uiSettings } )
				} )
				.then( result => {
					this.props.onLandingPage( true )
					browserHistory.push( '/' )
				} )
				.catch( error => {
					this.setState( { inProgress: false } )
					errorHandler( error )
				} )
	}

	render() {
		const { t, account } = this.props
		const AppForm = Form.create( {} )( ApplicationForm )

		if( _get( account, 'id.length' ) > 0 ) return ( <AppForm t={this.props.t} onSave={this.saveSupplierApplication}/> )

		if( _get( account, 'suppliersByAccountId.nodes[0].status' ) ) return (
				<div className="centered">
					<Alert showIcon type="info"
						   inProgress={this.state.inProgress}
						   message={t( 'Your application is registered.' )}
						   description={t( 'It will be processed shortly.' )}/>
				</div>
		)

		return ( <AccountRegistration {...this.props}/> )
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				account: state.account,
				authInfo: state.authInfo,
				authToken: state.authToken
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } )
			}
		}
)(
		compose(
				graphql( updateAccount.gql, updateAccount.options ),
				graphql( createSupplier.gql, createSupplier.options ),
				graphql( updateSupplierPrivate.gql, updateSupplierPrivate.options )
		)(
				translate( [ 'common' ], { wait: true } )( SupplierApplication )
		)
)
