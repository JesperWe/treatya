import React from 'react'
import AccountRegistration from './AccountRegistration'
import SupplierApplication from './SupplierApplication'

export default function( props ) {
	let view
	switch( props.params.view ) {
		case 'supplier':
			view = <SupplierApplication {...props}/>
			break
		default:
			view = <AccountRegistration {...props}/>
	}
	return view
}
