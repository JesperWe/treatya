import React from 'react';
import PropTypes from 'prop-types';
import { Button, Alert } from 'antd';

// Code based on https://github.com/keppelen/react-facebook-login, updated until 2017-01-05

class FacebookLogin extends React.Component {

	static propTypes = {
		isDisabled: PropTypes.bool,
		callback: PropTypes.func.isRequired,
		appId: PropTypes.string.isRequired,
		xfbml: PropTypes.bool,
		cookie: PropTypes.bool,
		reAuthenticate: PropTypes.bool,
		scope: PropTypes.string,
		textButton: PropTypes.string,
		typeButton: PropTypes.string,
		autoLoad: PropTypes.bool,
		disableMobileRedirect: PropTypes.bool,
		size: PropTypes.string,
		fields: PropTypes.string,
		cssClass: PropTypes.string,
		version: PropTypes.string,
		icon: PropTypes.any,
		language: PropTypes.string,
		onClick: PropTypes.func,
		containerStyle: PropTypes.object,
		buttonStyle: PropTypes.object,
	};

	static defaultProps = {
		scope: 'public_profile,email',
		xfbml: false,
		cookie: false,
		reAuthenticate: false,
		size: 'metro',
		fields: 'name,first_name,last_name',
		version: '2.8',
		language: 'en_US',
		disableMobileRedirect: false,
	};

	state = {
		isSdkLoaded: false,
		isProcessing: false,
	};

	componentDidMount() {
		if (document.getElementById('facebook-jssdk')) {
			this.sdkLoaded();
			return;
		}
		this.setFbAsyncInit();
		this.loadSdkAsynchronously();
		let fbRoot = document.getElementById('fb-root');
		if (!fbRoot) {
			fbRoot = document.createElement('div');
			fbRoot.id = 'fb-root';
			document.body.appendChild(fbRoot);
		}
	}

	sdkLoaded() {
		this.setState( { isSdkLoaded: true } );
	}

	setFbAsyncInit() {
		const { appId, xfbml, cookie, version, autoLoad } = this.props;
		window.fbAsyncInit = () => {
			window.FB.init( {
				version: `v${version}`,
				appId,
				xfbml,
				cookie,
			} );
			this.setState( { isSdkLoaded: true } );
			if( autoLoad || window.location.search.includes( 'facebookdirect' ) ) {
				window.FB.getLoginStatus( this.checkLoginAfterRefresh );
			}
		};
	}

	loadSdkAsynchronously() {
		const { language } = this.props;
		(( d, s, id ) => {
			const element = d.getElementsByTagName( s )[ 0 ];
			const fjs = element;
			let js = element;
			if( d.getElementById( id ) ) {
				return;
			}
			js = d.createElement( s );
			js.id = id;
			js.src = `//connect.facebook.net/${language}/all.js`;
			fjs.parentNode.insertBefore( js, fjs );
		})( document, 'script', 'facebook-jssdk' );
	}

	responseApi = ( authResponse ) => {
		window.FB.api( '/me', { fields: this.props.fields }, ( me ) => {
			Object.assign( me, authResponse );
			this.props.callback( me );
		} );
	};

	checkLoginState = ( response ) => {
		console.log( 'checkLoginState', response.status )
		this.setState( { isProcessing: false } );
		if( response.authResponse ) {
			this.responseApi( response.authResponse );
		} else {
			if( this.props.callback ) {
				this.props.callback( { status: response.status } );
			}
		}
	};

	checkLoginAfterRefresh = ( response ) => {
		if( response.status === 'unknown' ) {
			window.FB.login( loginResponse => this.checkLoginState( loginResponse ), true );
		} else {
			this.checkLoginState( response );
		}
	};

	onClickLogin = () => {
		if( !this.state.isSdkLoaded || this.state.isProcessing || this.props.isDisabled ) {
			return;
		}
		this.setState( { isProcessing: true } );
		const { scope, appId, reAuthenticate, disableMobileRedirect } = this.props;

		let isMobile = false;

		try {
			isMobile = ((window.navigator && window.navigator.standalone) ||
			navigator.userAgent.match( 'CriOS' ) || navigator.userAgent.match( /mobile/i ));
		} catch( ex ) {
			// continue regardless of error
		}

		const params = {
			client_id: appId,
			redirect_uri: window.location.href,
			state: 'facebookdirect',
			scope
		};

		if( reAuthenticate ) {
			params.auth_type = 'reauthenticate';
		}

		if( isMobile && !disableMobileRedirect ) {
			window.location.href = `//www.facebook.com/dialog/oauth?${_objectToParams( params )}`;
		} else {
			window.FB.login( this.checkLoginState, { scope, auth_type: params.auth_type } );
		}
	};

	render() {
		const { t } = this.props;
		let content;
		if( this.props.facebookLoggedIn ) content =
				<Alert message={this.props.account.fullName}
					   description={t( 'You are logged in via Facebook' )}
					   type="success" showIcon/>;
		else content =
				<Button
						onClick={this.onClickLogin}
						type="primary" size="large"
						loading={this.state.isProcessing}
						className="responsive">
					{t( 'Login' )}
				</Button>;
		return (<div>{ content }</div>);
	}
}

export default FacebookLogin;

function _objectToParams( paramsObj ) {
	let str = '';
	for( const key in paramsObj ) {
		if( str !== '' ) {
			str += '&';
		}
		str += `${key}=${encodeURIComponent( paramsObj[ key ] )}`;
	}
	return str;
}
