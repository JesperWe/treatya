import React, { Component } from 'react';
import http from 'axios';
import { Upload, Icon, Button, notification } from 'antd';
import config from '../config';

class StripeFileUpload extends Component {
	customRequest = ( options ) => {
		let formData = new FormData();
		formData.append( 'purpose', 'identity_document' );
		formData.append( 'file', options.file );

		http.post( 'https://uploads.stripe.com/v1/files', formData, {
					headers: {
						'Authorization': 'Bearer ' + (config.stripe.isLive ? config.stripe.pk_live : config.stripe.pk_test),
					},
					onUploadProgress: event => {
						event.percent = Math.round( (event.loaded * 100) / event.total );
						options.onProgress( event );
					}
				} )
				.then( result => {
					options.onSuccess( result.data );
					if( this.props.onUploadComplete ) this.props.onUploadComplete( result.data );
				} )
				.catch( error => {
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Stripe File Upload' ] );
					notification.error( { message:  error.message } )
					console.log( error.message );
					console.log( error.stack );
				} )
	}

	render() {

		let uploadOptions = {
			name: 'file',
			customRequest: this.customRequest
		}

		return (
				<Upload {...uploadOptions}>
					<Button type="ghost">
						<Icon type="upload"/> {this.props.text}
					</Button>
				</Upload>
		);
	}
}

export default StripeFileUpload;
