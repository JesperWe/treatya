import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { translate } from 'react-i18next'
import { graphql, compose } from 'react-apollo'
import http from 'axios'
import jwt from '../jwt'
import _forEach from 'lodash/forEach'
import { doLogin, registerAccount } from '../graphql/account'
import cookie from 'browser-cookies'
import moment from 'moment'
import errorHandler from '../errorHandler'
import i18n from '../i18n'
import FacebookLogin from './FacebookLogin'
import InfoSplash from '../InfoSplash'
import Loading from '../Loading'
import { Input, Button, Tabs, Form, Checkbox, Alert, Modal, Icon, Popconfirm, notification } from 'antd'
import config from '../config'
import './Login.less'

const DEBUG = false

class Login extends React.Component {
	state = {
		facebook: {},
		facebookLoggedIn: false,
		email: '',
		inProgress: false
	}

	componentWillReceiveProps( next ) {
		// Infosplach on first log in. Was overkill maybe....
		/*let { type } = next.routeParams
		 if( type && type === 'first' ) {
		 const { t } = this.props
		 this.props.onInfoSplash( {
		 id: 'firstLogin',
		 title: t( 'first-login-title' ),
		 content: t( 'first-login-content' )
		 } )
		 }*/
	}

	register = () => {
		let values = this.props.form.getFieldsValue()
		delete values.remember
		let url = new URL( window.location )
		values.url = url.pathname
		if( url.pathname === '/login' ) {
			values.url = '/'
			values.noVerification = true
		}

		_forEach( values, ( value, key ) => {
			if( !value || !value.length ) return
			if( key === 'password' ) {
				// To not expose passwd in URL, we cheat a bit and store it in a global var.
				url.searchParams.append( key, 'true' )
				window._registerPrefilledPassword = value
			}
			else {
				url.searchParams.append( key, value )
			}
		} )
		browserHistory.push( '/register' + url.search )
	}

	login = () => {
		this.props.form.validateFields( ( error, values ) => {
			if( error ) {
				DEBUG && console.log( 'Received values of form: ', values )
				return
			}
			this.props.login( this.props.form.getFieldsValue() )
					.then( result => {
								let authToken = result.data.authenticate.jwtToken
								if( authToken ) {
									let authInfo = jwt.decode( authToken, '', true )
									DEBUG && console.log( authInfo )
									let cookieExpiration
									if( values.remember ) cookieExpiration = { expires: moment( authInfo.exp * 1000 ).toDate() }
									cookie.set( 'auth-token', authToken, cookieExpiration )
									this.props.onLogin( authToken, authInfo )
									if( this.props.onComplete ) this.props.onComplete() // Parent component.
									else {
										browserHistory.push( '/' )
									}
								} else {
									notification.warning( { message: 'Authetication failed.' } )
								}
							},
							error => {
								cookie.erase( 'auth-token' )
								errorHandler( error )
							}
					)
		} )
	}

	getUsername() {
		if( !this.props.account ) return '<unknown>'
		if( this.props.account.fullName ) return this.props.account.fullName
	}

	facebookResponse = ( fbResponse ) => {
		const { t } = this.props
		if( fbResponse.accessToken ) {
			http.post( config.backend.url + '/auth/facebook', fbResponse )
					.then( ( response ) => {
						let { authToken } = response.data
						if( authToken ) {
							cookie.set( 'auth-token', authToken )
							this.setState( { facebookLoggedIn: true } )
							let authInfo = jwt.decode( authToken, '', true )
							this.props.onLogin( authToken, authInfo ) // Redux
							console.log( authToken )
						}
					} )
					.catch( ( error ) => {
						console.log( error )
						if( error.response && error.response.data && error.response.data.sameEmail ) {
							Modal.info( {
								title: t( 'Duplicate Account' ),
								content: (
										<p>{t( 'You already have a Treatya account with the same email as your Facebook account.' )}</p>
								)
							} )
							return
						}
						notification.error( {
							message: t( 'Server did not verify access token.' ),
							description: error.response.data.error
						} )
					} )
		}
	}

	sendResetPasswd = () => {
		const { t } = this.props
		if( !this.state.email || this.state.email.length < 6 ) {
			notification.warning( { message: t( 'Fill in your e-mail address first!' ) } )
			return
		}

		this.setState( { inProgress: true } )
		http.post( config.backend.url + '/auth/send-reset-pwd',
				{ email: this.state.email },
				{ headers: { 'Accept-Language': i18n.language } }
				)
				.then( response => {
					this.setState( { inProgress: false } )

					if( response.data === 'not found' ) {
						notification.success( {
							message: t( 'No user found.' )
						} )
					} else {
						notification.success( {
							message: t( 'Check your inbox!' ),
							description: t( 'You  should receive a reset code shortly.' )
						} )
						browserHistory.push( '/reset-passwd' )
					}
				} )
				.catch( errorHandler )
	}

	render() {
		const { getFieldDecorator } = this.props.form
		const { t } = this.props

		let localTab
		if( this.props.authToken )
			localTab =
					<div>
						<Alert message={<span>{t( 'You are logged in as' )} <b>{this.getUsername()}</b></span>}
							   type="success" showIcon className="vertical-spacing"/>
					</div>
		else {
			var self = this
			localTab =
					<Form onSubmit={this.login} className="login-form">
						<Form.Item>
							{getFieldDecorator( 'email', {
								rules: [ {
									type: 'email', message: t( 'The input is not valid E-mail.' ),
								}, {
									required: true, message: t( 'Please input your E-mail.' ),
								} ],
								getValueFromEvent: function( e ) { // Enable access to entered email outside of form submit.
									self.setState( { email: e.target.value } )
									return e.target.value
								}
							} )(
									<Input addonBefore={<Icon type="user"/>}
										   size="large" placeholder={t( 'E-mail' )}/>
							)}
						</Form.Item>
						<Form.Item>
							{getFieldDecorator( 'password', {
								rules: [ { required: true, message: t( 'Please input your Password.' ) } ],
							} )(
									<Input addonBefore={<Icon type="lock"/>} type="password"
										   size="large" placeholder={t( 'Password' )}/>
							)}
						</Form.Item>
						<Form.Item>
							{getFieldDecorator( 'remember', {
								valuePropName: 'checked',
								initialValue: true,
							} )(
									<Checkbox className="float-left">{t( 'Remember me' )}</Checkbox>
							)}

							<Popconfirm title={
								<span>
								{t( 'Do you want to send password reset information to:' )}
									<br/><b>{this.state.email}</b>
								</span>}
										okText={t( 'Yes' )} cancelText={t( 'No' )}
										onConfirm={this.sendResetPasswd}>
								<a className="login-form-forgot">{t( 'Forgot password' )}</a>
							</Popconfirm>

							<Button type="primary" onClick={this.login} size="large" value="login-form-button">
								{t( 'Log in' )}
							</Button>

							<div className="centered">{t( 'Or' )}</div>

							<Button type="default" onClick={this.register} size="large" value="register-button">
								{t( 'Register New Account' )}
							</Button>
						</Form.Item>
					</Form>
		}

		return (
				<div className="relative">

					{this.state.inProgress &&
					<div className="spinner">
						<Loading/>
					</div>
					}

					<InfoSplash/>

					<div className="login-widget">
						{(this.props.header) ? this.props.header : <h2>{t( 'Log in with:' )}</h2>}
						<Tabs>

							<Tabs.TabPane tab="Treatya" key="1">{localTab}</Tabs.TabPane>

							<Tabs.TabPane tab="Facebook" key="2">
								<FacebookLogin
										appId="258339574583090"
										autoLoad={false}
										t={this.props.t}
										account={this.props.account}
										fields="name,first_name,last_name,email,picture"
										callback={this.facebookResponse}
										disableMobileRedirect={true}
										facebookLoggedIn={this.state.facebookLoggedIn}
								/>
							</Tabs.TabPane>

						</Tabs>
					</div>
				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				authToken: state.authToken,
				account: state.account
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onInfoSplash: ( infoSplash ) => dispatch( { type: 'INFOSPLASH', infoSplash } ),
				onLogin: ( authToken, authInfo ) => dispatch( { type: 'LOGIN', authToken, authInfo } ),
				onLogout: () => dispatch( { type: 'LOGOUT' } )
			}
		}
)(
		compose(
				graphql( doLogin.gql, doLogin.options ),
				graphql( registerAccount.gql, registerAccount.options )
		)(
				Form.create()(
						translate( [ 'common' ], { wait: true } )(
								Login
						)
				)
		)
)
