import React from 'react'
import http from 'axios'
import { translate } from 'react-i18next'
import { Button, Icon, notification } from 'antd'
import i18n from '../i18n'
import config from '../config'
import './VerifyButtons.less'

class VerifyEmailButton extends React.Component {
	state = {
		sendingEmail: false,
	}

	verifyEmail = () => {
		const { t } = this.props

		this.setState( { sendingEmail: true } )

		http.post( config.backend.url + '/verify/email', {
					id: this.props.id,
					jwt: this.props.authToken,
					email: this.props.emailUnsaved ? this.props.emailUnsavedValue : this.props.email,
					language: this.props.language
				}, {
					headers: { 'Accept-Language': i18n.language }
				} )
				.then( ( response ) => {
					notification.info( {
						message: t( 'Check your inbox!' ),
						description: t( 'A verification message has been sent to your E-Mail.' )
					} )
					this.setState( { sendingEmail: false } )
				} )
				.catch( error => {
					this.setState( { sendingEmail: false } )
					if( error.response && error.response.data ) {
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.response.data, 'Verification Email' ] )
						notification.error( { message: 'From e-mail service: ' + error.response.data } )
					} else {
						config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Verification Email' ] )
						notification.error( { message: 'Send e-mail request failed: ' + error.message + ' (' + error.status + ')' } )
					}
				} )
	}

	render() {
		const { t } = this.props

		let emailIcon = (<Icon type="exclamation-circle"/>)
		if( this.state.sendingEmail ) emailIcon = (<Icon type="loading"/>)

		let emailButton = this.props.email && !this.props.emailVerified

		return (
				<div className="verify-button e-mail">
					{emailButton &&
					<Button type="danger" size="large" onClick={this.verifyEmail} disabled={this.props.emailUnsaved}>
						{emailIcon} {t( 'Click to verify E-Mail' )}
					</Button>
					}
				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( VerifyEmailButton )
