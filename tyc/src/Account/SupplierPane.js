import React from 'react'
import { browserHistory } from 'react-router'
import _get from 'lodash/get'
import _set from 'lodash/set'
import _cloneDeep from 'lodash/cloneDeep'
import { translate } from 'react-i18next'
import { Input, Form, Row, Col, Button, Select, Alert, Popconfirm, notification } from 'antd'
import S3Upload from './S3Upload'
import errorHandler from '../errorHandler'
import { graphql, compose } from 'react-apollo'
import { getSupplier, updateSupplier, setSupplierStatus } from '../graphql/supplier'
import { updateAccount } from '../graphql/account'

class SupplierFormClass extends React.Component {

	handleSubmit = ( event ) => {
		event.preventDefault()
		this.props.form.validateFields( ( error, values ) => {
			if( error ) {
				notification.warning( { message: this.props.t( 'Please fill in all required fields.' ), duration: 2 } )
			} else {
				console.log( 'Received values of form: ', values )
				let supplier = { ...values }

				supplier.imageProfileRot = 0
				if( supplier.imageProfile && this.orientation > 0 ) {
					switch( this.orientation ) {
						case 3:
							supplier.imageProfileRot = 180
							break
						case 6:
							supplier.imageProfileRot = 90
							break
						case 8:
							supplier.imageProfileRot = 270
							break
						default:
							break
					}
				}
				this.props.onSave( supplier )
			}

		} )
	}

	render() {
		const { getFieldDecorator } = this.props.form
		const { t } = this.props
		return (
				<Form className="supplier" hideRequiredMark={true} onSubmit={this.handleSubmit}>

					<Row gutter={12}>
						<Col xs={24} sm={24}>
							<Form.Item label={t( 'Supplier name' )} hasFeedback={true}>
								{getFieldDecorator( 'title', {
									initialValue: this.props.supplier && this.props.supplier.title,
									validateTrigger: 'onBlur',
									rules: [ { required: true, message: t( 'Enter a supplier title.' ) } ]
								} )( <Input size="large" placeholder={t( 'Supplier Title' )}/> )}
							</Form.Item>
						</Col>
					</Row>

					<Row gutter={12}>
						<Col xs={24} sm={24}>
							<Form.Item label={t( 'Intro' )} hasFeedback={true}>
								{getFieldDecorator( 'intro', {
									initialValue: this.props.supplier && this.props.supplier.intro,
									validateTrigger: 'onBlur',
									rules: [ { required: true, message: t( 'Enter an introductory text.' ) } ]
								} )( <Input size="large" placeholder={t( 'Supplier Introduction' )}/> )}
							</Form.Item>
						</Col>
					</Row>

					<Row gutter={12}>
						<Col xs={24}>
							<Form.Item label={t( 'Description' )} hasFeedback={true}>
								{getFieldDecorator( 'description', {
									initialValue: this.props.supplier && this.props.supplier.description,
									rules: [ { required: true, message: t( 'Enter the supplier description.' ) } ]
								} )( <Input.TextArea autosize={{ minRows: 2, maxRows: 6 }}
													 placeholder={t( 'Supplier description' )}/> )}
							</Form.Item>
						</Col>
					</Row>

					<Row gutter={12}>

						<Col xs={24} sm={12}>
							<Form.Item label={t( 'Image (profile)' )}>
								{getFieldDecorator( 'imageProfile', {
									initialValue: _get( this.props, 'supplier.imageProfile' ),
									rules: [ { required: false, message: t( 'Upload an image.' ) } ]
								} )( <S3Upload text={t( 'Upload Image' )} {...this.props}
											   rotation={_get( this.props, 'supplier.imageProfileRot' )}
											   exifOrientation={o => this.orientation = o}/> )}
							</Form.Item>
						</Col>

						<Col xs={24} sm={12}>
							<Form.Item label={t( 'Image (background)' )}>
								{getFieldDecorator( 'imageBg', {
									initialValue: _get( this.props, 'supplier.imageBg' ),
									rules: [ { required: false, message: t( 'Upload an image.' ) } ]
								} )( <S3Upload text={t( 'Upload Image' )} {...this.props} multiple={true}/> )}
							</Form.Item>
						</Col>

					</Row>

					<Row gutter={12}>
						<Col xs={24}>
							{t( 'Address information for the individual or corporate entity responsible for the treatments.' )}
						</Col>
					</Row>

					<Row gutter={12}>

						<Col xs={24} sm={12}>
							<Form.Item label={t( 'Postal address' )} hasFeedback={true}>
								{getFieldDecorator( 'address1', {
									initialValue: this.props.supplier && this.props.supplier.address1,
									rules: [ { required: true, message: t( 'Enter postal address.' ) } ]
								} )( <Input size="large" placeholder={t( 'Postal address line 1' )}/> )}
							</Form.Item>
							<Form.Item label={t( 'Line 2' )} hasFeedback={true}>
								{getFieldDecorator( 'address2', {
									initialValue: this.props.supplier && this.props.supplier.address2,
									rules: [ { required: false } ]
								} )( <Input size="large" placeholder={t( 'Postal address line 2' )}/> )}
							</Form.Item>
						</Col>

						<Col xs={24} sm={12}>
							<Form.Item label={t( 'Postal code' )} hasFeedback={true}>
								{getFieldDecorator( 'zip', {
									initialValue: this.props.supplier && this.props.supplier.zip,
									rules: [ { required: true, message: t( 'Enter postal code.' ) } ]
								} )( <Input size="large" placeholder={t( 'Postal code' )}/> )}
							</Form.Item>
							<Form.Item label={t( 'City' )} hasFeedback={true}>
								{getFieldDecorator( 'city', {
									initialValue: this.props.supplier && this.props.supplier.city,
									rules: [ { required: true, message: t( 'Enter postal city.' ) } ]
								} )( <Input size="large" placeholder={t( 'City' )}/> )}
							</Form.Item>
							<Form.Item label={t( 'Country' )} hasFeedback={true}>
								{getFieldDecorator( 'country', {
									initialValue: this.props.supplier && this.props.supplier.country,
									rules: [ { required: true, message: t( 'Select country' ) } ]
								} )(
										<Select size="large" placeholder={t( 'Select country' )}>
											<Select.Option value="SE">{t( 'Sweden' )}</Select.Option>
										</Select> )}
							</Form.Item>
						</Col>

					</Row>

					<Row gutter={12}>
						<Col xs={24} sm={12}>
							{this.props.account.role === 'ADMIN' &&
							<Form.Item label={t( 'Status' )} hasFeedback={true}>
								{getFieldDecorator( 'status', {
									initialValue: this.props.supplier && this.props.supplier.status,
									validateTrigger: 'onChange',
									rules: [ { required: true, message: t( 'Select supplier status.' ) } ]
								} )(
										<Select size="large" placeholder={t( 'Status' ) + '...'}>
											<Select.Option
													value="APPLICATION">{t( 'Has applied' )}</Select.Option>
											<Select.Option value="CONFIRMED">{t( 'Confirmed' )}</Select.Option>
											<Select.Option value="DENIED">{t( 'Denied' )}</Select.Option>
										</Select>
								)}
							</Form.Item>
							}
						</Col>
						<Col xs={24} sm={12}>
							<Form.Item style={{ marginTop: '35px' }}>
								{getFieldDecorator( 'save', {} )(
										<Button type="primary" size="large" htmlType="submit">{t( 'Save' )}</Button>
								)}
							</Form.Item>
						</Col>
					</Row>
				</Form>
		)
	}
}

const SupplierForm = Form.create( {
	onFieldsChange( props, changedFields ) {
		props.onChange( changedFields )
	}
} )( SupplierFormClass )

class SupplierPane extends React.Component {
	state = {
		modified: false
	}

	updateSupplier = ( supplierForm ) => {
		const { t } = this.props
		let supplierStatus = supplierForm.status
		delete supplierForm.status

		if( supplierStatus && supplierStatus !== this.props.supplier.status ) {
			this.props.setSupplierStatus( supplierStatus ).catch( errorHandler )
		}

		this.props.updateSupplier( supplierForm )
				.then( result => {
					notification.success( { message: t( 'Supplier saved.' ) } )
					this.setState( { modified: false } )
					if( !supplierForm.imageBg && !supplierForm.imageProfile ) return
					if( !_get( this.account, 'uiSettings.supplierRegistration.steps.images' ) ) {
						let uiSettings = _cloneDeep( this.props.account.uiSettings )
						_set( uiSettings, 'supplierRegistration.steps.images', { done: true } )
						_set( uiSettings, 'infoSplash.supplierWelcome', { dismissed: true } )
						this.props.updateAccount( this.props.authInfo.id, { uiSettings } )
					}
				} )
				.catch( errorHandler )
	}

	render() {
		const { t } = this.props

		let previewText = t( 'Preview your Supplier Page as clients see it' )
		if( window.matchMedia( "(max-width: 600px)" ).matches ) previewText = t( 'Preview Supplier Page' )

		if( _get( this, 'props.account.suppliersByAccountId.nodes[0].status' ) !== 'CONFIRMED' )
			return (
					<div className="vertical-spacing centered" style={{ paddingBottom: '1rem' }}>
						<Alert message={t( 'You application is currently being processed.' )} description={' '}
							   type="info" showIcon/>
					</div>
			)
		else
			return (
					<div className="supplier-pane">
						<div className="vertical-spacing">
							{this.state.modified &&
							<Popconfirm title={t( 'You will loose some unsaved changed. Ar you sure?' )}
										okText={t( 'Yes' )} cancelText={t( 'No' )}
										onConfirm={() => browserHistory.push( '/supplier/' + this.props.supplier.id )}>
								<Button type="primary" size="large" className="responsive preview">
									{previewText}
								</Button>
							</Popconfirm>
							}

							{!this.state.modified &&
							<Button type="primary" size="large" className="responsive preview"
									onClick={() => browserHistory.push( '/supplier/' + this.props.supplier.id )}>
								{previewText}
							</Button>
							}
						</div>

						<SupplierForm {...this.props} onSave={this.updateSupplier}
									  onChange={() => this.setState( { modified: true } )}/>
					</div>
			)
	}
}

export default compose(
		graphql( getSupplier.gql, getSupplier.options ),
		graphql( setSupplierStatus.gql, setSupplierStatus.options ),
		graphql( updateSupplier.gql, updateSupplier.options ),
		graphql( updateAccount.gql, updateAccount.options )
)( translate( [ 'common' ], { wait: true } )( SupplierPane ) )
