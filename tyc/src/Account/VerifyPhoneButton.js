import React from 'react'
import http from 'axios'
import { translate } from 'react-i18next'
import cookie from 'browser-cookies'
import _get from 'lodash/get'
import _set from 'lodash/set'
import PropTypes from 'prop-types'
import _cloneDeep from 'lodash/cloneDeep'
import { Button, Modal, Input, Form, notification } from 'antd'
import i18n from '../i18n'
import errorHandler from '../errorHandler'
import config from '../config'
import './VerifyButtons.less'
import { withApollo, compose, graphql } from 'react-apollo'
import { updatePhone, updateAccount } from '../graphql/account'

const PhoneForm = Form.create( {
	onFieldsChange( props, changedFields ) {
		props.onChange( changedFields )
	}
} )( ( props ) => {
			const { getFieldDecorator } = props.form
			const { t } = props

			return (
					<Form hideRequiredMark={true}>
						<Form.Item label={t( 'Mobile phone' )} hasFeedback={true}>
							{getFieldDecorator( 'phone', {
								initialValue: props.phone,
								rules: [ {
									required: true, pattern: /^\+[0-9,+,-]{9,}$/,
									message: t( 'Please enter your mobile phone number including country code.' )
								} ]
							} )( <Input type="tel" size="large" placeholder={t( '+46701111111' )}/> )}
						</Form.Item>
					</Form>
			)
		}
)

class VerifyPhoneButton extends React.Component {
	state = {
		sendingSMS: false,
		phoneModal: false,
		phoneToken: null,
		phone: undefined
	}

	sendVerifyPhone = () => {
		let phone = this.props.phone
		if( this.props.phoneUnsaved ) {
			phone = this.props.phoneUnsavedValue
			this.props.updateAccountPhone( this.props.id, phone )
					.catch( errorHandler )
		}

		if( !/^\+[0-9,+,-]{9,}$/.test( phone ) ) return

		this.setState( { sendingSMS: true } )

		if( cookie.get( 'no-sms' ) ) { // console> document.cookie="no-sms=1"
			this.setState( { sendingSMS: false, phoneModal: true } )
			return
		}

		http.post( config.backend.url + '/verify/phone/send', {
					id: this.props.id,
					jwt: this.props.authToken,
					language: i18n.language,
					phone
				} )
				.then( ( response ) => {
					this.setState( { sendingSMS: false, phoneModal: true } )
				} )
				.catch( error => {
					this.setState( { sendingSMS: false } )
					errorHandler( error )
				} )
	}

	checkVerifyPhoneToken = ( event ) => {
		const { t, onSuccess } = this.props

		http.post( config.backend.url + '/verify/phone/check', {
					id: this.props.id,
					jwt: this.props.authToken,
					phone: this.props.phone,
					hmac: this.state.phoneToken
				} )
				.then( ( response ) => {
					this.setState( { phoneModal: false } )
					notification.success( { message: t( 'Verification completed successfully.' ) } )
					onSuccess && onSuccess()
					// Update supplier registration steps wizard.
					if( this.props.emailVerified ) {
						let uiSettings = _cloneDeep( this.props.uiSettings )
						_set( uiSettings, 'supplierRegistration.steps.contacts', { done: true } )
						_set( uiSettings, 'infoSplash.supplierWelcome', { dismissed: true } )
						this.props.updateAccount( this.props.id, { uiSettings } )
					}
				} )
				.catch( error => {
					this.setState( { sendingSMS: false } )
					if( _get( error, 'response.data', '' ).indexOf( 'Verification failed' ) >= 0 )
						notification.warning( { message: t( 'Verification failed.' ) } )
					else
						errorHandler( error )
				} )
	}

	onPhoneInput = ( values ) => {
		let { phone } = values
		this.setState( { phone } )
		if( phone.validating || phone.errors ) return
		this.props.updateAccountPhone( this.props.authInfo.id, phone.value )
				.catch( error => {
					console.log( error.message )
					notification.error( { message: error.message } )
				} )
	}

	render() {
		const { phoneVerified, phoneUnsaved, t } = this.props
		const { phoneModal, phoneToken } = this.state

		let disabled = false
		if( this.props.withInput ) {
			if( this.props.phoneVerified ) return null
			let { phone } = this.state
			if( phone ) {
				disabled = phone.errors || phone.validating
			}
		}
		else {
			if( phoneVerified && !phoneUnsaved ) return null
			disabled = false
			if( phoneUnsaved === undefined ) disabled = true
		}

		return (
				<div className="verify-button phone">

					{this.props.withInput &&
					<PhoneForm t={t} onChange={this.onPhoneInput} phone={this.props.phone}/>
					}

					{!this.state.phoneVerified &&
					<Button disabled={disabled} type="danger" size="large"
							onClick={this.sendVerifyPhone}
							loading={this.state.sendingSMS}
							style={{ marginBottom: '1rem' }}>
						{t( 'Click to verify phone' )}
					</Button>
					}

					<Modal visible={phoneModal} closable={false}
						   className="verify-button phone"
						   onCancel={() => this.setState( { phoneModal: false } )}
						   footer={null}>

						<h2>{t( 'We have sent an SMS to you with a code.' )}</h2>
						{t( 'Enter this code:' )}<br/>
						<Input className="code" size="large"
							   onChange={event => this.setState( { phoneToken: event.currentTarget.value } )}/>
						<Button type="primary" size="large" className="responsive"
								disabled={!phoneToken || phoneToken.length < 4}
								onClick={this.checkVerifyPhoneToken}>{t( 'Verify' )}</Button>

					</Modal>
				</div>
		)
	}
}

VerifyPhoneButton.propTypes = {
	phone: PropTypes.string,
	phoneUnsaved: PropTypes.bool,
	phoneUnsavedValue: PropTypes.string,
	phoneVerified: PropTypes.bool,
	withInput: PropTypes.bool
}

export default withApollo(
		compose(
				graphql( updatePhone.gql, updatePhone.options ),
				graphql( updateAccount.gql, updateAccount.options )
		)(
				translate( [ 'common' ], { wait: true } )( VerifyPhoneButton )
		)
)
