import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { browserHistory } from 'react-router'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import { Input, Button, Form, notification } from 'antd'
import config from '../config'

class ResetPasswd extends Component {

	state = {
		password: '',
		passwordDirty: false
	}

	handleSubmit = ( event ) => {
		const { t } = this.props
		event.preventDefault()
		this.props.form.validateFields( ( error, values ) => {
			if( !error ) {
				this.props.accountPwd( values )
						.then( result => {
							if( result.data.accountPwd.integer ) {
								notification.success( { message: t( 'Your password was successfully updated.' ) } )
								browserHistory.push( '/login' )
							} else {
								notification.error( { message: t( 'Check your code. Update failed.' ) } )
							}
						} )
						.catch( error => {
							config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'Password Reset' ] )
							notification.error( { message: error.message } )
						} )
			}
		} )
	}

	checkPassword = ( rule, value, callback ) => {
		const { t } = this.props
		const form = this.props.form
		if( value && value !== form.getFieldValue( 'password' ) ) {
			callback( t( 'The passwords do not match.' ) )
		} else {
			callback()
		}
	}

	handlePasswordBlur = ( event ) => {
		const value = event.target.value
		this.setState( { passwordDirty: this.state.passwordDirty || !!value } )
	}

	render() {
		const { t } = this.props
		const { getFieldDecorator } = this.props.form

		return (
				<div className="reset-pwd content-width-small content-pad">
					<Form layout="vertical" onSubmit={this.handleSubmit} hideRequiredMark={true}>
						<Form.Item label={t( 'Reset Code' )} hasFeedback>
							{getFieldDecorator( 'token', {
								initialValue: this.props.params.token,
								rules: [ {
									required: true, message: t( 'Please paste the reset code from the E-mail.' ),
								} ],
							} )( <Input size="large"/> )}
						</Form.Item>
						<Form.Item label={t( 'New Password' )} hasFeedback>
							{getFieldDecorator( 'password', {
								initialValue: this.state.password,
								rules: [ {
									required: true,
									message: t( 'Minimum 8 characters of mixed types.' ),
									min: 8
								}, { validator: this.checkConfirm } ]
							} )(
									<Input size="large" type="password" onBlur={this.handlePasswordBlur}/>
							)}
						</Form.Item>
						<Form.Item label={t( 'Confirm Password' )} hasFeedback>
							{getFieldDecorator( 'confirm', {
								rules: [ {
									required: true, message: t( 'Please confirm your password.' ),
								}, { validator: this.checkPassword } ],
							} )(
									<Input size="large" type="password"/>
							) }
						</Form.Item>
						<Form.Item>
							<Button type="primary" htmlType="submit" size="large">{t( 'Save' )}</Button>
						</Form.Item>
					</Form>
				</div>
		)
	}
}

const gqlAccountPwd = gql`
mutation ($token: String!, $password: String!) {
  accountPwd(input: {password: $password, token: $token}) {
    integer
  }}`
let accountPwdOptions = {
	props: ( { mutate } ) => ({
		accountPwd: ( { token, password } ) => mutate( {
			variables: { token, password }
		} )
	} )
}

export default
graphql( gqlAccountPwd, accountPwdOptions )(
		Form.create()(
				translate( [ 'common' ], { wait: true } )( ResetPasswd )
		)
)
