import React, { Component } from 'react'
import http from 'axios'
import { Upload, Icon, Button, Popconfirm, notification } from 'antd'
import config from '../config'
import './S3Upload.less'

const DEBUG = false

function getExifOrientation( file, callback ) {
	if( !callback ) return

	let reader = new FileReader()

	reader.onload = function( e ) {
		let view = new DataView( e.target.result )
		if( view.getUint16( 0, false ) !== 0xFFD8 ) return callback( -2 )
		let length = view.byteLength, offset = 2
		while( offset < length ) {
			let marker = view.getUint16( offset, false )
			offset += 2

			if( marker === 0xFFE1 ) {
				offset += 2
				if( view.getUint32( offset, false ) !== 0x45786966 )
					return callback( -1 )

				let little = view.getUint16( offset += 6, false ) === 0x4949
				offset += view.getUint32( offset + 4, little )
				let tags = view.getUint16( offset, little )
				offset += 2

				for( let i = 0; i < tags; i++ )
					if( view.getUint16( offset + (i * 12), little ) === 0x0112 )
						return callback( view.getUint16( offset + (i * 12) + 8, little ) )
			}

			else if( (marker & 0xFF00) !== 0xFF00 ) break
			else
				offset += view.getUint16( offset, false )
		}
		return callback( -1 )
	}

	reader.readAsArrayBuffer( file )
}

class S3Upload extends Component {

	state = { img: null, fileList: null }

	init = ( props ) => {
		if( props.value && props.value.length ) this.setState( {
			img: props.value,
			rotation: props.rotation
		} )
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( 'S3Upload.componentWillReceiveProps()' )
		this.init( next )
	}

	componentWillMount() {
		DEBUG && console.log( 'S3Upload.componentWillMount()' )
		this.init( this.props )
	}

	customRequest = ( options ) => {
		http.get( config.backend.url + '/s3/url', {
					params: {
						id: this.props.authInfo.id,
						jwt: this.props.authToken,
						filename: options.file.name
					}
				} )
				.then( result => {
					return http.put( result.data.url, options.file, {
						headers: { 'Content-Type': options.file.type },
						onUploadProgress: event => {
							event.percent = Math.round( (event.loaded * 100) / event.total )
							options.onProgress( event )
						}
					} )
				} )
				.then( result => {
					try {
						let url = result.config.url.split( '?' )[ 0 ]
						let { img } = this.state
						let newState

						if( !img && this.props.multiple ) newState = { img: [ url ] }
						else if( img instanceof Array ) newState = { img: [ ...img, url ] }
						else newState = { img: url }

						this.setState( newState )
						if( this.props.onChange ) this.props.onChange( newState.img )
						options.onSuccess( result.data )
					}
					catch( e ) {
						console.log( 'Image upload result problem:', e.message )
						options.onError( e )
					}
				} )
				.catch( error => {
					config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, 'S3 Upload' ] )
					notification.error( { message: error.message } )
					console.log( error.message )
					console.log( error.stack )
				} )
	}

	beforeUpload = ( file ) => {
		getExifOrientation( file, this.props.exifOrientation )
	}

	handleDelete = ( index ) => {
		if( !(this.state.img instanceof Array) ) return
		let newImg = [ ...this.state.img ]
		newImg.splice( index, 1 )
		this.setState( { img: newImg } )
		if( this.props.onChange ) this.props.onChange( newImg )
	}

	render() {

		let uploadOptions = {
			name: 'file',
			accept: 'image/*',
			customRequest: this.customRequest,
			beforeUpload: this.beforeUpload
		}

		const { img } = this.state
		const { t } = this.props

		return (
				<div className="upload">
					{img && (typeof img === 'string') &&
					<div style={{
						transform: (this.state.rotation ? 'rotate(' + this.state.rotation + 'deg)' : undefined)
					}}>
						<img src={img} alt="" className="responsive"/>
					</div>
					}

					{img && (img instanceof Array) &&
					<div className="image-list">
						{img.map( ( src, index ) => (
								<div className="img-wrap" key={index}>
									<img src={src} alt="" className="responsive"/>

									<div className="img-buttons">
										<Popconfirm title={t( 'Are you sure?' )} okText={t( 'Yes' )}
													cancelText={t( 'No' )}
													onConfirm={() => this.handleDelete( index )}>
											<Icon type="close-circle-o" title={src}/>
										</Popconfirm>
									</div>

								</div>
						) )}
					</div>
					}

					<Upload {...uploadOptions} className="maybe-rotated">
						<Button type="primary">
							<Icon type="upload"/> {this.props.text}
						</Button>
					</Upload>
				</div>
		)
	}
}

export default S3Upload
