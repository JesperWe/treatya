import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { graphql, compose, withApollo } from 'react-apollo'
import { translate } from 'react-i18next'
import _get from 'lodash/get'
import _isEqual from 'lodash/isEqual'
import { notification } from 'antd'
import io from 'socket.io-client'
import diff from 'deep-diff'
import errorHandler from '../errorHandler'
import cookie from 'browser-cookies'
import jwt from '../jwt'
import i18n from '../i18n'
import moment from 'moment'
import { getSupplierInfo, allMotds, accountMotd, accountInfo, accountCounters } from '../graphql/account'
import { typeImages } from '../graphql/browse'
import config from '../config'

// This service receives an accountId prop from Redux, and returns the complete account record
// to Redux once the graphQL query completes.

const DEBUG = false

class AccountService extends React.Component {
	state = {
		checkMOTD: true,
		showSupplierSplash: true,
	}

	handleEvent = () => {
		// Take care of reloading supplier info when external (Stripe, mostly) events cause
		// account.event_seq to increment.

		let supplierId = _get( this, 'props.accountById.data.suppliersByAccountId.nodes[0].id' )
		DEBUG && console.log( '>>>> RESET by Event Sequence Trigger, supplierId', supplierId )

		this.props.client.reFetchObservableQueries()

		if( supplierId > 0 ) {
			this.props.client.query( {
						query: getSupplierInfo.gql,
						variables: { id: supplierId },
						fetchPolicy: 'network-only'
					} )
					.then( result => {
						this.props.onSupplierInfo( result.data.getSupplierInfo.nodes[ 0 ] )
						if( window.location.pathname === '/profile/supplier' ) browserHistory.push( '/profile/contact' )
					} )
					.catch( errorHandler )
		}
	}

	pollCounters = ( options ) => {
		/*DEBUG &&*/
		console.log( 'AccountService.pollCounters' )
		let accountId = _get( options, 'accountId' ) || 	_get( this.props, 'authInfo.id' )
		if( !accountId ) {
			DEBUG && console.log( 'pollCounters: No account id.' )
			return
		}

		this.props.client.query( {
					query: accountCounters.gql,
					variables: { accountId },
					fetchPolicy: 'network-only'
				} )
				.then( result => {
					let counters = _get( result, 'data.accountCounters.nodes[0]' )
					this.props.onCounters( counters )
					if( !options || !options.noHandler ) this.handleEvent()
				} )
				.catch( error => {
					if( (_get( error, 'graphQLErrors[0].message' ) || '').indexOf( 'permission denied' ) >= 0 ) {
						// The auth token probably expired.
						DEBUG && console.log( 'Logged out. Expired token?' )
						this.props.onLogout()
					} else {
						errorHandler( error )
					}
				} )
	}

	connectEventNotifier = () => {
		if( !this.props.authToken || this.connection ) return

		DEBUG && console.log( 'Open Event WebSocket' )
		try {
			this.connection = io( config.backend.event )

			this.connection.on( 'connect', () => {
				DEBUG && console.log( 'Event WebSocket connected.' )
				this.connection.emit( 'authorize', { jwt: this.props.authToken } )
			} )

			this.connection.on( 'disconnect', ( message ) => {
				DEBUG && console.log( 'Event WebSocket closed:', message )
			} )

			this.connection.on( 'reconnect', ( message ) => {
				DEBUG && console.log( 'Event WebSocket reconnect:', message )
			} )

			this.connection.on( 'reconnect_attempt', ( message ) => {
				DEBUG && console.log( 'Event WebSocket reconnect_attempt:', message )
			} )

			this.connection.on( 'reconnecting', ( message ) => {
				DEBUG && console.log( 'Event WebSocket reconnecting:', message )
			} )

			this.connection.on( 'reconnect_error', ( message ) => {
				DEBUG && console.log( 'Event WebSocket reconnect_error:', message )
			} )

			this.connection.on( 'event', this.pollCounters )
			this.connection.on( 'error', message => {
				console.log( 'Event push error.\n', message )
				errorHandler( new Error( 'Event push error: ' + message ) )
			} )
		}
		catch( error ) {
			errorHandler( error )
		}
	}

	componentWillMount() {
		this.connectEventNotifier()
		this.props.client.query( {
					query: typeImages.gql,
					variables: { name: config.backend.settings }
				} )
				.then( result => {
					this.props.onTypeImages( result.data.typeImages )
				} )
				.catch( errorHandler.bind( {}, { from: 'AccountService.componentWillMount', fatal: true } ) )
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( '\n(AccountService) componentWillReceiveProps\n', JSON.stringify( diff( next, this.props ) ) )

		const { t } = this.props
		let nextAccount = _get( next, 'accountById.data' )
		let prevAccount = _get( this.props, 'accountById.data' )

		// If we have accountId but it doesn't load, report error...
		if( next.accountById && !next.accountById.loading && !next.accountById.data && nextAccount && next.accountId !== nextAccount.id ) {
			notification.error( { message: t( 'Your account is no longer available in the system.' ) } )
			cookie.erase( 'auth-token' )
			DEBUG && console.log( 'Logged out, account not loading correctly:\n' )
			DEBUG && console.log( JSON.stringify( next.accountById, null, 2 ) )
			this.props.onLogout()
			browserHistory.push( '/' )
			this.setState( { checkMOTD: true } )
			return
		}

		// Did we get a new account?
		if( next.accountById && next.accountById.data ) {
			let { error } = next.accountById
			if( error ) errorHandler( error )
			else {
				if( !_isEqual( nextAccount, prevAccount ) ) {
					DEBUG && console.log( '...account:', nextAccount )

					if( _get( prevAccount, 'role' ) === 'ACCOUNT' && _get( nextAccount, 'role' ) !== 'ACCOUNT'
							&& _get( nextAccount, 'id' ) === _get( prevAccount, 'id' ) ) {
						console.log( this.props.accountById.data, nextAccount )
						notification.warning( {
							message: t( 'Your account has been upgraded.' ),
							description: t( 'You need to log in again to pick up the changes.' ),
							duration: 0
						} )
						browserHistory.push( '/' )
						this.props.onLogout()
					}

					DEBUG && console.log( 'account -> Redux' )
					this.props.onAccount( nextAccount ) // -> Redux

					this.connectEventNotifier()
					this.pollCounters( { noHandler: true, accountId: nextAccount.id } )

					if( this.loggedIn !== nextAccount.email ) {
						this.loggedIn = nextAccount.email
						config.tracking && window._paq.push( [ 'setUserId', nextAccount.email ] )
						config.tracking && window._paq.push( [ 'trackEvent', 'Account', 'Login', JSON.stringify(nextAccount.email) ] )
					}

					let nextSupplierId = _get( nextAccount, 'suppliersByAccountId.nodes[0].id' )
					if( nextSupplierId > 0 ) {
						this.props.client.query( {
									query: getSupplierInfo.gql,
									variables: { id: nextSupplierId }
								} )
								.then( result => {
									let supplierInfo = result.data.getSupplierInfo.nodes[ 0 ]
									this.props.onSupplierInfo( supplierInfo )

									let supplierStatus = _get( this, 'props.accountById.data.suppliersByAccountId.nodes[0].status' )
									if( window.location.pathname === '/' && this.state.showSupplierSplash && supplierStatus === 'CONFIRMED' ) {
										this.setState( { showSupplierSplash: false } )
										this.props.onInfoSplash( {
											id: 'supplierWelcome',
											title: t( 'supplier-welcome-title' ),
											content: { // object --> Interpolate when rendering.
												i18nKey: 'supplier-welcome-content',
												link: { to: '/profile', text: t( 'Supplier' ) }
											}
										} )
									}
								} )
								.catch( errorHandler )
					}
				}

				if( this.state.checkMOTD ) {
					let anythingShowed = false
					this.props.client.query( {
								query: allMotds.gql,
								fetchPolicy: 'network-only'
							} )
							.then( result => {
								result.data.allMotds.nodes.forEach( msg => {
									let at = moment( msg.publishedAt )
									let last = nextAccount.motd ? moment( nextAccount.motd ) : moment( '2000-01-01 00:00' )
									if( at.isAfter( last ) ) {
										anythingShowed = true
										notification.info( {
											message: at.format( config.date.default ),
											description: msg.message[ i18n.language ],
											duration: 0,
											placement: 'topRight'
										} )
									}
								} )
								this.setState( { checkMOTD: false } )
								if( anythingShowed ) {
									DEBUG && console.log( 'Update Account MOTD...' )
									this.props.client.mutate( {
												mutation: accountMotd.gql,
												variables: {
													id: nextAccount.id,
													motd: moment().format()
												}
											} )
											.then( result => {
												DEBUG && console.log( result.data )
											} )
								}
							} )
							.catch( errorHandler )
				}
			}
		}

		// Poll for events when coming back from idle.
		if( next.isIdle === false && this.props.isIdle === true ) this.pollCounters()

		// Send notification event.
		if( next.sendNotificationToAccountId !== this.props.sendNotificationToAccountId ) {
			if( next.sendNotificationToAccountId && next.sendNotificationToAccountId.length > 0 ) {
				this.connection.emit( 'event', { receiver: next.sendNotificationToAccountId } )
				this.props.onSendNotificationToAccountId( null )
			}
		}

		// This is where we come at logout.
		if( next.accountId !== this.props.accountId && !next.accountId ) {
			this.setState( {
				checkMOTD: true,
				showSupplierSplash: true
			} )
			this.props.onInfoSplash( {} )
			this.connection && this.connection.disconnect()
		}
	}

	componentDidMount() {
		if( !this.props.accountId ) {
			let authCookie = cookie.get( 'auth-token' )
			if( authCookie ) {
				let authInfo = jwt.decode( authCookie, '', true )
				if( moment( authInfo.exp * 1000 ).isBefore() ) { // Token has expired
					cookie.erase( 'auth-token' )
				} else {
					console.log( '(AccountService) login from cookie', authInfo.id )
					this.props.onLogin( authCookie, authInfo )
				}
			}
		}
	}

	componentDidUpdate() {
		if( !this.props.accountId ) {
			cookie.erase( 'auth-token' )
			this.props.onAccount( {} ) // Logged out -> Clear Account in Redux
			this.props.onInfoSplash( {} )
			this.loggedIn = undefined
		}
	}

	render() {
		return (null)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				accountId: state.authInfo && state.authInfo.id,
				authToken: state.authToken,
				authInfo: state.authInfo,
				isIdle: state.isIdle,
				sendNotificationToAccountId: state.sendNotificationToAccountId,
				eventSeq: state.counters && state.counters.eventSeq
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onLogout: () => dispatch( { type: 'LOGOUT' } ),
				onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } ),
				onLogin: ( authToken, authInfo ) => dispatch( { type: 'LOGIN', authToken, authInfo } ),
				onAccount: ( account ) => dispatch( { type: 'ACCOUNT', account } ),
				onSendNotificationToAccountId: ( id ) => dispatch( { type: 'SENDNOTIFICATION', id } ),
				onSupplierInfo: ( supplierInfo ) => dispatch( { type: 'SUPPLIERINFO', supplierInfo } ),
				onCounters: ( counters ) => dispatch( { type: 'COUNTERS', counters } ),
				onInfoSplash: ( infoSplash ) => dispatch( { type: 'INFOSPLASH', infoSplash } ),
				onTypeImages: ( typeImages ) => dispatch( { type: 'TYPEIMAGES', typeImages } ),
			}
		}
)(
		withApollo(
				compose(
						graphql( accountInfo.gql, accountInfo.options )
				)(
						translate( [ 'common' ], { wait: true } )( AccountService )
				)
		)
)
