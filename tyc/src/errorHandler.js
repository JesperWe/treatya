import React from 'react'
import { Modal, notification } from 'antd'
import http from 'axios'
import _get from 'lodash/get'
import cookie from 'browser-cookies'
import config from './config'

export default ( _error, _info ) => {

	let error = _error, info = _info || {}
	if( _info instanceof Error ) {
		// Swap if .bind( {}, info ) was used.
		info = _error
		error = _info
	}

	const offline = 'Failed to fetch' === _get( error, 'networkError.message' )
	if( offline ) {
		let content = 'The app will try to reload.'
		if( info.fatal ) content = <span>Failed to contact the Treatya backend services. Please try again later. <br/>Contact support@treatya.com if the problem persists.</span>
		Modal.error( {
			title: 'Network Error',
			content,
			onOk: () => window.location.reload()
		} )
		return
	}

	let fromString = ''
	if( this && this.from ) fromString = '(From: ' + info.from + ')'
	let serverError = error

	let { response } = error

	let url = _get( response, 'config.url' )
	if( url && !fromString ) {
		url = new URL( url )
		fromString = '(' + url.pathname + ')'
	}

	if( response && response.data ) {
		let message = (typeof response.data === 'string') ? response.data : JSON.stringify( response.data )
		message += ' ' + fromString
		console.log( message )
		config.tracking && window._paq.push( [ 'trackEvent', 'Error', message ] )
		notification.error( { message } )
	}
	else {
		serverError = {
			message: error.message,
			stack: error.stack
		}

		// Remove uninteresting stuff
		if( serverError.message.indexOf( 'supplier_overlapping_timeslot_not_allowed' ) >= 0 ) return
		if( serverError.message.indexOf( 'websocket error' ) >= 0 ) return
		if( serverError.message.indexOf( 'websocket error' ) >= 0 ) return
		if( serverError.message.indexOf( 'NetworkError when attempting to fetch' ) >= 0 ) return
		if( serverError.message.indexOf( 'xhr post error' ) >= 0 ) return

		config.tracking && window._paq.push( [ 'trackEvent', 'Error', error.message, fromString ] )
		console.log( fromString + '\n', error )
		notification.error( { message: error.message + fromString, duration: 10 } )
	}

	let data = {
		error: serverError,
		jwt: cookie.get( 'auth-token' ),
		id: 0,
		agent: navigator && navigator.userAgent
	}

	http.post( config.backend.url + '/notify/error', data ).catch( oops => {
		console.log( 'Error in error handler. The universe is imploding.' )
	} )

}
