export const path = '/booking/:bookingId'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Booking/BookingPage' ).default )
	} )
}

