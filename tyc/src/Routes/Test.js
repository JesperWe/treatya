export const path = 'test(/:param)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Test' ).default )
	} )
}
