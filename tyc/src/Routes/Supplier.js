export const path = '/supplier/:supplierId'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Browser/SupplierRedux' ).default )
	} )
}

