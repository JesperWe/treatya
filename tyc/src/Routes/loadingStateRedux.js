// This module is used to provide a loading indicator for larg codesplitted files
// that take a noticeable amount of time to load.

const loadQueue = []

export const loading = loading => {
	if( loading ) {
		loadQueue.push( true )
	} else {
		loadQueue.pop()
	}

	return {
		type: 'COMPONENTS_LOADING',
		payload: loadQueue.length > 0
	}
}

export const actions = {
	loading
}

const ACTION_HANDLERS = {
	COMPONENTS_LOADING: ( state, action ) => (action.payload)
}

const initialState = false

export default function reducer( state = initialState, action ) {
	const handler = ACTION_HANDLERS[ action.type ]
	return handler ? handler( state, action ) : state
}

export const withLoadingState = ( fn, store ) => {
	return ( nextState, cb ) => {
		store.dispatch( loading( true ) )

		fn( nextState, ( err, cmp ) => {
			store.dispatch( loading( false ) )
			cb( err, cmp )
		} )
	}
}
