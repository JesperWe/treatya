export const path = 'stripe'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Account/StripeAccount' ).default )
	} )
}
