import React from 'react';
import Browser from '../Browser/Browser';

export default props => {
	return <Browser query={props.location.query}/>;
}
