export const path = '/reset-passwd(/:token)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Account/ResetPasswd' ).default )
	} )
}
