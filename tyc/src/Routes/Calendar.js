import { withLoadingState } from './loadingStateRedux'
import reduxStore from '../reduxStore'

export const path = 'calendar(/:view)'

export const getComponent  = withLoadingState( ( nextState, cb ) => {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Calendar/CalendarData' ).default )
	} )
}, reduxStore )

