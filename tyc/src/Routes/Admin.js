export const path = '/admin(/:view)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Admin/Admin' ).default )
	} )
}
