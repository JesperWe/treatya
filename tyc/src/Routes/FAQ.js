export const path = '/faq(/:question)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../CMS/FAQ' ).default )
	} )
}
