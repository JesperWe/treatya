export const path = '/booking-overview(/messages)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Booking/BookingOverview' ).default )
	} )
}
