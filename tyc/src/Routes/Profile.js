export const path = 'profile'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Account/Profile' ).default )
	} )
}
