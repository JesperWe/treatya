export const path = '/register(/:view)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Account/Registration' ).default )
	} )
}
