export const path = '/page(/:route)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../CMS/Page' ).default )
	} )
}
