export const path = 'review/:type/:bookingId(/:hash)'

export function getComponent( nextState, cb ) {
	require.ensure( [], ( require ) => {
		cb( null, require( '../Review/Review' ).default )
	} )
}
