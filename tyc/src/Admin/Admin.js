import React from 'react'
import { connect } from 'react-redux'
import { Alert } from 'antd'
import moment from 'moment'
import AdminUsers from './AdminUsers'
import AdminBookings from './AdminBookings'
import AdminStripe from './AdminStripe'
import AdminSettings from './AdminSettings'

class Admin extends React.Component {
	state = {
		createdFrom: moment().subtract( 1, 'week' ).startOf('day'),
		createdTo: moment().endOf( 'day' ),
	}

	setDateRange = ( range ) => {
		this.setState( {
			createdFrom: range[ 0 ].startOf('day'),
			createdTo: range[ 1 ].endOf( 'day' )
		} )
	}

	render() {
		if( !this.props.authInfo || !this.props.authInfo.id )
			return (<div className="content-width-medium content-pad">
				<Alert showIcon type="warning" message="Not authenticated!"/>
			</div>)

		let view
		switch( this.props.params.view ) {
			case 'users':
				view = <AdminUsers {...this.props}/>
				break
			case 'bookings':
				view = <AdminBookings {...this.props} {...this.state} onDateRange={this.setDateRange}/>
				break
			case 'stripe':
				view = <AdminStripe {...this.props}/>
				break
			case 'settings':
				view = <AdminSettings {...this.props}/>
				break
			default:
				view = null  // Todo: <AdminDashboard/>
		}
		return (
				<div className="admin" style={{ height: '100%' }}>
					{view}
				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				authInfo: state.authInfo,
				authToken: state.authToken,
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {}
		}
)( Admin )
