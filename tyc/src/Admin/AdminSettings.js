import React from 'react'
import { withApollo } from 'react-apollo'
import gql from 'graphql-tag'
import { Button, notification } from 'antd'
import scriptLoader from 'react-async-script-loader'
import 'jsoneditor/dist/jsoneditor.css'

const gqlSettings = gql`query setting { getSettings( settingName: "live" ) }`

const gqlUpdateSetting = gql`
mutation ($name: String!, $value: Json!) {
  updateSettings(input: { settingName: $name, settingValue: $value }){clientMutationId}
}`

const DEBUG = true

class AdminSettings extends React.Component {
	updateSettings = ( event ) => {
		this.props.client.mutate( {
					mutation: gqlUpdateSetting,
					variables: {
						name: 'live',
						value: this.JSONeditor.get()
					}
				} )
				.then( result => {
					notification.success( { message: 'Settings updated.' } )
				} )
				.catch( error => {
					console.log( error.message )
					console.log( error.stack )
					notification.error( { message: error.message } )
				} )
	}

	componentWillReceiveProps( next ) {
		// Check for errors loading JSONEditor library.
		const { t, isScriptLoaded, isScriptLoadSucceed } = next
		if( isScriptLoaded !== this.props.isScriptLoaded || isScriptLoadSucceed !== this.props.isScriptLoadSucceed ) {
			if( isScriptLoaded ) {
				if( isScriptLoadSucceed ) {
					DEBUG && console.log( 'JSON Editor configured Ok.' )
					this.props.client.query( { query: gqlSettings } )
							.then( result => {
								const options = {
									modes: [ 'tree', 'code' ]
								}
								this.JSONeditor = new window.JSONEditor( this.editor, options, result.data.getSettings )
							} )
				} else {
					notification.error( { message: t( 'Failed to load JSON Editor module.' ) } )
				}
			}
		}
	}

	render() {
		return (
				<div className="admin-settings" style={{ padding: '1rem', height: '100%' }}>
					<Button type="primary" className="responsive" onClick={this.updateSettings}
							style={{ marginBottom: '6px' }}>Save</Button>
					<div style={{ height: '80vh' }} ref={( el ) => {
						this.editor = el
					}}></div>
				</div>
		)
	}
}

export default withApollo( scriptLoader( '/js/jsoneditor.min.js' )( AdminSettings ) )
