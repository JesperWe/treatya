import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { browserHistory } from 'react-router'
import moment from 'moment'
import _get from 'lodash/get'
import { Table, Tag, DatePicker, Modal, Icon } from 'antd'
import ReactJson from 'react-json-view'
import config from '../config'
import { graphql, compose } from 'react-apollo'
import { getBookings } from '../graphql/admin'
import './AdminUsers.less'

const { Column } = Table

//const DEBUG = false

function noNulls( obj ) {
	let noNullsObj = Object.assign( {}, obj )
	Object.keys( obj ).forEach( key => {
		if( obj[ key ] === null ) delete noNullsObj[ key ]
	} )
	return noNullsObj
}

class AdminBookings extends Component {

	state = {
		modalVisible: false,
		sorter: {
			field: "createdAt",
			order: "ascend"
		}
	}

	onTableChange = ( table, pagination, sorter ) => {
		if( sorter ) this.setState( { sorter } )
	}

	sorter = ( a, b ) => {
		let { sorter } = this.state
		if( !sorter ) return null
		let sort = 0
		if( a[ sorter.field ] < b[ sorter.field ] ) sort = -1
		else if( a[ sorter.field ] > b[ sorter.field ] ) sort = 1
		return sort
	}

	dateFormatter = ( dateString ) => {
		return moment( dateString ).format( config.date.default )
	}

	clientFormatter = ( firstName, booking ) => {
		return booking.firstName + ' ' + booking.lastName
	}

	statusFormatter = ( status, booking ) => {
		let color
		color = {
			UNCONFIRMED: 'orange',
			CONFIRMED: 'green',
			CANCELLED: 'red',
			DELIVERED: 'blue'
		}[ status ]
		return (
				<Tag color={color} onClick={() => Modal.info( {
					title: 'Events', maskClosable: true, width: 600,
					content: <ReactJson src={booking.events} collapsed={1} style={{ fontSize: '12px' }}
										displayDataTypes={false}/>
				} )}>
					{this.props.t( status )}
				</Tag>)

	}

	paymentFormatter = ( payment, booking ) => {
		const { t } = this.props
		if( !payment ) return '?'

		let tag = <Tag color="green">{t( 'succeeded' )}</Tag>

		if( payment.refunds.total_count || _get( booking, 'refund.id' ) )
			tag = <Tag color="magenta">{t( 'refunded' )}</Tag>

		if( payment.captured === false )
			tag = <Tag color="cyan">{t( 'not-captured' )}</Tag>

		return (
				<span>
					{tag}

					{booking.refund && Object.keys( booking.refund ).length > 0 &&
					<Icon type="rollback"
						  onClick={() => Modal.info( {
							  title: 'Refund', maskClosable: true, width: 500,
							  content: <ReactJson src={booking.refund} collapsed={1} style={{ fontSize: '12px' }}
												  displayDataTypes={false}/>
						  } )}/>}

					<Icon type="credit-card"
						  onClick={() => Modal.info( {
							  title: 'Payment', maskClosable: true, width: 500,
							  content: <ReactJson src={noNulls( booking.payment )} collapsed={1}
												  style={{ fontSize: '12px' }} displayDataTypes={false}/>
						  } )}/>

					{booking.payout && Object.keys( booking.payout ).length > 0 &&
					<Icon type="export"
						  onClick={() => Modal.info( {
							  title: 'Payout', maskClosable: true, width: 500,
							  content: <ReactJson src={booking.payout} collapsed={1} style={{ fontSize: '12px' }}
												  displayDataTypes={false}/>
						  } )}/>}

				</span>
		)
	}

	onRowClick = ( booking, index ) => {
		browserHistory.push( '/booking/' + booking.id )
	}

	render() {
		const { createdFrom, createdTo, t } = this.props

		return (
				<div className="admin-bookings" style={{ padding: '1rem' }}>
					<DatePicker.RangePicker
							value={[ createdFrom, createdTo ]} size="large" className="vertical-spacing"
							onChange={this.props.onDateRange}
					/>
					{(this.props.bookingsLoading === false) &&
					<Table
							pagination={{
								defaultPageSize: 25,
								showSizeChanger: true,
								pageSizeOptions: [ '10', '25', '50', '100' ]
							}}
							onRow={( record, index ) => ({
								onDoubleClick: event => this.onRowClick( record, index, event )
							})}
							dataSource={this.props.bookings} onChange={this.onTableChange}
							rowKey={record => record.id}>
						<Column title={t( 'Id' )} dataIndex="id" key="id"/>
						<Column title={t( 'Starting at' )} dataIndex="period.start.value" key="startingAt"
								render={this.dateFormatter} sorter={this.sorter}/>
						<Column title={t( 'Created' )} dataIndex="createdAt" key="createdAt"
								render={this.dateFormatter} sorter={this.sorter} defaultSortOrder="descend"/>
						<Column title={t( 'Status' )} dataIndex="status" key="status"
								render={this.statusFormatter} sorter={this.sorter}/>
						<Column title={t( 'Client' )} key="client"
								dataIndex="firstName" render={this.clientFormatter}
								className="col-supplier"/>
						<Column title={t( 'Supplier' )} dataIndex="title" key="supplier"
								className="col-supplier"/>
						<Column title={t( 'Payment' )} dataIndex="payment" key="payment"
								render={this.paymentFormatter} className="col-supplier"/>
					</Table>
					}
				</div>
		)
	}
}

export default compose(
		graphql( getBookings.gql, getBookings.options )
)( translate( [ 'common' ], { wait: true } )( AdminBookings ) )
