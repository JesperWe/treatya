import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { Checkbox, Icon, Button, notification } from 'antd'
import { graphql, compose } from 'react-apollo'
import { getAccounts } from '../graphql/admin'

const DEBUG = false

class AdminMailings extends Component {
	state = {
		account: false,
		supplier: false,
		mailingList: false
	}

	render() {
		const { t } = this.props

		return (
				<div className="admin-mailings" style={{ padding: '1rem' }}>
					{this.props.accounts && this.props.accountsLoading === false &&
					<div>
						<div>
							<Checkbox onChange={}>{t( 'Registered' )}</Checkbox>
							<Checkbox onChange={}>{t( 'Supplier' )}</Checkbox>
						</div>
						<div>
							{this.props.accounts.map( (account, index) => ( <span key={index}>{account.email}, </span>) )}
						</div>
					</div>
					}
				</div>
		)
	}
}

export default compose(
		graphql( getAccounts.gql, getAccounts.options )
)( translate( [ 'common' ], { wait: true } )( AdminMailings ) )
