import React, { Component } from 'react'
import { translate } from 'react-i18next'
import http from 'axios'
import { Input, Button, notification } from 'antd'
import config from '../config'
import './AdminUsers.less'

class AdminStripe extends Component {

	state = {
		stripeId: '',
		patch: '',
	}

	patch = () => {
		http.post( config.backend.url + '/stripe/connect/patch', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					stripeId: this.state.stripeId,
					patch: eval( '(' + this.state.patch + ')' ) // eslint-disable-line no-eval
				} )
				.then( ( response ) => {
				} )
				.catch( error => {
					if( error.response.data ) {
						console.log( error.response.data )
						notification.error( { message: error.response.data } )
					} else {
						console.log( error.stack )
						notification.error( { message: error.message } )
					}
				} )
	}

	deleteAccount = ( accountId ) => {
		http.post( config.backend.url + '/stripe/connect/delete', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					stripeId: this.state.stripeId
				} )
				.then( ( response ) => {
					notification.success( { message: 'Deleted.' } )
				} )
				.catch( error => {
					if( error.response.data ) {
						console.log( error.response.data )
						notification.error( { message: error.response.data } )
					} else {
						console.log( error.stack )
						notification.error( { message: error.message } )
					}
				} )
	}

	render() {
		const { t } = this.props

		return (
				<div className="admin-stripe" style={{ padding: '0 1rem' }}>
					<div className="function">
						<Input className="responsive" placeholder="Stripe ID"
							   onChange={( e ) => this.setState( { stripeId: e.target.value } )}/>
						<Input type="textarea" autosize={{ minRows: 4, maxRows: 20 }} className="responsive"
							   placeholder="JSON patch"
							   onChange={( e ) => this.setState( { patch: e.target.value } )}/>
						<Button className="responsive" onClick={this.patch}>{t( 'Apply Patch' )}</Button>
					</div>
					<div className="function">
						<Input className="responsive" placeholder="Stripe Account ID"
							   onChange={( e ) => this.setState( { stripeId: e.target.value } )}/>
						<Button className="responsive" onClick={this.deleteAccount}>{t( 'Delete Account' )}</Button>
					</div>
				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( AdminStripe )
