import React, { Component } from 'react'
import { translate } from 'react-i18next'
import http from 'axios'
import _get from 'lodash/get'
import moment from 'moment'
import cookie from 'browser-cookies'
import { Table, Select, Icon, Modal, Button, Popconfirm, notification } from 'antd'
import config from '../config'
import ReactJson from 'react-json-view'
import errorHandler from '../errorHandler'
import { graphql, compose } from 'react-apollo'
import { deleteAccount } from '../graphql/account'
import {
	getAccounts,
	roles,
	supplierStatuses,
	updateAccountRole,
	updateSupplierStatus,
	supplierPrivate
} from '../graphql/admin'
import './AdminUsers.less'

const { Column } = Table
const { Option } = Select

const DEBUG = false

class AdminUsers extends Component {

	state = {
		modalVisible: false,
		sorter: {
			field: "createdAt",
			order: "ascend"
		}
	}

	onTableChange = ( table, pagination, sorter ) => {
		this.setState( { sorter } )
	}

	sorter = ( a, b ) => {
		let { sorter } = this.state
		if( !sorter ) return null
		let sort = 0
		if( a[ sorter.field ] < b[ sorter.field ] ) sort = -1
		else if( a[ sorter.field ] > b[ sorter.field ] ) sort = 1
		return sort
	}

	dateFormatter = ( dateString ) => {
		return moment( dateString ).format( config.date.default )
	}

	updateRole = ( id, role ) => {
		if( id && role ) {
			this.props.updateAccountRole( id, role )
					.then( result => {
						const { t } = this.props
						notification.success( { message: t( 'Role updated.' ) } )
					} )
					.catch( errorHandler )
		}
	}

	impersonate = ( accountId ) => {
		http.post( config.backend.url + '/verify/impersonate', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					accountId
				} )
				.then( ( response ) => {
					let { authToken } = response.data
					if( authToken ) {
						let adminToken = cookie.get( 'auth-token' )
						cookie.set( 'admin-token', adminToken )
						cookie.set( 'auth-token', authToken )
						window.location.assign( '/' )
					}
				} )
				.catch( errorHandler )
	}

	emailCell = ( email, account ) => {
		return (
				<span>
				<Icon type="solution"
					  onClick={() => Modal.info( {
						  title: 'Account', maskClosable: true, width: 500,
						  content: <ReactJson src={account} collapsed={1} style={{ fontSize: '12px' }}
											  displayDataTypes={false}/>
					  } )}/> {email}
				</span>)
	}

	nameCell = ( _name, account, index ) => {
		const { t } = this.props

		return (
				<div>
					<Icon type="logout" title={account.id} onClick={() => this.impersonate( account.id )}/>
					{' '}
					{account.firstName} {account.lastName}
					{' '}
					<Popconfirm title={t( 'Are you sure?' )} okText={t( 'Yes' )} cancelText={t( 'No' )}
								onConfirm={() => this.handleDelete( account )}>
						<Icon type="close" title={account.id}/>
					</Popconfirm>
				</div>
		)
	}

	rolesCell = ( roleValue, account, index ) => {
		return (
				<Select style={{ width: 120 }}
						defaultValue={roleValue}
						onChange={( role ) => this.updateRole( account.id, role )}>

					{this.props.roles.map( ( role, index ) => (
							<Option key={index} value={role.name}>{role.name}</Option>
					) )}

				</Select>
		)
	}

	supplierStatus = ( supplier, account, index ) => {
		let display
		let payment = null

		if( !supplier ) return null

		let suplPriv = this.props.supplierPrivate.data[ supplier.id ]
		if( suplPriv ) {
			if( suplPriv.stripeNeeded === null ) payment = <Icon type="credit-card" className="disabled"/>
			else {
				if( suplPriv.stripeNeeded.length === 0 ) payment = <Icon type="credit-card" className="green7"/>
				if( suplPriv.stripeNeeded.length > 0 ) payment =
						<Icon type="credit-card" className="red7" title={JSON.stringify( suplPriv.stripeNeeded )}/>
			}
		}

		switch( supplier.status ) {
			case 'APPLICATION':
				display = <Icon type="question-circle"/>
				break
			case 'CONFIRMED':
				display = <Icon type="check-circle"/>
				break
			case 'DENIED':
				display = <Icon type="close-circle"/>
				break
			default:
				display = <Icon/>
		}

		if( payment ) display = <span>{display} {payment}</span>
		return display
	}

	updateSupplierStatus = ( value, label ) => {
		this.props.updateSupplierStatus( this.state.supplier.id, value )
				.then( result => {
					const { t } = this.props
					notification.success( { message: t( 'Supplier status updated.' ) } )

					// If the account was made a supplier, tell the backend he needs payment account.
					if( value === 'CONFIRMED' ) {
						return http.post( config.backend.url + '/stripe/connect/create', {
							id: this.props.authInfo.id,
							jwt: this.props.authToken,
							forUserId: this.state.supplier.accountId
						} )
					} else
						return Promise.resolve( true )
				} )
				.then( ( response ) => {
					if( value === 'CONFIRMED' ) {
						return http.post( config.backend.url + '/notify/supplier/confirmed', {
							id: this.props.authInfo.id,
							jwt: this.props.authToken,
							accountId: this.state.supplier.accountId
						} )
					} else
						return Promise.resolve( true )
				} )
				.catch( errorHandler )
	}

	supplierClick = ( account, event ) => {
		let supplier = account.suppliersByAccountId
		if( !supplier || !supplier.nodes || !supplier.nodes[ 0 ] ) return
		supplier = Object.assign( {}, supplier.nodes[ 0 ] )
		supplier.accountId = account.id

		this.setState( { supplier, modalVisible: true } )
	}

	handleDelete = ( account ) => {
		const supplierId = _get( account, 'suppliersByAccountId.nodes[0].id' )
		if( supplierId ) {
			const stripeId = _get( this.props, 'supplierPrivate.data[' + supplierId + '].stripeId' )
			if( stripeId )
				http.post( config.backend.url + '/stripe/connect/delete', {
							stripeId,
							id: this.props.authInfo.id,
							jwt: this.props.authToken
						} )
						.then( ( response ) => {
							notification.success( { message: 'Stripe Account Deleted.' } )
						} )
						.catch( errorHandler )
		}

		this.props.deleteAccount( account.id )
				.then( () => notification.success( { message: 'Account Deleted.' } ) )
				.catch( errorHandler )
	}

	render() {
		const { t } = this.props
		DEBUG && console.log( '\nrender()' )

		return (
				<div className="admin-users" style={{ padding: '1rem' }}>
					{(this.props.accountsLoading === false) && (this.props.rolesLoading === false) &&
					<Table
							pagination={{
								defaultPageSize: 25,
								showSizeChanger: true,
								pageSizeOptions: [ '10', '25', '50', '100' ]
							}}
							dataSource={this.props.accounts} onChange={this.onTableChange}
							rowKey={account => account.id}>
						<Column title={t( 'Name' )} dataIndex="firstName" key="firstName"
								render={this.nameCell}/>
						<Column title={t( 'Email' )} sorter={this.sorter} dataIndex="email" render={this.emailCell}/>
						<Column title={t( 'Role' )} dataIndex="role" key="role" render={this.rolesCell}/>
						<Column title={t( 'Created' )} dataIndex="createdAt" key="createdAt"
								render={this.dateFormatter} sorter={this.sorter} defaultSortOrder="descend"/>
						<Column title={t( 'Supplier' )} dataIndex="suppliersByAccountId.nodes[0]" key="supplier"
								render={this.supplierStatus} className="col-supplier" onCellClick={this.supplierClick}/>
					</Table>
					}

					{(this.props.statusesLoading === false && this.state.supplier) &&
					<Modal visible={this.state.modalVisible} closable={false} className="admin-users"
						   maskClosable={true}
						   footer={[ <Button key="close" type="ghost" size="large"
											 onClick={() => this.setState( { modalVisible: false } )}>{t( 'Close' )}</Button> ]}>
						<h3>{t( 'Supplier Application' )}</h3>
						<p className="title"><b>{this.state.supplier.title}</b></p>
						<p className="descr">{this.state.supplier.description}</p>
						<p className="descr">
							{this.state.supplier.address1}<br/>
							{this.state.supplier.address2}<br/>
							{this.state.supplier.zip} {this.state.supplier.city}, {this.state.supplier.country}
						</p>
						<label>{t( 'Update supplier status:' )}</label><br/>
						<Select style={{ width: 150 }}
								defaultValue={this.state.supplier.status}
								onChange={this.updateSupplierStatus}>

							{this.props.supplierStatuses.map( ( status, index ) => (
									<Option key={index} value={status.name}>{status.name}</Option>
							) )}

						</Select>
					</Modal>
					}

				</div>
		)
	}
}

export default compose(
		graphql( getAccounts.gql, getAccounts.options ),
		graphql( roles.gql, roles.options ),
		graphql( deleteAccount.gql, deleteAccount.options ),
		graphql( supplierStatuses.gql, supplierStatuses.options ),
		graphql( supplierPrivate.gql, supplierPrivate.options ),
		graphql( updateAccountRole.gql, updateAccountRole.options ),
		graphql( updateSupplierStatus.gql, updateSupplierStatus.options )
)( translate( [ 'common' ], { wait: true } )( AdminUsers ) )
