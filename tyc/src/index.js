import React from 'react'
import ReactDOM from 'react-dom'
import cookie from 'browser-cookies'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { onError } from 'apollo-link-error'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from 'react-apollo'
import errorHandler from './errorHandler'
import config from './config'
import App from './App'

const httpLink = new HttpLink( { uri: config.backend.url + config.backend.graphql } )

const withAuth = new ApolloLink( ( operation, forward ) => {
	const token = cookie.get( 'auth-token' )
	if( token ) operation.setContext( { headers: { authorization: `Bearer ${token}` } } )
	return forward( operation )
} )

const withError = onError( ( { graphQLErrors, networkError } ) => {
	if( graphQLErrors )
		graphQLErrors.forEach( ( { message } ) => {
			if( message === 'jwt expired' ) {
				cookie.erase( 'auth-token' )
			}
			// Hide spam messages
			if( message.indexOf( 'account_email_key' ) ) return
			const fullMessage = `[GraphQL error]: ${message}`
			errorHandler( new Error( fullMessage ) )
		} )
	if( networkError ) console.log( `[Network error]: ${networkError}` );
} )

const client = new ApolloClient( {
	link: withError.concat( withAuth.concat( httpLink ) ),
	cache: new InMemoryCache()
} )

document.querySelector( 'body' ).attributes.removeNamedItem( 'class' ) // remove loading class

ReactDOM.render(
		<ApolloProvider client={client}>
			<App/>
		</ApolloProvider>,
		document.getElementById( 'app-container' )
)
