import React from 'react'
import { translate } from 'react-i18next'
import moment from 'moment'
import { Row, Col, Button, Rate } from 'antd'
import Favourite from './Favourite'
import config from '../config'
import './SupplierCard.less'

class SupplierCard extends React.Component {

	state = {
		btnLoading: false
	}

	render() {
		const { t } = this.props
		let supplier = this.props.supplier

		let action, atTime, newTime

		if( supplier.timeDist ) {
			let startDate = moment( supplier.slot.start.value ).format( config.date.friendlyNoTime )
			newTime = moment( supplier.slot.start.value )
			let startH = newTime.format( 'H:mm' )
			let endH = moment( supplier.slot.end.value ).format( 'H:mm' )

			atTime =
					<span className="available">{t( 'Next available:' )}<br/>
						<span className="time">{startDate} {startH}&ndash;{endH}</span>
					</span>

		} else if( supplier.timeslotId ) {
			atTime = <span className="available">{t( 'Available!' )}</span>
		}

		if( !atTime ) {
			atTime = <span className="available">{t( 'No available timeslots.' )}</span>
		}

		action =
				<Button type="primary" size="large"
						loading={this.state.btnLoading}
						onClick={() => {
							this.props.selectProduct( supplier )
							this.setState( { btnLoading: true } )
							if( newTime ) this.props.onBookingMoment( newTime )
						}}>
					{t( 'Show more...' )}
				</Button>

		let { imageBg } = supplier
		if( imageBg instanceof Array ) imageBg = imageBg[ 0 ]
		if( !imageBg ) {
			let serviceType = this.props.desiredServiceType
			let typeImages = this.props.typeImages[ serviceType ]
			imageBg = config.cdn.promo + typeImages[ supplier.supplierId % typeImages.length ]
		}
		imageBg = imageBg || 'https://placehold.it/800x250'

		let imageProfile = supplier.imageProfile || 'https://placehold.it/250x250'

		return (
				<div className="supplier-card">

					<div className="bg-profile">
						<div className="bg background-image"
							 style={{ backgroundImage: 'url(' + imageBg + ')' }}/>
						<div className="profile-frame">
							<div className="profile background-image"
								 style={{
									 backgroundImage: 'url(' + imageProfile + ')',
									 transform: (supplier.imageProfileRot ? 'rotate(' + supplier.imageProfileRot + 'deg)' : undefined)
								 }}/>
						</div>
					</div>

					<div className="card-info">
						<h3 className="title">
							{supplier.title}{' '}
							<Favourite supplierId={supplier.supplierId} {...this.props}/>
						</h3>

						<div className="supplier-descr">
							{supplier.intro}
						</div>

						<div className="supplier-product">

							<Row align="bottom">

								<Col xs={24} sm={12}>
									<span onClick={this.setRating}>
											<Rate count={5} value={supplier.rating} allowHalf={true} disabled/>
									</span>
									<br/>
									{atTime}
								</Col>

								<Col xs={24} sm={12}>{action}</Col>
							</Row>

						</div>
					</div>
				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( SupplierCard )
