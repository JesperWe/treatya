import { connect } from 'react-redux';
import Supplier from './Supplier';

const SupplierRedux = connect(
		function mapStateToProps( state ) {
			return {
				location: state.location,
				place: state.place,
				account: state.account,
				authInfo: state.authInfo,
				authToken: state.authToken,
				typeImages: state.typeImages,
				language: state.language,
				desiredServiceType: state.desiredServiceType,
				desiredBookingMoment: state.desiredBookingMoment
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onServiceType: ( serviceType ) => dispatch( { type: 'SERVICETYPE', serviceType } ),
				onBookingMoment: ( bookingMoment ) => dispatch( { type: 'BOOKINGMOMENT', bookingMoment } ),
				requestManualLocation: (status) => dispatch( { type: 'REQUEST_MANUAL_LOCATION', status } ),
				requestDeviceLocation: (status) => dispatch( { type: 'REQUEST_DEVICE_LOCATION', status } ),
				onLocate: ( location ) => dispatch( { type: 'GEOLOCATE', location } ),
				onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } )
			}
		}
)( Supplier );

export default SupplierRedux;
