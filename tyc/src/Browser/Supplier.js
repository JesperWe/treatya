import React from 'react'
import { translate } from 'react-i18next'
import { browserHistory } from 'react-router'
import moment from 'moment'
import { Button, Icon, Rate, Tabs, Table, Alert, Modal, Carousel, Row, Col, notification } from 'antd'
import diff from 'deep-diff'
import http from 'axios'
import _get from 'lodash/get'
import Loading from '../Loading'
import Login from '../Account/Login'
import Amount from '../Amount'
import DateTimePicker from '../DateTimePicker'
import errorHandler from '../errorHandler'
import CMSText from '../CMS/CMSText'
import BookNowModal from '../Booking/BookNowModal'
import Favourite from './Favourite'
import config from '../config'
import './Supplier.less'
import { graphql, compose } from 'react-apollo'
import { getSupplierAndProducts, supplierFreeslots } from '../graphql/supplier'

const DEBUG = true

class Supplier extends React.Component {

    state = {
        tab: 1,
        loading: true,
        buttonDisabled: true,
        buttonText: '',
        selectedProduct: {},
        paymentVisible: false,
        loginModalVisible: false,
        bookingModalVisible: false,
        inProgress: false
    }

    // Called from componentWillReceiveProps: Arguments are next props and currently selected product.
    // Called from rowClick: Arguments are current props and clicked product to select.
    updateBookButtonState = ( _props, _selectedProduct ) => {
        const props = _props || this.props
        const selectedProduct = _selectedProduct || this.state.selectedProduct
        DEBUG && console.log( '\n(Supplier) updateBookButtonState\n' )

        let loading = false
        let buttonDisabled = true
        let buttonText = 'No product selected.'

        let { supplierFreeslots } = props

        if( supplierFreeslots.loading ) {
            loading = true
            this.setState( { loading, buttonDisabled, buttonText } )
            return
        }

        if( selectedProduct && selectedProduct.id ) {
            let desiredStart = props.desiredBookingMoment.valueOf()
            let isFree = false
            supplierFreeslots.data.forEach( slot => {
                if( desiredStart < slot.start || desiredStart > slot.end ) return
                let desiredEnd = props.desiredBookingMoment.clone()
                    .add( selectedProduct.duration.hours, 'hours' )
                    .add( selectedProduct.duration.minutes, 'minutes' )
                    .valueOf()
                if( desiredEnd > slot.end ) return
                isFree = true
            } )

            if( isFree ) {
                buttonText = 'Book This!'
                buttonDisabled = false
            }
            if( !isFree ) {
                buttonText = 'Not Available'
            }
        }
        this.setState( { loading, buttonDisabled, buttonText } )
    }

    componentWillReceiveProps( next ) {
        DEBUG && console.log( '\n(Supplier) componentWillReceiveProps\n', JSON.stringify( diff( next, this.props ), null, 2 ) )

        let productPreselected

        if( next.supplierLoading !== this.props.supplierLoading && !next.supplierLoading ) {
            if( next.supplierError ) {
                errorHandler( next.supplierError )
            } else {
                // If the supplier has only one product we can preselect it.
                if( next.products && next.products.length === 1 ) {
                    this.setState( { selectedProduct: next.products[ 0 ] } )
                    productPreselected = next.products[ 0 ]
                }
                document.title = 'Treatya - ' + this.props.t( 'supplier' ) + ' - ' + next.supplier.title
            }
        }

        if( !next.supplierFreeslots ) return

        if( next.supplierFreeslots.error && !this.props.supplierFreeslots.error )
            errorHandler( next.supplierFreeslots.error )
        else
            this.updateBookButtonState( next, productPreselected || this.state.selectedProduct )

        if( _get( next, 'account.id.length' ) ) {
            this.setState( { loginModalVisible: false } )
        }

    }

    onChangeBookingMoment = ( bookingMoment ) => {
        DEBUG && console.log( '\n(Supplier) onBookingMoment', bookingMoment.format() )
        this.props.onBookingMoment( bookingMoment )
    }

    tabClick = ( event ) => {
        this.setState( { tab: event } )
    }

    onRowClick = ( product, index, event ) => {
        if( this.state.selectedProduct && this.state.selectedProduct.id === product.id )
            this.setState( { selectedProduct: null } )
        else {
            this.setState( { selectedProduct: product } )
            this.updateBookButtonState( this.props, product )
            if( config.tracking ) {
                window._paq.push( [ 'setEcommerceView', 'product-' + product.id, product.prodTitle, product.type, product.price, 1 ] )
                window._paq.push( [ 'setDocumentTitle', 'Product Select ' + product.prodTitle ] )
                window._paq.push( [ 'setCustomUrl', window.location.pathname + '/' + product.id ] )
                window._paq.push( [ 'trackPageView' ] )
            }
        }
    }

    rowClassName = ( product, index ) => {
        if( this.state.selectedProduct && this.state.selectedProduct.id === product.id ) return 'selected'
        return ''
    }

    bookProduct = () => {
        const { account } = this.props
        const product = this.state.selectedProduct

        DEBUG && console.log( 'Supplier::bookProduct', account )

        if( !account || !account.id ) {
            config.tracking && window._paq.push( [ 'trackEvent', 'Booking', 'Select Product - Login' ] )
            this.setState( { loginModalVisible: true } )
            return
        }

        if( !account.emailVerified || !account.phoneVerified ) {
            let url = new URL( window.location )
            url.searchParams.append( 'url', window.location.pathname )
            browserHistory.push( '/register' + url.search )
            return
        }

        if( config.tracking && product ) {
            window._paq.push( [ 'addEcommerceItem', 'product-' + product.id, product.prodTitle, product.type, product.price, 1 ] )
            window._paq.push( [ 'trackEcommerceCartUpdate', product.price ] )
        }

        this.setState( { loginModalVisible: false } )
        DEBUG && console.log( 'BookNowModal bookProduct() show.' )
        this.setState( { bookingModalVisible: true } )
    }

    handlePayment = () => {
        config.tracking && window._paq.push( [ 'trackEvent', 'Booking', 'Go to payment' ] )
        /* DEBUG && */
        console.log( 'BookNowModal handlePayment() hide.' )
        this.setState( {
            bookingModalVisible: false,
            paymentVisible: true
        } )
    }

    sendInquiry = () => {
        this.setState( { inProgress: true } )
        let { inquiry } = this.state

        http.post( config.backend.url + '/notify/inquiry', {
            id: _get( this.props, 'authInfo.id', 0 ),
            jwt: this.props.authToken,
            language: this.props.language,
            inquiry,
            accountId: this.props.supplier.accountId
        } )
            .then( result => {
                this.setState( { inProgress: false, inquirySent: true } )
                notification.success( { message: this.props.t( 'Your inquiry has been forwarded.' ) } )
            } )
            .catch( error => {
                this.setState( { inProgress: false } )
                errorHandler( error )
            } )
    }

    render() {
        const { t, products } = this.props
        const freeslots = _get( this.props, 'supplierFreeslots.data.length' ) > 0;

        DEBUG && console.log( 'Supplier.render()' )

        if( this.props.supplierLoading && !this.state.bookingModalVisible ) {
            return <Loading/>
        }

        let supplier = this.props.supplier || {}

        let imageBg = supplier.imageBg

        let serviceImage
        let serviceType = this.props.desiredServiceType || ( products[ 0 ] && products[ 0 ].type )
        if( serviceType ) {
            let typeImages = this.props.typeImages[ serviceType ] || []
            serviceImage = config.cdn.promo + typeImages[ supplier.id % typeImages.length ]
        }

        imageBg = imageBg || serviceImage || 'https://placehold.it/800x250'

        const imageProfile = supplier.imageProfile || 'https://placehold.it/250x250'

        let images
        if( imageBg instanceof Array ) {
            if( imageBg.length === 0 )
                images = <div className="bg background-image" style={{ backgroundImage: 'url(' + serviceImage + ')' }}/>
            else if( imageBg.length === 1 )
                images = <div className="bg background-image" style={{ backgroundImage: 'url(' + imageBg[ 0 ] + ')' }}/>
            else {
                images = (
                    <Carousel autoplay={true}>
                        {imageBg.map( ( src, index ) =>
                            <div key={index}>
                                <div className="background-image"
                                     style={{ backgroundImage: 'url(' + src + ')' }}/>
                            </div> )
                        }
                    </Carousel> )
            }
        } else {
            images = <div className="bg background-image" style={{ backgroundImage: 'url(' + imageBg + ')' }}/>
        }

        let { loading, buttonDisabled, buttonText, selectedProduct } = this.state

        let productSelector = (
            <Table dataSource={products} rowKey={prod => prod.id}
                   className="product-table  content-width-small"
                   bordered={false} pagination={false} showHeader={false}
                   onRow={( record, index ) => ( {
                       onClick: event => this.onRowClick( record, index, event )
                   } )}
                   rowClassName={this.rowClassName}>
                <Table.Column dataIndex="id" key="title" render={( id, product ) => (
                    <Row key={id}>

                        <Col xs={24} sm={16}>
                            <div className="prod-title">{product.prodTitle}</div>
                        </Col>

                        <Col xs={12} sm={4}>
                            <div className="databox">
                                <Icon
                                    type="clock-circle-o"/> {product.duration.hours ? product.duration.hours + 'h' : ''} {product.duration.minutes}min
                            </div>
                        </Col>

                        <Col xs={12} sm={4}>
                            <div className="databox">
                                <Amount value={product.price || 0}/>
                                <span className="currency">{product.currency}</span>
                            </div>
                        </Col>

                        <Col xs={24} sm={16}>
                            <div className="prod-descr">{product.prodDescr}</div>
                        </Col>

                    </Row>
                )}/>
            </Table> )

        return (
            <div className="supplier content-width content-pad">

                <div className="bg-profile">
                    <div className="images">{images}</div>
                    <div className="profile-frame">
                        <div className="profile background-image"
                             style={{
                                 backgroundImage: 'url(' + imageProfile + ')',
                                 transform: ( supplier.imageProfileRot ? 'rotate(' + supplier.imageProfileRot + 'deg)' : undefined )
                             }}/>
                    </div>
                </div>

                <div className="content-width-small">
                    <h3 className="title">
                        {supplier.title}{' '}
                        <Favourite supplierId={supplier.id} {...this.props}/>
                    </h3>

                    <div className="supplier-descr">
                        {supplier.intro}
                    </div>

                    <Rate count={5} value={supplier.rating} allowHalf={true} disabled/>

                    {freeslots && <div className="supplier-product">
                        <DateTimePicker
                            supplierFreeslots={this.props.supplierFreeslots}
                            datetime={this.props.desiredBookingMoment}
                            onChange={this.onChangeBookingMoment}/>

                        <Button type="primary" className="responsive" size="large"
                                disabled={buttonDisabled || !selectedProduct || !selectedProduct.id}
                                loading={loading}
                                onClick={this.bookProduct}>{t( buttonText )}</Button>
                    </div>}

                    {!freeslots && !this.state.inquirySent && <div className="supplier-product">
                        <h4><CMSText identifier="supplier-no-free-slots"/></h4>
                        <Button type="primary" size="large" className="responsive"
                                onClick={this.sendInquiry} loading={this.state.inProgress}>
                            {t( 'Send booking inquiry' )}
                        </Button>
                    </div>}
                </div>

                {window.matchMedia( "(max-width: 499px)" ).matches &&
                <div className="product-table">

                    {productSelector}

                    <h2>{t( 'About' )}</h2>

                    <div className="supplier-descr">{supplier.description}</div>

                    <h2>{t( 'Reviews' )}</h2>

                    {( this.props.reviews && this.props.reviews.length > 0 ) &&
                    this.props.reviews.map( ( review, index ) => (
                        <div className="review" key={index}>
                            <Rate count={5} value={review.rating} allowHalf disabled/>
                            <span
                                className="review-date"> {moment( review.createdAt ).format( config.date.noTime )}</span>
                            <br/>
                            <p>{review.comment}</p>
                        </div>
                    ) )
                    }

                    {( !this.props.reviews || !this.props.reviews.length ) &&
                    <Alert message={t( 'This supplier has not been reviewed yet.' )} showIcon
                           type="warning"/>
                    }
                </div>
                }

                {window.matchMedia( "(min-width: 500px)" ).matches &&
                <Tabs activeKey={this.state.tab.toString()} onTabClick={this.tabClick}>

                    <Tabs.TabPane tab={t( 'Products' )} key="1">{productSelector}</Tabs.TabPane>

                    <Tabs.TabPane tab={t( 'About' )} key="2">
                        <div className="content-width-small">
                            {supplier.description}
                        </div>
                    </Tabs.TabPane>

                    <Tabs.TabPane tab={t( 'Reviews' )} key="3">
                        <div className="content-width-small">
                            {( this.props.reviews && this.props.reviews.length > 0 ) &&
                            this.props.reviews.map( ( review, index ) => (
                                <div className="review" key={index}>
                                    <Rate count={5} value={review.rating} allowHalf disabled/>
                                    <span
                                        className="review-date"> {moment( review.createdAt ).format( config.date.noTime )}</span>
                                    <br/>
                                    <p>{review.comment}</p>
                                </div>
                            ) )
                            }

                            {( !this.props.reviews || !this.props.reviews.length ) &&
                            <Alert message={t( 'This supplier has not been reviewed yet.' )} showIcon
                                   type="warning"/>
                            }
                        </div>
                    </Tabs.TabPane>

                </Tabs>
                }

                <Modal visible={this.state.loginModalVisible}
                       closable={false} className="login"
                       onCancel={() => this.setState( { loginModalVisible: false } )}
                       footer={null}>
                    <Login onComplete={this.bookProduct}/>
                </Modal>

                <BookNowModal
                    visible={this.state.bookingModalVisible}
                    product={this.state.selectedProduct}
                    {...this.props}
                    handlePayment={this.handlePayment}
                    onBookingId={newBookingId => this.setState( { newBookingId } )}
                    onCancel={() => {
                        /* DEBUG && */
                        console.log( 'BookNowModal cancelled.' )
                        this.setState( { bookingModalVisible: false } )
                        if( this.state.newBookingId ) {
                            browserHistory.push( '/booking/' + this.state.newBookingId )
                            this.setState( { newBookingId: undefined } )
                        }
                    }}
                />

            </div>
        )
    }
}

export default compose(
    graphql( getSupplierAndProducts.gql, getSupplierAndProducts.options ),
    graphql( supplierFreeslots.gql, supplierFreeslots.options )
)(
    translate( [ 'common' ], { wait: true } )( Supplier )
)
