import React from 'react'
import { withApollo } from 'react-apollo'
import _get from 'lodash/get'
import errorHandler from '../errorHandler'
import { Icon } from 'antd'
import { addFavourite, deleteFavourite } from '../graphql/supplier'
import './Favourite.less'

class Favourite extends React.Component {

	toggleFavourite = ( isFavourite ) => {
		if( !this.props.authInfo || !this.props.authInfo.id ) return

		this.props.client.mutate( {
					mutation: isFavourite ? deleteFavourite.gql : addFavourite.gql,
					variables: {
						accountId: this.props.authInfo.id,
						supplierId: this.props.supplierId
					},
					refetchQueries: [ 'accountInfo' ]
				} )
				.catch( errorHandler )
	}

	render() {
		const { supplierId } = this.props
		const favouriteSuppliers = _get( this.props, 'account.accountFavoriteSuppliersByAccountId.nodes', [] )
		const isFavourite = (favouriteSuppliers.findIndex( s => s.supplierId === supplierId ) >= 0)

		return (
				<span className="favorite-icon">
					<Icon
							className={isFavourite ? "favourite" : "not-favourite"}
							type={isFavourite ? "heart" : "heart-o"}
							onClick={() => this.toggleFavourite( isFavourite )}/>
				</span>
		)
	}
}

export default withApollo( Favourite )
