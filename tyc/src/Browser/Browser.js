import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { translate } from 'react-i18next'
import moment from 'moment'
import _get from 'lodash/get'
import http from 'axios'
import { graphql, compose } from 'react-apollo'
import { Alert, Row, Col, Modal, Input, Icon, notification } from 'antd'
import Loading from '../Loading'
import CMSText from '../CMS/CMSText'
import config from '../config'
import BrowserMenu from '../Navigation/BrowserMenu'
import SupplierCard from './SupplierCard'
import InfoSplash from '../InfoSplash'
import { findSuppliers } from '../graphql/supplier'
import { nearbyUnavailableSuppliers } from '../graphql/browse'
import './Browser.less'
import errorHandler from '../errorHandler'

// GeoJSON sample: {"type": "Point", "coordinates": [ 52.3, 18.2 ] }

const DEBUG = false

class Browser extends React.Component {

	state = {
		categories: [],
		loading: false,
		showEmailModal: false,
		menuBackground: config.cdn.promo + config.cdn.promoImages[ Math.floor( moment().valueOf() / 5000 ) % config.cdn.promoImages.length ]
	}

	selectProduct = ( product ) => {
		this.setState( { selectedProduct: product } )
		browserHistory.push( '/supplier/' + product.supplierId )
	}

	componentWillMount() {
		const queryType = _get( this.props, 'query.t' )
		if( queryType ) {
			this.props.onServiceType( queryType )
		}
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( "Browser.componentWillReceiveProps()" )
		if( next.suppliersError ) {
			config.tracking && window._paq.push( [ 'trackEvent', 'Error', next.suppliersError.message, 'Browser findSupplier' ] )
			notification.error( { message: next.suppliersError.message } )
			console.log( next.suppliersError.message )
			console.log( next.suppliersError.stack )
		}

		if( next.searchCounter === 2 && this.props.searchCounter === 1 ) {
			if( !next.account || !next.account.id )
				this.setState( { showEmailModal: true } )
		}
	}

	componentDidMount() {
		const { t } = this.props
		let errNo = this.props.location && this.props.location.error
		let errorMsg = t( 'Please enable location services for this site in your browser.' )
		if( errNo === 2 ) errorMsg = t( 'Your device does not report its location correctly.' )
		if( errNo === 3 ) errorMsg = t( 'Your device took too long to find your location.' )
		let { location } = this.props
		if( location && (location.error || !location.position) ) {
			let description =
					<span>
						{errorMsg} <br/>
						{t( 'Treatya needs to know where you are located to search for treatments in your area.' )}<br/>
						{this.props.authInfo && this.props.authInfo.id &&
						<span>{t( 'You can set your position manually in your' )}
							<Link to="/profile">{t( 'account preferences' )}</Link>.</span>
						}
					</span>

			notification.warning( {
				message: t( 'Your geographic location is unkown' ),
				duration: 0,
				description
			} )
		}
	}

	componentWillUnmount() {
		this.props.onLandingPage( false )
	}

	componentWillUpdate( next ) {
		this.props.onLandingPage( next.query.t === undefined )

		// Update background when moving between landingpage and result.
		if( next.query.t !== this.props.query.t )
			this.setState( {
				menuBackground: config.cdn.promo +
				config.cdn.promoImages[ Math.floor( moment().valueOf() / 5000 ) % config.cdn.promoImages.length ]
			} )
	}

	submitEmail = () => {
		this.setState( { showEmailModal: false } )
		const { email, firstName, lastName } = this.state

		http.post( config.backend.url + '/notify/mailinglist', { email, firstName, lastName } )
				.then( () => {
					notification.success( { message: this.props.t( `Thanks, I'll get back to you.` ) } )
					config.tracking && window._paq.push( [ 'trackEvent', 'Account', 'mailinglist' ] )
				} )
				.catch( errorHandler )
	}

	render() {
		const { t, loading, suppliers, desiredServiceType } = this.props
		const unavailableSuppliers = _get( this.props, 'nearbyUnavailableSuppliers.suppliers' )

		let menuDecoration = 'have-result'
		let landingIntro = null
		if( this.props.landingPage ) {
			menuDecoration = 'no-result'
			landingIntro = t( 'Beauty treatments in the comfort of your home' )
		}

		return (
				<div className={'browser-component' + ((this.props.landingPage) ? ' landing' : '')}>
					<InfoSplash/>

					<BrowserMenu {...this.props} decoration={menuDecoration}
								 background={landingIntro && this.state.menuBackground}
								 intro={landingIntro} counter={this.props.onSearch}/>

					{!this.props.landingPage &&
					<div className="browser">

						{loading && <Loading/>}

						{(!loading && desiredServiceType && suppliers && suppliers.length === 0) &&
						<div className="suppliers centered">
							<Alert showIcon type="info" message={t( 'No results' )}
								   description={t( 'We could not find any supplier that matches.' )}/>
						</div>
						}

						<Row type="flex" className="suppliers" gutter={12}>

							{suppliers && suppliers.map( ( supplier, index ) => (
									<Col xs={24} sm={24} md={12} lg={8} key={index}>
										<SupplierCard supplier={supplier}
													  selectProduct={this.selectProduct}
													  toggleFavourite={this.toggleFavourite}
													  {...this.props} />
									</Col>
							) )}

							{unavailableSuppliers && unavailableSuppliers.map( ( supplier, index ) => (
									<Col xs={24} sm={24} md={12} lg={8} key={index}>
										<SupplierCard supplier={supplier} unavailable
													  selectProduct={this.selectProduct}
													  toggleFavourite={this.toggleFavourite}
													  {...this.props} />
									</Col>
							) )}

						</Row>

					</div>
					}

					<Modal visible={this.state.showEmailModal}
						   title={t( `Not finding what you're looking for?` )}
						   onCancel={() => this.setState( { showEmailModal: false } )}
						   confirmLoading={this.state.inProgress}
						   onOk={this.submitEmail}>
						<Row gutter={20}>
							<Col xs={24} sm={6} lg={7}>
								<img className="avatar" src={config.treatya.davatar} alt=""/>
							</Col>
							<Col xs={24} sm={18} lg={17}>
								<CMSText identifier="emailPopupText"/>
								<br/><br/>
								{t( 'Dominika @ Treatya' )}
							</Col>
						</Row>
						<br/>
						<div className="vertical-spacing">
							<Input type="text" name="first_name"
								   placeholder={t( 'First name' )}
								   size="large" addonBefore={<Icon type="user"/>}
								   onChange={e => {
									   this.setState( { firstName: e.target.value } )
								   }}/>
						</div>
						<div className="vertical-spacing">
							<Input type="text" name="last_name"
								   placeholder={t( 'Last name' )}
								   size="large" addonBefore={<Icon type="team"/>}
								   onChange={e => {
									   this.setState( { lastName: e.target.value } )
								   }}/>
						</div>
						<div className="vertical-spacing">
							<Input type="email" name="email"
								   placeholder={t( 'Email' )}
								   size="large" addonBefore={<Icon type="mail"/>}
								   onChange={e => {
									   this.setState( { email: e.target.value } )
								   }}/>
						</div>

					</Modal>

				</div>
		)
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				location: state.location,
				place: state.place,
				account: state.account,
				authInfo: state.authInfo,
				typeImages: state.typeImages,
				desiredServiceType: state.desiredServiceType,
				desiredBookingMoment: state.desiredBookingMoment,
				landingPage: state.onLandingPage,
				searchCounter: state.searchCounter
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onServiceType: ( serviceType ) => dispatch( { type: 'SERVICETYPE', serviceType } ),
				onBookingMoment: ( bookingMoment ) => dispatch( { type: 'BOOKINGMOMENT', bookingMoment } ),
				requestManualLocation: ( status ) => dispatch( { type: 'REQUEST_MANUAL_LOCATION', status } ),
				requestDeviceLocation: ( status ) => dispatch( { type: 'REQUEST_DEVICE_LOCATION', status } ),
				onLocate: ( location ) => dispatch( { type: 'GEOLOCATE', location } ),
				onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } ),
				onSearch: () => dispatch( { type: 'SEARCHCOUNT' } )
			}
		}
)(
		compose(
				graphql( findSuppliers.gql, findSuppliers.options ),
				graphql( nearbyUnavailableSuppliers.gql, nearbyUnavailableSuppliers.options )
		)(
				translate( [ 'common' ], { wait: true } )( Browser )
		)
)
