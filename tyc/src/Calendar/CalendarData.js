import React from 'react'
import { connect } from 'react-redux'
import { graphql, compose } from 'react-apollo'
import moment from 'moment'
import { message } from 'antd'
import isEqual from 'lodash/isEqual'

import CalendarWidget from './CalendarWidget'
import { myTimeslots, findBookings, createTimeslot, updateTimeslot, deleteTimeslot } from '../graphql/calendar'
import { updateAccount } from '../graphql/account'

const DEBUG = false

class CalendarData extends React.Component {
	componentWillReceiveProps( next ) {
		if( next.bookingsError && (!isEqual( next.bookingsError, this.props.bookingsError )) )
			message.error( next.bookingsError.message )
	}

	calEventsFromBookings = () => {
		let events = []
		if( this.props.bookings ) events = this.props.bookings.map( b => {
			let start = moment( b.period.start.value ).toDate()
			let end = moment( b.period.end.value ).toDate()
			return {
				title: b.prodTitle,
				start, end,
				id: b.id,
				address: b.address,
				status: b.status,
				geoPosition: b.geoPosition,
				className: (b.clientId === this.props.authInfo.id) ? 'client' : 'supplier'
			}
		} )
		return events
	}

	calEventsFromSlots = () => {
		let events = []
		if( this.props.slots ) events = this.props.slots.map( one => {
			let start = moment( one.slot.start.value ).toDate()
			let end = moment( one.slot.end.value ).toDate()
			return {
				start, end,
				className: 'timeslot',
				editable: true,
				//rendering: 'background',
				id: 'slot-' + one.id // Ids have to be unique, may collide with booking id.
			}
		} )
		DEBUG && console.log( 'Timeslot count: ', events.length )
		return events
	}

	render() {
		let events = this.calEventsFromSlots().concat( this.calEventsFromBookings() )
		DEBUG && console.log( 'Event count:', events.length )
		return (
				<CalendarWidget
						events={events}
						{...this.props}
				/> )
	}
}

export default connect(
		function mapStateToProps( state ) {
			return {
				place: state.place,
				account: state.account,
				authInfo: state.authInfo,
				infoSplash: state.infoSplash
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onInfoSplash: ( infoSplash ) => dispatch( { type: 'INFOSPLASH', infoSplash } )
			}
		}
)( compose(
		graphql( updateAccount.gql, updateAccount.options ),
		graphql( findBookings.gql, findBookings.options ),
		graphql( myTimeslots.gql, myTimeslots.options ),
		graphql( createTimeslot.gql, createTimeslot.options ),
		graphql( updateTimeslot.gql, updateTimeslot.options ),
		graphql( deleteTimeslot.gql, deleteTimeslot.options )
		)(
		CalendarData
		)
)
