import React from 'react'
import { Row, Col, Button, Icon } from 'antd'
import { translate } from 'react-i18next'
import download from 'downloadjs'
import moment from 'moment'

const googleTime = 'YYYYMMDDTHHmmss';

class CalendarExport extends React.Component {

	state = {}

	googleLink = () => {
		let event = this.props.event;
		let link = 'https://calendar.google.com/calendar/render?action=TEMPLATE';
		link += '&dates=' + moment( event.start ).format( googleTime ) + 'Z/' + moment( event.end ).format( googleTime ) + 'Z';
		link += '&location=' + encodeURIComponent( event.address );
		link += '&text=' + encodeURIComponent( event.title );
		return link;
	}

	onGoogleClick = ( event ) => {
		event.stopPropagation()
		window.open( this.googleLink(), '_blank' )
	}

	iCalLink = ( e ) => {
		e.stopPropagation()

		let event = this.props.event;
		let id = 'TreatyaEvent-' + moment().format( 'YYYYMMDDTHHmmss' );
		let start = moment( event.start ).format( googleTime );
		let end = moment( event.end ).format( googleTime );
		let tstamp = moment().format( googleTime )
		let text =
				`BEGIN:VCALENDAR
					VERSION:2.0
					PRODID:treatya.com
					X-PUBLISHED-TTL:P1W
					BEGIN:VEVENT
						UID:${id}@treatya.com
						DTSTART:${start}
						SEQUENCE:0
						TRANSP:OPAQUE
						DTEND:${end}
						LOCATION:${event.address}
						SUMMARY:${event.title}
						CLASS:PUBLIC
						DTSTAMP:${tstamp}
					END:VEVENT
				END:VCALENDAR`;
		text = text.replace( /\t/g, '' );
		let blob = new Blob( [ text ], { type: "text/plain;charset=utf-8" } );
		download( blob, id + '.ics', 'text/calendar' );
	}

	render() {
		if( this.props.size === 'small' ) return (
				<div className="calendar-export-buttons" style={{display: 'inline-block', zIndex: 10}}>
					<Button size="small" icon="google" onClick={this.onGoogleClick}/>
					{' '}
					<Button size="small" icon="apple" onClick={this.iCalLink}/>
				</div>
		);
		else return (
				<Row className="calendar-export-buttons" gutter={12}>
					<Col xs={12}>
						<Button onClick={this.onGoogleClick}><Icon type="google"/></Button>
					</Col>
					<Col xs={12}>
						<Button onClick={this.iCalLink}><Icon type="apple"/> </Button>
					</Col>
				</Row>
		);
	}
}

export default translate( [ 'common' ], { wait: true } )( CalendarExport );
