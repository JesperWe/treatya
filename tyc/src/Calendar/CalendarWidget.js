import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { translate } from 'react-i18next'
import _get from 'lodash/get'
import _set from 'lodash/set'
import _cloneDeep from 'lodash/cloneDeep'
import moment from 'moment'
import 'moment/locale/sv'
import { Button, Alert, Modal, notification } from 'antd'
import $ from 'jquery'
import 'fullcalendar/dist/fullcalendar.css'
import 'fullcalendar/dist/fullcalendar.js'
import 'fullcalendar/dist/locale/sv.js'
import errorHandler from '../errorHandler'
import InfoSplash from '../InfoSplash'
import Page from '../CMS/Page'
import config from '../config'
import './Calendar.less'

const DEBUG = false

function HMM( date ) {
	return moment( date ).format( 'H:mm' )
}

function _EventStatusIcon( status ) {
	let icon
	if( status ) icon = "anticon-question-circle-o"
	if( status === 'CANCELLED' ) icon = "anticon-close-circle-o"
	if( status === 'DENIED' ) icon = "anticon-close-circle-o"
	if( status === 'CONFIRMED' ) icon = "anticon-check-circle-o"
	return icon
}

class CalendarWidget extends Component {
	state = {
		helpVisible: false,
		popoverLeft: 0,
		popoverTop: 0,
		popover: 0,
		event: null,
		eventPopup: null
	}

	overlapErrorHandler = ( error ) => {
		if( error.message.indexOf( 'overlapping_timeslot' ) >= 0 ) {
			notification.warning( {
				message: this.props.t( 'Free time slot not created.' ),
				description: this.props.t( 'Time slots should not overlap each other.' )
			} )
		}
		else
			errorHandler( error )
	}

	onViewChange = ( view ) => {
		this.setState( { helpVisible: false } )
		if( this.props.params.view ) {
			browserHistory.push( '/calendar/' + view.name )
		} else {
			browserHistory.replace( '/calendar/' + view.name )
		}
	}

	eventRender = ( event, element ) => {
		//DEBUG && console.log( 'eventRender()', event )
		if( event.status ) {
			let title = element.find( '.fc-title' )
			let icon = '<i class="anticon ' + _EventStatusIcon( event.status ) + '" /> ' + title.html()
			title.html( icon )
		}
		return element
	}

	gotoBooking = ( domEvent ) => {
		DEBUG && console.log( 'gotoBooking()', this.state.event.id )
		this.setState( { helpVisible: false } )
		browserHistory.push( '/booking/' + this.state.event.id )
	}

	deleteTimeslot = ( domEvent ) => {
		DEBUG && console.log( 'deleteTimeslot()', this.state.event.id )
		let id = +(this.state.event.id.split( '-' )[ 1 ]) // Remove 'slot-' before id number.
		this.setState( { event: null } )
		config.tracking && window._paq.push( [ 'trackEvent', 'Account', 'Delete Timeslot' ] )
		this.props.deleteTimeslot( id )
				.catch( errorHandler )
	}

	onSelectSlot = ( start, end, domEvent, view ) => {
		DEBUG && console.log( 'onSelectSlot()', start.format(), end.format() )
		if( !this.props.account || !this.props.account.id ) return

		if( start.isSame( end ) ) return

		if( start.isBefore() ) {
			notification.warning( { message: this.props.t( 'You can not create timeslots in the passed.' ) } )
			this.calendar.fullCalendar( 'unselect' )
			return
		}

		this.props.createTimeslot( {
					supplierId: this.props.account.supplierId,
					start: start.format(),
					end: end.format()
				} )
				.then( result => {
					this.calendar.fullCalendar( 'unselect' )
					this.setState( { event: null } )
					config.tracking && window._paq.push( [ 'trackEvent', 'Account', 'Add Timeslot' ] )
					notification.success( { message: this.props.t( 'Time slot created.' ) } )
					if( !_get( this.account, 'uiSettings.supplierRegistration.steps.slots' ) ) {
						let uiSettings = _cloneDeep( this.props.account.uiSettings )
						_set( uiSettings, 'supplierRegistration.steps.slots', { done: true } )
						_set( uiSettings, 'infoSplash.supplierWelcome', { dismissed: true } )
						this.props.updateAccount( this.props.authInfo.id, { uiSettings } )
					}
				} )
				.catch( this.overlapErrorHandler )
	}

	checkEventSelected = () => {
		if( !this.state.event ) return
		DEBUG && console.log( 'checkEventSelected()' )
		let calView = this.calendar.fullCalendar( 'getView' )
		if( _get( calView, 'selectedEventInstance.id' ) !== this.state.event.id ) this.setState( { event: null } )
	}

	unhighlightAll = () => {
		let previous = document.querySelectorAll( '.fc-event.selected' )
		for( let item of previous ) {
			item.classList.remove( 'selected' )
		}
	}

	onDayClick = ( day ) => {
		DEBUG && console.log( day.format(), moment( day ).format() )
		if( this.state.event ) {
			this.setState( { event: null } )
			this.calendar.fullCalendar( 'unselect' )
			this.unhighlightAll()
		}
		else {
			if( this.calendar.fullCalendar( 'getView' ).name === 'month' )
				this.calendar.fullCalendar( 'changeView', 'agendaDay', day )
			else
				this.onSelectSlot( day, day.clone().add( 1, 'hour' ) )
		}
	}

	onSelectEvent = ( event, domEvent, view ) => {
		DEBUG && console.log( 'onSelectEvent()', HMM( event.start ), HMM( event.end ), event.className, event.id, this )
		this.setState( { event } )
		this.unhighlightAll()
		domEvent.currentTarget.classList.add( 'selected' )
		let calView = this.calendar.fullCalendar( 'getView' )
		calView.selectEventInstance( event )
	}

	onEventDropOrResize = ( event, delta, revertFunc, jsEvent, ui, view ) => {
		let id = +(event.id.split( '-' )[ 1 ]) // Remove 'slot-' before id number.
		let patch = {
			slot: {
				start: { value: event.start.format(), inclusive: true },
				end: { value: event.end.format(), inclusive: true }
			}
		}
		this.props.updateTimeslot( id, patch )
				.catch( error => {
					revertFunc()
					this.overlapErrorHandler( error )
				} )
	}

	componentWillReceiveProps( next ) {
		if( next.params.view !== this.props.params.view ) {
			if( this.calendar ) {
				this.calendar.fullCalendar( 'changeView', next.params.view )
			}
		}
	}

	componentDidMount() {
		const { t } = this.props
		this.props.onInfoSplash( {
			id: 'calendarIntro',
			title: t( 'calendar-intro-title' ),
			content: t( 'calendar-intro-content' )
		} )
	}

	componentWillUpdate( next ) {
		if( !this.calendar ) {
			const { calendar } = this.refs
			if( calendar ) {
				this.calendar = $( calendar )

				DEBUG && console.log( 'Calendar init: ', typeof this.calendar, typeof calendar )
				if( DEBUG ) window.fullcal = this.calendar

				let defaultView = 'agendaWeek'
				let middleView = 'agendaWeek'
				if( window.matchMedia( "(max-width: 480px)" ).matches ) {
					defaultView = 'month'
					middleView = 'agendaDay'
				}

				this.calendar.fullCalendar( {
					height: 'parent',
					locale: this.props.account.language || 'sv',
					header: {
						left: 'prev,next today help',
						center: 'title',
						right: 'month,' + middleView + ',listMonth'
					},
					viewRender: this.onViewChange,
					longPressDelay: 400,
					eventLongPressDelay: 400,
					selectable: true,
					select: this.onSelectSlot,
					selectMinDistance: 10,
					snapDuration: { minutes: 15 },
					dayClick: this.onDayClick,
					eventClick: this.onSelectEvent,
					eventDrop: this.onEventDropOrResize,
					eventResize: this.onEventDropOrResize,
					eventRender: this.eventRender,
					customButtons: {
						help: {
							icon: 'question',
							click: ( e ) => {
								this.setState( { helpVisible: true } )
							}
						}
					},
					defaultView,
					scrollTime: '08:30',
					timeFormat: 'H:mm',
					allDaySlot: false,
					slotLabelFormat: 'H:mm',
					events: ( start, end, tz, callback ) => {
						callback( (this.props.events || []).map( e => {
							let ev = Object.assign( {}, e )
							if( !e.title ) ev.title = this.props.t( 'Available' )
							return ev
						} ) )
					}
				} )
			}
		}
	}

	render() {
		if( this.calendar ) {
			const newNo = (this.props.events && this.props.events.length) || 0
			if( !this.oldNo || newNo !== this.oldNo ) {
				DEBUG && console.log( 'Calendar refetch', this.props.events.length, 'events.' )
				this.calendar.fullCalendar( 'refetchEvents' )
			}
			this.oldNo = (this.props.events && this.props.events.length) || 0
		}

		DEBUG && console.log( '               CalendarWidget.render()', this.props.events.length, 'events.' )

		if( !this.props.account || !this.props.account.role )
			return (<div className="content-width-medium content-pad">
				<Alert showIcon type="warning" message="Not authenticated!"/>
			</div>)

		let showDeleteButton = ''
		let showGotoButton = ''

		let { event } = this.state
		if( event ) {
			let eventClass = event.className[ 0 ]
			if( eventClass === 'timeslot' ) {
				showDeleteButton = ' in'
				showGotoButton = ''
			}
			if( eventClass === 'supplier' || eventClass === 'client' ) {
				showDeleteButton = ''
				showGotoButton = ' in'
			}
		}

		let height = (document.querySelector( 'div.router' ).offsetHeight - document.querySelector( 'div.topbar' ).offsetHeight) + 'px';

		return (
				<div className="calendar" style={{ height, paddingTop: '1rem', paddingBottom: '10px' }}
					 onClick={this.checkEventSelected}>
					<InfoSplash/>
					<div ref='calendar'/>

					<div className={"action-button bottom-right fade" + showDeleteButton}>
						<Button type="primary" onClick={this.deleteTimeslot} icon="delete"/>
					</div>
					<div className={"action-button bottom-right fade" + showGotoButton}>
						<Button type="primary" onClick={this.gotoBooking} icon="idcard"/>
					</div>

					<Modal visible={this.state.helpVisible}
						   wrapClassName="calendar-help" width="748px"
						   footer={[
							   <Button onClick={() => this.setState( { helpVisible: false } )} key="1">{this.props.t( 'Close' )}</Button>
						   ]}
						   onClose={() => this.setState( { helpVisible: false } )}>
						<Page pageRoute="calendar-help"/>
					</Modal>
				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( CalendarWidget )
