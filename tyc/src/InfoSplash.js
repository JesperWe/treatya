import React from 'react'
import { connect } from 'react-redux'
import { translate, Interpolate } from 'react-i18next'
import _cloneDeep from 'lodash/cloneDeep'
import _isEqual from 'lodash/isEqual'
import _get from 'lodash/get'
import { graphql } from 'react-apollo'
import { Link } from 'react-router'
import { Checkbox, Button } from 'antd'
import './InfoSplash.less'
import { updateAccount } from './graphql/account'

class InfoSplash extends React.Component {
	state = {
		infoSplash: {},
		hide: 'hide',
		dismissed: false,
		content: null
	}

	componentWillMount() {
		this.infoSplash( this.props )
	}

	componentWillUnmount() {
		this.setState( { hide: 'hide' } )
	}

	componentWillReceiveProps( next ) {
		this.infoSplash( next )
	}

	infoSplash = ( props ) => {
		const { t } = props
		if( !props.account || !props.account.uiSettings ) return

		if( _isEqual( this.state.infoSplash, props.infoSplash ) ) return

		if( !props.infoSplash ) return
		if( props.infoSplash.shown ) return
		if( !props.infoSplash.id ) return

		this.setState( {
			infoSplash: props.infoSplash,
		} )

		let { uiSettings } = props.account
		if( _get( uiSettings, 'infoSplash.' + props.infoSplash.id + '.dismissed' ) ) return

		let content = props.infoSplash.content
		if( typeof content === 'object' ) {
			if( content.link ) content.link = <Link to={content.link.to}>{content.link.text}</Link>
			content = <Interpolate {...content} />
		}
		else if( content.indexOf('\n') >= 0 ) {
			let lines = content.split('\n')
			content = <span>{lines.map( (line,i) => <p key={i} dangerouslySetInnerHTML={{__html: line}}/>)}</span>
		}

		this.setState( {
			content: <div className="splash-body vertical-spacing">
				<div className="vertical-spacing">{content}</div>
				<Checkbox onChange={ ( e ) => this.setState( { dismissed: e.target.checked } ) }
						  className="vertical-spacing">
					{t( 'Do not show this message again' )}
				</Checkbox>
			</div>
		} )

		this.setState( { hide: '' } )
	}

	onOk = () => {
		let is = Object.assign( {}, this.props.infoSplash )
		is.shown = true
		this.props.onInfoSplash( is )

		this.setState( { hide: 'hide' } )

		if( this.state.dismissed ) {
			let uiSettings = _cloneDeep( this.props.account.uiSettings )
			if( !uiSettings.infoSplash ) uiSettings.infoSplash = {}

			let me = uiSettings.infoSplash[ this.props.infoSplash.id ]
			if( !me ) me = {}
			me.dismissed = true
			uiSettings.infoSplash[ this.props.infoSplash.id ] = me
			this.props.updateAccount( this.props.authInfo.id, { uiSettings } )
		}
	}

	render() {
		const { t } = this.props
		return (
				<div className={'infosplash ' + this.state.hide}>
					<h2>{_get(this.props, 'infoSplash.title' )}</h2>
					{this.state.content}
					<Button className="responsive" size="large" onClick={this.onOk}>{t( 'Ok' )}</Button>
				</div>
		)
	}
}

export default
graphql( updateAccount.gql, updateAccount.options )(
		connect(
				function mapStateToProps( state ) {
					return {
						infoSplash: state.infoSplash,
						account: state.account,
						authInfo: state.authInfo,
					}
				},
				function mapDispatchToProps( dispatch ) {
					return {
						onInfoSplash: ( infoSplash ) => dispatch( { type: 'INFOSPLASH' } )
					}
				}
		)(
				translate( [ 'common' ], { wait: true } )(
						InfoSplash
				)
		)
)
