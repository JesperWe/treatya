import React from 'react'
import { graphql, compose } from 'react-apollo'
import moment from 'moment'
import { connect } from 'react-redux'
import http from 'axios'
import _get from 'lodash/get'
import diff from 'deep-diff'
import { browserHistory } from 'react-router'
import { translate, Interpolate } from 'react-i18next'
import { booking } from '../graphql/booking'
import Login from '../Account/Login'
import { createReview, getReview, updateReview } from '../graphql/review'
import errorHandler from '../errorHandler'
import config from '../config'
import './Review.less'
import { Rate, Input, Button, Row, Col, Spin, Alert, Modal, Popconfirm, notification } from 'antd'

const DEBUG = true

class ReviewPage extends React.Component {

	state = {
		rating: undefined,
		comment: undefined,
		unmodified: true,
		outcomeSaved: false
	}

	componentWillMount() {
		if( this.props.review )
			this.setState( {
				rating: _get( this.props, 'review.rating' ),
				comment: _get( this.props, 'review.comment' )
			} )
	}

	componentWillReceiveProps( next ) {
		DEBUG && console.log( '\nReviewPage.componentWillReceiveProps\n', JSON.stringify( diff( next, this.props ) ) )

		if( next.reviewLoading === false && this.props.reviewLoading === true ) {
			this.setState( {
				rating: _get( next, 'review.rating' ),
				comment: _get( next, 'review.comment' )
			} )
		}
	}

	changeRating = ( rating ) => {
		this.setState( { rating, unmodified: false } )
	}

	changeComment = ( event ) => {
		this.setState( { comment: event.target.value, unmodified: false } )
	}

	saveReview = ( event ) => {
		let { booking, review, t } = this.props

		config.tracking && window._paq.push( [ 'trackEvent', 'Booking', 'Review Save' ] )

		let accountId = booking.clientId
		let target = 'CLIENT'
		if( this.props.authInfo.id === booking.clientId ) {
			accountId = booking.accountId
			target = 'SUPPLIER'
		}

		if( review && review.id )
		// Update previously made review.
			this.props.updateReview( review.id, this.state.rating || review.rating, this.state.comment || review.comment )
					.then( () => {
						this.setState( { unmodified: true } )
						notification.success( { message: t( 'Saved successfully.' ) } )
					} )
					.catch( errorHandler )
		else
		// Create new review.
			this.props.createReview( {
						accountId, target,
						reviewerId: this.props.authInfo.id,
						supplierId: booking.supplierId,
						bookingId: booking.id,
						rating: this.state.rating,
						comment: this.state.comment
					} )
					.then( result => {
						// Client makes review implies treatment was delivered.
						if( target === 'SUPPLIER' ) this.saveChangedStatus( 'DELIVERED' )
						this.setState( { unmodified: true } )
						notification.success( { message: t( 'Saved successfully.' ) } )
					} )
					.catch( errorHandler )
	}

	saveChangedStatus = ( newStatus ) => {
		this.setState( { outcomeSaved: true } )
		http.post( config.backend.url + '/notify/booking/status-change', {
					id: this.props.authInfo.id,
					jwt: this.props.authToken,
					bookingId: this.props.booking.id,
					status: newStatus
				} )
				.then( result => {
					if( newStatus === 'NOTDELIVERED' || newStatus === 'NOSHOW' || newStatus === 'DENIED' ) {
						browserHistory.push( '/booking/' + this.props.booking.id )
					}
				} )
				.catch( errorHandler )
	}

	onStatusChange = ( newStatus ) => {
		DEBUG && console.log( 'Changing booking status:', newStatus )
		this.saveChangedStatus( newStatus )
	}

	render() {
		let { t, authInfo } = this.props

		if( !authInfo ) return (
				<div>
					<Alert type="warning" showIcon message={t( 'You need to login before you can submit reviews.' )}
						   className="login-widget"/>
					<Login onComplete={this.tryGetBooking}/>
				</div>)

		if( !this.props.booking ) return (
				<div className="content-width content-pad centered">
					{this.props.bookingLoading && <Spin size="large"/>}
					{!this.props.bookingLoading &&
					<Alert
							message={t( 'Oops!' )}
							description={t( 'You do not have a booking with this id.' )}
							type="warning"
							showIcon/>
					}
				</div>)

		let introText
		let isSupplier = false
		let reviewable = false
		let supplierModalVisible = false
		let clientModalVisible = false

		let booking = Object.assign( {}, this.props.booking )
		if( !booking || !booking.id ) {
			return <Alert type="error" className="login-widget"
						  showIcon message={t( 'Booking not found' )}/>
		}

		booking.start = moment( booking.period.start.value )
		booking.end = moment( booking.period.end.value )

		if( booking.start.isAfter() ) return (
				<Alert type="error" className="login-widget"
					   showIcon message={t( 'You can not review this booking' )}
					   description={t( 'It has not even started yet.' )}/>)

		isSupplier = (authInfo.id === booking.accountId)

		reviewable =
				booking.status === 'DELIVERED' ||
				booking.status === 'CONFIRMED' ||
				booking.status === 'UNCONFIRMED'

		if( !reviewable ) return (
				<Alert type="warning" className="login-widget"
					   showIcon message={t( 'You can not review this booking' )}
					   description={t( 'It seems the booking was not executed.' )}/>)

		introText = isSupplier ?
				<Interpolate i18nKey='Review client intro' name={booking.accountByClientId.fullName}/>
				:
				<Interpolate i18nKey='Review supplier intro' name={booking.supplierBySupplierId.title}/>

		supplierModalVisible =
				isSupplier
				&& ((booking.status === 'UNCONFIRMED') || (booking.status === 'CONFIRMED'))
				&& !this.state.outcomeSaved

		clientModalVisible =
				!isSupplier
				&& ((booking.status === 'UNCONFIRMED') || (booking.status === 'CONFIRMED'))
				&& !this.state.outcomeSaved

		const confirmText = (
				<div dangerouslySetInnerHTML={{ __html: t( 'Are you sure?' ) + '<br/>' + t( 'You can <b><u>not</b></u> change the response later.' ) }}/>)

		return (
				<div className="review">
					<Row type="flex" gutter={25}>

						<Col xs={24} sm={12}>
							<div className={'info-box'}>
								<h2 className="vertical-spacing">{t( 'Your Review' )}</h2>
								<p className="left vertical-spacing">{introText}</p>

								<div className="vertical-spacing">
									<Rate count={5} defautValue={5}
										  value={this.state.rating}
										  onChange={this.changeRating}/>
								</div>
								<div className="vertical-spacing">
									<Input.TextArea autosize={{ minRows: 2, maxRows: 10 }}
													onChange={this.changeComment}
													value={this.state.comment}
													placeholder={t( 'Review comments' )}/>
								</div>
								<Button type="primary" className="responsive" size="large"
										disabled={this.state.unmodified || this.state.rating === undefined}
										onClick={this.saveReview}>
									{t( 'Save' )}
								</Button>
							</div>
						</Col>

						<Col xs={24} sm={12}>
							<div className={'info-box'}>
								<h2>{t( 'Treatment' )}</h2>

								{booking &&
								<div className="table-wrap">
									<table className="vertical-spacing">
										<tbody>

										<tr>
											<td>{t( 'Date' )}</td>
											<td>{moment( booking.period.start.value ).format( config.date.default )}</td>
										</tr>

										<tr>
											<td>{t( 'Supplier' )}</td>
											<td>{booking.supplierBySupplierId.title}</td>
										</tr>

										<tr>
											<td>{t( 'Treatment' )}</td>
											<td>{booking.productByProductId.prodTitle}</td>
										</tr>

										<tr>
											<td>{t( 'Client' )}</td>
											<td>{booking.accountByClientId.fullName}</td>
										</tr>

										</tbody>
									</table>
								</div>}

							</div>
						</Col>

					</Row>

					<Modal visible={supplierModalVisible} maskClosable={false}
						   closable={false} className="result-modal" footer={null}>
						<h2 className="vertical-spacing">{t( 'What was the treatment outcome?' )}</h2>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'DELIVERED' )}>
							<Button className="responsive vertical-spacing" size="large" type="primary">
								{t( 'Delivered as planned' )}</Button>
						</Popconfirm>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'NOSHOW' )}>
							<Button className="responsive vertical-spacing" size="large">
								{t( 'The client was not available' )}</Button>
						</Popconfirm>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'DENIED' )}>
							<Button className="responsive vertical-spacing" size="large">
								{t( 'The client did not want the treatment' )}</Button>
						</Popconfirm>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'NOTDELIVERED' )}>
							<Button className="responsive vertical-spacing" size="large">
								{t( 'You were unable to deliver as planned' )}</Button>
						</Popconfirm>

					</Modal>

					<Modal visible={clientModalVisible} maskClosable={false}
						   closable={false} className="result-modal" footer={null}>
						<h2 className="vertical-spacing">{t( 'What was the treatment outcome?' )}</h2>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'DELIVERED' )}>
							<Button className="responsive vertical-spacing" type="primary" size="large">
								{t( 'Delivered as planned' )}
							</Button>
						</Popconfirm>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'NOTDELIVERED' )}>
							<Button className="responsive vertical-spacing" size="large">
								{t( 'The supplier never showed up' )}</Button>
						</Popconfirm>

						<Popconfirm okText={t( 'Yes' )} cancelText={t( 'No' )} title={confirmText}
									onConfirm={() => this.onStatusChange( 'NOSHOW' )}>
							<Button className="responsive vertical-spacing" size="large">
								{t( 'I was not present to receive it' )}</Button>
						</Popconfirm>

					</Modal>

				</div>
		)
	}
}

const Review =
		connect(
				function mapStateToProps( state ) {
					return {
						authInfo: state.authInfo,
						authToken: state.authToken,
					}
				},
				function mapDispatchToProps( dispatch ) {
					return {}
				}
		)(
				compose(
						graphql( booking.gql, booking.options ),
						graphql( getReview.gql, getReview.options ),
						graphql( createReview.gql, createReview.options ),
						graphql( updateReview.gql, updateReview.options )
				)(
						translate( [ 'common' ], { wait: true } )( ReviewPage )
				)
		)

export default function( props ) {
	return <Review bookingId={props.params.bookingId}/>
}
