import React from 'react'
import { Router, browserHistory } from 'react-router'
import { Provider as ReduxProvider, connect } from 'react-redux'
import reduxStore from './reduxStore'
import http from 'axios'
import AccountService from './Account/AccountService'
import SiteMenu from './Navigation/SiteMenu'
import RobotLinks from './Navigation/RobotLinks'
import LocatorService from './Location/LocatorService'
import { I18nextProvider } from 'react-i18next'
import i18n from './i18n'
import { translate } from 'react-i18next'
import moment from 'moment'
import 'moment/locale/sv'
import cookie from 'browser-cookies'
import en_US from 'antd/lib/locale-provider/en_US'
import sv_SE from 'antd/lib/locale-provider/sv_SE'
import { LocaleProvider, Spin, Icon, notification, Modal } from 'antd'
import CookieConsent from './CookieConsent'
import errorHandler from './errorHandler'
import createActivityDetector from 'activity-detector'
import config from './config'
import './Style.less'

const DEBUG = false

const rootRoute = {
	childRoutes: [
		{
			path: '/',
			component: require( './Routes/BrowserRoute' ).default
		}, {
			path: '/login(/:type)',
			component: require( './Routes/Login' ).default
		},
		require( './Routes/Booking' ),
		require( './Routes/Bookings' ),
		require( './Routes/Supplier' ),
		require( './Routes/Register' ),
		require( './Routes/Profile' ),
		require( './Routes/Calendar' ),
		require( './Routes/ResetPasswd' ),
		require( './Routes/Page' ),
		require( './Routes/Admin' ),
		require( './Routes/FAQ' ),
		require( './Routes/Stripe' ),
		require( './Routes/Review' ),
		require( './Routes/Test' ),
		/*{
		 path: '/__debug',
		 component: require( './Debug/DebugCalendar' ).default
		 },*/ {
			path: '*',
			component: require( './Error404' ).default
		} ]
}

// The HOC tree has to be divided to insert the Ant Design language change stuff.
// Do Active/Idle here as well since we're inside Redux.
class AntDesignPlusRouter extends React.Component {
	state = {
		antd_locale: sv_SE,
		clientIsRobot: false
	}

	componentWillMount() {
		const urlParams = new URLSearchParams( window.location.search )
		if( urlParams.has( 'robot' ) ) this.setState( { clientIsRobot: true } )

		const activityDetector = createActivityDetector( {
			timeToIdle: (config.idle && config.idle.timeout) || 30000
		} )
		activityDetector.on( 'idle', () => {
			this.props.onIdle( true )
		} )
		activityDetector.on( 'active', () => {
			this.props.onIdle( false )

			// When coming back from ide, check appVersion against index.html
			http.get( config.idle.versionCheckUrl, {
						headers: { 'Cache-Control': 'no-cache' }
					} )
					.then( indexFile => {
						let htmlString = indexFile.data
						let searchString = 'property="version" content="'
						let versionPos = htmlString.indexOf( searchString ) + searchString.length
						let versionEnd = htmlString.indexOf( '"', versionPos )
						let serverVersion = htmlString.substring( versionPos, versionEnd )
						if( serverVersion !== window.appVersion ) {
							console.log( 'New App Version:', serverVersion )
							Modal.info( {
								title: this.props.t( 'New version available' ),
								content: (
										<div>
											<p>{this.props.t( 'A new version of the Treatya App has been published. The page will now be reloaded.' )}</p>
											<p><br/>{serverVersion}</p>
										</div>
								),
								onOk() {
									window.location.reload()
								},
							} )
						}
					} )
					.catch( error => {
						console.log( 'Version update check failed.' )
					} )
		} )
	}

	componentWillReceiveProps( next ) {
		moment.locale( next.i18n.language )
		this.props.onLanguage( next.i18n.language )
		switch( next.i18n.language ) {
			case 'sv':
				this.setState( { antd_locale: sv_SE } )
				break
			default:
			case 'en':
				this.setState( { antd_locale: en_US } )
				break
		}
	}

	onRouteUpdate = () => {
		DEBUG && console.log( 'onRouteUpdate()', window.location.pathname )
		const { t } = this.props

		let title = 'Treatya - '
		const routeParts = window.location.pathname.split( '/' )
		const urlParams = new URLSearchParams( window.location.search )

		if( routeParts[ 1 ] === '' ) {
			this.props.onLandingPage( true )

			if( urlParams.has( 't' ) ) {
				title += t( urlParams.get( 't' ) ) + ' - '
				title += t( 'Search results' )
			} else {
				title += t( 'Search and book beauty treatments straight to your home' )
			}
		} else {
			title += t( routeParts[ 1 ] )
			if( routeParts[ 2 ] ) title += ' - ' + routeParts[ 2 ]
		}

		const hamburger = [ ...document.getElementsByClassName( 'hamburger' ) ]
		if( hamburger.length ) hamburger[ 0 ].scrollIntoView()

		document.title = title
		config.tracking && window._paq.push( [ 'setDocumentTitle', title ] )
		config.tracking && window._paq.push( [ 'setCustomUrl', window.location.pathname ] )
		config.tracking && window._paq.push( [ 'trackPageView' ] )
	}

	render() {
		return (
				<LocaleProvider locale={this.state.antd_locale}>
					<div className="router relative">

						{this.props.componentsLoading &&
						<div className="components-loading">
							<Spin indicator={<Icon type="loading" style={{ fontSize: 60 }}/>}/>
						</div>
						}

						{!this.state.clientIsRobot && <LocatorService/>}
						<AccountService/>
						<SiteMenu/>
						<div className={"content-fill" + ((this.props.landingPage) ? ' landing' : '')}>
							<Router history={browserHistory} children={rootRoute}
									onUpdate={this.onRouteUpdate} t={this.props.t}/>
						</div>
						<CookieConsent/>

						{this.state.clientIsRobot && <RobotLinks/>}

					</div>
				</LocaleProvider>
		)
	}
}

const WrappedAntDesignPlusRouter = connect(
		function mapStateToProps( state ) {
			return {
				landingPage: state.onLandingPage,
				componentsLoading: state.componentsLoading
			}
		},
		function mapDispatchToProps( dispatch ) {
			return {
				onLanguage: ( language ) => dispatch( { type: 'LANGUAGE', language } ),
				onIdle: ( idle ) => dispatch( { type: 'IDLESTATUS', idle } ),
				onLandingPage: ( status ) => dispatch( { type: 'ON_LANDING_PAGE', status } )
			}
		}
)(
		// Also wrap the component in translate() to make sure we receive props when language changes.
		translate( [ 'common' ] )( AntDesignPlusRouter )
)

function urlB64ToUint8Array( base64String ) {
	const padding = '='.repeat( (4 - base64String.length % 4) % 4 )
	const base64 = (base64String + padding)
			.replace( /-/g, '+' ).replace( /_/g, '/' )
	const rawData = window.atob( base64 )
	const outputArray = new Uint8Array( rawData.length )
	for( let i = 0; i < rawData.length; ++i ) {
		outputArray[ i ] = rawData.charCodeAt( i )
	}
	return outputArray
}

class App extends React.Component {

	componentWillMount() {
		moment.locale( cookie.get( 'language' ) || 'sv' )

		notification.config( {
			placement: 'bottomRight'
		} )

		// Accurately measure time spent on the page
		config.tracking && window._paq.push( [ 'enableHeartBeatTimer', 10 ] )

		// ServiceWorker for Push Notifications
		if( config.backend.pushNotifications.enabled ) {
			let isSubscribed, subscription
			if( 'serviceWorker' in navigator && 'PushManager' in window ) {
				console.log( 'Service Worker and Push is supported' )
				navigator.serviceWorker.register( '/js/pushWorker.js' )
						.then( swReg => {
							console.log( 'Service Worker is registered' )
							window.pushWorker = swReg
							return window.pushWorker.pushManager.getSubscription()
						} )
						.then( sub => {
							isSubscribed = (sub !== null)
							if( isSubscribed ) {
								console.log( 'User IS subscribed.' )
								subscription = sub
								return Promise.resolve( sub )
							} else {
								console.log( 'User is NOT subscribed.' )
								return window.pushWorker.pushManager.subscribe( {
									userVisibleOnly: true,
									applicationServerKey: urlB64ToUint8Array( config.backend.push.apiKey )
								} )
							}
						} )
						.then( newSub => {
							console.log( 'User is subscribed.' )
							//updateSubscriptionOnServer( newSubscription )
							subscription = newSub
							isSubscribed = true
							console.log( JSON.stringify( subscription ) )
						} )
						.catch( errorHandler )
			} else {
				console.warn( 'Push messaging is not supported' )
			}
		}
	}

	render() {
		return (
				<ReduxProvider store={reduxStore}>
					<I18nextProvider i18n={i18n}>
						<WrappedAntDesignPlusRouter i18n={i18n}/>
					</I18nextProvider>
				</ReduxProvider>
		)
	}
}

export default App
