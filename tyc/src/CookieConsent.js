import React from 'react'
import { translate } from 'react-i18next'
import cookie from 'browser-cookies'
import { Button, Icon } from 'antd'
import './CookieConsent.css'

class CookieConsent extends React.Component {
	state = {
		consent: false
	}

	componentWillMount() {
		this.setState( { consent: cookie.get( 'euCookieConsent' ) } )
	}

	consent = () => {
		cookie.set( 'euCookieConsent', 'yes', { expires: 999 } )
		this.setState( { consent: true } )
	}

	render() {
		const { t } = this.props

		if( this.state.consent ) return null

		const urlParams = new URLSearchParams( window.location.search )
		if( urlParams.has( 'robot' ) ) return null

		return (
				<div className="cookie-consent">
					<div className="info">
						<Icon type="pushpin"/>{' '}
						{t( 'We use Cookies!' )}{' '}<a
							href="http://www.minacookies.se/">{t( 'Read more Cookie info' )}</a>
					</div>
					<Button type="primary" onClick={this.consent}>{t( 'I Understand' )}</Button>
				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( CookieConsent )
