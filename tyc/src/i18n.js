import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

// Special version of https://github.com/i18next/i18next-browser-languageDetector/blob/master/src/browserLookups/navigator.js
// We only want the first two letters of the browser accepted language codes.
const navigatorOnlyLanguageNoCountry = {
	name: 'navigatorOnlyLanguageNoCountry',
	lookup( options ) {
		let found = []

		if( typeof navigator !== 'undefined' ) {
			if( navigator.languages ) { // chrome only; not an array, so can't use .push.apply instead of iterating
				for( let i = 0; i < navigator.languages.length; i++ ) {
					found.push( navigator.languages[ i ] )
				}
			}
			if( navigator.userLanguage ) {
				found.push( navigator.userLanguage )
			}
			if( navigator.language ) {
				found.push( navigator.language )
			}
		}

		found = found.map( lang => lang.substr( 0, 2 ) )
		//return found.length > 0 ? found : undefined
		return 'sv'
	}
}

let languageDetector = new LanguageDetector()
languageDetector.addDetector( navigatorOnlyLanguageNoCountry )

i18n
		.use( XHR )
		.use( languageDetector )
		.init( {
			detection: {
				// order and from where user language should be detected
				order: [ 'querystring', 'cookie',  'navigatorOnlyLanguageNoCountry', 'localStorage' ],

				// keys or params to lookup language from
				lookupQuerystring: 'language',
				lookupCookie: 'language',
				lookupLocalStorage: 'language',

				// cache user language on
				caches: [ 'cookie' ],

				// optional expire and domain for set cookie
				cookieMinutes: 60 * 24 * 365,
				//cookieDomain: 'myDomain',

				// optional htmlTag with lang attribute, the default is:
				//	htmlTag: document.documentElement
			},

			fallbackLng: 'sv',
			nsSeparator: false,
			keySeparator: false,
			debug: false,

			ns: [ 'common' ],
			defaultNS: 'common',

			interpolation: {
				escapeValue: false // not needed for react!!
			},

			backend: {
				loadPath: "/locales/{{lng}}/{{ns}}.json"
			}
		} );

export default i18n;
