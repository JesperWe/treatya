import { createStore } from 'redux'
import update from 'immutability-helper'
import wkx from 'wkx'
import _get from 'lodash/get'
import bufferImpl from 'buffer'
import moment from 'moment'

const Buffer = bufferImpl.Buffer

const reduce = function( state = {
	language: undefined,
	status: 'logged-out',
	authToken: null,
	authInfo: null,
	account: {},
	counters: {},
	supplierInfo: {},
	infoSplash: {},
	location: null,
	desiredBookingMoment: moment().locale( 'sv' ).add( 90, 'm' ).startOf( 'hour' ),
	desiredServiceType: undefined,
	manualLocationRequested: false,
	deviceLocationRequested: false,
	deviceLocatorError: false,
	typeImages: {},
	onLandingPage: false, // Because Sitemenu needs to know, and it sits above React Router
	isIdle: false,
	searchCounter: 0,
	componentsLoading: false,
	faq: null,
	staticPages: null,
	cmsTexts: null,
	sendNotificationToAccountId: null,
	googleApiStatus: null
}, action ) {
	switch( action.type ) {
		case 'LANGUAGE':
			return {
				...state,
				language: action.language
			}

		case 'ON_LANDING_PAGE':
			return {
				...state,
				onLandingPage: action.status
			}

		case 'IDLESTATUS':
			return {
				...state,
				isIdle: action.idle
			}

		case 'SEARCHCOUNT':
			return {
				...state,
				searchCounter: state.searchCounter + 1
			}

		case 'COMPONENTS_LOADING':
			return {
				...state,
				componentsLoading: action.payload
			}

		case 'GOOGLE_API':
			return {
				...state,
				googleApiStatus: action.googleApiStatus
			}

		case 'TYPEIMAGES':
			return {
				...state,
				typeImages: action.typeImages
			}

		case 'FAQ':
			return {
				...state,
				faq: action.faq
			}

		case 'STATICPAGES':
			return {
				...state,
				staticPages: action.staticPages
			}

		case 'CMSTEXTS':
			return {
				...state,
				cmsTexts: action.cmsTexts
			}

		case 'REQUEST_MANUAL_LOCATION':
			return {
				...state,
				manualLocationRequested: action.status
			}

		case 'REQUEST_DEVICE_LOCATION':
			return {
				...state,
				deviceLocationRequested: action.status
			}

		case 'BOOKINGMOMENT':
			return {
				...state,
				desiredBookingMoment: action.bookingMoment,
			}

		case 'SERVICETYPE':
			return {
				...state,
				desiredServiceType: action.serviceType,
			}

		case 'LOGIN':
			return {
				...state,
				status: 'logged-in',
				authToken: action.authToken,
				authInfo: action.authInfo
			}

		case 'COUNTERS':
			return {
				...state,
				counters: action.counters
			}

		case 'SUPPLIERINFO':
			return {
				...state,
				supplierInfo: action.supplierInfo
			}

		case 'ACCOUNT': {
			let { account } = action

			// Add .location.position to account
			let position
			if( account && account.geoPosition ) {
				let wkbBuffer = new Buffer( action.account.geoPosition, 'hex' )
				let geoPoint = wkx.Geometry.parse( wkbBuffer )
				position = { lat: geoPoint.x, lng: geoPoint.y }
			}

			// Lift supplierId to a more accessible location
			const supplierId = _get( account, 'suppliersByAccountId.nodes[0].id' )
			account = Object.assign( { supplierId, location: { position } }, account )

			// If the account has position but the device not, we override device pos with the pos from the account.
			if( !state.location || !state.location.position || state.location.error ) {
				return { ...state, location: { position }, account }
			}
			return { ...state, account }
		}

		case 'LOGOUT':
			return {
				...state,
				status: 'logged-out',
				authToken: null,
				authInfo: null,
				account: {}
			}

		case 'GEOLOCATE':
			if( action.deviceLocatorError === undefined ) action.deviceLocatorError = state.deviceLocatorError
			return {
				...state,
				manualLocationRequested: false,
				deviceLocationRequested: false,
				componentsLoading: false,
				location: action.location,
				deviceLocatorError: action.deviceLocatorError
			}

		case 'GEOREVERSE':
			return update( state, { location: { $set: action.location } } )

		case 'INFOSPLASH':
			return {
				...state,
				infoSplash: action.infoSplash,
			}

		case 'SENDNOTIFICATION':
			return {
				...state,
				sendNotificationToAccountId: action.id,
			}

		default:
			return state
	}
}

const reduxStore = createStore( reduce, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() )

export default reduxStore
