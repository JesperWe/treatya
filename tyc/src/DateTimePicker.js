import React, { Component } from 'react'
import { translate } from 'react-i18next'
import moment from 'moment'
import _get from 'lodash/get'
import { DatePicker, TimePicker, Row, Col, Icon } from 'antd'
import './DateTimePicker.less'

class DateTimePicker extends Component {
	state = {
		datetime: null
	}

	componentWillMount() {
		this.componentWillReceiveProps( this.props )
	}

	componentWillReceiveProps( next ) {
		let { datetime } = next
		if( datetime && datetime !== this.state.datetime ) {
			this.setState( { datetime } )
		}
	}

	setDate = ( date ) => {
		if( !date ) return
		let { datetime } = this.state
		datetime.year( date.year() )
		datetime.month( date.month() )
		datetime.date( date.date() )
		this.setState( { datetime, selectedSlot: undefined } )
		if( this.props.onChange ) this.props.onChange( datetime )
	}

	setTime = ( time ) => {
		if( !time ) return
		let { datetime } = this.state
		datetime.hours( time.hours() )
		datetime.minutes( time.minutes() )
		this.setState( { datetime } )
		if( this.props.onChange ) this.props.onChange( datetime )
	}

	disabledDate = ( current ) => {
		let date = moment( current ).startOf( 'day' ).valueOf()
		return this.props.supplierFreeslots.data.findIndex( slot => slot.date === date ) < 0
	}

	onSelectSlot = ( item ) => {
		let slot = _get( this.props, 'supplierFreeslots.data' ).filter( slot => moment( slot.start ).isSame( this.state.datetime, 'day' ) )
		slot = slot && slot.length && slot[ item ]
		if( slot ) {
			const datetime = moment( slot.start )
			this.setState( {
				datetime,
				selectedSlot: datetime.format( 'H:mm' ) + ' - ' + moment( slot.end ).format( 'H:mm' )
			} )
			if( this.props.onChange ) this.props.onChange( datetime )
		}
	}

	render() {
		const { t } = this.props

		let freeSlotsMenuItems = _get( this.props, 'supplierFreeslots.data', [] )
				.filter( slot => moment( slot.start ).isSame( this.state.datetime, 'day' ) )
				.map( ( slot, index ) => {
					return (
							<div key={index} onClick={() => this.onSelectSlot(index)}>
								{moment( slot.start ).format( 'H:mm' )} - {moment( slot.end ).format( 'H:mm' )} <Icon type="arrow-right"/> <Icon type="clock-circle-o"/>
							</div>
					)
				} )

		return (
				<div className={'date-time-picker'}>
					<Row gutter={6} type="flex">
						<Col xs={{ span: 14, order: 1 }} sm={{ span: 10, order: 1 }} className={'calendar-picker'}>
							<DatePicker value={this.state.datetime} onChange={this.setDate}
										disabledDate={this.disabledDate}
										allowClear={false} size="large"/>
						</Col>

						<Col xs={{ span: 10, order: 2 }} sm={{ span: 7, order: 2 }} className={'time-picker'}>
							<TimePicker value={this.state.datetime} size="large" allowClear={false}
										minuteStep={15} format="HH:mm" hideDisabledOptions={true}
										onChange={this.setTime} inputReadOnly={true} focusOnOpen={false}
							/>
						</Col>

						<Col xs={{ span: 24, order: 3 }} sm={{ span: 7, order: 3 }}>
							<div className="free-slots">
								<div className="header">{t( 'Available slots' )}:</div>
								<div className="items">
									{freeSlotsMenuItems}
								</div>
							</div>
						</Col>
					</Row>
				</div>
		)
	}
}

export default translate( [ 'common' ], { wait: true } )( DateTimePicker )
